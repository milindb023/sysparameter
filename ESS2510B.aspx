﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ESS2510B.aspx.cs" Inherits="SysParameter.ESS2510B" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!-- bootstrap-->
    <link rel="shortcut icon" type="image/x-icon" href="et-uiassets/images/faviconn.ico" />
    <link href="et-uiassets/css/et-style.css" rel="stylesheet" />
    <script type="text/javascript" src="et-uiassets/js/jquery.js"></script>
    <script type="text/javascript" src="et-uiassets/js/bootstrap.js"></script>
    <script type="text/javascript" src="et-uiassets/js/common.js"></script>
    <!-- bootstrap-->

    <script src="et-uiassets/texteditor/ckeditor.js"></script>
    <script src="et-uiassets/texteditor/samples/js/sample.js"></script>
    <link rel="stylesheet" href="et-uiassets/texteditor/samples/css/samples.css" />
    <link rel="stylesheet" href="et-uiassets/texteditor/samples/toolbarconfigurator/lib/codemirror/neo.css" />

    <!-- date time picker -->
    <link href="et-uiassets/datetimepicker/datetimepicker.css" rel="stylesheet" />
    <script src="et-uiassets/datetimepicker/moment.js" type="text/javascript"></script>
    <script src="et-uiassets/datetimepicker/datetimepicker.js" type="text/javascript"></script>
    <!-- date time picker -->


    <!-- range slider -->
    <link href="et-uiassets/rangleslider/bootstrap-slider.css" rel="stylesheet" />
    <link href="et-uiassets/rangleslider/jquerysctipttop.css" rel="stylesheet" />
    <script src="et-uiassets/rangleslider/bootstrap-slider.js" type="text/javascript"></script>
    <!-- range slider -->

    <script>
        $(document).ready(function () {
            $('#finYear').prop('disabled', true).trigger("chosen:updated");
            $('#btnSwitch').prop('checked', false); 
            LoadFinYear();
            SetMonth();
            $("#btnSaveMyTab1").click(function () {
                SaveMyTab1();
            });
            $("#btnSaveMyTab2").click(function () {
                SaveMyTab2();
            });
        })
        //   Financial Year
        function SaveMyTab1() {
            $.ajax({
                type: "POST",
                url: "ESS2510B.aspx/SaveFinancialYear",
                data: "{FinYear: '" + $("#finYear").val() + "',Month: '" + $("#ddlMonths").val() + "',AnnualSalaryYear: '" + $("#txtAnnualSalaryYear").val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg != '' && msg != null) {
                        alert(msg.d);
                    }
                }
            });
        }

        function LoadFinYear() {

            $.ajax({
                type: "POST",
                url: "ESS2510B.aspx/GetFinYears",
                data: '',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg != '' && msg != null) {

                        var sDdlItem1; var sDdlItem2; var sDdlItem3;
                        var iFinYear = parseInt(msg.d);
                        sDdlItem1 = (iFinYear - 1).toString() + "-" + (iFinYear).toString();
                        sDdlItem2 = (iFinYear).toString() + "-" + (iFinYear + 1).toString();
                        sDdlItem3 = (iFinYear + 1).toString() + "-" + (iFinYear + 2).toString();
                        var vOption = "";
                        
                        $('#finYear').empty();
                        for (var i = 1; i <= 3; i++) {
                            if (i == 1) {
                                $('#finYear').append($('<option>', {
                                    value: (iFinYear - 1).toString(),
                                    text: sDdlItem1
                                }, '</option>'));
                                $('#finYear').trigger("chosen:updated");
                            }
                            else if (i == 2) {
                                $('#finYear').append($('<option>', {
                                    value: (iFinYear).toString(),
                                    text: sDdlItem2
                                }, '</option>'));
                                $('#finYear').trigger("chosen:updated");
                            }
                            else {
                                $('#finYear').append($('<option>', {
                                    value: (iFinYear + 1).toString(),
                                    text: sDdlItem3
                                }, '</option>'));
                                $('#finYear').trigger("chosen:updated");
                            }
                        }
                        $('#finYear').val(iFinYear.toString()).trigger('chosen:updated');
                    } else {
                    }
                },
                error: function (msg) {
                    // alert("UpdateNewMessages:An error has been occurred during processing your request");
                }
            });
        }

        function SetMonth() {
            $.ajax({
                type: "POST",
                url: "ESS2510B.aspx/SetMonth",
                data: '',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg != '' && msg != null) {

                        var sYear = new Array();
                        sYear = msg.d.split('|');
                        $('#ddlMonths').val(sYear[0].toString()).trigger('chosen:updated');
                        $('#txtAnnualSalaryYear').val(sYear[1].toString()).trigger('chosen:updated');
                    }
                }
            });
        }

        function ActiveInactive() {
          
            if (document.getElementById('finYear').disabled) {
                $('#finYear').prop('disabled', false).trigger("chosen:updated");
            }
            else {
                $('#finYear').prop('disabled', true).trigger("chosen:updated");
            }
        }

        //  End Financial Year
    </script>
    <script type="text/javascript">
        function EnableDisableReimAccrual(obj) {
            if (obj.checked) {
                $('#txtCurrMonthAccrual').prop('disabled', false).trigger("chosen:updated");
            }
            else {
                $('#txtCurrMonthAccrual').prop('disabled', true).trigger("chosen:updated");
            }
        }

        function IsValidLetout(txtBoxName) {
            // change();
            if (isNaN(txtBoxName.value) && txtBoxName.value != "") {
                alert("Non-numeric characters are not allowed.");
                txtBoxName.value = "";
                txtBoxName.focus();
                return false;
            }
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function checkcharactercountReim(nField) {
            var userInput = Number(nField.value);
            if (userInput > 25) {
                nField.value = "";
                alert("Day(s) should not be greater than 25.");
                return false;
            }
        }

        function DisplayControl(chkbox, radcontrol) {

            if (chkbox.checked == true) {
                radcontrol.style.display = 'inline-block';
                if (radcontrol.id == "txtReimbClaim") {
                    document.getElementById("txtReimbClaim").style.display = 'inline-block';
                }
            }
            else {
                radcontrol.style.display = 'none';
                if (radcontrol.id == "txtReimbClaim") {
                    document.getElementById("txtReimbClaim").style.display = 'none';
                }
            }
        }

        function EmailValidation(obj) {

            var email = document.getElementById(obj.id).value;
            if (email != "") {
                var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                var splitemail = email.split(',');
                for (var i = 0; i < splitemail.length; i++) {
                    if (splitemail[i].trim() == "" || !regex.test(splitemail[i].trim())) {
                        alert("Not a valid E-mail address" + '-' + splitemail[i]);
                        document.getElementById(obj.id).focus();
                        return false;
                    }
                }
            }
        }

        function LoadReimData() {

            $.ajax({
                type: "POST",
                url: "ESS2510B.aspx/GetReimClaim",
                data: '',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success:OnSuccess, 
                error: function (msg) {
                }
            });
        }

        function OnSuccess(responseData) {
            if (responseData != '' && responseData != null) {
                
                var oReimClaimDetails = $.parseJSON(responseData.d);
                if (oReimClaimDetails.sESSREIMSHOWBAL == "0")
                    $('#chkDoNotShowBal').prop('checked', true);
                else
                    $('#chkDoNotShowBal').prop('checked', false);

                if (oReimClaimDetails.sESSREIMALLOWACCESSCLAIM == "1")
                    $('#chkAllowtoReimClaimMoreAnual').prop('checked', true);
                else
                    $('#chkAllowtoReimClaimMoreAnual').prop('checked', false);

                if (oReimClaimDetails.sESSREIMLASTMONACC == "1") {
                    $('#chkCurrMonthAccrual').prop('checked', true);
                    $('#txtCurrMonthAccrual').prop('disabled', false).trigger("chosen:updated");
                    $('#txtCurrMonthAccrual').val(parseInt(oReimClaimDetails.sESSREIMLASTMONACC1)).trigger('chosen:updated');
                }
                else
                    $('#chkCurrMonthAccrual').prop('checked', false);

                if (oReimClaimDetails.sESSREIMATTACHMENTS == "1")
                    $('#chkShowClaims').prop('checked', true);
                else
                    $('#chkShowClaims').prop('checked', false);
                
                if (oReimClaimDetails.sESSREIMPARTICULARMANDATE == "1")
                    $('#chkParticularsClaim').prop('checked', true);
                else
                    $('#chkParticularsClaim').prop('checked', false);

                if (parseInt(oReimClaimDetails.sESSREIMISACKNOWLEDGE) == 1)
                    $('#chkacknowle').prop('checked', true);
                else
                    $('#chkacknowle').prop('checked', false);


                if (oReimClaimDetails.sESSREIMAPMAIL == "1")
                    $('#chkAppReimMail').prop('checked', true);
                else
                    $('#chkAppReimMail').prop('checked', false);

                if (oReimClaimDetails.sESSREIMPAPMAIL == "1")
                    $('#chkPartAppReimMail').prop('checked', true);
                else
                    $('#chkPartAppReimMail').prop('checked', false);

                if (oReimClaimDetails.sESSREIMMAIL == "1") {
                    $('#chkReimMail').prop('checked', true);
                    document.getElementById("DivRejClaim").style.display = 'inline-block';
                }
                else
                    $('#chkReimMail').prop('checked', false);
                //var el = document.getElementById('RadEditor1').dom.element.createFromHtml(oReimClaimDetails.sESSREIMMAILEDITOR);
                //document.getElementById('RadEditor1').document.getBody().append(el);
               // document.getElementById('RadEditor1').innerHTML = oReimClaimDetails.sESSREIMMAILEDITOR;
                $('#RadEditor1').val(oReimClaimDetails.sESSREIMMAILEDITOR);
               // document.getElementById("RadEditor1").innerHTML = return_data;
               // CKEDITOR.replace('editor1');
                //var instance = CKEDITOR.instances.RadEditor1;
                //instance.updateElement();

                
                if (oReimClaimDetails.sESSREIMDECL == "1") {
                    $('#chkSelfDeclaration').prop('checked', true);
                    document.getElementById("DivSelfDeclaration").style.display = 'inline-block';
                    $('#txtSelfDeclaration').val(oReimClaimDetails.sESSREIMDECLEDITOR).trigger('chosen:updated');
                }
                else
                    $('#chkSelfDeclaration').prop('checked', false);

                if (oReimClaimDetails.sESSREIMMAILBCC == "1") {
                    $('#chkReimbClaimBCC').prop('checked', true);
                    document.getElementById("DivReimbClaimBCC").style.display = 'inline-block';
                    $('#txtReimbClaim').val(oReimClaimDetails.sESSREIMMAILBCCMAIL).trigger('chosen:updated');
                }
                else
                    $('#chkReimbClaimBCC').prop('checked', false);

            } else {
            }
        }

        function SaveMyTab2() {
            var oReimData = {};

            if ($('#chkDoNotShowBal').prop("checked") == true) {
                oReimData.sESSREIMSHOWBAL = "0";
            }
            else
                oReimData.sESSREIMSHOWBAL = "1";

            if ($('#chkAllowtoReimClaimMoreAnual').prop("checked") == true) {
                oReimData.sESSREIMALLOWACCESSCLAIM = "1";
            }
            else
                oReimData.sESSREIMALLOWACCESSCLAIM = "0";

            if ($('#chkCurrMonthAccrual').prop("checked") == true) {
                oReimData.sESSREIMLASTMONACC = "1";
                oReimData.sESSREIMLASTMONACC1 = $("#txtCurrMonthAccrual").val();
            }
            else {
                oReimData.sESSREIMLASTMONACC = "0";
                oReimData.sESSREIMLASTMONACC1 = "";
            }

            if ($('#chkShowClaims').prop("checked") == true) {
                oReimData.sESSREIMATTACHMENTS = "1";
            }
            else
                oReimData.sESSREIMATTACHMENTS = "0";

            if ($('#chkParticularsClaim').prop("checked") == true) {
                oReimData.sESSREIMPARTICULARMANDATE = "1";
            }
            else
                oReimData.sESSREIMPARTICULARMANDATE = "0";

            if ($('#chkacknowle').prop("checked") == true) {
                oReimData.sESSREIMISACKNOWLEDGE = "1";
            }
            else
                oReimData.sESSREIMISACKNOWLEDGE = "0";

            if ($('#chkAppReimMail').prop("checked") == true) {
                oReimData.sESSREIMAPMAIL = "1";
            }
            else
                oReimData.sESSREIMAPMAIL = "0";

            if ($('#chkPartAppReimMail').prop("checked") == true) {
                oReimData.sESSREIMPAPMAIL = "1";
            }
            else
                oReimData.sESSREIMPAPMAIL = "0";

            if ($('#chkReimMail').prop("checked") == true) {
                oReimData.sESSREIMMAIL = "1";
                oReimData.sESSREIMMAILEDITOR = $('#RadEditor1').val();
            }
            else {
                oReimData.sESSREIMMAIL = "0";
                oReimData.sESSREIMMAILEDITOR = "";
            }

            if ($('#chkSelfDeclaration').prop("checked") == true) {
                oReimData.sESSREIMDECL = "1";
                oReimData.sESSREIMDECLEDITOR = $('#txtSelfDeclaration').val();
            }
            else {
                oReimData.sESSREIMDECL = "0";
                oReimData.sESSREIMDECLEDITOR = "";
            }

            if ($('#chkReimbClaimBCC').prop("checked") == true) {
                oReimData.sESSREIMMAILBCC = "1";
                oReimData.sESSREIMMAILBCCMAIL = $('#txtReimbClaim').val();
            }
            else {
                oReimData.sESSREIMMAILBCC = "0";
                oReimData.sESSREIMMAILBCCMAIL = "";
            }

            $.ajax({
                type: "POST",
                url: "ESS2510B.aspx/SaveReimClaim",
                data: "{oReimData:" + JSON.stringify(oReimData) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg != '' && msg != null) {
                        alert(msg.d);
                    }
                }
            });
        }

        function getPaging(event, str,  divID) {
            
            if (divID.id == "mytab1") {
                $('#finYear').prop('disabled', true).trigger("chosen:updated");
                $('#btnSwitch').prop('checked', false);
                LoadFinYear();
                SetMonth();
            }
            if (divID.id == "mytab2") {
                LoadReimData();
            }
            if (divID.id == "mytab3") {
                LoadExpenceClaimData();
            }
            return;
        }
    </script>

    <script type="text/javascript">
        function DisplayControlActivatePeriod(chkbox, radcontrol) {
            
            var chk = document.getElementById("chkOldDaysClaim");
            var txt = document.getElementById("txtClainRestrictDays")
            if (chkbox.checked == true) {
                radcontrol.classList.remove('hide');
                radcontrol.classList.add('et-inline-block');
                document.getElementById("txtClainRestrictDays").value = '';
                document.getElementById('txtClainRestrictDays').disabled = true;
                document.getElementById('chkOldDaysClaim').value = '';
            }
        }

        function EnableDisableDaysBox(radcontrol) {
            if (document.getElementById('chkOldDaysClaim').checked) {
                radcontrol.classList.remove('et-inline-block');
                radcontrol.classList.add('hide');
                document.getElementById("txtClainRestrictDays").disabled = false;
                document.getElementById("txtClainRestrictDays").focus();
                document.getElementById('Chkvalidate').checked = false;
                
            }
        }
        function EnableDisableDiv(chkboxname, labelName, textName) {
            
            var check = document.getElementById(chkboxname);
            var label = document.getElementById(labelName);
            var text = document.getElementById(textName);
            if (check.checked) {
                label.classList.remove('hide');
                label.classList.add('et-inline-block');
                text.classList.remove('hide');
                text.classList.add('et-inline-block');
            }
            else {
                text.value = "";
                label.classList.remove('et-inline-block');
                label.classList.add('hide');
                text.classList.remove('et-inline-block');
                text.classList.add('hide');
            }
        }
    </script>
</head>
<body>
    <form id="Form1" runat="server">

        <et-fixedtopbar>
		<%--<et-fixedtop-burger>
			<a href="javascript:void(0);" class="slide-toggle">
				<et-fixedtop-burger-set>
					<et-fixedtop-burger-set-line></et-fixedtop-burger-set-line>
					<et-fixedtop-burger-set-line></et-fixedtop-burger-set-line>
					<et-fixedtop-burger-set-line></et-fixedtop-burger-set-line>
				</et-fixedtop-burger-set>
			</a>
		</et-fixedtop-burger>--%>
		<%--<et-fixedtop-logospace>
			<img src="et-uiassets/images/logo.jpg">
			</et-fixedtop-logospace>--%>

	</et-fixedtopbar>


<%--        <et-fixedrightbar>
		<a class="samepagelink" href="#breadcrumbssection"><span><et-right-menu-code-icon></et-right-menu-code-icon></span>Bread crumbs</a>
		<a class="samepagelink" href="#startpage"><span><et-right-menu-code-icon></et-right-menu-code-icon></span>Start Page</a>		
		<a class="samepagelink" href="#gridssection"><span><et-right-menu-code-icon></et-right-menu-code-icon></span>Grids</a>
		<a class="samepagelink" href="#tablesection"><span><et-right-menu-table-icon/></span> Tables</a>
		<a class="samepagelink" href="#formsection"><span><et-right-menu-code-icon></et-right-menu-code-icon></span>Form Controls</a>
		<a class="samepagelink" href="#loginformsectionheading"><span><et-right-menu-code-icon></et-right-menu-code-icon></span>Diffrent Layouts</a>
		<a class="samepagelink" href="#collapsiblesections"><span><et-right-menu-code-icon></et-right-menu-code-icon></span>Collapsibles</a>
		<a class="samepagelink" href="#tabs"><span><et-right-menu-code-icon></et-right-menu-code-icon></span>Tabs</a>
		<a class="samepagelink" href="#listcard"><span><et-right-menu-code-icon></et-right-menu-code-icon></span>List Card</a>
		<a class="samepagelink" href="#gridcard"><span><et-right-menu-code-icon></et-right-menu-code-icon></span>Grid Card</a>
		<a class="samepagelink" href="#modalpopups"><span><et-right-menu-code-icon></et-right-menu-code-icon></span>Modal Popups</a>
		<a class="samepagelink" href="#tasklist"><span><et-right-menu-code-icon></et-right-menu-code-icon></span>Task List</a>
		<a  href="travel-expenses-claims.html"><span><et-right-menu-code-icon></et-right-menu-code-icon></span>Travel Expenses claims</a>
	</et-fixedrightbar>--%>

        <et-mainpagecontent>

            <et-five-tab-section> <et-tabpage-bg> 
                <ul class="nav nav-tabs"> 
                    <li  >
                        <a  onclick="javascript:return getPaging(event,this,mytab1);" data-toggle="tab" href="#mytab1" >Financial Year</a>
                    </li> 
                    <li >
                        <a  onclick="javascript:return getPaging(event,this,mytab2);" data-toggle="tab" href="#mytab2">Reimbursement Claims</a>
                    </li> 
                    <li class="active">
                        <a onclick="javascript:return getPaging(event,this,mytab3);" data-toggle="tab" href="#mytab3">Expense Claim</a>
                    </li> 
                    <li>
                        <a data-toggle="tab" href="#menu18">Menu 3</a>
                    </li> 
                    <li>
                        <a data-toggle="tab" href="#menu19">Menu 4</a>
                    </li> 

                </ul> 
                <et-tab-content> 
                    <div id="mytab1" class="tab-pane fade"> 
                        
                        <div class="clearfix"> 
                            
            <et-row>
        <et-content-area-24>
		<et-contentbgcolor-white>
			<et-mainpagecontent-title>
				<et-content-area-title-text>Financial Year</et-content-area-title-text>
				
			</et-mainpagecontent-title>									
			<et-content-body>
				 <et-content-area-6>
                     <et-block-form-group >
                         <et-label-block >Accept Income Tax Declaration for Financial Year</et-label-block>
                     <select id="finYear" class="chosen-select"  tabindex="-1" aria-multiselectable="false" data-placeholder="Choose fin year...">
                         
                         </select>
                         </et-block-form-group>
                    
                     </et-content-area-6>
                 <et-content-area-2>
                    <et-block-form-group >
                        
                     <et-switch-new>			
															<label class="et-switch" onchange="ActiveInactive()" >
															<input type="checkbox" id="btnSwitch" >
															<span class="et-switch-slider"></span>
															</label>
															
																	
															</et-switch-new>	
                         </et-block-form-group>
                    </et-content-area-2>
             

                <et-content-area-6>
		
			
				<et-block-form-group >
	<et-label-block >Show Annual Salary Statement Starting From</et-label-block>							
	<select id="ddlMonths"  data-placeholder="Choose a Month..." class="chosen-select" tabindex="-1" aria-multiselectable="false" >
		<option value=""></option>
		<option value="1">January</option>
		<option value="2">February</option>
		<option value="3">March</option>
		<option value="4">April</option>
		<option value="5">May</option>
		<option value="6">June</option>
		<option value="7">July</option>
		<option value="8">August</option>
		<option value="9">September</option>
		<option value="10">October</option>
		<option value="11">November</option>
		<option value="12">December</option>
	</select>
           
</et-block-form-group>
               
		
	</et-content-area-6>
                   <et-content-area-2>
                    <et-block-form-group>
	<et-label-block>Year</et-label-block>
	<input id="txtAnnualSalaryYear" type="text" />
</et-block-form-group>
                    </et-content-area-2>
                <et-block-form-group-button-right>
                    <button type="button" class="et-btn et-btn-primary" id="btnSaveMyTab1" >Save</button>
                    </et-block-form-group-button-right>
                
			</et-content-body>
		</et-contentbgcolor-white>
	</et-content-area-24>
	
</et-row>
</div>
                    </div> 

                    <div id="mytab2" class="tab-pane fade"> 
                       
                        <div class="clearfix"> 
                          
 <et-row >
        <et-content-area-24>
		<et-contentbgcolor-white>
			<et-mainpagecontent-title>
				<et-content-area-title-text>Reimbursement Claims</et-content-area-title-text>
				
			</et-mainpagecontent-title>		
            <et-content-body>
				 <et-content-area-24>
                     <et-block-form-group>
	<%--<et-label-block>Checkbox block</et-label-block>	--%>						
	<et-checkbox>
		<label class="chekboxcontainer">Do not show balances for claims when delegated
			<input type="checkbox" id="chkDoNotShowBal">
			<span class="checkboxmark"></span>
		</label>
		
	</et-checkbox>
</et-block-form-group>
                     <et-block-form-group>
	<et-checkbox>
		<label class="chekboxcontainer">Allow to claim more than annual eligibility
			<input type="checkbox" id="chkAllowtoReimClaimMoreAnual">
			<span class="checkboxmark"></span>
		</label>
		
	</et-checkbox>
</et-block-form-group>

                     <div class="clearfix">
                     <et-inline-block-form-group>
			<label class="chekboxcontainer">Do not display current month accrual till 
			<input type="checkbox" id="chkCurrMonthAccrual" onclick="EnableDisableReimAccrual(this);"/>
			<span class="checkboxmark"></span>
		</label>
                         </et-inline-block-form-group>	
                        	
                         <et-inline-block-form-group class="et-width-3">
			<input id="txtCurrMonthAccrual" type="text" onblur="IsValidLetout(this);" maxlength="2" onkeypress="return isNumberKey(event)"
                onkeyup="return checkcharactercountReim(document.getElementById('txtCurrMonthAccrual'));" disabled/>
		</et-inline-block-form-group>	
                         <et-inline-block-form-group class="padd-left-20">
                         day of the month
		</et-inline-block-form-group>	                            
                         </div>	
                      <et-block-form-group>
                     	<et-checkbox>
		<label class="chekboxcontainer">Show Claims Attachments
			<input type="checkbox" id="chkShowClaims">
			<span class="checkboxmark"></span>
		</label>
		
	</et-checkbox>
                          </et-block-form-group>
                      <et-block-form-group>
                     <et-checkbox>
		<label class="chekboxcontainer">Do not force to fill Particulars for claim
			<input type="checkbox" id="chkParticularsClaim">
			<span class="checkboxmark"></span>
		</label>
		
	</et-checkbox>
                           </et-block-form-group>
                      <et-block-form-group>
                     <et-checkbox>
		<label class="chekboxcontainer">Send acknowledgement email to employee for proof receipt
			<input type="checkbox" id="chkacknowle">
			<span class="checkboxmark"></span>
		</label>
		
	</et-checkbox>
                           </et-block-form-group>
                      <et-block-form-group>
                     <et-checkbox>
		<label class="chekboxcontainer">Send mail to employee when claim is approved
			<input type="checkbox" id="chkAppReimMail">
			<span class="checkboxmark"></span>
		</label>
		
	</et-checkbox>
                           </et-block-form-group>
                      <et-block-form-group>
                     <et-checkbox>
		<label class="chekboxcontainer">Send mail to employee when claim is partially approved
			<input type="checkbox" id="chkPartAppReimMail">
			<span class="checkboxmark"></span>
		</label>
		
	</et-checkbox>
                           </et-block-form-group>
                      <et-block-form-group class="mar-bottom-0">
                     <div class="clearfix">
                     <et-inline-block-form-group>
			<label class="chekboxcontainer">Send BCC for Submitted Claim 
			<input type="checkbox" id="chkReimbClaimBCC" onclick="javascript: DisplayControl(this, DivReimbClaimBCC);"/>
			<span class="checkboxmark"></span>
		</label>
                         </et-inline-block-form-group>	
                        	<div id="DivReimbClaimBCC" style="display:none;">
                        <et-inline-block-form-group >
			<input id="txtReimbClaim" type="text" maxlength="100" onchange="return EmailValidation(this);"/>
		</et-inline-block-form-group>
                                 </div>	
                         </div>	
                           </et-block-form-group>
                    



                     <div>
                     <et-inline-block-form-group>
			<label class="chekboxcontainer">Send mail for Rejected Claims
			<input type="checkbox" id="chkReimMail" onclick="javascript: DisplayControl(this, DivRejClaim);">
			<span class="checkboxmark"></span>
		</label>
                         </et-inline-block-form-group>	
                        	<div id="DivRejClaim" style="display:none;">
                         <et-block-form-group>
	<et-label-block>Editor</et-label-block>

	<textarea id="RadEditor1" class="editor1 et-height-100" name="editor1"  placeholder="placeholder"></textarea>
	<script type="text/javascript">
	    CKEDITOR.replace('editor1');
	</script>																
</et-block-form-group>	
                                </div>
                         </div>	


                      <div>
                     <et-inline-block-form-group>
			<label class="chekboxcontainer">Self declaration for reimbursement claims
			<input type="checkbox" id="chkSelfDeclaration" onclick="javascript: DisplayControl(this, DivSelfDeclaration);">
			<span class="checkboxmark"></span>
		</label>
                         </et-inline-block-form-group>	
                        	<div id="DivSelfDeclaration" style="display:none;">
                         <et-block-form-group>
	<et-label-block>Editor</et-label-block>
	<textarea id="txtSelfDeclaration" class="editor1 et-height-100" name="editor2"  placeholder="placeholder"></textarea>
	<script type="text/javascript">
	    CKEDITOR.replace('editor2');
	</script>																
</et-block-form-group>	
                                </div>
                         </div>	


                     </et-content-area-24>
                <et-block-form-group-button-right>
                    <button type="button" class="et-btn et-btn-primary" id="btnSaveMyTab2" >Save</button>
                    </et-block-form-group-button-right>
                </et-content-body>
            </et-contentbgcolor-white>
            </et-content-area-24>
     </et-row>
    </div>
                    </div> 

                    <div id="mytab3" class="tab-pane fade in active"> 
                        <div class="clearfix"> 
 <et-row >
        <et-content-area-24>
		<et-contentbgcolor-white>
			<et-mainpagecontent-title>
				<et-content-area-title-text>Expense Claims</et-content-area-title-text>
				
			</et-mainpagecontent-title>		
            <et-content-body>
                     <et-block-form-group class="mar-bottom-10">
	<et-checkbox>
		<label class="chekboxcontainer">Allow to edit Business Area / Cost Centre in Expense Claim
			<input type="checkbox" id="chkExpenseClaim">
			<span class="checkboxmark"></span>
		</label>
		
	</et-checkbox>
</et-block-form-group>
                         <label class="chekboxcontainer">Expence Claim (From-To)</label>
                     <et-inline-block-form-group-text-set> 
				<et-inline-block-form-group-text>									
					<et-radio-inline>
						<label class="radioboxcontainer">Activate Claim From - To Validation
							<input type="radio" id="chkActivatePeriod" name="radio1" onclick="javascript: DisplayControlActivatePeriod(this, tdvalidate);"/>
							<span class="radiocheck"></span>
						</label>
																
					</et-radio-inline>	
                   		
				</et-inline-block-form-group-text>	
				<et-inline-block-form-group-text class="hide" id="tdvalidate">
				 <et-checkbox-inline>
						<label class="chekboxcontainer">Validate Dates on Items
						<input type="checkbox" id="Chkvalidate" />
							<span class="checkboxmark"></span>
						</label>
					</et-checkbox-inline>	
				</et-inline-block-form-group-text>	
			</et-inline-block-form-group-text-set>

                <et-inline-block-form-group-text-set> 
				<et-inline-block-form-group-text>									
					<et-radio-inline>
						<label class="radioboxcontainer">Do not allow backdated expense claim before
							<input type="radio" id="chkOldDaysClaim" name="radio1" onclick="javascript: EnableDisableDaysBox(tdvalidate);"/>
							<span class="radiocheck"></span>
						</label>
					</et-radio-inline>	
				</et-inline-block-form-group-text>	
				
				<et-inline-block-form-group-text class="et-width-3">
                    <input id="txtClainRestrictDays" type="text" disabled="disabled" /></et-inline-block-form-group-text>
                    <et-inline-block-form-group-text class="padd-left-3">
                    days</et-inline-block-form-group-text>   
			</et-inline-block-form-group-text-set>

               <et-block-form-group>
	<et-checkbox>
		<label class="chekboxcontainer">Activate Validation for Bill Date and Bill No
			<input type="checkbox" id="chkclaimvalidation">
			<span class="checkboxmark"></span>
		</label>
		
	</et-checkbox>
</et-block-form-group>
                <et-block-form-group>
	<et-checkbox>
		<label class="chekboxcontainer">Attachment is Mandatory
			<input type="checkbox" id="chkAttachmentMandatory">
			<span class="checkboxmark"></span>
		</label>
		
	</et-checkbox>
</et-block-form-group>
                <et-block-form-group class="mar-bottom-5">
	<et-checkbox >
		<label class="chekboxcontainer">Hide grade in Expense Claim report
			<input type="checkbox" id="chkHideGrade">
			<span class="checkboxmark"></span>
		</label>
		
	</et-checkbox>
</et-block-form-group>
                <et-inline-block-form-group-text-set> 
				<et-inline-block-form-group-text class="mar-left-0">									
					<et-checkbox-inline>
						<label class="chekboxcontainer">Include Advance amount in output file.
							<input type="checkbox" id="ChkAdvanceAmt"  onclick="EnableDisableDiv('ChkAdvanceAmt', 'lblAdvanceAmt','txtAdvanceAmtAccountCode');"/>
							<span class="checkboxmark"></span>
						</label>
					</et-checkbox-inline>	
				</et-inline-block-form-group-text>	
				 <et-inline-block-form-group-text >
                    <label id="lblAdvanceAmt" class="chekboxcontainer padd-left-0 hide">Book advance amount in A/c code</label>
				 </et-inline-block-form-group-text>   
				<et-inline-block-form-group-text class="et-width-10">
                    <input id="txtAdvanceAmtAccountCode" type="text" class="hide" />
				</et-inline-block-form-group-text>
			</et-inline-block-form-group-text-set>
                </et-content-body>
            </et-contentbgcolor-white>
            </et-content-area-24>
     </et-row>


                        </div> 
                    </div> 


                    <div id="menu18" class="tab-pane fade"> 
                        <h3>Menu 3</h3> 
                        <div class="clearfix"> 
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris </p> 

                        </div> 

                    </div> 
                    <div id="menu19" class="tab-pane fade"> 
                        <h3>Menu 4</h3> 
                        <div class="clearfix"> 
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris </p> 

                        </div> 

                    </div> 

                </et-tab-content> 

                                  </et-tabpage-bg> 

            </et-five-tab-section> 
            






    

		
	</et-mainpagecontent>

        <!-- combobox picker -->
        <link href="et-uiassets/combobox/chosen.css" rel="stylesheet" />
        <script src="et-uiassets/combobox/chosen.jquery.js" type="text/javascript"></script>
        <script src="et-uiassets/combobox/prism.js" type="text/javascript"></script>
        <script src="et-uiassets/combobox/init.js" type="text/javascript"></script>
        <!-- combobox picker -->

        <!-- fixed right slider -->
        <link href="et-uiassets/fancyscroll/simplebar.css" rel="stylesheet" />
        <script src="et-uiassets/fancyscroll/simplebar.js" type="text/javascript"></script>
        <!-- fixed right slider -->

    </form>
    <script>initSample();</script>
</body>
</html>
