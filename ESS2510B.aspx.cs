﻿using ESSLib.Business.BusinessObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SysParameter
{
    public partial class ESS2510B : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        [WebMethod]
        public static string GetFinYears()
        {
            string sESSITFINYEAR = SystemParameters.GetParameter("ESSITFINYEAR").ToString();
            int iFinYear = (object)sESSITFINYEAR != DBNull.Value ? Convert.ToInt32(sESSITFINYEAR.ToString()) : 2018;//Convert.ToInt16(Session["selectedYear"]);
            return iFinYear.ToString();
        }

        [WebMethod]
        public static string SetMonth()
        {
            string sYear = "";
            string sESSANNSTMONTH = SystemParameters.GetParameter("ESSANNSTMONTH").ToString();
            if (!string.IsNullOrEmpty(sESSANNSTMONTH))
            {
                int year = Convert.ToInt32(SystemParameters.GetParameter("ESSANNSTMONTH", SystemParameters.ParameterConstants.FIELD_VALUE1));
                sYear = year == 0 ? "" : year.ToString();
            }
            return sESSANNSTMONTH + "|" + sYear;
        }

        [WebMethod]
        public static string SaveFinancialYear(string FinYear, string Month, string AnnualSalaryYear)
        {
            string msg = "";
            try
            {
                // string sFinYear = txthiddenSelectedYear.Value == "" ? Session["selectedYear"].ToString() : txthiddenSelectedYear.Value;
                SystemParameters.SetParameter("ESSITFINYEAR", FinYear);
                SystemParameters.SetParameter("ESSANNSTMONTH", Month);
                int year = AnnualSalaryYear == "" ? 0 : Convert.ToInt32(AnnualSalaryYear);
                SystemParameters.SetParameter("ESSANNSTMONTH", year, SystemParameters.ParameterConstants.FIELD_VALUE1);
                msg = "Record Saved Successfully.";
            }
            catch (Exception ex)
            {
            }
            return msg;
        }

        // Reimbursement Claims

        [WebMethod]
        public static string GetReimClaim()
        {
            string responseData = string.Empty;
            try
            {
                ReimClaimDTO oReimClaimDTO = new ReimClaimDTO();
                oReimClaimDTO.sESSREIMSHOWBAL = SystemParameters.GetParameter("ESSREIMSHOWBAL").ToString();
                oReimClaimDTO.sESSREIMLASTMONACC = SystemParameters.GetParameter("ESSREIMLASTMONACC").ToString();
                oReimClaimDTO.sESSREIMLASTMONACC1 = SystemParameters.GetParameter("ESSREIMLASTMONACC", SystemParameters.ParameterConstants.FIELD_VALUE1).ToString();
                oReimClaimDTO.sESSREIMALLOWACCESSCLAIM = SystemParameters.GetParameter("ESSREIMALLOWACCESSCLAIM", SystemParameters.ParameterConstants.FIELD_DESC).ToString();
                oReimClaimDTO.sESSREIMATTACHMENTS = SystemParameters.GetParameter("ESSREIMATTACHMENTS", SystemParameters.ParameterConstants.FIELD_DESC).ToString();
                oReimClaimDTO.sESSREIMPARTICULARMANDATE = SystemParameters.GetParameter("ESSREIMPARTICULARMANDATE", SystemParameters.ParameterConstants.FIELD_DESC).ToString();
                oReimClaimDTO.sESSREIMISACKNOWLEDGE = SystemParameters.GetParameter("ESSREIMISACKNOWLEDGE", SystemParameters.ParameterConstants.FIELD_VALUE1).ToString();
                oReimClaimDTO.sESSREIMAPMAIL = SystemParameters.GetParameter("ESSREIMAPMAIL").ToString();
                oReimClaimDTO.sESSREIMPAPMAIL = SystemParameters.GetParameter("ESSREIMPAPMAIL").ToString();
                oReimClaimDTO.sESSREIMMAIL = SystemParameters.GetParameter("ESSREIMMAIL").ToString();

                if (SystemParameters.GetParameter("ESSREIMMAIL").ToString() == "1")
                {
                    oReimClaimDTO.sESSREIMMAILEDITOR = GetBody("007", SystemParameters.GetParameter("ESSREIMMAIL", SystemParameters.ParameterConstants.FIELD_DESC2).ToString()).ToString();
                }
                else
                {
                    oReimClaimDTO.sESSREIMMAILEDITOR = GetBodyTemplate("007", SystemParameters.GetParameter("ESSREIMMAIL", SystemParameters.ParameterConstants.FIELD_DESC2).ToString()).ToString();
                }
                string sESSREIMDECL = SystemParameters.GetParameter("ESSREIMDECL").ToString();
                oReimClaimDTO.sESSREIMDECL = sESSREIMDECL;

                if (!string.IsNullOrEmpty(sESSREIMDECL))
                {
                    if (sESSREIMDECL == "1")
                    {
                        oReimClaimDTO.sESSREIMDECLEDITOR = SystemParameters.GetParameter("ESSREIMDECL", SystemParameters.ParameterConstants.FIELD_DESC2).ToString();
                    }
                }

                oReimClaimDTO.sESSREIMMAILBCC = SystemParameters.GetParameter("ESSREIMMAILBCC").ToString();
                oReimClaimDTO.sESSREIMMAILBCCMAIL = SystemParameters.GetParameter("ESSREIMMAILBCC", SystemParameters.ParameterConstants.FIELD_DESC2).ToString();

                responseData = JsonConvert.SerializeObject(oReimClaimDTO);
            }
            catch (Exception ex)
            {
            }
            return responseData;
        }

        [WebMethod]
        public static string SaveReimClaim(ReimClaimDTO oReimData)
        {
            string result = "";
            try
            {
                SystemParameters.SetParameter("ESSREIMSHOWBAL", oReimData.sESSREIMSHOWBAL);
                SystemParameters.SetParameter("ESSREIMALLOWACCESSCLAIM", oReimData.sESSREIMALLOWACCESSCLAIM);

                SystemParameters.SetParameter("ESSREIMLASTMONACC", oReimData.sESSREIMLASTMONACC);
                SystemParameters.SetParameter("ESSREIMLASTMONACC", oReimData.sESSREIMLASTMONACC1, SystemParameters.ParameterConstants.FIELD_VALUE1);

                SystemParameters.SetParameter("ESSREIMATTACHMENTS", oReimData.sESSREIMATTACHMENTS);
                SystemParameters.SetParameter("ESSREIMPARTICULARMANDATE", oReimData.sESSREIMPARTICULARMANDATE);

                SystemParameters.SetParameter("ESSREIMISACKNOWLEDGE", Convert.ToInt16(oReimData.sESSREIMISACKNOWLEDGE), SystemParameters.ParameterConstants.FIELD_VALUE1);
                SystemParameters.SetParameter("ESSREIMAPMAIL", oReimData.sESSREIMAPMAIL);
                SystemParameters.SetParameter("ESSREIMPAPMAIL", oReimData.sESSREIMPAPMAIL);

                SystemParameters.SetParameter("ESSREIMMAIL", oReimData.sESSREIMMAIL, SystemParameters.ParameterConstants.FIELD_DESC);
                SystemParameters.SetParameter("ESSREIMMAIL", oReimData.sESSREIMMAILEDITOR, SystemParameters.ParameterConstants.FIELD_DESC2);

                SystemParameters.SetParameter("ESSREIMDECL", oReimData.sESSREIMDECL);
                SystemParameters.SetParameter("ESSREIMDECL", oReimData.sESSREIMDECLEDITOR, SystemParameters.ParameterConstants.FIELD_DESC2);

                SystemParameters.SetParameter("ESSREIMMAILBCC", oReimData.sESSREIMMAILBCC);
                SystemParameters.SetParameter("ESSREIMMAILBCC", oReimData.sESSREIMMAILBCCMAIL, SystemParameters.ParameterConstants.FIELD_DESC2);
                result = "Record Saved Successfully.";
            }
            catch (Exception ex)
            {
            }

            return result;
        }

        public static string GetBody(string sEmpNo, string dscr)
        {
            try
            {
                Employee oEmployee = new Employee(sEmpNo);
                DataSet dsEmp = oEmployee.GetEmployeesSysParameter("1", sEmpNo);
                string sMailID = "";

                SMTPMail oMail = new SMTPMail();
                FunctionResponse oResponse = new FunctionResponse();
                string sBody = "";
                if (dsEmp != null && dsEmp.Tables[0].Rows.Count != 0)
                {
                    foreach (DataRow dRow in dsEmp.Tables[0].Rows)
                    {
                        sMailID = dRow["Email"].ToString();
                    }
                }
                sBody = dscr;

                return sBody;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetBodyTemplate(string sEmpNo, string dscr)
        {
            try
            {
                Employee oEmployee = new Employee(sEmpNo);
                DataSet dsEmp = oEmployee.GetEmployeesSysParameter("1", sEmpNo);
                string sMailID = "";

                SMTPMail oMail = new SMTPMail();
                FunctionResponse oResponse = new FunctionResponse();
                string sBody = "";

                string sReimMailBody = dscr;
                if (dsEmp != null && dsEmp.Tables[0].Rows.Count != 0)
                {
                    foreach (DataRow dRow in dsEmp.Tables[0].Rows)
                    {
                        sMailID = dRow["Email"].ToString();
                    }
                }

                sBody = "<html><body style='FONT-SIZE: 8pt; FONT-FAMILY: verdana;'><TABLE style='WIDTH:100%;height:100%'><TBODY>" +
                     "<TR><TD style='FONT-SIZE: 8pt; FONT-FAMILY: verdana' align='left'>Dear [$name]</TD></TR>" +
                    "<tr><td style='FONT-SIZE: 8pt; FONT-FAMILY: verdana' align='left'>" + sReimMailBody.Replace("\r\n", "<br>") + "</td></tr>" +
                    "<TR><TD style='FONT-SIZE: 8pt; FONT-FAMILY: verdana' align='left'>In order to resubmit your claim [$claimno], visit the following URL.</TD></TR>" +
                    "<tr><td style='FONT-SIZE: 8pt; FONT-FAMILY: verdana' align='left'><a href=[$url] target='_blank'>Click Here to Resubmit</a></td></tr>" +
                    "<TR><TD style='FONT-SIZE: 8pt; FONT-FAMILY: verdana' align='left'>Comments:<BR><BR>[$comments]</TD></TR>" +
                    "<TR><TD style='FONT-SIZE: 8pt; FONT-FAMILY: verdana' align='left'>DO NOT REPLY TO THIS MESSAGE<br></TD></TR>" +
                    "<TR><TD style='FONT-SIZE: 8pt; FONT-FAMILY: verdana' align='left'>Yours Sincerely,<BR>Payroll Team</TD></TR>" +
                    "</TBODY></TABLE></body></html>";

                return sBody;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // End Reimbursement Claims

        // Expense Claim



        // End Expense Claim
    }

    public class ReimClaimDTO
    {
        public string sESSREIMSHOWBAL { get; set; }
        public string sESSREIMLASTMONACC { get; set; }
        public string sESSREIMLASTMONACC1 { get; set; }

        public string sESSREIMALLOWACCESSCLAIM { get; set; }
        public string sESSREIMATTACHMENTS { get; set; }
        public string sESSREIMPARTICULARMANDATE { get; set; }

        public string sESSREIMISACKNOWLEDGE { get; set; }
        public string sESSREIMAPMAIL { get; set; }
        public string sESSREIMPAPMAIL { get; set; }

        public string sESSREIMMAIL { get; set; }
        public string sESSREIMMAILEDITOR { get; set; }
        public string sESSREIMDECL { get; set; }
        public string sESSREIMDECLEDITOR { get; set; }

        public string sESSREIMMAILBCC { get; set; }
        public string sESSREIMMAILBCCMAIL { get; set; }

    }
}