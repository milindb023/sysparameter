﻿function CodeFileService() {
    this.initObject();
    this.lastIndex = null;
}
CodeFileService.prototype = {
    moduleEnum: {
        CostCenter: 1,
        BusinessArea: 2,
        Occupation: 3,
        Status: 4,
        Category: 5,
        Grade: 6,
        Location: 7,
        UDF: 8,
        ChangeReason: 9,
        InActiveReason: 10,
        HoldReason: 11,
        Event: 12,
        Bank: 13,
        Designation: 14,
        PayrollGroup: 15,
        //RelationMaster: 16,
        MediClaimMaster: 17
    },
    initObject: function () {
        //template = new Template();
    },

    loadData: function (url, input, element, template) {
        var self = this;
        $.ajax({
            type: "POST",
            url: url,
            data: input,
            success: function (response) {
                self.loadByModule(response, element, template);
            },
            failure: function (response) {
                return response;
            },
            error: function (response) {
                return response;
            }
        });
    },

    addData: function (moduleValue, url, input, moduleObject, element, id) {
        var self = this;
        $.ajax({
            type: "POST",
            url: url,
            data: input,
            success: function (response) {
                self.addModule(moduleObject, element, id, moduleValue);
                messagePopup(response,'res');
            },
            failure: function (response) {
                messagePopup(response.responseText, 'res');
            },
            error: function (response) {
                messagePopup(response.responseText, 'res');
            }
        });
    },

    updateData: function (moduleValue, url, input, moduleObject, element) {
        var self = this;
        $.ajax({
            type: "POST",
            url: url,
            data: input,
            success: function (response) {
                self.updateModule(moduleValue, element, moduleObject);
                closeRow(element);
                messagePopup(response, 'res');
            },
            failure: function (response) {
                messagePopup(response.responseText, 'res');
            },
            error: function (response) {
                messagePopup(response.responseText, 'res');
            }
        });
    },

    removeData: function (url, input, $element, $id) {

        var self = this;
        $.ajax({
            type: "POST",
            url: url,
            data: input,
            success: function (response) {
              //  var count = parseInt($('#spanCount').html());
             //   count--;
             //   $('#spanCount').html(count);

             //   if (count == 0)
             //       $element.html('<div style="text-align: center; padding-top: 20px;">No record found</div>');

                if (input.IsActive != 'true') {
                    $id.html('<span class="icon-cancel text-danger font-size-15">');
                    $id.attr('row-status', true);
                }
                else {
                    $id.html('<span class="icon-check-circle text-success font-size-15">');
                    $id.attr('row-status', false);
                }

                messagePopup(response, 'res');
                // removeRow();

            },
            failure: function (response) {
                messagePopup(response.responseText, 'res');
            },
            error: function (response) {
                messagePopup(response.responseText, 'res');
            }
        });
    },

    loadByModule: function (response, $element, $template) {
        var self = this;
        var html = '';
        if (response != 'null') {

            //$(document).ready(function () {
            var moduleData = JSON.parse(response);

            $element.html($template.tmpl(moduleData));

            self.regEvent();
            $('#spanCount').html(moduleData.length);

            if (moduleData.length == 0)
                $element.html('<div style="text-align: center; padding-top: 20px;">No record found</div>');
            //});
        }
        else {
            $element.html('<div style="text-align: center; padding-top: 20px;">No record found</div>');
            $('#spanCount').html('0');
        }
    },

    addModule: function (objModule, $element, id, moduleValue) {

        var self = this;

        var count = parseInt($('#spanCount').html());

        if (count === 0)
            $element.html($("#dataTemplate").tmpl(objModule));
        else
            $element.append($("#dataTemplate").tmpl(objModule));

        count++;
        $('#spanCount').html(count);
        self.regEvent();
        closeRow(id);
        self.clearData(moduleValue)
    },

    clearData: function (moduleValue) {
        var self = this;
        switch (parseInt(moduleValue)) {
            case self.moduleEnum.CostCenter:
                $('#txtCostCode').val('');
                $('#txtCostCenter').val('');
                break;
            case self.moduleEnum.BusinessArea:
                $('#txtBACode').val('');
                $('#txtBADesc').val('');
                break;
            case self.moduleEnum.Occupation:
                $('#txtOccCode').val('');
                $('#txtOccDesc').val('');
                break;
            case self.moduleEnum.Status:
                $('#txtStatusCode').val('');
                $('#txtStatusDesc').val('');
                break;
            case self.moduleEnum.Category:
                $('#txtCategoryCode').val('');
                $('#txtCategoryDesc').val('');
                $('#txtWODeduction').val('');
                break;
            case self.moduleEnum.Grade:
                $('#txtGradeCode').val('');
                $('#txtGradeDesc').val('');
                break;
            case self.moduleEnum.Location:
                $('#txtLocationCode').val('');
                $('#txtLocationDesc').val('');
                $('#ddlState').val('0');
                $('#ddlWO1').val('None');
                $('#ddlWO2').val('None');
                $('#ddlPTF').val('None');
                $('#ddlPT').val('None');
                $('#ddlLWF').val('None');
                $('#ddlESI').val('None');
                $('#txtESIOffice').val('');
                $('#txtAddress1').val('');
                $('#txtAddress2').val('');
                $('#txtAddress3').val('');
                $('#chkESI').attr('checked', false);
                $('.chkWO1').attr('checked', false);
                $('.chkWO2').attr('checked', false);
                break;
            case self.moduleEnum.UDF:
                $('#txtUDFCode').val('');
                $('#txtUDFDesc').val('');
                break;
            case self.moduleEnum.Bank:
                $('#txtBankCode').val('');
                $('#txtBankName').val('');
                $('#txtBranchName').val('');
                $('#txtBankAddress1').val('');
                $('#txtBankAddress2').val('');
                $('#txtBankAddress3').val('');
                $('#txtBankAddress4').val('');
                $('#txtContactName').val('');
                $('#txtAccountLen').val('');
                $('#txtBankBSRCode').val('');
                break;
            case self.moduleEnum.ChangeReason:
                $('#txtReasonCode').val('');
                $('#txtReasonDesc').val('');
                break;
            case self.moduleEnum.InActiveReason:
                $('#txtInactiveID').val('');
                $('#txtInactiveDesc').val('');
                $('#txtPFReason').val('');
                break;
            case self.moduleEnum.HoldReason:
                $('#txtHoldID').val('');
                $('#txtHoldReason').val('');
                break;
            case self.moduleEnum.Event:
                $('#txtHoldID').val('');
                $('#txtHoldReason').val('');
                break;
            case self.moduleEnum.Designation:
                $('#txtHoldID').val('');
                $('#txtHoldReason').val('');
                break;
            case self.moduleEnum.PayrollGroup:
                $('#txtHoldID').val('');
                $('#txtHoldReason').val('');
                break;
            case self.moduleEnum.RelationMaster:
                $('#txtRelationCode').val('');
                $('#txtRelationDesc').val('');
                $('#chkIsApplicable').val('');
                break;
            case self.moduleEnum.MediClaimMaster:
                $('#txtGradeCode').val('');
                $('#txtGradeDesc').val('');
                $('#txtGradeAmt').val('');
                $('#txtCategoryCode').val('');
                $('#txtCategoryDesc').val('');
                break;
        }
    },

    updateModule: function (moduleValue, element, objModule) {
        var self = this;
        switch (parseInt(moduleValue)) {
            case self.moduleEnum.CostCenter:
                var $cCode = objModule.CostCode;
                var $cCenter = objModule.CostCentre;
                var index = $(element).attr('row-index');

                $('#lblCostCode' + index).html($cCode);
                $('#lblCostCentre' + index).html($cCenter);

                break;
            case self.moduleEnum.BusinessArea:
                var $baCode = objModule.BACode;
                var $baDesc = objModule.BADesc;
                var index = $(element).attr('row-index');

                $('#lblBACode' + index).html($baCode);
                $('#lblBADesc' + index).html($baDesc);
                break;
            case self.moduleEnum.Occupation:
                var $occCode = objModule.OccCode;
                var $occDesc = objModule.OccDesc;
                var index = $(element).attr('row-index');

                $('#lblOccCode' + index).html($occCode);
                $('#lblOccDesc' + index).html($occDesc);
                break;
            case self.moduleEnum.Status:
                var $sCode = objModule.StatusCode;
                var $sDesc = objModule.StatusDesc;
                var index = $(element).attr('row-index');

                $('#lblStatusCode' + index).html($sCode);
                $('#lblStatusDesc' + index).html($sDesc);
                break;
            case self.moduleEnum.Category:
                var $cCode = objModule.CategoryCode;
                var $cDesc = objModule.CategoryDesc;
                var $cWODeduction = objModule.WODeduction;

                $cWODeduction = $cWODeduction == true ? 'Yes' : 'No';

                var index = $(element).attr('row-index');

                $('#lblCategoryCode' + index).html($cCode);
                $('#lblCategoryDesc' + index).html($cDesc);
                $('#lblWODeduction' + index).html($cWODeduction);
                break;
            case self.moduleEnum.Grade:
                var $gCode = objModule.GradeCode;
                var $gDesc = objModule.GradeDesc;

                var index = $(element).attr('row-index');

                $('#lblGradeCode' + index).html($gCode);
                $('#lblGradeDesc' + index).html($gDesc);

                break;
            case self.moduleEnum.Location:
                var $locationCode = objModule.LocationCode;
                var $locationDesc = objModule.LocationDesc;
                var $WO1 = objModule.WeeklyOff1.substring(0, 3);
                var $WO2 = objModule.WeeklyOff2.substring(0, 3);
                var $chkWO1 = objModule.WeekNos1;
                var $chkWO2 = objModule.WeekNos2;
                var $stateCode = objModule.StateCode;
                var $isESI = objModule.IsESI;

                var index = $(element).attr('row-index');

                var $nchkWO1 = splitStr($chkWO1, ',');
                var $nchkWO2 = splitStr($chkWO2, ',');

                if ($chkWO1.length == 9) {
                    $WO1 = $WO1 + " - all";
                }
                else {
                    $WO1 = $WO1 + " - <small>" + $nchkWO1 + "</small>";
                }

                if ($chkWO2.length == 9) {
                    $WO2 = $WO2 + " - all";
                }
                else {
                    $WO2 = $WO2 + " -  <small>" + $nchkWO2 + "</small>";
                }
                
                $('#lblLocationCode' + index).html($locationCode);
                $('#lblLocationDesc' + index).html($locationDesc);
                $('#lblWeeklyOff1' + index).html($WO1);
                $('#lblWeeklyOff2' + index).html($WO2);
                $('#lblStateCode' + index).html($stateCode);
                $('#lblIsESI' + index).html($isESI == true ? 'Yes' : 'No');
                $('#txtState' + index).val($stateCode);

                break;
            case self.moduleEnum.UDF:
                var $udfCode = objModule.UDFCode;
                var $udfDesc = objModule.UDFDesc;
                var index = $(element).attr('row-index');
                $('#lblUDFCode' + index).html($udfCode);
                $('#lblUDFDesc' + index).html($udfDesc);
                break;
            case self.moduleEnum.Bank:

                var $bankCode = objModule.BankCode;
                var $bankName = objModule.BankName;
                var $branchName = objModule.BranchName;
                var $contactName = objModule.ContactName;
                var $bankBSRCode = objModule.BankBSRCode;
                var $accountLen = objModule.AccountLen;
                var index = $(element).attr('row-index');
                $('#lblBankCode' + index).html($bankCode);
                $('#lblBankName' + index).html($bankName);
                $('#lblBranchName' + index).html($branchName);
                $('#lblContactName' + index).html($contactName);
                $('#lblAccountLen' + index).html($accountLen);
                $('#lblBankBSRCode' + index).html($bankBSRCode);
                break;
            case self.moduleEnum.ChangeReason:

                var $reasonCode = objModule.ReasonCode;
                var $reasonDesc = objModule.ReasonDesc;
                var index = $(element).attr('row-index');
                $('#lblReasonCode' + index).html($reasonCode);
                $('#lblReasonDesc' + index).html($reasonDesc);
                break;
            case self.moduleEnum.InActiveReason:
                var $inactiveID = objModule.InactiveID;
                var $inactiveDesc = objModule.InactiveDesc;
                var $pfReason = objModule.PFReason;
                var index = $(element).attr('row-index');
                $('#lblInactiveID' + index).html($inactiveID);
                $('#lblInactiveDesc' + index).html($inactiveDesc);
                $('#lblPFReason' + index).html($pfReason);
                break;
            case self.moduleEnum.HoldReason:
                var $holdID = objModule.HoldID;
                var $holdReason = objModule.HoldReason;
                var index = $(element).attr('row-index');
                $('#lblHoldID' + index).html($holdID);
                $('#lblHoldReason' + index).html($holdReason);
                break;
            case self.moduleEnum.Event:
                var $eventCode = objModule.EventCode;
                var $eventDetails = objModule.EventDetails;
                var index = $(element).attr('row-index');
                $('#lblEventCode' + index).html($eventCode);
                $('#lblEventDetails' + index).html($eventDetails);
                break;
            case self.moduleEnum.Designation:
                var $designationID = objModule.DesignationID;
                var $name = objModule.Name;
                var index = $(element).attr('row-index');
                $('#lblDesignationID' + index).html($designationID);
                $('#lblName' + index).html($name);
                break;
            case self.moduleEnum.PayrollGroup:
                var $payrollCode = objModule.PayrollCode;
                var $name = objModule.Name;
                var index = $(element).attr('row-index');
                $('#lblPayrollCode' + index).html($payrollCode);
                $('#lblName' + index).html($name);
                break;
            case self.moduleEnum.RelationMaster:
                var $cCode = objModule.RelationID;
                var $cDesc = objModule.Name;
                var $cWODeduction = objModule.IsActive;
                $cWODeduction = $cWODeduction == true ? 'Yes' : 'No';
                var index = $(element).attr('row-index');

                $('#lblRelationCode' + index).html($cCode);
                $('#lblRelationDesc' + index).html($cDesc);
                $('#lblchkIsApplicable' + index).html($cWODeduction);
                break;
                
            case self.moduleEnum.MediClaimMaster:
                var $cCode = objModule.Code;
                var $cDesc = objModule.Name;
                var $cAllowMediClaim = objModule.AllowForMediclaim;
                $cAllowMediClaim = $cAllowMediClaim == true ? 'Yes' : 'No';
                var index = $(element).attr('row-index');
                var $cCode1 = objModule.GradeCode;
                var $cDesc1 = objModule.GradeDesc;
                var $cLimit = objModule.Limit;

                if ($cLimit == null) {
                    $('#lblCode' + index).html($cCode);
                    $('#lblEventDetails' + index).html($cDesc);
                    $('#lblchkIsApplicable' + index).html($cAllowMediClaim);
                } else {
                    $('#lblCode1' + index).html($cCode1);
                    $('#lblDesc1' + index).html($cDesc1);
                    $('#lblLimit' + index).html($cLimit);
                }
                break;
        }
    },

    loadPolicy: function (url, input, element) {
        var self = this;
        $.ajax({
            type: "POST",
            url: url,
            data: input,
            success: function (response) {
                var html = '';
                html = template.bindPolicy(response);
                element.html(html);
            },
            failure: function (response) {
                return response;
            },
            error: function (response) {
                return response;
            }
        });
    },

    loadRegistration: function (url, input, element, template) {
        var self = this;
        $.ajax({
            type: "POST",
            url: url,
            data: input,
            success: function (response) {

                var moduleData = JSON.parse(response);

                $element.html(templat.tmpl(moduleData));
            },
            failure: function (response) {
                return response;
            },
            error: function (response) {
                return response;
            }
        });
    },
    regEvent: function () {

        $('.collapse').on('show.bs.collapse', function () {
            $('.collapse.in').collapse('hide');
            $(this).closest('tr').addClass('opentr').removeClass('closetr');
            $('.opentr').prev().addClass('opentrp').removeClass('closetrp');
            $('.opentrp').find('.fa').addClass('fa-minus-circle').removeClass('fa-plus-circle');
            //$(this).parent().hide();

        }).on('hidden.bs.collapse', function () {
            $(this).closest('tr').addClass('closetr').removeClass('opentr')
            $('.closetr').prev().addClass('closetrp').removeClass('opentrp');
            $('.closetrp').find('.fa').addClass('fa-plus-circle').removeClass('fa-minus-circle');

        });

        $('.collapseanchor').click(function () {
            $('.opentrp').css('display', 'table-row');

        });

        /**** for loaction code file**/
        $('.divcollapse.collapse').on('show.bs.collapse', function () {
            $('.collapse.in').collapse('hide');
            /*$(this).addClass('happy')
			$(this).prev().addClass('vhappy')*/
            $(this).prev().hide();


        }).on('hidden.bs.divcollapse.collapse', function () {
            /*$(this).parent('tablesshover').removeClass('happy')
			$(this).removeClass('happy')
			$(this).prev().removeClass('vhappy');*/
            $(this).prev().show();
        });

        //$('.deleterow').click(function () {
        //    $(this).parentsUntil('.tablesshover').parent().parent().remove();			
        //});		

        /**** for loaction code file**/

    },
};


