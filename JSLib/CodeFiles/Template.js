﻿function Template() {
    this.Code = null;
    this.Desc = null;
}

Template.prototype = {
    moduleEnum: {
        CostCenter: 1,
        BusinessArea: 2,
        Occupation: 3,
        Status: 4,
        Category: 5,
        Grade: 6,
        Location: 7,
        UDF:8
    },
    bindTemplate: function (index, data, moduleName) {
        debugger;
        var self = this;
        switch (parseInt(moduleName)) {
            case self.moduleEnum.CostCenter:
                var html = '';
                html += self.viewCostCenterMode(index, data) + self.editCostCenterMode(index, data);
                return html;
                break;
            case self.moduleEnum.BusinessArea:
                html += self.viewBusinessArea(index, data) + self.editBusinessArea(index, data);
                return html;
                break;
            case self.moduleEnum.Occupation:
                html += self.viewOccupationMode(index, data) + self.editOccupationMode(index, data);
                return html;
                break;
            case self.moduleEnum.Status:
                html += self.viewStatusMode(index, data) + self.editStatusMode(index, data);
                return html;
                break;
            case self.moduleEnum.Category:
                html += self.viewCategoryMode(index, data) + self.editCategoryMode(index, data);
                return html;
                break;
            case self.moduleEnum.Grade:
                html += self.viewGradeMode(index, data) + self.editGradeMode(index, data);
                return html;
                break;
        }
    },
    viewCostCenterMode: function (index, response) {
        var html = '<tr class="accordion-toggle accordiontoggle">' +
                  '<td  id="lblCostCode' + index + '" class="text-left col-md-3 padd-left-30 vCCode">' + response.CostCode + '</td>' +
                  '<td  id="lblCostCentre' + index + '" class="text-left col-md-6 vCCenter">' + response.CostCentre + '</td>' +
                  '<td class="text-right  col-md-3 padd-right-20">' +
                  '<input class="hfCCode" value=' + response.CostCode + ' type="hidden"/>' +
                  '<a data-toggle="collapse" data-target="#sec' + index + '" class="edit-btn  card-edit-btn editrow collapseanchor" title="Edit"><i class="icon-edit"></i></a>' +
                  '<a href="javascript:void(0);"  row-index=' + index + ' class="edit-btn  card-edit-btn deletrow  padd-left-20" title="Delete">   <span class="icon-icon-delete"></span></a>' +
                  '</td>' +
                  '</tr>';
        return html;
    },
    editCostCenterMode: function (index, response) {
        var html = '<tr>' +
        '<td colspan="12" class="hiddenRow ">' +
            '<div class="accordian-body collapse" id="sec' + index + '">' +
                '<table class="table greycolortable">' +
                '<tr>' +
                '<td class="text-left col-md-3 padd-left-30">' +
                '<label class="clearfix btn-block">Cost Code</label>' +
                '<input class="hfCCode" value=' + response.CostCode + ' type="hidden"/>' +
                            '<input id="txtCostCode' + index + '"  readonly type="text" class="clearfix eCCode" value=' + response.CostCode + ' />' +
                        '</td>' +
                        '<td class="text-left col-md-6">' +
                            '<label class="clearfix btn-block">Cost Center</label>' +
                            '<input  id="txtCostCentre' + index + '" type="text" required="required" class="clearfix btn-block eCCenter" value="' + response.CostCentre + '"/>' +
                        '</td>' +
                        '<td class="text-right  col-md-3  padd-right-20">' +
                            '<label class="clearfix btn-block">&nbsp;</label>' +
                            '<button type="button" class="btn btn-primary pull-right withoutbg cancelclose marg-left10 cancelclosedes">Cancel</button>' +
                            '<button type="button" row-index=' + index + '  class="btn btn-primary pull-right btnsave">Save</button>' +
                        '</td>' +
                '</tr>' +
                '</table>' +
            '</div>' +
        '</td>' +
        '</tr>';
        return html;
    },
    viewBusinessArea: function (index, response) {
        debugger
        var html = '<tr class="accordion-toggle accordiontoggle">' +

                 '<td id="lblBACode' + index + '" class="text-left col-md-3 padd-left-30 vBACode">' + response.BACode + '</td>' +
                 '<td id="lblBADesc' + index + '" class="text-left col-md-6 vBADesc">' + response.BADesc + '</td>' +
                 '<td class="text-right  col-md-3 padd-right-20">' +
                  '<input class="hfBACode" value=' + response.BACode + ' type="hidden"/>' +
                 '<a data-toggle="collapse" data-target="#sec' + index + '" class="edit-btn card-edit-btn editrow collapseanchor" title="Edit"><i class="icon-edit"></i></a>' +
                 '<a href="javascript:void(0);"  row-index=' + index + ' class="edit-btn card-edit-btn deletrow padd-left-20" title="Delete">    <span class="icon-icon-delete"></span></a>' +
                 '</td>' +
                 '</tr>';
        return html;
    },
    editBusinessArea: function (index, response) {
        var html = '<tr>' +
        '<td colspan="12" class="hiddenRow">' +
            '<div class="accordian-body collapse" id="sec' + index + '">' +
                '<table class="table greycolortable">' +
                '<tr>' +
            '<td class="text-left col-md-3 padd-left-30">' +
                '<input class="hfBACode" value=' + response.BACode + ' type="hidden"/>' +
                            '<label class="clearfix btn-block">Business Area Code</label>' +
                            '<input id="txtBACode' + index + '" readonly type="text" class="clearfix eBACode" value=' + response.BACode + ' />' +
                        '</td>' +
                        '<td class="text-left col-md-6">' +
                            '<label class="clearfix btn-block">Business Area Description</label>' +
                            '<input id="txtBADesc' + index + '"  type="text" required="required" class="clearfix btn-block eBADesc" value="' + response.BADesc + '"/>' +
                        '</td>' +
                           '<td class="text-right  col-md-3  padd-right-20">' +
                            '<label class="clearfix btn-block">&nbsp;</label>' +
                            '<button type="button" class="btn btn-primary pull-right withoutbg cancelclose marg-left10 cancelclosedes">Cancel</button>' +
                            '<button type="button" row-index=' + index + '  class="btn btn-primary pull-right btnsave">Save</button>' +
                        '</td>' +
                '</tr>' +
                '</table>' +
            '</div>' +
        '</td>' +
        '</tr>';
        return html;
    },
    viewOccupationMode: function (index, response) {
        var html = '<tr class="accordion-toggle accordiontoggle">' +
                 '<td id="lblOccCode' + index + '" class="text-left col-md-3 padd-left-30 vOCode">' + response.OccCode + '</td>' +
                 '<td id="lblOccDesc' + index + '" class="text-left col-md-6 vODesc">' + response.OccDesc + '</td>' +
                 '<td class="text-right  col-md-3 padd-right-20">' +
                 '<input class="hfOCode" value=' + response.OccCode + ' type="hidden"/>' +
                 '<a data-toggle="collapse" data-target="#secOcc' + index + '" class="edit-btn card-edit-btn editrow collapseanchor" title="Edit"><i class="icon-edit"></i></a>' +
                 '<a href="javascript:void(0);"  row-index=' + index + ' class="edit-btn card-edit-btn deletrow padd-left-20" title="Delete">   <span class="icon-icon-delete"></span></a>' +
                 '</td>' +
                 '</tr>';
        return html;
    },
    editOccupationMode: function (index, response) {
        var html = '<tr>' +
        '<td colspan="12" class="hiddenRow">' +
            '<div class="accordian-body collapse" id="secOcc' + index + '">' +
                '<table class="table greycolortable">' +
                '<tr>' +
               '<td class="text-left col-md-3 padd-left-30">' +
                '<label class="clearfix btn-block">Occupation Code</label>' +
                '<input class="hfOCode" value=' + response.OccCode + ' type="hidden"/>' +
                            '<input id="txtOccCode' + index + '" readonly type="text" class="clearfix eOCode" value=' + response.OccCode + ' />' +
                        '</td>' +
                         '<td class="text-left col-md-6">' +
                            '<label class="clearfix btn-block">Occupation Description</label>' +
                            '<input id="txtOccDesc' + index + '" type="text" required="required" class="clearfix btn-block eODesc" value="' + response.OccDesc + '"/>' +
                        '</td>' +
                         '<td class="text-right  col-md-3  padd-right-20">' +
                            '<label class="clearfix btn-block">&nbsp;</label>' +
                            '<button type="button" class="btn btn-primary pull-right withoutbg cancelclose marg-left10 cancelclosedes">Cancel</button>' +
                            '<button type="button" row-index=' + index + '  class="btn btn-primary pull-right btnsave">Save</button>' +
                        '</td>' +
                '</tr>' +
                '</table>' +
            '</div>' +
        '</td>' +
        '</tr>';
        return html;
    },
    viewStatusMode: function (index, response) {
        var html = '<tr class="accordion-toggle accordiontoggle">' +
                 '<td id="lblStatusCode' + index + '" class="text-left col-md-3 padd-left-30 vSCode">' + response.StatusCode + '</td>' +
                 '<td id="lblStatusDesc' + index + '" class="text-left col-md-6 vSDesc">' + response.StatusDesc + '</td>' +
                 '<td class="text-right  col-md-3 padd-right-20">' +
                 '<input class="hfOCode" value=' + response.StatusCode + ' type="hidden"/>' +
                 '<a data-toggle="collapse" data-target="#secOcc' + index + '" class="edit-btn card-edit-btn editrow collapseanchor" title="Edit"><i class="icon-edit"></i></a>' +
                 '<a href="javascript:void(0);"  row-index=' + index + ' class="edit-btn card-edit-btn deletrow padd-left-20" title="Delete">   <span class="icon-icon-delete"></span></a>' +
                 '</td>' +
                 '</tr>';
        return html;
    },
    editStatusMode: function (index, response) {
        var html = '<tr>' +
        '<td colspan="12" class="hiddenRow">' +
            '<div class="accordian-body collapse" id="secOcc' + index + '">' +
                '<table class="table greycolortable">' +
                '<tr>' +
               '<td class="text-left col-md-3 padd-left-30">' +
                '<label class="clearfix btn-block">Occupation Code</label>' +
                '<input class="hfSCode" value=' + response.StatusCode + ' type="hidden"/>' +
                            '<input id="txtStatusCode' + index + '"  readonly type="text" class="clearfix eSCode" value=' + response.StatusCode + ' />' +
                        '</td>' +
                         '<td class="text-left col-md-6">' +
                            '<label class="clearfix btn-block">Occupation Description</label>' +
                            '<input  id="txtStatusDesc' + index + '"  type="text" required="required" class="clearfix btn-block eSDesc" value="' + response.StatusDesc + '"/>' +
                        '</td>' +
                         '<td class="text-right  col-md-3  padd-right-20">' +
                            '<label class="clearfix btn-block">&nbsp;</label>' +
                            '<button type="button" class="btn btn-primary pull-right withoutbg cancelclose marg-left10 cancelclosedes">Cancel</button>' +
                            '<button type="button" row-index=' + index + ' class="btn btn-primary pull-right btnsave">Save</button>' +
                        '</td>' +
                '</tr>' +
                '</table>' +
            '</div>' +
        '</td>' +
        '</tr>';
        return html;
    },
    viewCategoryMode: function (index, response) {
        debugger
        var WODeductionStatus = (response.WODeduction == true) ? "Yes" : "No";
        var html = '<tr class="accordion-toggle accordiontoggle">' +
                 '<td id="lblCategoryCode' + index + '" class="text-left col-md-3 padd-left-30 vOCode">' + response.CategoryCode + '</td>' +
                 '<td  id="lblCategoryDesc' + index + '" class="text-left col-md-4 vODesc">' + response.CategoryDesc + '</td>' +
                    '<td id="lblWODeduction' + index + '" class="text-left col-md-2 vchkWO">' + WODeductionStatus + '</td>' +
                 '<td class="text-right  col-md-3 padd-right-20">' +
                 '<input class="hfCCode" value=' + response.CategoryCode + ' type="hidden"/>' +
                 '<a data-toggle="collapse" data-target="#secOcc' + index + '" class="edit-btn card-edit-btn editrow collapseanchor" title="Edit"><i class="icon-edit"></i></a>' +
                 '<a href="javascript:void(0);" class="edit-btn card-edit-btn deletrow padd-left-20" title="Delete">   <span class="icon-icon-delete"></span></a>' +
                 '</td>' +
                 '</tr>';
        return html;
    },
    editCategoryMode: function (index, response) {
        var html = '<tr>' +
        '<td colspan="12" class="hiddenRow">' +
            '<div class="accordian-body collapse" id="secOcc' + index + '">' +
                '<table class="table greycolortable">' +
                '<tr>' +
               '<td class="text-left col-md-3 padd-left-30">' +
                '<label class="clearfix btn-block">Category Code</label>' +
                '<input class="hfOCode" value=' + response.CategoryCode + ' type="hidden"/>' +
                            '<input readonly  id="txtCategoryCode' + index + '" type="text" class="clearfix eOCode" value=' + response.CategoryCode + ' />' +
                        '</td>' +
                         '<td class="text-left col-md-4">' +
                            '<label class="clearfix btn-block">Category Description</label>' +
                            '<input  id="txtCategoryDesc' + index + '" type="text" required="required" class="clearfix btn-block eODesc" value="' + response.CategoryDesc + '"/>' +
                        '</td>' +
                          '<td class="text-left col-md-2">' +
                            '<label>Weekly Off Deduction</label>';
        if (response.WODeduction)
            html += '<input id="chkWODeductionStatus' + index + '" checked type="checkbox" class="echkWO" />';
        else
            html += '<input id="chkWODeductionStatus' + index + '" type="checkbox" class="echkWO" />';

        html += '</td>' +
              '<td class="text-right  col-md-3  padd-right-20">' +
                 '<label class="clearfix btn-block">&nbsp;</label>' +
                 '<button type="button" class="btn btn-primary pull-right withoutbg cancelclose marg-left10 cancelclosedes">Cancel</button>' +
                 '<button type="button" class="btn btn-primary pull-right btnsave">Save</button>' +
             '</td>' +
     '</tr>' +
                '</table>' +
            '</div>' +
        '</td>' +
        '</tr>';
        return html;
    },

    viewGradeMode: function (index, response) {
        debugger
        var html = '<tr class="accordion-toggle accordiontoggle">' +
                 '<td id="lblGradeCode' + index + '" class="text-left col-md-3 padd-left-30 vOCode">' + response.GradeCode + '</td>' +
                 '<td  id="lblGradeDesc' + index + '" class="text-left col-md-4 vODesc">' + response.GradeDesc + '</td>' +
                 '<td id="lblLeavePolicy' + index + '" class="text-left col-md-2 vchkWO">' + response.PolicyID+ '</td>' +
                 '<td class="text-right  col-md-3 padd-right-20">' +
                 '<input class="hfCCode" value=' + response.GradeCode + ' type="hidden"/>' +
                 '<a data-toggle="collapse" data-target="#secOcc' + index + '" class="edit-btn card-edit-btn editrow collapseanchor" title="Edit"><i class="icon-edit"></i></a>' +
                 '<a href="javascript:void(0);" class="edit-btn card-edit-btn deletrow padd-left-20" title="Delete">   <span class="icon-icon-delete"></span></a>' +
                 '</td>' +
                 '</tr>';
        return html;
    },
    editGradeMode: function (index, response) {
        var html = '<tr>' +
        '<td colspan="12" class="hiddenRow">' +
            '<div class="accordian-body collapse" id="secOcc' + index + '">' +
                '<table class="table greycolortable">' +
                '<tr>' +
               '<td class="text-left col-md-3 padd-left-30">' +
                '<label class="clearfix btn-block">Grade Code</label>' +
                '<input class="hfOCode" value=' + response.GradeCode + ' type="hidden"/>' +
                            '<input readonly  id="txtGradeCode' + index + '" type="text" class="clearfix eOCode" value=' + response.GradeCode + ' />' +
                        '</td>' +
                         '<td class="text-left col-md-4">' +
                            '<label class="clearfix btn-block">Grade Description</label>' +
                            '<input  id="txtGradeDesc' + index + '" type="text" required="required" class="clearfix btn-block eODesc" value="' + response.GradeDesc + '"/>' +
                        '</td>' +
                          '<td class="text-left col-md-2">' +
                            '<label>Leave Policy</label>' +
                            '<select class="ddl-policy"  id="ddlPolicy' + index + '"></select>' +
                            '</td>' +
              '<td class="text-right  col-md-3  padd-right-20">' +
                 '<label class="clearfix btn-block">&nbsp;</label>' +
                 '<button type="button" class="btn btn-primary pull-right withoutbg cancelclose marg-left10 cancelclosedes">Cancel</button>' +
                 '<button type="button" class="btn btn-primary pull-right btnsave">Save</button>' +
             '</td>' +
     '</tr>' +
                '</table>' +
            '</div>' +
        '</td>' +
        '</tr>';
        return html;
    },

    bindPolicy:function(response)
    {
        var html = '';
        response = JSON.parse(response);
        html += '<option value="0">All</option>'
        $(response).each(function (index, policy) {
            html += '<option value="' + policy.PolicyID + '">' + policy.PolicyName + '</option>';
        });
        return html;
    },
    noRecord: function () {
        var html = '<tr><td colspan="3"><div style="text-align: center;">No record found</div></td></tr>';
        return html;
    }


}

