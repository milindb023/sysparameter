$(document).ready(function () {
    var app = new App();   
});

//global variables
var empNo = $('#hidEmpNo').val();
var empToken = $('#hidEmpToken').val();

//global methods
var messagePopup = function (msgText, msgType) {
    
    var html='';
    if (msgType == 'error') {
        $('#message').parent().removeClass('text-center');
        $('#message').removeClass('text-success').addClass('text-danger');
        html = '<ul>';
        $(msgText).each(function (index, data) {
            html += '<li>' + data + '</li>';
        });
        html += '</ul>';
    }
    else {
        $('#message').parent().addClass('text-center');
        $('#message').removeClass('text-danger').addClass('text-success');
        html = msgText;
    }
    
    $('#message').html(html);
    $('#alert').modal();
}

var closeRow = function (id) {   
        $(id).parentsUntil('.hiddenRow').addClass('sedr');
        $('.accordian-body.sedr').collapse('hide');
        $(id).closest('.hiddenRow').parent('tr').prev('tr').addClass('newshowhide').show();
   
        $('.teamleaves-bg.sedr').collapse('hide');
        $('.teamleaves-bg.sedr').removeClass('in');
  
}
var removeRow = function () {
    $('.sedr').parent('.no-brd-btm').remove();
    $('.accordion-toggle.sedr').remove().next('tr').remove();
}

//global Class
function App() {
	this.currentModule = null;
	this.initBaseEvents();
	this.loadModule('Dashboard', 'JSLib/CodeFiles/view/dashboard.html');
}

App.prototype.loadModule = function (moduleName, templateUrl, moduleDesc, moduleType) {
	var self = this;
	var currentModule = eval(moduleName);
	$('#content').load(templateUrl, function () {
		self.currentModule = null
		self.currentModule = new currentModule();
	
		if (moduleType == undefined) {
		    self.currentModule.init();
		    moduleName = moduleDesc;
		}
		else {
		    self.currentModule.init(moduleType, moduleDesc);
		    moduleName = moduleDesc;
		}

		$('#spanCodeFile').text(moduleName);
	});
};

App.prototype.initBaseEvents = function () {
    var self = this;

    $('#content').on('click', '.link', function () {
        var moduleLoad = $(this).attr('module-name');
        var moduleUrl = $(this).attr('templateUrl');
        var moduleDesc = $(this).attr('module-desc');

        if (moduleLoad == 'UDF') {
            var moduleType = $(this).attr('module-type');
            self.loadModule(moduleLoad, moduleUrl, moduleDesc, moduleType);
        }
        else {
            self.loadModule(moduleLoad, moduleUrl, moduleDesc);
        }
    });


    $('body').on('click', '.cancelclose', function () {
        $(this).parent('.btn-next').addClass('btnnext')
        $('.btnnext').parent().addClass('btnnext1')
        $('.btnnext1').removeClass('in');
        $('.clonies').remove();
    })

    $('body').on('click', '.cancelclosetable', function () {
        $(this).parentsUntil('.hiddenRow').addClass('btnnexttable')
        $('.btnnexttable').parent().addClass('btnnexttable1')
        $('.accordian-body.btnnexttable1').removeClass('in')
        $('#divscroll').load();
    })

    $('body').on('click', '.cancelclosedes', function () {
        $(this).parentsUntil('.hiddenRow').addClass('sedr');
	    $('.accordian-body.sedr').collapse('hide');
        
	    /* for parent tr hide 9 jan 2017**/
	    $(this).closest('.hiddenRow').parent('tr').prev('tr').addClass('newshowhide').show();
	    /* for parent tr hide 9 jan 2017**/
	  
	})

    /* for parent tr hide 9 jan 2017**/
	$('body').on('click', '.editrow', function () {
	    $(this).closest('tr').hide();
	    $('.hiddenRow').css('border-top', '0px')

	})
    /* for parent tr hide 9 jan 2017**/
    
	$("#btnExport").click(function (e) {

	});

};




