﻿function Bank() {
}

Bank.prototype = {
    init: function () { this.loadBank(); this.initEvents();},
    loadBank: function () {
        var $searchText = $("#txtSearch").val();
        var $template = $("#dataTemplate");
        var $element = $('#tblBank');
        var service = new CodeFileService();

        var Input = { SearchText: $searchText, EmpNo: empNo, EmployeeToken: empToken };

        service.loadData('../api/Bank/GetAllBank', Input, $element, $template);
    },
    initEvents: function () {
        var self = this;
        $('#tblBank').on('click', '.deletrow', function () {

            $(this).parentsUntil('.no-brd-btm').addClass('sedr');
            $(this).parentsUntil('.accordion-toggle').addClass('sedr');
            var $element = $('#tblBank');
            var index = $(this).attr('row-index');
            var bankCode = $('#lblBankCode' + index).html();
            var $isActive = $(this).attr('row-status');
            var $id = $(this);
            $('#cnfMessage').html($isActive != 'true' ? 'Do you want to deactivate this record?' : 'Do you want to activate this record?');
            $('#delete').html($isActive != 'true' ? 'Deactivate' : 'Activate');
            $('#confirm').modal({ keyboard: false }).one('click', '#delete', function (e) {
                var service = new CodeFileService();
                var Input = { BankCode: bankCode, EmpNo: empNo, EmployeeToken: empToken, IsActive: $isActive };
                service.removeData('../api/Bank/RemoveBank', Input, $element, $id);
            });
        });


        $('#tblBank').on('click', '.btnsave', function () {
            var $element = this;
            var index = $(this).attr('row-index');
            var $bankCode = $('#txtBankCode' + index).val().trim();
            var $bankName = $('#txtBankName' + index).val().trim();
            var $branchName = $('#txtBranchName' + index).val().trim();
            var $bankAddress1 = $('#txtBankAddress1' + index).val().trim();
            var $bankAddress2 = $('#txtBankAddress2' + index).val().trim();
            var $bankAddress3 = $('#txtBankAddress3' + index).val().trim();
            var $bankAddress4 = $('#txtBankAddress4' + index).val().trim();
            var $contactName = $('#txtContactName' + index).val().trim();
            var $bankBSRCode = $('#txtBankBSRCode' + index).val().trim();
            var $accountLen = $('#txtAccountLen' + index).val().trim();

            var service = new CodeFileService();

            var bank = {}
            bank["BankCode"] = $bankCode;
            bank["BankName"] = $bankName;
            bank["BranchName"] = $branchName;
            bank["BankAddress1"] = $bankAddress1;
            bank["BankAddress2"] = $bankAddress2;
            bank["BankAddress3"] = $bankAddress3;
            bank["BankAddress4"] = $bankAddress4;
            bank["ContactName"] = $contactName;
            bank["BankBSRCode"] = $bankBSRCode;
            bank["AccountLen"] = $accountLen;

            var Input = {
                EmpNo: empNo, EmployeeToken: empToken, BankCode: $bankCode, BankName: $bankName,
                BranchName: $branchName, BankAddress1: $bankAddress1, BankAddress2: $bankAddress2, BankAddress3: $bankAddress3,
                BankAddress4: $bankAddress4, ContactName: $contactName, BankBSRCode: $bankBSRCode, AccountLen: $accountLen
            };

            var isValid = self.validate(bank);

            if (isValid) {
                service.updateData(13, '../api/Bank/UpdateBank', Input, bank, $element);
            }
        });

        $('#btnAdd').on('click', function () {

            var $bankCode = $('#txtBankCode').val().trim();
            var $bankName = $('#txtBankName').val().trim();
            var $branchName = $('#txtBranchName').val().trim();
            var $bankAddress1 = $('#txtBankAddress1').val().trim();
            var $bankAddress2 = $('#txtBankAddress2').val().trim();
            var $bankAddress3 = $('#txtBankAddress3').val().trim();
            var $bankAddress4 = $('#txtBankAddress4').val().trim();
            var $contactName = $('#txtContactName').val().trim();
            var $bankBSRCode = $('#txtBankBSRCode').val().trim();
            var $accountLen = $('#txtAccountLen').val().trim();
            var $element = $('#tblBank');
            var $id = this;
            var service = new CodeFileService();

            var bank = {}
            bank["BankCode"] = $bankCode;
            bank["BankName"] = $bankName;
            bank["BranchName"] = $branchName;
            bank["BankAddress1"] = $bankAddress1;
            bank["BankAddress2"] = $bankAddress2;
            bank["BankAddress3"] = $bankAddress3;
            bank["BankAddress4"] = $bankAddress4;
            bank["ContactName"] = $contactName;
            bank["BankBSRCode"] = $bankBSRCode;
            bank["AccountLen"] = $accountLen;
            bank["IsActive"] = true;

            var Input = {
                EmpNo: empNo, EmployeeToken: empToken, BankCode: $bankCode, BankName: $bankName,
                BranchName: $branchName, BankAddress1: $bankAddress1, BankAddress2: $bankAddress2, BankAddress3: $bankAddress3,
                BankAddress4: $bankAddress4, ContactName: $contactName, BankBSRCode: $bankBSRCode, AccountLen: $accountLen
            };

            var isValid = self.validate(bank);

            if (isValid) {
                service.addData(13, '../api/Bank/AddBank', Input, bank, $element, $id);
            }
        });

        $('#btnSearch').on('click', this.loadBank);

        $('#txtSearch').on('keypress', function (event) {
            if (event.keyCode == 13) {
                Bank.prototype.loadBank(); event.preventDefault();
                return false;
            }
        });
    
        $('body').on('click', '.cancel22', function () {
            $('.divcollapse.collapse').collapse('hide');
            $('.divcollapse.collapse').removeClass('in');
            $('.tablesshover').show()
            $('.tablesshover div').show()
        });
     
    },
    validate: function (bank) {
        var errArray = [];
        //  return $(element).valid();
        var $bankCode = bank.BankCode;
        var $bankName = bank.BankName;
        var $branchName = bank.BranchName;
        var $bankAddress1 = bank.BankAddress1;
        var $bankAddress2 = bank.BankAddress2;
        var $bankAddress3 = bank.BankAddress3;
        var $bankAddress4 = bank.BankAddress4;
        var $contactName = bank.ContactName;
        var $bankBSRCode = bank.BankBSRCode;
        var $accountLen = bank.AccountLen;
        var alpharegx = /^[a-zA-Z\-\s]+$/;
        var alphanumregx = /^[a-zA-Z0-9_/.&-(),\-\s]+$/;
        var alphanumrwsegx = /^[a-zA-Z0-9_/.&-(),]+$/;
        var numericregx = /^[0-9]{0,}$/;
        
        if ($bankCode == '') {
            errArray.push('Bank code can not be blank');
        }
        else if (!alphanumrwsegx.test($bankCode)) {
            errArray.push('Bank code should be alphanumeric without space');
        }
         if ($bankName == '') {
            errArray.push('Bank name can not be blank');
        }
         else if (!alphanumregx.test($bankName)) {
            errArray.push('Bank name should contain only alphabets and only "_, /, &, -, ., (, )" special characters are allowed.');
        }
         if ($branchName == '') {
             errArray.push('Branch name can not be blank');
        }
         else if (!alphanumregx.test($branchName)) {
            errArray.push('Branch name should contain only alphabets and only "_, /, &, -, ., (, )" special characters are allowed.');
        }
         if ($bankAddress1 == '') {
             errArray.push('Address line 1 can not be blank');
        }
        else if (!alphanumregx.test($bankAddress1)) {
            errArray.push('Address line 1 should be alphanumeric  and only "_, /, &, -, ., (, )" special characters are allowed.');
        }
         if ($bankAddress2 == '') {
             errArray.push('Address line 2 can not be blank');
        }
        else if (!alphanumregx.test($bankAddress2)) {
            errArray.push('Address line 2 should be alphanumeric  and only "_, /, &, -, ., (, )" special characters are allowed.');
        }
         if ($bankAddress3 == '') {
             errArray.push('Address line 3 can not be blank');
        }
        else if (!alphanumregx.test($bankAddress3)) {
            errArray.push('Address line 3  should be alphanumeric  and only "_, /, &, -, ., (, )" special characters are allowed.');
        }
         if ($bankAddress4 == '') {
             errArray.push('Address line 4 can not be blank');
        }
        else if (!alphanumregx.test($bankAddress4)) {
            messagePopup('Address line 4 should be alphanumeric  and only "_, /, &, -, ., (, )" special characters are allowed.');
        }
         if ($contactName == '') {
            errArray.push('Contact name can not be blank');
        }
        else if (!alpharegx.test($contactName)) {
            errArray.push('Contact name should contain only alphabets');
        }
         if ($bankBSRCode == '') {
             errArray.push('Bank BSR code can not be blank');
        }
        else if (!alphanumrwsegx.test($bankBSRCode)) {
            errArray.push('Bank BSR code should be alphanumeric without space and only "_, /, &, -, ., (, )" special characters are allowed.');
        }
         if ($accountLen == '') {
            errArray.push('Account length can not be blank');
        }
        else if (!numericregx.test($accountLen)) {
            errArray.push('Account length should be numeric');
        }


         if (errArray.length > 0) {
             messagePopup(errArray, 'error');
             return false;
         }
         else {
             return true;
         }
    }
};