﻿function BusinessArea() {
    this.BACode = null;
    this.BADesc = null;
    this.baService = null;
}

BusinessArea.prototype = {
    init: function () {
        this.loadBusinessArea();
        this.initEvents();
    },
  
    loadBusinessArea: function () {
        var service = new CodeFileService();

        var $searchText = $("#txtSearch").val();
        var $template = $("#dataTemplate");
        var $element = $('#tblBA');
        var Input = { SearchText: $searchText, EmpNo: empNo, EmployeeToken: empToken };

        service.loadData('../api/BusinessArea/GetAllBusinessArea', Input, $element, $template);
    },
    initEvents: function () {
        $('#tblBA').on('click', '.deletrow', function () {
            $(this).parentsUntil('.no-brd-btm').addClass('sedr');
            $(this).parentsUntil('.accordion-toggle').addClass('sedr');
            var $element = $('#tblBA');
            var index = $(this).attr('row-index');
            var $id = $(this);
            var $isActive = $(this).attr('row-status');
            var baCode = $('#lblBACode' + index).html();
            $('#cnfMessage').html($isActive != 'true' ? 'Do you want to deactivate this record?' : 'Do you want to activate this record?');
            $('#delete').html($isActive != 'true' ? 'Deactivate' : 'Activate');
        
            $('#confirm').modal({ keyboard: false }).one('click', '#delete', function (e) {
                var service = new CodeFileService();

                var Input = { BACode: baCode, EmpNo: empNo, EmployeeToken: empToken,IsActive:$isActive };
                service.removeData('../api/BusinessArea/RemoveBusinessArea', Input, $element, $id);
            });
        });

        $('#tblBA').on('click', '.btnsave', function () {
            var $element = this;
            var index = $(this).attr('row-index');
            var $baCode = $('#txtBACode' + index).val();
            var $baDesc = $('#txtBADesc' + index).val().trim();

            var service = new CodeFileService();

            var businessArea = {}
            businessArea["BACode"] = $baCode.trim();
            businessArea["BADesc"] = $baDesc.trim();

            var Input = { EmpNo: empNo, EmployeeToken: empToken, BACode: $baCode, BADesc: $baDesc };
            var isValid = validateBusinessArea(businessArea);

            if (isValid) {
                service.updateData(2, '../api/BusinessArea/UpdateBusinessArea', Input, businessArea, $element);
            }
        });

        $('#btnAdd').on('click', function () {
            var $baCode = $('#txtBACode').val().trim();
            var $baDesc = $('#txtBADesc').val().trim();
            var $element = $('#tblBA');
            var $id = this;
            var service = new CodeFileService();

            var businessArea = {}
            businessArea["BACode"] = $baCode.trim();
            businessArea["BADesc"] = $baDesc.trim();
            businessArea["IsActive"] = true;

            var Input = { EmpNo: empNo, EmployeeToken: empToken, BACode: $baCode, BADesc: $baDesc };

            var isValid = validateBusinessArea(businessArea);

            if (isValid) {
                service.addData(2,'../api/BusinessArea/AddBusinessArea', Input, businessArea, $element, $id);
            }
        });

        $('#btnSearch').on('click', this.loadBusinessArea);

        $('#txtSearch').on('keypress', function (event) {

            if (event.keyCode == 13) {
                BusinessArea.prototype.loadBusinessArea(); event.preventDefault();
                return false;
            }
        });

        //$('#divscroll').on('click', '.reset', function () {
        //    var index = $(this).attr('row-index');         
        //        $('#txtBADesc' + index).val($('#lblBADesc' + index).html());        
        //});
    }
};

var validateBusinessArea = function (businessArea) {
    var errArray = [];
    var $baCode = businessArea.BACode;
    var $baDesc = businessArea.BADesc;
    var alpharegx = /^[a-zA-Z\-\s]+$/;
    var alphanumregx = /^[a-zA-Z0-9_/.&-(),\-\s]+$/;
    var alphanumrwsegx = /^[a-zA-Z0-9_/.&-(),]+$/;
    var numericregx = /^[0-9]{0,}$/;

    var html = '';
    if ($baCode == '') {
        errArray.push('Business area code can not be blank');
    }
    else if (!alphanumrwsegx.test($baCode)) {
        errArray.push('Business area Code should be alphanumeric and only "_, /, &, -, ., (, )" special characters are allowed.');
    }
    if ($baDesc == '') {
        errArray.push('Business area description can not be blank');
    }
    else if (!alphanumregx.test($baDesc)) {
        errArray.push('Business area description should be alphanumeric and only "_, /, &, -, ., (, )" special characters are allowed.');
    }

    if (errArray.length > 0) {
        messagePopup(errArray, 'error');
        return false;
    }
    else {
        return true;
    }
}

