﻿function Category() {
    this.CategoryCode = null;
    this.CategoryDesc = null;
    this.service = null;
}

Category.prototype = {
    init: function () {
        this.loadCategory();
        this.initEvents();
    },
    loadCategory: function () {
        var service = new CodeFileService();
        var $template = $("#dataTemplate");
        var $searchText = $("#txtSearch").val();
        var $element = $('#tblCategory');
        var Input = { SearchText: $searchText, EmpNo: empNo, EmployeeToken: empToken };

        service.loadData('../api/Category/GetAllCategory', Input, $element, $template);
    },
    initEvents: function () {

        $('#tblCategory').on('click', '.deletrow', function () {
            $(this).parentsUntil('.no-brd-btm').addClass('sedr');
            $(this).parentsUntil('.accordion-toggle').addClass('sedr');
            var $element = $('#tblCategory');
            var index = $(this).attr('row-index');
            var $isActive = $(this).attr('row-status');
            var cCode = $('#lblCategoryCode' + index).html();
            var $id = $(this);
            $('#cnfMessage').html($isActive != 'true' ? 'Do you want to deactivate this record?' : 'Do you want to activate this record?');
            $('#delete').html($isActive != 'true' ? 'Deactivate' : 'Activate');
           
            $('#confirm').modal({ keyboard: false }).one('click', '#delete', function (e) {
                var service = new CodeFileService();

                var Input = { CategoryCode: cCode, EmpNo: empNo, EmployeeToken: empToken ,IsActive:$isActive};

                service.removeData('../api/Category/RemoveCategory', Input, $element, $id);
            });
        });

        $('#tblCategory').on('click', '.btnsave', function () {
          
            var $element = this;
            var index = $(this).attr('row-index');
            var $categoryCode = $('#txtCategoryCode' + index).val();
            var $categoryDesc = $('#txtCategoryDesc' + index).val().trim();
            var $woDeduction = $('#chkWODeductionStatus' + index).prop('checked');
  var service = new CodeFileService();

            var category = {}
            category["CategoryCode"] = $categoryCode;
            category["CategoryDesc"] = $categoryDesc;
            category["WODeduction"] = $woDeduction;

            var Input = { EmpNo: empNo, EmployeeToken: empToken, CategoryCode: $categoryCode, CategoryDesc: $categoryDesc, WODeduction: $woDeduction };

            var isValid = validateCategory(category);

            if (isValid) {
                service.updateData(5, '../api/Category/UpdateCategory', Input, category, $element);
            }
        });

        $('#btnAdd').on('click', function () {
            var index = $(this).attr('row-index');
            var $costCode = $('#txtCategoryCode').val().trim();
            var $category = $('#txtCategoryDesc').val().trim();
            var $woDeduction = $('#chkWODeductionStatus' + index).prop('checked');
            var service = new CodeFileService();

            var $element = $('#tblCategory');
            var $id = this;

            var category = {}
            category["CategoryCode"] = $costCode;
            category["CategoryDesc"] = $category;
            category["WODeduction"] = $woDeduction;
            category["IsActive"] = true;

            var Input = { EmpNo: empNo, EmployeeToken: empToken, CategoryCode: $costCode, CategoryDesc: $category };

            var isValid = validateCategory(category);

            if (isValid) {
                service.addData(5,'../api/Category/AddCategory', Input, category, $element, $id);
            }
        });

        $('#btnSearch').on('click', this.loadCategory);

        $('#txtSearch').on('keypress', function (event) {

            if (event.keyCode == 13) {
                Category.prototype.loadCategory(); event.preventDefault();
                return false;
            }
        });

        //$('#divscroll').on('click', '.reset', function () {
        //    var index = $(this).attr('row-index');
      
        //        $('#txtCategoryDesc' + index).val($('#lblCategoryDesc' + index).html());
        //        var result = $('#lblWODeduction' + index).html();
        //        result == 'Yes' ? $('#chkWODeductionStatus' + index).prop('checked', true) : $('#chkWODeductionStatus' + index).prop('checked', false);
           
        //});
    }
};

var validateCategory = function (category) {
    var errArray = [];
    var $categoryCode = category.CategoryCode;
    var $categoryDesc = category.CategoryDesc;

    var alpharegx = /^[a-zA-Z\-\s]+$/;
    var alphanumregx = /^[a-zA-Z0-9_/.&-(),\-\s]+$/;
    var alphanumrwsegx = /^[a-zA-Z0-9_/.&-(),]+$/;
    var numericregx = /^[0-9]{0,}$/;

    if ($categoryCode == '') {
        errArray.push('Category code can not be blank');
    }
    else if (!alphanumregx.test($categoryCode)) {
        errArray.push('Category code should be alphanumeric  and only "_, /, &, -, ., (, )" special characters are allowed.');
    }
     if ($categoryDesc == '') {
         errArray.push('Category description can not be blank');
    }
    else if (!alphanumregx.test($categoryDesc)) {
        errArray.push('Category description should be alphanumeric  and only "_, /, &, -, ., (, )" special characters are allowed.');
    }

     if (errArray.length > 0) {
         messagePopup(errArray, 'error');
         return false;
     }
     else {
         return true;
     }
}














