function CostCenter() {
    this.CostCode = null;
    this.CostCentre = null;
    this.service = null;
    //this.empNo = $('#hidEmpNo').val();
    //this.empToken = $('#hidEmpToken').val();
}

CostCenter.prototype = {
    self: this,
    init: function () {
        this.loadCostCode();
        this.initEvents();
    },
    loadCostCode: function () {
       
        var service = new CodeFileService();
        var self = this;
        var $template = $("#dataTemplate");
        var $searchText = $("#txtSearch").val();
        var $element = $('#tblCostCenter');
        var Input = { SearchText: $searchText, EmpNo: empNo, EmployeeToken: empToken };

        service.loadData('../api/CostCode/GetAllCostCenter', Input, $element, $template);

    },
    initEvents: function () {
        var self = this;
        $('#tblCostCenter').on('click', '.deletrow', function () {

            $(this).parentsUntil('.no-brd-btm').addClass('sedr');
            $(this).parentsUntil('.accordion-toggle').addClass('sedr');
            var $element = $('#tblCostCenter');
            var index = $(this).attr('row-index');
            var $id = $(this);
            var $isActive = $(this).attr('row-status');
            var cCode = $('#lblCostCode' + index).html();
            $('#cnfMessage').html($isActive != 'true' ? 'Do you want to deactivate this record?' : 'Do you want to activate this record?');
            $('#delete').html($isActive != 'true' ? 'Deactivate' : 'Activate');

            $('#confirm').modal({ keyboard: false }).one('click', '#delete', function (e) {
                var service = new CodeFileService();

                var Input = { CostCode: cCode, EmpNo: empNo, EmployeeToken: empToken,IsActive:$isActive };

                service.removeData('../api/CostCode/RemoveCostCenter', Input, $element, $id);
            });
        });

        $('#tblCostCenter').on('click', '.btnsave', function () {
      
            var $element = this;
            var index = $(this).attr('row-index');
            var $costCode = $('#txtCostCode' + index).val();
            var $costCenter = $('#txtCostCentre' + index).val().trim();
            var service = new CodeFileService();

            var costCenter = {}
            costCenter["CostCode"] = $costCode.trim();
            costCenter["CostCentre"] = $costCenter.trim();

            var Input = { EmpNo:empNo, EmployeeToken:empToken, CostCode: $costCode, CostCenter: $costCenter };


            //   var Input = {EmpNo: self.empNo, EmployeeToken: self.empToken, objCostCenter: costCenter };

            var isValid = validateCostCenter(costCenter);

            if (isValid) {
                service.updateData(1, '../api/CostCode/UpdateCostCenter', Input, costCenter, $element);
            }
        });

        $('#btnAdd').on('click', function () {

            var $costCode = $('#txtCostCode').val().trim();
            var $costCenter = $('#txtCostCenter').val().trim();
            var $element = $('#tblCostCenter');
            var $id = this;
            var service = new CodeFileService();

            var costCenter = {}
            costCenter["CostCode"] = $costCode.trim();
            costCenter["CostCentre"] = $costCenter.trim();
            costCenter["IsActive"] = true;

            var Input = { EmpNo:empNo, EmployeeToken:empToken, CostCode: $costCode, CostCenter: $costCenter };

            var isValid = validateCostCenter(costCenter);

            if (isValid) {
                service.addData(1,'../api/CostCode/AddCostCenter', Input, costCenter, $element, $id);
            }
        });

        $('#btnSearch').on('click', this.loadCostCode);

        $('#txtSearch').on('keypress', function (event) {
            if (event.keyCode == 13) {
                CostCenter.prototype.loadCostCode(); event.preventDefault();
                return false;
            }
        });

        //$('#tblCostCenter').on('click', '.reset', function () {

        //    var index = $(this).attr('row-index');
 
        //        $('#txtCostCentre' + index).val($('#lblCostCentre' + index).html());
           
        //});
    }
};

var validateCostCenter = function (costCenter) {
    var errArray = [];

    if (costCenter != null) {
        var $costCode = costCenter.CostCode;
        var $costCenter = costCenter.CostCentre;
        var alpharegx = /^[a-zA-Z\-\s]+$/;
        var alphanumregx = /^[a-zA-Z0-9_/.&-(),\-\s]+$/;
        var alphanumrwsegx = /^[a-zA-Z0-9_/.&-(),]+$/;
        var numericregx = /^[0-9]{0,}$/;
        var html = '';

        if ($costCode == '') {
            errArray.push('Cost Code can not be blank');
        }
        else if (!alphanumrwsegx.test($costCode)) {
            errArray.push('Cost Code should be alphanumeric without space and only "_, /, &, -, ., (, )" special characters are allowed.');
        }
         if ($costCenter == '') {
             errArray.push('Cost Center can not be blank');
        }
        else if (!alphanumregx.test($costCenter)) {
            errArray.push('Cost Center should be alphanumeric and only "_, /, &, -, ., (, )" special characters are allowed.');
        }

        if (errArray.length > 0) {
            messagePopup(errArray, 'error');
            return false;
        }
        else {
            return true;
        }
    }
}
