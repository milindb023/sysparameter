﻿
function Dashboard() {
}

Dashboard.prototype = {
    init: function () {
        this.loadDashboard();
        this.initEvents();
    },

    loadDashboard: function () {
        var service = new CodeFileService();

        var $searchText = $("#txtSearch").val();

        var $template = $("#dataTemplate");
        var $element = $('#divCodeFiles');
        var Input = { SearchText: $searchText, EmpNo: empNo, EmployeeToken: empToken };

        service.loadData('../api/CostCode/GetAllCodeFiles', Input, $element, $template);
    },
    initEvents: function () {
        $('#btnSearch').on('click', this.loadDashboard);

        $('#txtSearch').on('keypress', function (event) {

            if (event.keyCode == 13) {
                Dashboard.prototype.loadDashboard();
                event.preventDefault();
                return false;
            }
           
        });
    }
};

