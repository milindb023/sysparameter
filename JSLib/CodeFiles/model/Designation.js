﻿/*Designation*/
function Designation() {
}

Designation.prototype = {
    init: function () {
        this.loadDesignation(); this.initEvents();
    },
    loadDesignation: function () {
        var service = new CodeFileService();

        var $searchText = $("#txtSearch").val();
        var $template = $("#dataTemplate");
        var $element = $('#tblDesignation');
        var Input = { SearchText: $searchText, EmpNo: empNo, EmployeeToken: empToken };

        service.loadData('../api/Designation/GetAllDesignation', Input, $element, $template);
    },
    initEvents: function () {
        var self = this;
        $('#tblDesignation').on('click', '.deletrow', function () {

            var $element = $('#tblDesignation');
            var index = $(this).attr('row-index');
            var $isActive = $(this).attr('row-status');
           // var designationCode = $('#lblDesignationID' + index).html();
            var $id = $(this);
            $('#cnfMessage').html($isActive != 'true' ? 'Do you want to deactivate this record?' : 'Do you want to activate this record?');
            $('#delete').html($isActive != 'true' ? 'Deactivate' : 'Activate');
            $('#confirm').modal({ keyboard: false }).one('click', '#delete', function (e) {

                var service = new CodeFileService();

                var Input = { DesignationID: index, EmpNo: empNo, EmployeeToken: empToken, IsActive: $isActive };

                service.removeData('../api/Designation/RemoveDesignation', Input, $element, $id);
            });
        });

        $('#tblDesignation').on('click', '.btnsave', function () {
            var $element = this;
            var index = $(this).attr('row-index');
            var $designationID = $('#txtDesignationID' + index).val();
            var $name = $('#txtName' + index).val().trim();

            var service = new CodeFileService();

            var designation = {}
            designation["DesignationID"] = $designationID;
            designation["Name"] = $name;

            var Input = { EmpNo: empNo, EmployeeToken: empToken, DesignationID: index, DesignationName: $name };

            var isValid = self.validate(designation);

            if (isValid) {
                service.updateData(14, '../api/Designation/UpdateDesignation', Input, designation, $element);
            }

        });

        $('#btnAdd').on('click', function () {
         //   var $designationID = $('#txtDesignationID').val().trim();
            var $name = $('#txtName').val().trim();
            var $element = $('#tblDesignation');
            var $id = this;

            var designation = {}
            designation["DesignationID"] = '';
            designation["Name"] = $name.trim();
            designation["IsActive"] = true;
            var Input = { EmpNo: empNo, EmployeeToken: empToken, DesignationName: $name };

            var service = new CodeFileService();

            var isValid = self.validate(designation);
            if (isValid) {
                service.addData(14, '../api/Designation/AddDesignation', Input, designation, $element, $id);
            }

            setTimeout(self.loadDesignation, 1000);

        });

        $('#btnSearch').on('click', this.loadDesignation);

        $('#txtSearch').on('keypress', function (event) {

            if (event.keyCode == 13) {
                Designation.prototype.loadDesignation();
            }
        });
    },
    validate: function (designation) {
        var errArray = [];

        var $designationID = designation.DesignationID;
        var $name = designation.Name;
        var alphanumregx = /^[a-zA-Z0-9_/.&-(),\-\s]+$/;
        var numericregx = /^[0-9]*$/;

       if ($name == '') {
           errArray.push('Designation name can not be blank');
        }
        //else if (!alphanumregx.test($name)) {
        //    errArray.push("Designation name should be alphanumeric");
        //}
       if (errArray.length > 0) {
           messagePopup(errArray, 'error');
           return false;
       }
       else {
           return true;
       }
    }
};