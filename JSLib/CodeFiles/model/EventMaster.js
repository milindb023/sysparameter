﻿/*Event*/
function EventMaster() {
}

EventMaster.prototype = {
    init: function () {
        this.loadEventMaster(); this.initEvents();
    },
    loadEventMaster: function () {
        var service = new CodeFileService();

        var $searchText = $("#txtSearch").val();
        var $template = $("#dataTemplate");
        var $element = $('#tblEventMaster');
        var Input = { SearchText: $searchText, EmpNo: empNo, EmployeeToken: empToken };

        service.loadData('../api/EventMaster/GetAllEventMaster', Input, $element, $template);
    },
    initEvents: function () {
        var self = this;
        $('#tblEventMaster').on('click', '.deletrow', function () {

            var $element = $('#tblEvent');
            var index = $(this).attr('row-index');
            var eventCode = $('#lblEventCode' + index).html();
            var $isActive = $(this).attr('row-status');
            var $id = $(this);
            $('#cnfMessage').html($isActive != 'true' ? 'Do you want to deactivate this record?' : 'Do you want to activate this record?');
            $('#delete').html($isActive != 'true' ? 'Deactivate' : 'Activate');

            $('#confirm').modal({ keyboard: false }).one('click', '#delete', function (e) {
                var service = new CodeFileService();

                var Input = { EventCode: eventCode, EmpNo: empNo, EmployeeToken: empToken, IsActive: $isActive };

                service.removeData('../api/EventMaster/RemoveEventMaster', Input, $element, $id);
            });
        });

        $('#tblEventMaster').on('click', '.btnsave', function () {
            var $element = this;
            var index = $(this).attr('row-index');
            var $eventCode = $('#txtEventCode' + index).val();
            var $eventDetails = $('#txtEventDetails' + index).val().trim();

            var service = new CodeFileService();

            var eventMaster = {}
            eventMaster["EventCode"] = $eventCode;
            eventMaster["EventDetails"] = $eventDetails;

            var Input = { EmpNo: empNo, EmployeeToken: empToken, EventCode: $eventCode, EventDetails: $eventDetails };
       
            var isValid = self.validate(eventMaster);

            if (isValid) {
                service.updateData(12, '../api/EventMaster/UpdateEventMaster', Input, eventMaster, $element);
            }

        });

        $('#btnAdd').on('click', function () {
            var $eventCode = $('#txtEventCode').val().trim();
            var $eventDetails = $('#txtEventDetails').val().trim();
            var $element = $('#tblEventMaster');
            var $id = this;

            var eventMaster = {}
            eventMaster["EventCode"] = $eventCode.trim();
            eventMaster["EventDetails"] = $eventDetails.trim();
            eventMaster["IsActive"] = true;
            var Input = { EmpNo: empNo, EmployeeToken: empToken, EventCode: $eventCode, EventDetails: $eventDetails };

            var service = new CodeFileService();
          
            var isValid = self.validate(eventMaster);
            if (isValid) {
                service.addData(12, '../api/EventMaster/AddEventMaster', Input, eventMaster, $element, $id);
            }
        });

        $('#btnSearch').on('click', this.loadEventMaster);

        $('#txtSearch').on('keypress', function (event) {
            if (event.keyCode == 13) {
                EventMaster.prototype.loadEventMaster(); event.preventDefault();
                return false;
            }
        });
    },
    validate: function (eventMaster) {
        var errArray = [];

        var $eventCode = eventMaster.EventCode;
        var $eventDetails = eventMaster.EventDetails;
        var alphanumregx = /^[a-zA-Z0-9_/.&-(),\-\s]+$/;
        var numericregx = /^[0-9]*$/;

        if ($eventCode == '') {
            errArray.push('Event code can not be blank');
        }
        else if (!alphanumregx.test($eventCode)) {
            errArray.push('Event code should be alphanumeric and only "_, /, &, -, ., (, )" special characters are allowed.');
        }
         if ($eventDetails == '') {
             errArray.push('Event details can not be blank');
        }
        else if (!alphanumregx.test($eventDetails)) {
            errArray.push('Event details should be alphanumeric and only "_, /, &, -, ., (, )" special characters are allowed.');
        }

        if (errArray.length > 0) {
            messagePopup(errArray, 'error');
            return false;
        }
        else {
            return true;
        }
    }
};