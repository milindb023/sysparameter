﻿function Grade() {
    this.GradeCode = null;
    this.GradeDesc = null;
    this.baService = null;
}

Grade.prototype = {
    init: function () {
        this.loadGrade();      
        this.initEvents();
    },
  
    loadGrade: function () {
        var self = this;
        var service = new CodeFileService();
        var $searchText = $("#txtSearch").val();
        var $template = $("#dataTemplate");
        var $element = $('#tblGrade');
        var Input = { SearchText: $searchText, EmpNo: empNo, EmployeeToken: empToken };

        service.loadData('../api/Grade/GetAllGrade', Input, $element,$template);
    },
    initEvents: function () {
        $('#tblGrade').on('click', '.deletrow', function () {
            $(this).parentsUntil('.no-brd-btm').addClass('sedr');
            $(this).parentsUntil('.accordion-toggle').addClass('sedr');
            var $element = $('#tblGrade');
            var index = $(this).attr('row-index');
            var $isActive = $(this).attr('row-status');
            var gradeCode = $('#lblGradeCode' + index).html();
            var service = new CodeFileService();
            var $id = $(this);
            $('#cnfMessage').html($isActive != 'true' ? 'Do you want to deactivate this record?' : 'Do you want to activate this record?');
            $('#delete').html($isActive != 'true' ? 'Deactivate' : 'Activate');
            $('#confirm').modal({ keyboard: false }).one('click', '#delete', function (e) {
                var Input = { GradeCode: gradeCode, EmpNo: empNo, EmployeeToken: empToken,IsActive:$isActive };
                service.removeData('../api/Grade/RemoveGrade', Input, $element, $id);
            });
        });

        $('#tblGrade').on('click', '.btnsave', function () {
           
            var $element = this;
            var index = $(this).attr('row-index');
            var $gradeCode = $('#txtGradeCode' + index).val();
            var $gradeDesc = $('#txtGradeDesc' + index).val().trim();

            var grade = {}
            grade["GradeCode"] = $gradeCode.trim();
            grade["GradeDesc"] = $gradeDesc.trim();

            var Input = { EmpNo: empNo, EmployeeToken: empToken, GradeCode: $gradeCode, GradeDesc: $gradeDesc};
            var isValid = validateGrade(grade);
            var service = new CodeFileService();

            if (isValid) {
                service.updateData(6, '../api/Grade/UpdateGrade', Input, grade, $element);
            }
        });

        $('#btnAdd').on('click', function () {
          
            var $gradeCode = $('#txtGradeCode').val().trim();
            var $gradeDesc = $('#txtGradeDesc').val().trim();

            var $element = $('#tblGrade');
            var $id = this;
            var service = new CodeFileService();

            var grade = {}
            grade["GradeCode"] = $gradeCode.trim();
            grade["GradeDesc"] = $gradeDesc.trim();
            grade["IsActive"] = true;

            var Input = { EmpNo: empNo, EmployeeToken: empToken, GradeCode: $gradeCode, GradeDesc: $gradeDesc};

            var isValid = validateGrade(grade);

            if (isValid) {
                service.addData(6,'../api/Grade/AddGrade', Input, grade, $element, $id);
            }
        });

        $('#btnSearch').on('click', this.loadGrade);

        $('#txtSearch').on('keypress', function (event) {
            if (event.keyCode == 13) {
                Grade.prototype.loadGrade(); event.preventDefault();
                return false;
            }
        });

        //$('#tblGrade').on('click', '.reset', function () {
        //    var index = $(this).attr('row-index');
         
        //        $('#txtGradeDesc' + index).val($('#lblGradeDesc' + index).html());

        //});
    }
};

var validateGrade = function (grade) {
    var errArray = [];

    var $gradeCode = grade.GradeCode;
    var $gradeDesc = grade.GradeDesc;

    var alpharegx = /^[a-zA-Z\-\s]+$/;
    var alphanumregx = /^[a-zA-Z0-9_/.&-(),\-\s]+$/;
    var alphanumrwsegx = /^[a-zA-Z0-9_/.&-(),]+$/;
    var numericregx = /^[0-9]{0,}$/;

    if ($gradeCode == '') {
        errArray.push('Grade code can not be blank');
    }
    else if (!alphanumregx.test($gradeCode)) {
        errArray.push('Grade code should be alphanumeric and only "_, /, &, -, ., (, )" special characters are allowed.');
    }
    if ($gradeDesc == '') {
        errArray.push('Grade description can not be blank');
    }

    if (errArray.length > 0) {
        messagePopup(errArray, 'error');
        return false;
    }
    else {
        return true;
    }
}

