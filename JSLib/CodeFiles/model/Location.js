﻿

function Location() {
   
}

Location.prototype = {
    init: function () {
        this.loadLocation();
        this.initEvents();
    },
    loadLocation: function () {
        var self = this;
        var service = new CodeFileService();

        var $searchText = $("#txtSearch").val();
        var $template = $("#dataTemplate");
        var $element = $('#locationTbl');

        var Input = { SearchText: $searchText, EmpNo: empNo, EmployeeToken: empToken };
        $.when(
          service.loadData('../api/Location/GetAllLocations', Input, $element, $template)
          ).done(function () {
              setTimeout(self.populate, 1000);
          });
    },
    initEvents: function () {
        var self = this;

        $('#locationTbl').on('click', '.deletrow', function () {
            var self = this;
            var index = $(this).attr('row-index');
            var $element = $('#locationTbl');
            var $isActive = $(this).attr('row-status');
            var $id = $(this);
            $('#cnfMessage').html($isActive != 'true' ? 'Do you want to deactivate this record?' : 'Do you want to activate this record?');
            $('#delete').html($isActive != 'true' ? 'Deactivate' : 'Activate');
            $('#confirm').modal({ keyboard: false }).one('click', '#delete', function (e) {
                var service = new CodeFileService();
                var Input = { LocationCode: index, EmpNo: empNo, EmployeeToken: empToken, IsActive: $isActive };
                service.removeData('../api/Location/RemoveLocation', Input, $element, $id);

                //  $('#div' + index).remove();
            });
        });

        $('#locationTbl').on('click', '.btnsave', function () { 
            var index = $(this).attr('row-index');
            var $template = $("#dataTemplate");

            var $element = this;
            var $locationDesc = $('#txtLocationDesc' + index).val().trim();
            var $ddlState = $('#ddlState' + index).val()
            var $ddlWO1 = $('#ddlWO1' + index).val()
            var $ddlPTF = $('#ddlPTF' + index).val()
            var $ddlPT = $('#ddlPT' + index).val()
            var $ddlWO2 = $('#ddlWO2' + index).val()
            var $ddlLWF = $('#ddlLWF' + index).val()
            var $chkESI = $('#chkESI' + index).prop('checked');
            var $ddlESI = $('#ddlESI' + index).val()
            var $txtAddress1 = $('#txtAddress1' + index).val().trim();
            var $txtAddress2 = $('#txtAddress2' + index).val().trim();
            var $txtAddress3 = $('#txtAddress3' + index).val().trim();
            var $txtESIOffice = $('#txtESIOffice' + index).val().trim();

            //var chkArr1 = [];
            //var chkArr2 = [];

            //$('.chkWO1' + index).each(function () {
            //    if (this.checked) {
            //        chkArr1.push($(this).attr('value'));
            //    }
            //});

            //$('.chkWO2' + index).each(function () { debugger
            //    if (this.checked) {
            //        chkArr2.push($(this).attr('value'));
            //    }
            //});

            //var WO1 = chkArr1.join(',');
            //var WO2 = chkArr2.join(',');

            var chktempArr1 = [];
            var chktempArr2 = [];

            $('.chkWO1' + index).each(function () {
                if (this.checked) {
                    chktempArr1.push($(this).attr('value'));
                }
            });

            $('.chkWO2' + index).each(function () {

                if (this.checked) {
                    chktempArr2.push($(this).attr('value'));
                }
            });
            var WO1 = chktempArr1.join(',');
            var WO2 = chktempArr2.join(',');

            if ($ddlESI == "" || $ddlESI == null)
                $ddlESI = "None";

            var Input = {
                EmpNo: empNo, EmployeeToken: empToken, LocationCode: index,
                LocationDesc: $locationDesc, WeeklyOff1: $ddlWO1, WeeklyOff2: $ddlWO2, PTFileName: $ddlPTF,
                StateCode: $ddlState, PTRegNo: $ddlPT, ESIRegNo: $ddlESI, LWFRegNo: $ddlLWF,
                WeekNos1: WO1, WeekNos2: WO2, IsESI: $chkESI, Address1: $txtAddress1,
                Address2: $txtAddress2, Address3: $txtAddress3, ESIOffice: $txtESIOffice
            };

            //create temp obj to validate
            var location = {}
            location["LocationCode"] = index.trim();
            location["LocationDesc"] = $locationDesc.trim();
            location["WeeklyOff1"] = $ddlWO1;
            location["WeeklyOff2"] = $ddlWO2;
            location["WeekNos1"] = WO1;
            location["WeekNos2"] = WO2;
            location["PTFileName"] = $ddlPTF;
            location["PTRegNo"] = $ddlPT;
            location["StateCode"] = $ddlState;
            location["ESIRegNo"] = $ddlESI;
            location["IsESI"] = $chkESI;
            location["LWFRegNo"] = $ddlLWF;
            location["Address1"] = $txtAddress1.trim();
            location["Address2"] = $txtAddress2.trim();
            location["Address3"] = $txtAddress3.trim();
            location["ESIOffice"] = $txtESIOffice.trim();

            var isValid = validateLocation(location, index);
            var service = new CodeFileService();

            if (isValid)
                service.updateData(7, '../api/Location/UpdateLocation', Input, location, $element);
        });

        $('#btnAdd').on('click', function () {
            var $element = $('#locationTbl');
            var $id = this;

            var $locationCode = $('#txtLocationCode').val().trim();
            var $locationDesc = $('#txtLocationDesc').val().trim();
            var $ddlState = $('#ddlState').val();
            var $ddlWO1 = $('#ddlWO1').val();
            var $ddlPTF = $('#ddlPTF').val();
            var $ddlPT = $('#ddlPT').val();
            var $ddlWO2 = $('#ddlWO2').val();
            var $ddlLWF = $('#ddlLWF').val();
            var $chkESI = $('#chkESI').prop('checked');
            var $ddlESI = $('#ddlESI').val();
            var $txtAddress1 = $('#txtAddress1').val().trim();
            var $txtAddress2 = $('#txtAddress2').val().trim();
            var $txtAddress3 = $('#txtAddress3').val().trim();
            var $txtESIOffice = $('#txtESIOffice').val().trim();
            var chktempArr1 = [];
            var chktempArr2 = [];

            $('.chkWO1').each(function () {
                if (this.checked) {
                    chktempArr1.push($(this).attr('value'));
                }
            });

            $('.chkWO2').each(function () {
                if (this.checked) {
                    chktempArr2.push($(this).attr('value'));
                }
            });
            var WO1 = chktempArr1.join(',');
            var WO2 = chktempArr2.join(',');

            var $WeekNos1List;
            var $WeekNos2List;

            W1 = [{ "Key": 1, "Value": false }, { "Key": 2, "Value": false }, { "Key": 3, "Value": false }, { "Key": 4, "Value": false }, { "Key": 5, "Value": false }];
            W2 = [{ "Key": 1, "Value": false }, { "Key": 2, "Value": false }, { "Key": 3, "Value": false }, { "Key": 4, "Value": false }, { "Key": 5, "Value": false }];

            $(W1).each(function (i, w1) {

                $(chktempArr1).each(function (j, ca1) {
                    if (w1.Key == ca1) {
                        w1.Value = true;
                    }
                });
                $WeekNos1List = W1;
            });

            $(W2).each(function (i, w2) {

                $(chktempArr2).each(function (j, ca2) {
                    if (w2.Key == ca2) {
                        w2.Value = true;
                    }
                });
                $WeekNos2List = W2;
            });

            if ($ddlESI == "" || $ddlESI == null)
                $ddlESI = "None";

            var Input = {
                EmpNo: empNo, EmployeeToken: empToken, LocationCode: $locationCode,
                LocationDesc: $locationDesc, WeeklyOff1: $ddlWO1, WeekNos2List: $WeekNos2List, WeekNos1List: $WeekNos1List, WeeklyOff2: $ddlWO2, PTFileName: $ddlPTF,
                StateCode: $ddlState, PTRegNo: $ddlPT, ESIRegNo: $ddlESI, LWFRegNo: $ddlLWF,
                WeekNos1: WO1, WeekNos2: WO2, IsESI: $chkESI, Address1: $txtAddress1,
                Address2: $txtAddress2, Address3: $txtAddress3, ESIOffice: $txtESIOffice
            };

            //create temp obj to validate
            var location = {}
            location["LocationCode"] = $locationCode.trim();
            location["LocationDesc"] = $locationDesc.trim();
            location["WeeklyOff1"] = $ddlWO1;
            location["WeeklyOff2"] = $ddlWO2;
            location["WeekNos1List"] = $WeekNos1List;
            location["WeekNos2List"] = $WeekNos2List;
            location["WeekNos1"] = WO1;
            location["WeekNos2"] = WO2;
            location["PTFileName"] = $ddlPTF;
            location["PTRegNo"] = $ddlPT;
            location["StateCode"] = $ddlState;
            location["ESIRegNo"] = $ddlESI;
            location["IsESI"] = $chkESI;
            location["LWFRegNo"] = $ddlLWF;
            location["Address1"] = $txtAddress1.trim();
            location["Address2"] = $txtAddress2.trim();
            location["Address3"] = $txtAddress3.trim();
            location["ESIOffice"] = $txtESIOffice.trim();
            location["IsActive"] = true;

            var isValid = validateLocation(location);
            var service = new CodeFileService();

            if (isValid) {
                $.when(service.addData(7, '../api/Location/AddLocation', Input, location, $element, $id)
                    ).done(function () {
                        setTimeout(self.populate, 1000);
                    });
            }
        });

        $('#btnSearch').on('click', this.loadLocation);

        $('#txtSearch').on('keypress', function (event) {
            if (event.keyCode == 13) {
                Location.prototype.loadLocation(); event.preventDefault();
                return false;
            }
        });

        $('#locationTbl').on('click', '.editrow', function () {

            var index = $(this).attr('row-index');
            var $stateCode = $('#hfStateCode' + index).val();
            var $ptRegNo = $('#hfPTRegNo' + index).val();
            var $lwfRegNo = $('#hfLWFRegNo' + index).val();
            var $esiRegNo = $('#hfESIRegNo' + index).val();
            var $ptFileName = $('#hfPTF' + index).val();
            var $wo1 = $('#hfWO1' + index).val();
            var $wo2 = $('#hfWO2' + index).val();

            $('#ddlState' + index + ' option[value="' + $stateCode + '"]').attr('selected', 'selected');
            $('#ddlWO1' + index + ' option[value="' + $wo1 + '"]').attr('selected', 'selected');
            $('#ddlPTF' + index + ' option[value="' + $ptFileName + '"]').attr('selected', 'selected');
            $('#ddlPT' + index + ' option[value="' + $ptRegNo + '"]').attr('selected', 'selected');
            $('#ddlWO2' + index + ' option[value="' + $wo2 + '"]').attr('selected', 'selected');
            $('#ddlLWF' + index + ' option[value="' + $lwfRegNo + '"]').attr('selected', 'selected');
            $('#ddlESI' + index + ' option[value="' + $esiRegNo + '"]').attr('selected', 'selected');

            if ($('#ddlWO1' + index).val() == 'None')  {
                
                $('.chkWO1' + index).attr('disabled', 'disabled');
                $('.chkWO1' + index).prop('checked', false);
            }
            else {
                $('.chkWO1' + index).removeAttr('disabled');
            }

            if ($('#ddlWO2'+ index).val() == 'None') { 
              
                $('.chkWO2'+ index).attr('disabled', 'disabled');
                $('.chkWO2' + index).prop('checked', false);
            }
            else {
                $('.chkWO2' + index).removeAttr('disabled');
            }
        });

        $('body').on('change', '.chkESI', function () {
            var index = $(this).attr('row-index');
            if (index == undefined) {
                if ($(this).prop('checked') == true)
                    $('#ESI-Wripper').show();
                else
                    $('#ESI-Wripper').hide();
            }
            else {
                if ($(this).prop('checked') == true)
                    $('#ESI-Wripper' + index).show();
                else
                    $('#ESI-Wripper' + index).hide();
            }
        });

        $('body').on('change', '.ddl-state', function () {
            var index = $(this).attr('row-index');
            if ($(this).val() == "Select") {
                if (index != undefined)
                    $('#txtState' + index).val('');
                else
                    $('#txtState').val('');
            } else {
                if (index != undefined)
                    $('#txtState' + index).val($(this).val());
                else
                    $('#txtState').val($(this).val());
            }
        });

        $('body').on('change', '.wo-ddl', function () {
            

            var index = $(this).attr('row-index');
            if (index == undefined) {

                if ($('#ddlWO1').val() == 'None') {
                    $('.chkWO1').attr('disabled', 'disabled');
                    $('.chkWO1').prop('checked', false);
                }
                else {
                    $('.chkWO1').removeAttr('disabled');
                }
                if ($('#ddlWO2').val() == 'None') {
                    $('.chkWO2').attr('disabled', 'disabled');
                    $('.chkWO2').prop('checked', false);
                }
                else {
                    $('.chkWO2').removeAttr('disabled');
                }

                if ($('#ddlWO1').val() == $('#ddlWO2').val() && $('#ddlWO1').val() != 'None' && $('#ddlWO2').val() != 'None') {
                    messagePopup('Weekly off 1 and weekly off 2 can not be same');
                    $(this).val('None').change();
                    return false;
                }

            } else {

                if ($('#ddlWO1' + index).val() == 'None') {
                    $('.chkWO1' + index).attr('disabled', 'disabled');
                    $('.chkWO1' + index).prop('checked', false);
                }
                else {
                    $('.chkWO1' + index).removeAttr('disabled');
                }
                if ($('#ddlWO2' + index).val() == 'None') {
                    $('.chkWO2' + index).attr('disabled', 'disabled');
                    $('.chkWO2' + index).prop('checked', false);
                }
                else {
                    $('.chkWO2' + index).removeAttr('disabled');
                }



                if ($('#ddlWO1' + index).val() == $('#ddlWO2' + index).val() && $('#ddlWO1' + index).val() && $('#ddlWO2' + index).val() && $('#ddlWO1' + index).val() != 'None' && $('#ddlWO2' + index).val() != 'None') {
                    messagePopup('Weekly off 1 and weekly off 2 can not be same');
                    $(this).val('None').change();
                    return false;
                }


                return true;

            }
        });

        /**** for loaction code file**/
        $('body').on('click', '.cancel22', function () {
            $('.divcollapse.collapse').collapse('hide');
            $('.divcollapse.collapse').removeClass('in');
            $('.tablesshover').show()
            $('.tablesshover div').show()
        });
        /**** for loaction code file**/

    },
    populate: function () {
       
        var service = new CodeFileService();
        var $ptRegddl = $('#content .ddl-pt');
        var $ptfddl = $('#content .ddl-ptf');
        var $lwRegddl = $('#content .ddl-lw');
        var $esiRegddl = $('#content .ddl-esi');
        var $stateRegddl = $('#content .ddl-state');
        var $ddltemplate = $("#ddlTemplate");

        var $statetemplate = $("#stateTemplate");
        var $ptftemplate = $("#ptfTemplate");


        var InputPT = { EmpNo: empNo, EmployeeToken: empToken, RegType: 'PT' }
        var InputLW = { EmpNo: empNo, EmployeeToken: empToken, RegType: 'LW' }
        var InputESI = { EmpNo: empNo, EmployeeToken: empToken, RegType: 'ESI' }
        var Input = { EmpNo: empNo, EmployeeToken: empToken }
        var InputPTF = { EmpNo: empNo, EmployeeToken: empToken }


        service.loadData('../api/Location/GetRegistration', InputPT, $ptRegddl, $ddltemplate);
        service.loadData('../api/Location/GetRegistration', InputLW, $lwRegddl, $ddltemplate);
        service.loadData('../api/Location/GetRegistration', InputESI, $esiRegddl, $ddltemplate);
        service.loadData('../api/Location/GetState', Input, $stateRegddl, $statetemplate);
        service.loadData('../api/Location/GetPTFiles', Input, $ptfddl, $ptftemplate);



    }
}

var splitStr = function (string, seperator) {
    var str = '';
    if (string != null) {
        var arr = string.split(seperator);

        var a = [];
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] == 1) {
                a.push('1st');
            }
            if (arr[i] == 2) {
                a.push('2nd');
            }
            if (arr[i] == 3) {
                a.push('3rd');
            }
            if (arr[i] == 4) {
                a.push('4th');
            }
            if (arr[i] == 5) {
                a.push('5th');
            }
        }
        str = a.join(', ');
    }
    else {
        str = 'None';
    }
    return str;
}
var replaceStr = function (string) {
    var str = '';
    if (string == 1) {
        str = '1st';
    }
    if (string == 2) {
        str = '2nd';
    }
    if (string == 3) {
        str = '3rd';
    }
    if (string == 4) {
        str = '4th';
    }
    if (string == 5) {
        str = '5th';
    }
    return str;
}
var hideShow = function (id, isVisible) {


    if (isVisible)
        $('#ESI-Wripper' + id).show();
    else
        $('#ESI-Wripper' + id).hide();
}

var validateLocation = function (location, tempindex) {
    var isValid = false;
    var errArray = [];

    var $locCode = location.LocationCode;
    var $locDesc = location.LocationDesc;
    var $wOff1 = location.WeeklyOff1;
    var $wOff2 = location.WeeklyOff2;
    var $weekNos1 = location.WeekNos1;
    var $weekNos2 = location.WeekNos2;
    var $ptFileName = location.PTFileName;
    var $ptRegNo = location.PTRegNo;
    var $stateCode = location.StateCode;
    var $esiRegNo = location.ESIRegNo;
    var $lwfRegNo = location.LWFRegNo;
    var $address1 = location.Address1;
    var $address2 = location.Address2;
    var $address3 = location.Address3;
    var $esiOffice = location.ESIOffice;
    var $chkESI = location.IsESI;

    var alpharegx = /^[a-zA-Z\-\s]+$/;
    var alphanumregx = /^[a-zA-Z0-9_/.&-(),\-\s]+$/;
    var alphanumrwsegx = /^[a-zA-Z0-9_/.&-(),]+$/;
    var numericregx = /^[0-9]{0,}$/;

    if ($locCode == "") {
        errArray.push('Location code can not be blank');
    }
    else if (!alphanumrwsegx.test($locCode)) {
        errArray.push('Location code should be alphanumeric without space and only "_, /, &, -, ., (, )" special characters are allowed.');
    }
    if ($locDesc == "") {
        errArray.push('Location description can not be blank');
    }

    var chktempArr1 = [];
    var chktempArr2 = [];

    $('.chkWO1' + tempindex).each(function () {
        if (this.checked) {
            chktempArr1.push($(this).attr('value'));
        }
    });

    $('.chkWO2' + tempindex).each(function () {
        if (this.checked) {
            chktempArr2.push($(this).attr('value'));
        }
    });


    if ($wOff1 != "None" && chktempArr1.length <= 0 && $weekNos1 <= 0) {
        errArray.push('Please select the weekday for WeeklyOff1');
    }

    if ($wOff2 != "None" && chktempArr2.length <= 0 && $weekNos2 <= 0) {
        errArray.push('Please select the weekday for WeeklyOff2');
    }
    //if ($wOff1 != "None" || $weekNos1.length <= 0) {debugger
    //    errArray.push('Please select Weeklyoff');
    //}
    //if ($wOff2 == "None" || $weekNos2.length == 0) {
    //    errArray.push('Please select day(s) for weekly off two');
    //}
    if ($stateCode == "0") {
        errArray.push('State code can not be blank');
    }
    if ($chkESI) {
        if ($address1 == "") {
            errArray.push('Address line 1 can not be blank');
        }
        else if (!alphanumregx.test($address1)) {
            errArray.push('Address line 1 should be alphanumeric and only "_, /, &, -, ., (, )" special characters are allowed.');
        }
        if ($address2 == "") {
            errArray.push('Address line 2 can not be blank');
        }
        else if (!alphanumregx.test($address2)) {
            errArray.push('Address line 2 should be alphanumeric and only "_, /, &, -, ., (, )" special characters are allowed.');
        }
        if ($address3 == "") {
            errArray.push('Address line 3 area code can not be blank');
        }
        else if (!alphanumregx.test($address3)) {
            errArray.push('Address line 3 should be alphanumeric and only "_, /, &, -, ., (, )" special characters are allowed.');
        }
        if ($esiOffice == "") {
            errArray.push('ESI office can not be blank');
        }
        else if (!alphanumregx.test($esiOffice)) {
            errArray.push('ESI office should be alphanumeric and only "_, /, &, -, ., (, )" special characters are allowed.');
        }
    }
    if (errArray.length > 0) {
        messagePopup(errArray, 'error');
        return false;
    }
    else {
        return true;
    }

}


