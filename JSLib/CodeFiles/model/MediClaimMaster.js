﻿/*Event*/
function MediClaimMaster() {
}

MediClaimMaster.prototype = {
    init: function () {
        this.loadMediClaim(); this.initMediClaim(); this.loadGrade(); this.initGrade();
    },
    loadGrade: function () {
        
        var service = new CodeFileService();
        var $template = $("#dataTemplate1");
        var $searchText = $("#txtSearch1").val();
        var $element = $('#tblGrade');
        var Input = { SearchText: $searchText, EmpNo: empNo, EmployeeToken: empToken };

        service.loadData('../api/GradeLimit/GetAllGrade', Input, $element, $template);
    },
    initGrade: function () {
        $('#tblGrade').on('click', '.btnsave', function () {

            var $element = this;
            var index = $(this).attr('row-index');
            var $GradeCode = $('#txtGradeCode' + index).val();
            var $Limit = $('#txtGradeAmt' + index).val();
            var service = new CodeFileService();

            var grade = {}
            grade["GradeCode"] = $GradeCode;
            grade["Limit"] = $Limit;

            var Input = { EmpNo: empNo, EmployeeToken: empToken, GradeCode: $GradeCode, Limit: $Limit };

            var isValid = validateGradeLimit(grade);
            if (isValid) {
                service.updateData(17, '../api/GradeLimit/UpdateGradeLimit', Input, grade, $element);
            }
        });

        $('#btnSearch1').on('click', this.loadGrade);

        $('#txtSearch1').on('keypress', function (event) {

            if (event.keyCode == 13) {
                MediClaimMaster.prototype.loadGrade(); event.preventDefault();
                return false;
            }
        });
        
    },
    loadMediClaim: function () {
        
        var service = new CodeFileService();
        var $template = $("#dataTemplate");
        var $searchText = $("#txtSearch").val();
        var $element = $('#tblRelation');
        var Input = { SearchText: $searchText, EmpNo: empNo, EmployeeToken: empToken };

        service.loadData('../api/ManageMediClaim/GetAllManageMediclaim', Input, $element, $template);
    },
    initMediClaim: function () {
        var self = this;
        $('#tblRelation').on('click', '.btnsave', function () {
            
            var $element = this;
            var index = $(this).attr('row-index');
            var $Code = $('#txtCode' + index).val();
            var $AllowForMediclaim = $('#chkIsApplicable' + index).prop('checked');
            var service = new CodeFileService();

            var relation = {}
            relation["Code"] = $Code;
            relation["AllowForMediclaim"] = $AllowForMediclaim;

            var Input = { EmpNo: empNo, EmployeeToken: empToken, Code: $Code, AllowForMediclaim: $AllowForMediclaim };

            var isValid = true;
            if (isValid) {
                service.updateData(17, '../api/ManageMediClaim/UpdateManageMediclaim', Input, relation, $element);
            }
        });

        $('#btnSearch').on('click', this.loadMediClaim);

        $('#txtSearch').on('keypress', function (event) {

            if (event.keyCode == 13) {
                MediClaimMaster.prototype.loadMediClaim(); event.preventDefault();
                return false;
            }
        });
    }

};


var validateGradeLimit = function (grade) {
    var errArray = [];
    var $Limit = grade.Limit;

    var alpharegx = /^[a-zA-Z\-\s]+$/;
    var alphanumregx = /^[a-zA-Z0-9_/.&-(),\-\s]+$/;
    var alphanumrwsegx = /^[a-zA-Z0-9_/.&-(),]+$/;
    var numericregx = /^[0-9]{0,}$/;
    var numericregx1 = /^\d{1,9}$/;
    var numericregx2 = /^[0-9]*\.?[0-9]*$/;
    if ($Limit == '') {
        errArray.push('Amount cannot be blank.');
    }
    else if (!numericregx2.test($Limit)) {
        errArray.push('Only Numeric Value & Single Dot is Allowed.');
    }
    if (errArray.length > 0) {
        messagePopup(errArray, 'error');
        return false;
    }
    else {
        return true;
    }
}
