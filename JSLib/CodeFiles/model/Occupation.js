﻿function Occupation() {
    this.OccCode = null;
    this.OccDesc = null;
    this.service = null;
}

Occupation.prototype = {
    init: function () {
        this.loadOccupation();
        this.initEvents();
    },
    loadOccupation: function () {
        var $searchText = $("#txtSearch").val();
        var $template = $("#dataTemplate");
        var $element = $('#tblOccupation');
        var service = new CodeFileService();

        var Input = { SearchText: $searchText, EmpNo: empNo, EmployeeToken: empToken };

        service.loadData('../api/Occupation/GetAllOccupation', Input, $element, $template);
    },
    initEvents: function () {

        $('#tblOccupation').on('click', '.deletrow', function () {
            $(this).parentsUntil('.no-brd-btm').addClass('sedr');
            $(this).parentsUntil('.accordion-toggle').addClass('sedr');
            var $element = $('#tblOccupation');
            var $isActive = $(this).attr('row-status');
            var index = $(this).attr('row-index');
            var $id = $(this);
            $('#cnfMessage').html($isActive != 'true' ? 'Do you want to deactivate this record?' : 'Do you want to activate this record?');
            $('#delete').html($isActive != 'true' ? 'Deactivate' : 'Activate');
            var baCode = $('#lblOccCode' + index).html();
            $('#confirm').modal({ keyboard: false }).one('click', '#delete', function (e) {
                var service = new CodeFileService();

                var Input = { OccCode: baCode, EmpNo: empNo, EmployeeToken: empToken, IsActive: $isActive };
                service.removeData('../api/Occupation/RemoveOccupation', Input, $element, $id);
            });
        });

        $('#tblOccupation').on('click', '.btnsave', function () {

            var $element = this;
            var index = $(this).attr('row-index');
            var $occCode = $('#txtOccCode' + index).val();
            var $occDesc = $('#txtOccDesc' + index).val().trim();
            var service = new CodeFileService();

            var occupation = {}
            occupation["OccCode"] = $occCode.trim();
            occupation["OccDesc"] = $occDesc.trim();

            var Input = { EmpNo: empNo, EmployeeToken: empToken, OccCode: $occCode, OccDesc: $occDesc };
            var isValid = validateOccupation(occupation);

            if (isValid) {
                service.updateData(3, '../api/Occupation/UpdateOccupation', Input, occupation, $element);
            }
        });

        $('#btnAdd').on('click', function () {           
            var $occCode = $('#txtOccCode').val().trim();
            var $occDesc = $('#txtOccDesc').val().trim();
            var $element = $('#tblOccupation');
            var $id = this;
            var service = new CodeFileService();

            var occupation = {}
            occupation["OccCode"] = $occCode.trim();
            occupation["OccDesc"] = $occDesc.trim();
            occupation["IsActive"] = true;
            var Input = { EmpNo: empNo, EmployeeToken: empToken, OccCode: $occCode, OccDesc: $occDesc };

            var isValid = validateOccupation(occupation);

            if (isValid) {
                service.addData(3,'../api/Occupation/AddOccupation', Input, occupation, $element, $id);
            }
        });

        $('#btnSearch').on('click', this.loadOccupation);

        $('#txtSearch').on('keypress', function (event) {

            if (event.keyCode == 13) {
                Occupation.prototype.loadOccupation(); event.preventDefault();
                return false;
            }
        });


        //$('#tblOccupation').on('click', '.reset', function () {
        //    var index = $(this).attr('row-index');
        //    if (index != undefined)
        //        $('#txtOccDesc' + index).val($('#lblOccDesc' + index).html());
        //    else
        //        $('#txtOccDesc').val(''); $('#txtOccCode').val('');
        //});
    }
};

var validateOccupation = function (occupation) {
    var errArray = [];

    var $occCode = occupation.OccCode;
    var $occDesc = occupation.OccDesc;
   
    var alpharegx = /^[a-zA-Z\-\s]+$/;
    var alphanumregx = /^[a-zA-Z0-9_/.&-(),\-\s]+$/;
    var alphanumrwsegx = /^[a-zA-Z0-9_/.&-(),]+$/;
    var numericregx = /^[0-9]{0,}$/;
    
    if ($occCode == '') {
        errArray.push('Occupation code can not be blank');
    }
    else if (!alphanumregx.test($occCode)) {
        errArray.push('Occupation code should be alphanumeric and only "_, /, &, -, ., (, )" special characters are allowed.');
    }
    if ($occDesc == '') {
        errArray.push('Occupation description can not be blank');
    }
    else if (!alphanumregx.test($occDesc)) {
        errArray.push('Occupation description should be alphanumeric and only "_, /, &, -, ., (, )" special characters are allowed.');
    }

    if (errArray.length > 0) {
        messagePopup(errArray, 'error');
        return false;
    }
    else {
        return true;
    }
}














