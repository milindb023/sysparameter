﻿/*PayrollGroup*/
function PayrollGroup() {
}

PayrollGroup.prototype = {
    init: function () {
        this.loadPayrollGroup(); this.initEvents();
    },
    loadPayrollGroup: function () {
        var service = new CodeFileService();

        var $searchText = $("#txtSearch").val();
        var $template = $("#dataTemplate");
        var $element = $('#tblPayrollGroup');
        var Input = { SearchText: $searchText, EmpNo: empNo, EmployeeToken: empToken };

        service.loadData('../api/PayrollGroup/GetAllPayrollGroup', Input, $element, $template);
    },
    initEvents: function () {
        var self = this;
        $('#tblPayrollGroup').on('click', '.deletrow', function () {
            var $element = $('#tblPayrollGroup');
            var index = $(this).attr('row-index');
            var payrollCode = $('#lblPayrollCode' + index).html();
            var service = new CodeFileService();

            var Input = { PayrollCode: payrollCode, EmpNo: empNo, EmployeeToken: empToken };

            service.removeData('../api/PayrollGroup/UpdatePayrollGroup', Input, $element);

        });

        $('#tblPayrollGroup').on('click', '.btnsave', function () {
            var $element = this;
            var index = $(this).attr('row-index');
            var $payrollCode = $('#txtPayrollCode' + index).val();
            var $name = $('#txtName' + index).val().trim();

            var service = new CodeFileService();

            var payrollGroup = {}
            payrollGroup["PayrollCode"] = $payrollCode;
            payrollGroup["Name"] = $name;

            var Input = { EmpNo: empNo, EmployeeToken: empToken, PayrollCode: $payrollCode, PayrollName: $name };

            var isValid = self.validate(payrollGroup);

            if (isValid) {
                service.updateData(15, '../api/PayrollGroup/UpdatePayrollGroup', Input, payrollGroup, $element);
            }

        });

        $('#btnAdd').on('click', function () {
            var $payrollCode = $('#txtPayrollCode').val().trim();
            var $name = $('#txtName').val().trim();
            var $element = $('#tblPayrollGroup');
            var $id = this;

            var payrollGroup = {}
            payrollGroup["PayrollCode"] = $payrollCode.trim();
            payrollGroup["Name"] = $name.trim();
            businessArea["IsActive"] = true;
            var Input = { EmpNo: empNo, EmployeeToken: empToken, PayrollCode: $payrollCode, PayrollName: $name };

            var service = new CodeFileService();

            var isValid = self.validate(payrollGroup);
            if (isValid) {
                service.addData(15, '../api/PayrollGroup/AddPayrollGroup', Input, payrollGroup, $element, $id);
            }
        });

        $('#btnSearch').on('click', this.loadPayrollGroup);

        $('#txtSearch').on('keypress', function (event) {
            if (event.keyCode == 13) {
                PayrollGroup.prototype.loadPayrollGroup();
            }
        });
    },
    validate: function (payrollGroup) {
        var errArray = [];

        var $payrollCode = payrollGroup.PayrollCode;
        var $name = payrollGroup.Name;
        var alphanumregx = /^[a-zA-Z0-9_/.&-(),\-\s]+$/;
        var numericregx = /^[0-9]*$/;

        var html = '';
        if ($payrollCode == '') {
            errArray.push('Payroll Code can not be blank');
        }
        else if (!numericregx.test($payrollCode)) {
            errArray.push("Payroll Code should be numeric");
        }
        if ($name == '') {
            errArray.push('Payroll group name can not be blank');
        }
        else if (!alphanumregx.test($name)) {
            errArray.push("Payroll group name should be alphanumeric");
        }

        if (errArray.length > 0) {
            messagePopup(errArray, 'error');
            return false;
        }
        else {
            return true;
        }
    }
};