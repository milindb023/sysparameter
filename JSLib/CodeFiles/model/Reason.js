﻿/* Base */
function ReasonBase() {

}

ReasonBase.prototype = {
    loadReason: function (url, $element) {
        var service = new CodeFileService();
        var $searchText = $("#txtSearch").val();
        var $template = $("#dataTemplate");
        var Input = { SearchText: $searchText, EmpNo: empNo, EmployeeToken: empToken };
        service.loadData(url, Input, $element, $template);
    },
    addReason: function (moduleName, url, input, reasonObj, $element, $id) {
        var service = new CodeFileService();
        service.addData(moduleName, url, input, reasonObj, $element, $id);
    },
    updateReason: function (moduleName, url, input, reasonObj, $element) {
        var service = new CodeFileService();
        service.updateData(moduleName, url, input, reasonObj, $element);
    },
    removeReason: function (url, input, $element, $id) {
        $(this).parentsUntil('.no-brd-btm').addClass('sedr');
        var $isActive = $id.attr('row-status');
        $(this).parentsUntil('.accordion-toggle').addClass('sedr');
        $('#cnfMessage').html($isActive != 'true' ? 'Do you want to deactivate this record?' : 'Do you want to activate this record?');
        $('#delete').html($isActive != 'true' ? 'Deactivate' : 'Activate');
        $('#confirm').modal({ keyboard: false }).one('click', '#delete', function (e) {
            var service = new CodeFileService();
            service.removeData(url, input, $element, $id);
        });
    }
};


/*Change reason*/
function ChangeReason() {
}

ChangeReason.prototype = {
    init: function () {
        this.loadChangeReason(); this.initEvents();
    },
    loadChangeReason: function () {
        var reason = new ReasonBase();
        var $element = $('#tblChangeReason');
        reason.loadReason('../api/Reason/GetAllChangeReason', $element);
    },
    initEvents: function () {
        var self = this;
        $('#tblChangeReason').on('click', '.deletrow', function () {
            var $element = $('#tblChangeReason');
            var index = $(this).attr('row-index');
            var baCode = $('#lblReasonCode' + index).html();
            var $isActive = $(this).attr('row-status');
            var $id = $(this);
            var reason = new ReasonBase();
            var Input = { ReasonCode: baCode, EmpNo: empNo, EmployeeToken: empToken, IsActive: $isActive };
            reason.removeReason('../api/Reason/RemoveChangeReason', Input, $element, $id);
        });

        $('#tblChangeReason').on('click', '.btnsave', function () {
            var $element = this;
            var index = $(this).attr('row-index');
            var $reasonCode = $('#txtReasonCode' + index).val();
            var $reasonDesc = $('#txtReasonDesc' + index).val().trim();

            var reason = new ReasonBase();
            var changeReason = {}
            changeReason["ReasonCode"] = $reasonCode.trim();
            changeReason["ReasonDesc"] = $reasonDesc.trim();

            var Input = { EmpNo: empNo, EmployeeToken: empToken, ReasonCode: $reasonCode, ReasonDesc: $reasonDesc };

            var isValid = self.validateCR(changeReason);

            if (isValid) {
                reason.updateReason(9, '../api/Reason/UpdateChangeReason', Input, changeReason, $element);
            }

        });

        $('#btnAdd').on('click', function () {
            var $reasonCode = $('#txtReasonCode').val().trim();
            var $reasonDesc = $('#txtReasonDesc').val().trim();
            var $element = $('#tblChangeReason');
            var $id = this;

            var changeReason = {}
            changeReason["ReasonCode"] = $reasonCode.trim();
            changeReason["ReasonDesc"] = $reasonDesc.trim();
            changeReason["IsActive"] = true;
            var Input = { EmpNo: empNo, EmployeeToken: empToken, ReasonCode: $reasonCode, ReasonDesc: $reasonDesc };

            var reason = new ReasonBase();

            // var isValid = self.validateCR('#ChangeReason');
            var isValid = self.validateCR(changeReason);
            if (isValid) {
                reason.addReason(1, '../api/Reason/AddChangeReason', Input, changeReason, $element, $id);
            }
        });

        $('#btnSearch').on('click', this.loadChangeReason);

        $('#txtSearch').on('keypress', function (event) {
            if (event.keyCode == 13) {
                ChangeReason.prototype.loadChangeReason(); event.preventDefault();
                return false;
            }
        });
    },
    validateCR: function (changeReason) {
        var errArray = [];

        var $reasonCode = changeReason.ReasonCode;
        var $reasonDesc = changeReason.ReasonDesc;
        var alphanumregx = /^[a-zA-Z0-9_/.&-(),\-\s]+$/;
        var numericregx = /^[0-9]*$/;

        var html = '';
        if ($reasonCode == '') {
            errArray.push('Change reason code can not be blank');
        }
        else if (!alphanumregx.test($reasonCode)) {
            errArray.push("Change reason code should be alphanumeric");
        }
         if ($reasonDesc == '') {
             errArray.push('Change reason description can not be blank');
        }
        else if (!alphanumregx.test($reasonDesc)) {
            errArray.push("Change reason description should be alphanumeric");
        }
        if (errArray.length > 0) {
            messagePopup(errArray, 'error');
            return false;
        }
        else {
            return true;
        }
    }
};


/*Inactive reason*/
function InActiveReason() {
}

InActiveReason.prototype = {
    inactiveID:0,
    init: function () { this.loadInactiveReason(); this.initEvents(); },
    loadInactiveReason: function () {
        var reason = new ReasonBase();
        var $element = $('#tblInActiveReason');
        reason.loadReason('../api/Reason/GetAllInactiveReason', $element);
    },
    initEvents: function () {
        var self = this;
        $('#tblInActiveReason').on('click', '.deletrow', function () {
            var $element = $('#tblChangeReason');
            var index = $(this).attr('row-index');
           // var baCode = $('#lblInactiveID' + index).html();
            var $isActive = $(this).attr('row-status');
            var $id = $(this);
            var reason = new ReasonBase();
            var Input = { InactiveID: index, EmpNo: empNo, EmployeeToken: empToken, IsActive: $isActive };
            reason.removeReason('../api/Reason/RemoveInactiveReason', Input, $element, $id);
        });

        $('#tblInActiveReason').on('click', '.btnsave', function () {
            var $element = this;
            var index = $(this).attr('row-index');
            //var $inactiveID = $('#txtInactiveID' + index).val();
            var $inactiveDesc = $('#txtInactiveDesc' + index).val().trim();
            var $pfReason = $('#txtPFReason' + index).val().trim();
            var reason = new ReasonBase();
            var Input = { EmpNo: empNo, EmployeeToken: empToken, InactiveID: index, InactiveDesc: $inactiveDesc, PF_Reason: $pfReason };
            var inactiveReason = {}
          //  inactiveReason["InactiveID"] = $inactiveID;
            inactiveReason["InactiveDesc"] = $inactiveDesc;
            inactiveReason["PF_Reason"] = $pfReason;

            //var isValid = self.validateIAR('#InactiveReason' + index);
            var isValid = self.validateIAR(inactiveReason);
        
            if (isValid) {
                reason.updateReason(10, '../api/Reason/UpdateInactiveReason', Input, inactiveReason, $element);
            }
        });

        $('#btnAdd').on('click', function () {
            //var $inactiveID = $('#txtInactiveID').val().trim();
            var $inactiveDesc = $('#txtInactiveDesc').val().trim();
            var $pfReason = $('#txtPFReason').val().trim();
            var $element = $('#tblInActiveReason');
            var $id = this;
            var reason = new ReasonBase();
            var Input = { EmpNo: empNo, EmployeeToken: empToken, InactiveDesc: $inactiveDesc, PF_Reason: $pfReason };
           
            var inactiveReason = {}
            inactiveReason["InactiveID"] = 'IAR' + self.inactiveID++;
            inactiveReason["InactiveDesc"] = $inactiveDesc;
            inactiveReason["PF_Reason"] = $pfReason;
            inactiveReason["IsActive"] = true;
            var isValid = self.validateIAR(inactiveReason);
            if (isValid) {
                reason.addReason(10, '../api/Reason/AddInactiveReason', Input, inactiveReason, $element, $id);
            }
            
            setTimeout(self.loadInactiveReason(), 1000);
        });

        $('#txtSearch').on('keypress', function (event) {
            if (event.keyCode == 13) {
                InActiveReason.prototype.loadInactiveReason();
                return false;
            }
        });

        $('#btnSearch').on('click', this.loadInactiveReason);
    },
    validateIAR: function (inactiveReason) {
        var errArray = [];

        var $inactiveDesc = inactiveReason.InactiveDesc;
        var $pfReason = inactiveReason.PF_Reason;
        var alpharegx = /^[a-zA-Z\-\s]+$/;
        var alphanumregx = /^[a-zA-Z0-9_/.&-(),\-\s]+$/;
        var alphanumrwsegx = /^[a-zA-Z0-9_/.&-(),]+$/;
        var numericregx = /^[0-9]{0,}$/;

        if ($inactiveDesc == '') {
            errArray.push('Inactive description can not be blank');
        }
        else if (!alphanumregx.test($inactiveDesc)) {
            errArray.push('Inactive description should be alphanumeric  and only "_, /, &, -, ., (, )" special characters are allowed.');
        }

        if ($pfReason == '') {
            errArray.push('PF reason can not be blank');
        }
        else if (!alpharegx.test($pfReason)) {
            errArray.push('PF reason should be single character');
        }

        if (errArray.length > 0) {
            messagePopup(errArray, 'error');
            return false;
        }
        else {
            return true;
        }
    }
};  


/*Hold Reason*/
function HoldReason() {
}

HoldReason.prototype = {
    HoldID:0,
    init: function () { this.loadHoldReason(); this.initEvents(); },
    loadHoldReason: function () {
        var reason = new ReasonBase();
        var $element = $('#tblHoldReason');
        reason.loadReason('../api/Reason/GetAllHoldReason', $element);
    },
    initEvents: function () {
        var self = this;
        $('#tblHoldReason').on('click', '.deletrow', function () {
            var $element = $('#tblHoldReason');
            var index = $(this).attr('row-index');
            //   var $holdID = $('#lblHoldID' + index).html();
            var $isActive = $(this).attr('row-status');
            var $id = $(this);
            var reason = new ReasonBase();
            var $isActive = $(this).attr('row-status');
            var Input = { HoldID: index, EmpNo: empNo, EmployeeToken: empToken, IsActive: $isActive };
            reason.removeReason('../api/Reason/RemoveHoldReasonMaster', Input, $element, $id);
        });
        $('#tblHoldReason').on('click', '.btnsave', function () {
            var $element = this;
            var index = $(this).attr('row-index');
           // var $holdID = $('#txtHoldID' + index).val();
            var $holdReason = $('#txtHoldReason' + index).val().trim();
            var reason = new ReasonBase();
            var Input = { EmpNo: empNo, EmployeeToken: empToken, HoldID: index, HoldReason: $holdReason };
            var holdReason = {}
           // holdReason["HoldID"] = $holdID.trim();
            holdReason["HoldReason"] = $holdReason.trim();
            var isValid = self.validateHR(holdReason);
      
            if (isValid) {
                reason.updateReason(11, '../api/Reason/UpdateHoldReason', Input, holdReason, $element);
            }
        });
        $('#btnAdd').on('click', function () {
       //     var $holdID = $('#txtHoldID').val().trim();
            var $holdReason = $('#txtHoldReason').val().trim();
            var $element = $('#tblHoldReason');
            var $id = this;
            var Input = { EmpNo: empNo, EmployeeToken: empToken, HoldReason: $holdReason };
            var reason = new ReasonBase();
            var holdReason = {}
            holdReason["HoldID"] = 'HR' + self.HoldID++;
            holdReason["HoldReason"] = $holdReason.trim();
            holdReason["IsActive"] = true;
            var isValid = self.validateHR(holdReason);

            if (isValid) {
                reason.addReason(11, '../api/Reason/AddHoldReason', Input, holdReason, $element, $id);
            }
            setTimeout(self.loadHoldReason(), 1000);
        });
        $('#txtSearch').on('keypress', function (event) {
            if (event.keyCode == 13) {
                HoldReason.prototype.loadHoldReason(); return false;
            }
        });
        $('#btnSearch').on('click', this.loadHoldReason);
    },
    validateHR: function (holdReason) {
        var errArray = [];

        var $holdID = holdReason.HoldID;
        var $holdReason = holdReason.HoldReason;
        var alphanumregx = /^[a-zA-Z0-9_/.&-(),\-\s]+$/;
        var numericregx = /^[0-9]*$/;

         if ($holdReason == '') {
             errArray.push('Hold reason can not be blank');
        }
        else if (!alphanumregx.test($holdReason)) {
            errArray.push("Hold reason should be alphanumeric");
        }

         if (errArray.length > 0) {
             messagePopup(errArray, 'error');
             return false;
         }
         else {
             return true;
         }
    }
}