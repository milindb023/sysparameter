﻿function RelationMaster() {
    this.Code = null;
    this.Name = null;
    this.service = null;
}

RelationMaster.prototype = {
    init: function () {
        this.loadRelation();
        this.initRelation();
    },
    loadRelation: function () {
        var service = new CodeFileService();
        var $template = $("#dataTemplate");
        var $searchText = $("#txtSearch").val();
        var $element = $('#tblRelation');
        var Input = { SearchText: $searchText, EmpNo: empNo, EmployeeToken: empToken };

        service.loadData('../api/Relation/GetAllRelation', Input, $element, $template);
    },
    initRelation: function () {
        $('#tblRelation').on('click', '.btnsave', function () {
            
            var $element = this;
            var index = $(this).attr('row-index');
            var $Code = $('#txtRelationCode' + index).val();
            var $Name = $('#txtRelationDesc' + index).val().trim();
            var $IsActive = $('#chkIsApplicable' + index).prop('checked');
            var service = new CodeFileService();

            var relation = {}
            relation["Code"] = $Code;
            relation["Name"] = $Name;
            relation["IsActive"] = $IsActive;

            var Input = { EmpNo: empNo, EmployeeToken: empToken, Code: $Code, Name: $Name, IsActive: $IsActive };

            var isValid = validateRelation(relation);
            if (isValid) {
                service.updateData(16, '../api/Relation/UpdateRealtion', Input, relation, $element);
            }
        });

        $('#btnAdd').on('click', function () {
            var $Code = $('#txtRelationCode').val();
            var $Name = $('#txtRelationDesc').val().trim();
            var $IsActive = $('#chkWODeduction').prop('checked');
            var service = new CodeFileService();

            var $element = $('#tblRelation');
            var id = this;

            var relation = {}
            relation["Code"] = $Code;
            relation["Name"] = $Name;
            relation["IsActive"] = $IsActive;

            var Input = { EmpNo: empNo, EmployeeToken: empToken, Name: $Name, Code: $Code, IsActive: $IsActive };

            var isValid = validateRelation(relation);
            if (isValid) {
                service.addData(16, '../api/Relation/AddRelation', Input, relation, $element, id);
            }
        });

        $('#btnSearch').on('click', this.loadRelation);

        $('#txtSearch').on('keypress', function (event) {

            if (event.keyCode == 13) {
                RelationMaster.prototype.loadRelation(); event.preventDefault();
                return false;
            }
        });
    }
};

var validateRelation = function (relation) {
    var errArray = [];
    var $Code = relation.Code;
    var $Name = relation.Name;

    var alpharegx = /^[a-zA-Z\-\s]+$/;
    var alphanumregx = /^[a-zA-Z0-9_/.&-(),\-\s]+$/;
    var alphanumrwsegx = /^[a-zA-Z0-9_/.&-(),]+$/;
    var numericregx = /^[0-9]{0,}$/;

    if ($Code == '') {
        errArray.push('Relation code can not be blank');
    }
    else if (!alphanumregx.test($Code)) {
        errArray.push('Relation code should be alphanumeric  and only "_, /, &, -, ., (, )" special characters are allowed.');
    }
    if ($Name == '') {
        errArray.push('Relation description can not be blank');
    }
    else if (!alphanumregx.test($Name)) {
        errArray.push('Relation description should be alphanumeric  and only "_, /, &, -, ., (, )" special characters are allowed.');
    }

    if (errArray.length > 0) {
        messagePopup(errArray, 'error');
        return false;
    }
    else {
        return true;
    }
}