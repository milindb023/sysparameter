﻿function Status() {
    this.StatusCode = null;
    this.StatusDesc = null;
    this.service = null;
}

Status.prototype = {
    init: function () {
        this.loadStatus();
        this.initEvents();
    },
    loadStatus: function () {
        var $searchText = $("#txtSearch").val();
        var $element = $('#tblStatus');
        var $template = $("#dataTemplate");
        var service = new CodeFileService();

        var Input = { SearchText: $searchText, EmpNo: empNo, EmployeeToken: empToken };

        service.loadData('../api/Status/GetAllStatus', Input, $element,$template);
    },
    initEvents: function () {

        $('#tblStatus').on('click', '.deletrow', function () {
            $(this).parentsUntil('.no-brd-btm').addClass('sedr');
            $(this).parentsUntil('.accordion-toggle').addClass('sedr');
            var $element = $('#tblStatus');
            var $isActive = $(this).attr('row-status');
            var index = $(this).attr('row-index');
            var $id = $(this);
            var baCode = $('#lblStatusCode' + index).html();
            $('#cnfMessage').html($isActive != 'true' ? 'Do you want to deactivate this record?' : 'Do you want to activate this record?');
            $('#delete').html($isActive != 'true' ? 'Deactivate' : 'Activate');
            $('#confirm').modal({ keyboard: false }).one('click', '#delete', function (e) {
                var service = new CodeFileService();
                var Input = { StatusCode: baCode, EmpNo: empNo, EmployeeToken: empToken, IsActive: $isActive };
                service.removeData('../api/Status/RemoveStatus', Input, $element,$id);
            });
        });

        $('#tblStatus').on('click', '.btnsave', function () {

            var $element = this;
            var index = $(this).attr('row-index');
            var $statusCode = $('#txtStatusCode' + index).val();
            var $statusDesc = $('#txtStatusDesc' + index).val().trim();
            var service = new CodeFileService();

            var status = {}
            status["StatusCode"] = $statusCode.trim();
            status["StatusDesc"] = $statusDesc.trim();

            var Input = { EmpNo: empNo, EmployeeToken: empToken, StatusCode: $statusCode, StatusDesc: $statusDesc };
            var isValid = validateStatus(status);

            if (isValid) {
                service.updateData(4, '../api/Status/UpdateStatus', Input, status, $element);
            }
        });

        $('#btnAdd').on('click', function () {
            var $statusCode = $('#txtStatusCode').val().trim();
            var $statusDesc = $('#txtStatusDesc').val().trim();
            var $element = $('#tblStatus');
            var $id = this;
            var service = new CodeFileService();

            var status = {}
            status["StatusCode"] = $statusCode.trim();
            status["StatusDesc"] = $statusDesc.trim();
            status["IsActive"] = true;
            var Input = { EmpNo: empNo, EmployeeToken: empToken, StatusCode: $statusCode, StatusDesc: $statusDesc };

            var isValid = validateStatus(status);

            if (isValid) {
                service.addData(4,'../api/Status/AddStatus', Input, status, $element, $id);
            }
        });

        $('#btnSearch').on('click', this.loadStatus);

        $('#txtSearch').on('keypress', function (event) {

            if (event.keyCode == 13) {
                Status.prototype.loadStatus(); event.preventDefault();
                return false;
            }
        });
        //$('#tblBA').on('click', '.reset', function () {
        //    var index = $(this).attr('row-index');
        //    if(index!=undefined)
        //        $('#txtStatusDesc' + index).val($('#lblStatusDesc' + index).html());
        //    else
        //        $('#txtStatusDesc').val(''); $('#txtStatusCode').val('');
        //});
    }
};

var validateStatus = function (status) {
    var errArray = [];

    var $statusCode = status.StatusCode;
    var $statusDesc = status.StatusDesc;
    var alpharegx = /^[a-zA-Z\-\s]+$/;
    var alphanumregx = /^[a-zA-Z0-9_/.&-(),\-\s]+$/;
    var alphanumrwsegx = /^[a-zA-Z0-9_/.&-(),]+$/;
    var numericregx = /^[0-9]{0,}$/;

    if ($statusCode == '') {
        errArray.push('Status code can not be blank');
    }
    else if (!alphanumregx.test($statusCode)) {
        errArray.push('Status code should be alphanumeric and only "_, /, &, -, ., (, )" special characters are allowed.');
    }

    if ($statusDesc == '') {
        errArray.push('Status Description can not be blank');
    }
    else if (!alphanumregx.test($statusDesc)) {
        errArray.push('Status Description should be alphanumeric and only "_, /, &, -, ., (, )" special characters are allowed.');
    }

    if (errArray.length > 0) {
        messagePopup(errArray, 'error');
        return false;
    }
    else {
        return true;
    }
}














