﻿function UDF() {
    this.currentUDF = null;
}

UDF.prototype = {
    init: function (udfType,udfDesc) {
        var self = this;
        $('#hfUDFValue').val(udfType);
        $('#hfUDFDesc').val(udfDesc);
        this.loadUDF();
        this.initEvents();
    },
    loadUDF: function () {
        var self = this;
        var $searchText = $("#txtSearch").val();
        var $element = $('#tblUDF');
        var $template = $("#dataTemplate");
        var $udfType = $('#hfUDFValue').val();
        var $udfDesc = $('#hfUDFDesc').val();
        var service = new CodeFileService();

        var Input = { SearchText: $searchText, EmpNo: empNo, EmployeeToken: empToken, UDFValue: $udfType };

        //$.when(
            service.loadData('../api/UDF/GetAllUDF', Input, $element, $template)
        //).done(function () {
        //    setTimeout($('body .udf-type').html($udfType.substring(0,3)+" "+$udfType.substring(3)), 1000);
        //});

            $('body .udf-type').html($udfDesc);
    },
    initEvents: function () {

        $('#tblUDF').on('click', '.deletrow', function () {
            $(this).parentsUntil('.no-brd-btm').addClass('sedr');
            $(this).parentsUntil('.accordion-toggle').addClass('sedr');
            var $element = $('#tblUDF');
            var $isActive = $(this).attr('row-status'); var $id = $(this);
            var index = $(this).attr('row-index');
            var udfCode = $('#lblUDFCode' + index).html();
            $('#cnfMessage').html($isActive != 'true' ? 'Do you want to deactivate this record?' : 'Do you want to activate this record?');
            $('#delete').html($isActive != 'true' ? 'Deactivate' : 'Activate');
            $('#confirm').modal({ keyboard: false }).one('click', '#delete', function (e) {
                var $udfType = $('#hfUDFValue').val();
                var service = new CodeFileService();

                var Input = { UDFCode: udfCode, EmpNo: empNo, EmployeeToken: empToken, UDFValue: $udfType, IsActive: $isActive };
                service.removeData('../api/UDF/RemoveUDF', Input, $element, $id);
            });
        });

        $('#tblUDF').on('click', '.btnsave', function () {
            var $element = this;
            var index = $(this).attr('row-index');
            var $udfCode = $('#txtUDFCode' + index).val();
            var $udfDesc = $('#txtUDFDesc' + index).val().trim();
            var $udfType = $('#hfUDFValue').val();
            var service = new CodeFileService();

            var udf = {}
            udf["UDFCode"] = $udfCode.trim();
            udf["UDFDesc"] = $udfDesc.trim();

            var Input = { EmpNo: empNo, EmployeeToken: empToken, UDFCode: $udfCode, UDFDesc: $udfDesc, UDFValue: $udfType };
            var isValid = validateUDF(udf);

            if (isValid) {
                service.updateData(8, '../api/UDF/UpdateUDF', Input, udf, $element);
            }
        });

        $('#btnAdd').on('click', function () {
            var $udfCode = $('#txtUDFCode').val().trim();
            var $udfDesc = $('#txtUDFDesc').val().trim();
            var $element = $('#tblUDF');
            var $udfType = $('#hfUDFValue').val();
            var $id = this;
            var service = new CodeFileService();

            var udf = {}
            udf["UDFCode"] = $udfCode.trim();
            udf["UDFDesc"] = $udfDesc.trim();
            udf["IsActive"] = true;

            var Input = { EmpNo: empNo, EmployeeToken: empToken, UDFCode: $udfCode, UDFDesc: $udfDesc, UDFValue: $udfType };

            var isValid = validateUDF(udf);

            if (isValid) {
                service.addData(8, '../api/UDF/AddUDF', Input, udf, $element, $id);
            }
        });

        $('#btnSearch').on('click', this.loadUDF);

        $('#txtSearch').on('keypress', function (event) {
            if (event.keyCode == 13) {
                UDF.prototype.loadUDF(); event.preventDefault();
                return false;
            }
        });
        //$('#tblUDF').on('click', '.reset', function () {
        //    var index = $(this).attr('row-index');
        //    if(index!=undefined)
        //        $('#txtUDFDesc' + index).val($('#lblUDFDesc' + index).html());
        //    else
        //        $('#txtUDFDesc').val(''); $('#txtUDFCode').val('');
        //});
    }
};
var validateUDF = function (status) {
    var errArray = [];
    var $udfCode = status.UDFCode;
    var $udfDesc = status.UDFDesc;

    var alpharegx = /^[a-zA-Z\-\s]+$/;
    var alphanumregx = /^[a-zA-Z0-9_/.&-(),\-\s]+$/;
    var alphanumrwsegx = /^[a-zA-Z0-9_/.&-(),]+$/;
    var numericregx = /^[0-9]{0,}$/;

    var html = '';
    if ($udfCode == '') {
        errArray.push('UDF code can not be blank');
    }
    else if (!alphanumregx.test($udfCode)) {
        errArray.push('UDF code should be alphanumeric  and only "_, /, &, -, ., (, )" special characters are allowed.');
    }

    if ($udfDesc == '') {
        errArray.push('UDF description can not be blank');
    }
    else if (!alphanumregx.test($udfDesc)) {
        errArray.push('UDF code should be alphanumeric  and only "_, /, &, -, ., (, )" special characters are allowed.');
    }

    if (errArray.length > 0) {
        messagePopup(errArray, 'error');
        return false;
    }
    else {
        return true;
    }
}