	function IsCurrency(cmbHead, fld)
	{
		if(fld.value=='')fld.value=0;
		if (fld.value != '')
		{
			val = fld.value.replace(/\s/g, "");
			regex =	/^(\d+(\.\d{1,2})?)$/; //last with two digits working
	
			if(!regex.test(val))
			{
				alert("Please Enter Valid Amount");
				fld.focus();
				return false;
			}
		}
		if (eval(fld.value) >= 0)
		{
			AddDecimaltoCurr(fld);
			CalcFinalAmount(cmbHead, fld);
			CalcFinalAmountForCafeCode(cmbHead, fld);
			CalcTotals();
			CheckLimits(cmbHead, fld)
			CalcTotals();
		}
		return true;
	}
	
		
	function IsCurrency1(cmbHead, fld)
	{
		for(i=0;i<id-1;i++)
		{
			HeadVal = eval('document.thisform.drpHead' + eval(i+1) + '.value');
			var arr = HeadVal.split("|");
		}
		if (fld.value != '')
		{
			CalcFinalAmount(cmbHead, fld);
			CalcFinalAmountForCafeCode(cmbHead, fld);
			CalcTotals();
			CheckLimits(cmbHead, fld)
			CalcTotals();
			return true;
		}
	}
	
	function IsCurrency2(cmbHead, fld) //For existing Claims
	{
		AddDecimaltoCurr(fld);
		CalcTotals();
		return true;
	}
	
	//Function to caluculate totals
	function CalcTotals()
	{
		var TotalClaimedTillDate = 0.00;
		var TotalPendingClaims = 0.00;
		var TotalCurrentClaims = 0.00;
		
		for(i=0;i<iID-1;i++)
		{
			AmtClaimedTillDateVal = eval('document.thisform.txtClaimtillDate' + ArrEdCodes[i] +'.value');
			AmtPendingClaimsVal = eval('document.thisform.txtPendingClaimDetails' + ArrEdCodes[i] +'.value');
			AmtCurrentClaimsVal = eval('document.thisform.txtCurrentClaim' + ArrEdCodes[i] +'.value');
			
			if(AmtClaimedTillDateVal=="")AmtClaimedTillDateVal=0.00;
			if(AmtPendingClaimsVal=="")AmtPendingClaimsVal=0.00;
			if(AmtCurrentClaimsVal=="")AmtCurrentClaimsVal=0.00;
		
			if(AmtClaimedTillDateVal != 0)
			{
				TotalClaimedTillDate = TotalClaimedTillDate + eval(AmtClaimedTillDateVal);
			}
			if(AmtPendingClaimsVal != 0)
			{
				TotalPendingClaims = TotalPendingClaims + eval(AmtPendingClaimsVal);
			}
			if(AmtCurrentClaimsVal != 0)
			{
				TotalCurrentClaims = TotalCurrentClaims + eval(AmtCurrentClaimsVal);
			}
		}
		
		document.getElementById('txtTotClaimedTillDate').value = TotalClaimedTillDate;
		AddDecimaltoCurr(document.getElementById('txtTotClaimedTillDate'));
		
		document.getElementById('txtTotPendingClaims').value = TotalPendingClaims;
		AddDecimaltoCurr(document.getElementById('txtTotPendingClaims'));
		
		document.getElementById('txtTotCurrentClaims').value = TotalCurrentClaims;
		AddDecimaltoCurr(document.getElementById('txtTotCurrentClaims'));
		
		document.getElementById('txtTotAmount').value = TotalCurrentClaims;
		AddDecimaltoCurr(document.getElementById('txtTotAmount'));
	
		//Calculate totals
		for(i=0;i<iID-1;i++)
		{
			var RowTotal=0;
			if(document.getElementById('txtClaimtillDate' + ArrEdCodes[i]).value=="")document.getElementById('txtClaimtillDate' + ArrEdCodes[i]).value=0;
			if(document.getElementById('txtPendingClaimDetails' + ArrEdCodes[i]).value=="")document.getElementById('txtPendingClaimDetails' + ArrEdCodes[i]).value=0;
			if(document.getElementById('txtCurrentClaim' + ArrEdCodes[i]).value=="")document.getElementById('txtCurrentClaim' + ArrEdCodes[i]).value=0.00;
			
			RowTotal = (eval(document.getElementById('txtClaimtillDate' + ArrEdCodes[i]).value) +  eval(document.getElementById('txtPendingClaimDetails' + ArrEdCodes[i]).value) + eval(document.getElementById('txtCurrentClaim' + ArrEdCodes[i]).value));
			document.getElementById('txtTotal'+ ArrEdCodes[i]).value = RowTotal;
			AddDecimaltoCurr(document.getElementById('txtTotal'+ ArrEdCodes[i]));
			/*if(eval(document.getElementById('txtTotal'+ ArrEdCodes[i]).value) > eval(document.getElementById('txtLimit'+ ArrEdCodes[i]).value))
			{
				alert("The Limit is exceeded for the current reimbursement group");
			}
			*/
		}
		/*var tTotal=0;
		for(i=0;i<iID-1;i++)
		{
			if(document.getElementById('txtTotal' + ArrEdCodes[i]).value=="") document.getElementById('txtTotal' + ArrEdCodes[i]).value=0.00;
			tTotal = tTotal + eval(document.getElementById('txtTotal' + ArrEdCodes[i]).value);
		}
		*/

		TotalAmount=0;
		//Calculate totals for eligibility and accrual
		var dEligibility=0;
		var dAccrual=0;
			
		for(i=0;i<=ArrCafeCodes.length-1;i++)
		{
			dEligibility = dEligibility + eval(document.getElementById('txtGRPEligibility' + ArrCafeCodes[i]).value);
			dAccrual = dAccrual + eval(document.getElementById('txtGRPAccrual' + ArrCafeCodes[i]).value);
		}

		document.getElementById('txtTotEligibility').value = dEligibility;
		AddDecimaltoCurr(document.getElementById('txtTotEligibility'));
		
		document.getElementById('txtTotAccrual').value = dAccrual;
		AddDecimaltoCurr(document.getElementById('txtTotAccrual'));
		
		tTotal = eval(document.getElementById('txtTotEligibility').value) - (eval(document.getElementById('txtTotClaimedTillDate').value) + eval(document.getElementById('txtTotPendingClaims').value) + eval(document.getElementById('txtTotCurrentClaims').value));
		document.getElementById('txtTotTotal').value = tTotal;
		AddDecimaltoCurr(document.getElementById('txtTotTotal'));
	}
	
	//Function to Set Remaining Amount
	function CalcFinalAmount(head,fld)
	{
		var Total = new Array();
		var Temp = new Array();
		var BalanceAmount;
		var ClaimTotal;
		var i,j,n;
		
		for(i=0;i<=ArrEdCodes.length-1;i++)
		{
			//alert("EDCode="+ ArrEdCodes[i] + "parseintValue = " + parseFloat('001') + "eval= "+ parseFloat('016') + "parsefloat="+parseFloat('009'));
			Total[parseFloat(ArrEdCodes[i])] = 0;
		}
		
		for(i=0;i<id-1;i++)
		{
			HeadVal = eval('document.thisform.drpHead' + eval(i+1) + '.value');
			
			var arr=HeadVal.split("|");
			HeadVal=arr[1];			
			AmountVal = eval('document.thisform.txtAmount' + eval(i+1) +'.value');
			if(AmountVal=="")AmountVal=0;
			if (HeadVal != "00")
			{
				//Total[eval(HeadVal)] = Total[eval(HeadVal)] + eval(AmountVal);
				Total[parseFloat(HeadVal)] = Total[parseFloat(HeadVal)] + eval(AmountVal);
				
			}
		}
		
		for(i=0;i<iID-1;i++)
		{
			var val = parseFloat(ArrEdCodes[i]);
			document.getElementById('txtCurrentClaim' + ArrEdCodes[i]).value = eval(Total[val]);
		}
	}
	
	function CalcFinalAmountForCafeCode(head, fld)
	{
		var ClaimTotal;
		var i,j,n;
		var TotalCafeCode = new Array();
		var GrBalanceAmout;
		for(i=0;i<=ArrCafeCodes.length-1;i++)
		{
			TotalCafeCode[eval(ArrCafeCodes[i])] = 0;
		}
		
		for(i=0;i<id-1;i++)
		{
			HeadVal = eval('document.thisform.drpHead' + eval(i+1) + '.value');
			var arr=HeadVal.split("|");		
			HeadVal=arr[0];
			AmountVal = eval('document.thisform.txtAmount' + eval(i+1) +'.value');
			if(AmountVal=="")AmountVal=0;
			if (HeadVal != "0")
			{
				TotalCafeCode[eval(HeadVal)] = TotalCafeCode[eval(HeadVal)] + eval(AmountVal);
			}
		}
		
		for(i=0;i<iCafeCode-1;i++)
		{
			var val = eval(ArrCafeCodes[i]);
			var CafeTotal;
			
			var sValue = eval('document.thisform.txtExceedAccrual' + val + '.value');
			var arrExceedAccrual = sValue.split("|"); //0-bExceedAccrual, 1-Eligible, 2-Accrual, 3-Paid, 4-Pending
			
			document.getElementById('txtGRPCurrentClaim' + ArrCafeCodes[i]).value = eval(TotalCafeCode[val]);
			document.getElementById('txtGRPTotal' + ArrCafeCodes[i]).value = eval(arrExceedAccrual[1])- (eval(arrExceedAccrual[3]) + eval(arrExceedAccrual[4]) + eval(document.getElementById('txtGRPCurrentClaim' + ArrCafeCodes[i]).value));
			AddDecimaltoCurr(document.getElementById('txtGRPTotal' + ArrCafeCodes[i]));
		}
	}
	
	function CheckLimits(head, fld)
	{
		var arrValue = head.value.split("|");
		val = arrValue[0];
		var CafeTotal;
		if(val!=0)
		{
			//Check Annual Limit for EDCode 
			if(eval(document.getElementById('txtAnnualLimit' + arrValue[1]).value)!=0)
			{
				if(eval(document.getElementById('txtCurrentClaim' + arrValue[1]).value) > eval(document.getElementById('txtAnnualLimit' + arrValue[1]).value))
				{
					alert("The maximum limit for this component is exceeded");
					fld.value=0;
					CalcFinalAmount(head, fld);
					CalcFinalAmountForCafeCode(head, fld);
					return;
				}
			}
			
			var sValue = eval('document.thisform.txtExceedAccrual' + val + '.value');
			var arrExceedAccrual = sValue.split("|"); //0-bExceedAccrual, 1-Eligible, 2-Accrual, 3-Paid, 4-Pending
			var cTotalClaimed = eval(arrExceedAccrual[1]) - eval(document.getElementById('txtGRPTotal' + val).value);
			
			//Check ExceedAccrual
			if(eval(arrExceedAccrual[0])==0)
			{
				if( cTotalClaimed > eval(arrExceedAccrual[2]))
				{
					diff = cTotalClaimed - eval(arrExceedAccrual[2]);
					alert("Not allowed. Your total claims for the current group is exceeding accrual by " + eval(diff).toFixed(2));
					document.getElementById('txtGRPCurrentClaim' + val).value = 0.00;
					fld.value=0;
					CalcFinalAmount(head, fld);
					CalcFinalAmountForCafeCode(head, fld);
				}
			}
			
			//Check Exceed Eligibility
			if(cTotalClaimed >  eval(arrExceedAccrual[1]))
			{
				diff = cTotalClaimed - eval(arrExceedAccrual[1]);
				alert("Not allowed. Your total claims for the current group is exceeding eligibility by " + eval(diff).toFixed(1));
				document.getElementById('txtGRPCurrentClaim' + val).value = 0.00;
				fld.value=0;
				CalcFinalAmount(head, fld);
				CalcFinalAmountForCafeCode(head, fld);
			}
		}
	}
	
	// This will return value with two decimal places with rounded
	function AddDecimaltoCurr(fld)
	{
		var X =  eval(fld.value);
		fld.value = X.toFixed(2);
	}