﻿

/////////////////////////////////////////////// Using below code hide/show main message notification box///////////////////////////
var isUpdate = true;
$(document).ready(function () {

    $(".slidingDiv").hide();
    $(".btnnotification").show();

    //On Message button(top right Message button) click hide/show message notification box using this click event.
    $('.btnnotification').click(function () {
        if ($('#divarrow').css('display') == 'none') {
            $('#divarrow').css('display', 'block');
            $(".slidingDiv").css('display', 'block');
            isSeen = true;
        } else {
            $('#divarrow').css('display', 'none');
            $(".slidingDiv").css('display', 'none');
        }
    });
    UpdateNewMessages(); //At the time of page load display all new messages using this method call.
    UpdateOldMessages(); //At the time of page load display all old messages using this method call.

});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////Using this method read message id is updated in database ///////////////////////////
function UpdateMessage(MessageId) {
    $.ajax({
        type: "POST",
        url: "HomeNew.aspx/UpdateMessageId",
        data: "{ MessageId: '" + MessageId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (msg != null && msg != '') {

                var counts = msg.d.split('-');

                SetCurrentMailCount(counts[0], counts[1]);
            } else {
            }

        },
        error: function (msg) {

        }
    });
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////Using this method get latest old messages from database//////////////////////////////////////////
function UpdateOldMessages() {


    $.ajax({
        type: "POST",
        url: "HomeNew.aspx/GetOldMails",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (msg != '' && msg != null && msg.d != 'NO') {
                $('#divold').html(msg.d);

            } else {
            }

        },
        error: function (msg) {

            //  alert("UpdateOldMessages:An error has been occurred during processing your request");
        }
    });
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////// Using this method get latest emergency messages from database ///////////////////////////////////
function UpdateEmergencyMessages() {
    loademergencymsgshow();
    var left = 50;
    var top = 50;
    $.ajax({
        type: "POST",
        url: "HomeNew.aspx/GetEmergencyMails",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (msg != '' && msg != null && msg.d != 'NO') {
                debugger
                $('#divemergency').html(msg.d);
                $('#divemergency').load();
                loademergencymsghide();
                if (isUpdate) { window.EmergencyMsg.Show(); isUpdate = false; }
            } else {
            }

        },
        error: function (msg) {

            //alert("UpdateEmergencyMessages:An error has been occurred during processing your request");
        }
    });

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////Using this method get latest new messages from database //////////////////////////////////////////
function UpdateNewMessages() {

    $.ajax({
        type: "POST",
        url: "HomeNew.aspx/GetNewMails",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (msg != '' && msg != null && msg.d != 'NO') {
                $('#divnew').html(msg.d);

            } else {
            }

        },
        error: function (msg) {

            // alert("UpdateNewMessages:An error has been occurred during processing your request");
        }
    });

}
var varMessageText = '';
function GetMessageText(MessageId) {
    loadshow();
    $.ajax({
        type: "POST",
        url: "HomeNew.aspx/GetMessageTextFromId",
        data: "{ MessageId: '" + MessageId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (msg != '' && msg != null) {
                $('#divcontent').html(msg.d);
                loadhide();
            } else {
                varMessageText = msg.d;
            }
        },
        error: function (msg) {
            // alert("UpdateNewMessages:An error has been occurred during processing your request");
        }
    });

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////// Using this method to display current opened message in popup ////////////////////////
function OpenMsgNew(objThis, objMessageId) {
    UpdateMessage(objMessageId);
    GetMessageText(objMessageId);
    window.pcLogin.Show();
    UpdateNewMessages(); //At the time of page load display all new messages using this method call.
    UpdateOldMessages(); //At the time of page load display all old messages using this method call.
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function loadshow() {
    $('#weboverlay').css('visibility', 'visible');
}

function loadhide() {
    $('#weboverlay').css('visibility', 'hidden');

}
function loademergencymsgshow() {
    $('#divloademergency').css('visibility', 'visible');
}

function loademergencymsghide() {
    debugger
    $('#divloademergency').css('visibility', 'hidden');

}
////////////////////////////// Using this method open already read messages from Old section //////////////////////////////////////
function OpenMsgOld(objThis, objMessageId) {
    GetMessageText(objMessageId);
    window.pcLogin.Show();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////// Close mail notification popup ////////////////////////////////////////////////////////////////////////
function ClosePopupMsg() {

    $('#msgnotification').hide("slow");

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////// Close mail notification popup after 8 sec //////////////////////////////////////////////////
function NotificationClose() {
    setTimeout(function () { ClosePopupMsg(); }, 8000);

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////// Check emergency messages is available or not and then decide display or not using this method ///
function CheckEmergencyMessage() {
    UpdateEmergencyMessages(); //using this method get latest emergency messages from database.
    // show emergency messages popup.
    UpdateNewMessages(); //At the time of page load display all new messages using this method call.
    UpdateOldMessages();

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////// Using this method you can open main message notification box from 
//notification popup displayed on right top corner for 8 sec. //////////////////////////////////////////////////////////////////////
function showslidediv() {
    $('#divarrow').css('display', 'block');
    $(".slidingDiv").css('display', 'block');
    ClosePopupMsg();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////// Using this method you can open main message notification box from 
//main message notification box's close button ( X ). ////////////////////////////////////////////////////////////////////////////
function hideslidediv() {
    $('#divarrow').css('display', 'none');
    $(".slidingDiv").css('display', 'none');
    ClosePopupMsg();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////// Using this method you can set current message status count to the lables and tab ////////
function SetCurrentMailCount(totalCnt, oldCnt) {

    var tab0 = window.tabmessages.GetTab(0);
    tab0.SetText('New (' + totalCnt + ')');
    var tab1 = window.tabmessages.GetTab(1);
    tab1.SetText('Old (' + oldCnt + ')');
    window.aspxhmessages.SetText('Messages ' + '(' + totalCnt + ')');
    //    if (totalCnt == 0) {
    //        $("#tdnotification").css('display', 'none')
    //    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////