﻿var empNo = '';
var empToken = '';
var templateType = 'All';
var emailTemplateConstants = '';
var localTemplate = '';
var filterlist = [];

var editor = new Jodit('#jodit', {
    removeButtons: [
        "about"
    ],
    placeholder: '',
    popap: false
});

var GetFilteredTemplate = function () {
    $('#Templates').empty();
    var html = '';

    if (localTemplate != null && localTemplate != "") {
        var templates = localTemplate;

        templates = $.grep(templates, function (element, index) {
            return element.groupName == templateType;
        });

        $(templates).each(function (index, template) {
            html += AddTemplateGroupHeader(template);
        });
    }
    else {
        html += '<div class="row"><div class="col-md-12 col-sm-12 col-lg-12" style="padding:10px;text-align:center;">Unable to retrive data or currently data is not avilable</div></div>'
    }
    $('#Templates').append(html);
}

//--------------Load Templates-----------------//
var GetAllTemplates = function () {
    var Input = { EmpNo: empNo, EmployeeToken: empToken, TemplateType: templateType };
    $("#divloading").show();
    $.ajax({
        type: "POST",
        url: "../api/EmailTemplate/GetAllTemplates",
        data: Input,
        success: LoadAddTemplate,
        failure: function (response) {
            alert('Failure : ' + response);
        },
        error: function (response) {
            alert('Error : ' + response);
        }
    });
}

function LoadAddTemplate(response) {
    $('#Templates').empty();
    var html = '';
    filterlist = [];
    if (response != null && response != "") {
        var templates = response;
        localTemplate = response;
        $(templates).each(function (index, template) {
            filterlist.push(template.groupName);
            html += AddTemplateGroupHeader(template);
        });
        BindFilterType(filterlist);
        $("#divloading").hide();
    }
    else {
        html += '<div class="row"><div class="col-md-12 col-sm-12 col-lg-12" style="padding:10px;text-align:center;">Unable to retrive data or currently data is not avilable</div></div>'
    }
    $('#Templates').append(html);
}

function BindFilterType(response) {
    var html = '';
    $("#ddlTemplateType").empty();
    if (response != null && response != "") {
        var filters = response;
        html += '<option value="All">All</option>'
        $(filters).each(function (index, filter) {
            html += '<option value="' + filter + '">' + filter + '</option>';
        });
        $("#ddlTemplateType").append(html);
    }
}

//---------------Get Email Template By TemplateConstant------------------//
var GetEmailTemplate = function (emailTemplateConstants) {

    var Input = { EmpNo: empNo, EmployeeToken: empToken, EmailTemplateConstants: emailTemplateConstants };
    var heading = 'Email Template';

    editor.setMode('WYSIWYG');
    
    $("#spanEmailTemplate").text(heading);
    $("#hfTemplate").val(emailTemplateConstants);

    $.ajax({
        type: "POST",
        url: "../api/EmailTemplate/GetEmailTemplate",
        data: Input,
        success: function (response) {
            if (response != null) {
                $("#diveditor").show();
                var html = response
                editor.val(html.EmailBody);
                $("#txtSubject").val(html.EmailSubject);
                $("#spanEmailTemplate").text(html.EmailTemplateKey)
            GetEmailTemplateToken(emailTemplateConstants);
            }
            else {
                var message = "Some thing went wrong!.. Please try again later.";
                ShowErrorPopup("0", message);
            }
        },
        failure: function (response) {
            alert('Failure : ' + response);
        },
        error: function (response) {
            alert('Error : ' + response);
        }
    });
}

//---------------Get Email Token List w.r.t.  TemplateConstant-------------------//
var GetEmailTemplateToken = function (emailTemplateConstants) {
    var Input = { EmpNo: empNo, EmployeeToken: empToken, EmailTemplateConstants: emailTemplateConstants };

    $.ajax({
        type: "POST",
        url: "../api/EmailTemplate/GetEmailTemplateToken",
        data: Input,
        success: LoadEmailTemplateToken,
        failure: function (response) {
            alert('Failure : ' + response);
        },
        error: function (response) {
            alert('Error : ' + response);
        }
    });
}

var LoadEmailTemplateToken = function (response) {
    var html = '';
    $("#ddltokens").empty();
    if (response != null && response != "") {
        var filters = response;
        html += '<option value="0">Select email token</option>'
        $(filters).each(function (index, filter) {

            html += '<option value="' + filter.tokenValue + '">' + filter.tokenText + '</option>';
        });
        $("#ddltokens").append(html);
    }
    else {
        html += '<option value="0">No token available</option>';
        $("#ddltokens").append(html);
    }
}

var GetDefaultTemplate = function () {
    var isValid = confirm("Do you want to restore default template?");

    if (editor.getMode() == 2) {
        editor.setMode('WYSIWYG');
    }

    if (!isValid)
        return false;

    var emailTemplateConstants = $("#hfTemplate").val();

    var Input = { EmpNo: empNo, EmployeeToken: empToken, EmailTemplateConstants: emailTemplateConstants };

    $("#hfTemplate").val(emailTemplateConstants);
    $.ajax({
        type: "POST",
        url: "../api/EmailTemplate/GetDefaultTemplate",
        data: Input,
        success: function (response) {
            if (response != null) {
                
                $("#diveditor").show();
                var html = response
                editor.val(html.EmailBody);
                $("#txtSubject").val(html.EmailSubject);
            }
            else {
                
                var message = "Some thing went wrong!.. Please try again later.";
                ShowErrorPopup("0", message);
            }
        },
        failure: function (response) {
            alert('Failure : ' + response);
        },
        error: function (response) {
            alert('Error : ' + response);
        }
    });
}

var UpdateEmailTemplate = function () {
    var isValid = false;
    var emailBody = editor.getEditorValue();
    var emailTemplateConstants = $("#hfTemplate").val();
    var emailSubject = $("#txtSubject").val();
    var Input = { EmpNo: empNo, EmployeeToken: empToken, EmailTemplateConstants: emailTemplateConstants, EmailBody: emailBody, EmailSubject: emailSubject };

    isValid = ValidateEmailEditor();

    if (!isValid)
        return false;

    $.ajax({
        type: "POST",
        url: "../api/EmailTemplate/UpdateEmailTemplate",
        data: Input,
        success: function (response) {
            if (response == "true") {
                $("#diveditor").hide();
                editor.val('');
                var message = "Email Template Updated Successfully";
                ShowErrorPopup("2", message);
            }
            else if (response == "invalid") {
                alert('Error in saving. Please validate HTML code.');
            }
            else {
                var message = "Some thing went wrong!.. Please try again later.";
                ShowErrorPopup("0", message);
            }
        },
        failure: function (response) {
            alert('Failure : ' + response);
        },
        error: function (response) {
            alert('Error : ' + response);
        }
    });
}

function ValidateEmailEditor() {
    if ($("#txtSubject").val() == "") {
        alert("Email subject cannot be empty.")
        return false;
    } else if (editor.getEditorValue() == "") {
        alert("Email body cannot be empty.")
        return false;
    }
    else {
        return true;
    }
}

//for email tocken placement in email editor
function pasteHtmlAtCaret(html) {
   
    var sel, range;
    if (window.getSelection) {
        // IE9 and non-IE
        sel = window.getSelection();
     
        if (sel.getRangeAt && sel.rangeCount) {

            var exception = window.getSelection().anchorNode.data;

            var classnameexception = window.getSelection().anchorNode.parentNode.className;

            if (exception == 'Email Template' || exception == 'Subject' || exception == 'Body' || exception == 'Restore Default Template' || exception == 'Template' || exception == 'Email Template Editor' || classnameexception == "modal-body")
                return;

            range = sel.getRangeAt(0);
            range.deleteContents();

            // Range.createContextualFragment() would be useful here but is
            // non-standard and not supported in all browsers (IE9, for one)
            var el = document.createElement("div");
            el.innerHTML = html;
            var frag = document.createDocumentFragment(), node, lastNode;

            while ((node = el.firstChild)) {
                lastNode = frag.appendChild(node);
            }
            range.insertNode(frag);

            // Preserve the selection
            if (lastNode) {
                range = range.cloneRange();
                range.setStartAfter(lastNode);
                range.collapse(true);
                sel.removeAllRanges();
                sel.addRange(range);
            }
         
        }
        else {
            var ta = $("#txtSubject").get(0);
            var start = ta.selectionStart

            var f = $("#txtSubject").val().substring(0, start)
            var e = $("#txtSubject").val().substring(start)

            $("#txtSubject").val(f + " " + html + " " + e);
        }
    } else if (document.selection && document.selection.type != "Control") {
        // IE < 9
        document.selection.createRange().pasteHTML(html);
    }
}
//for popup alert
function ShowErrorPopup(etype, msg) {

    switch (etype) {
        case "0":
            $("#spanPopUpType").text("Error");
            $("#spanPopUpType").css("color", "red");
            break;
        case "1":
            $("#spanPopUpType").text("Warning");
            $("#spanPopUpType").css("color", "orange");
            break;
        case "2":
            $("#spanPopUpType").text("Success");
            $("#spanPopUpType").css("color", "green");
            break;
    }
    $("#txtMessage").html(msg);
    $("#errModel").show();
}

function AddTemplateGroupHeader(template) {
    var html = ''
    html = '<div class="row">' +
             '<div class="col-md-12 col-sm-12 col-lg-12">' +
                 '<div class="content-heading">' + template.groupName + ' (' + template.TempCollection.length + ')</div>' +
                 '</div> </div>' +
               ' <div class="row pd-m">'
    + AddTemplateList(template) +
    '</div>';

    return html;
}

function AddTemplateList(template) {
    var html = '';
    $(template.TempCollection).each(function (index) {
        html += '<div class="col-md-4 col-sm-4 col-lg-4">' +
            '<div class="content-box-email clearfix">' +
                '<div class="pull-left icon-width">&nbsp;</div>' +
                '<div class="pull-left content-width"><a class="reportdetail" href="javascript:void(0);"  onclick="GetEmailTemplate(\'' + this.TemplateConstant + '\')">' + this.TemplateName + '</a></div>' +
            '</div>' +
        '</div>';
    });
    return html;
}
