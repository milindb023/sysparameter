﻿
var currentTargetID;
var x;
var i;
var rows;
var groupcodeid = [];
var groupcodename1 = [];
var colorspanid = [];

//var colorspan = [];
//var ServerCodeID = [];
//var ServerCodeName = [];


$(document).ready(function () {
    $('.header1').scrollTop($('.header1').height());
    $("#hfRemoveStatus").val("0");
 //   groupcodename = [];
    var IDs = "";
    IDs = $("#hfID").val();
    groupcodename1 = IDs.split(',');
   

  
    //$('.slides').tooltipster({
    //    html: true,
    //    'trigger': 'click',
    //    'position': 'right',
    //    'multiple': true,


    //});
   
    var max_fields = 8; //maximum input boxes allowed
    var wrapper = $(".slides"); //Fields wrapper
    var add_button = $("#btnAddValue"); //Add button ID

    x = 1; //initlal text box count
    i = $("#hfServerValue").val();
    if (i == 0) {
        i++;
    }

   
    $('.color').tooltipster({
        html: true,
        'trigger': 'click',
        'position': 'right',
        'multiple': true,

        content: function () {
            return $('#tip2').html();
        },
        'functionReady': function () {
            $('.ColorPicker').on('click', function (e) {
                debugger
                var color = $(this).css("background-color");
                $("#" + currentTargetID).css("background-color", color);
                $('.ColorPicker').removeClass('ColorPicker DivBackImage').addClass('ColorPicker');
            });


        }

    });


    $(add_button).click(function (e) { //on add input button click 
        // groupcodename = $("#hfID").val();
        rows = "";
        e.preventDefault();
        if (x < max_fields) { //max input box allowed

            var incrt = groupcodename1.length;
            $(wrapper).append('<li class="slide slide1 DivClass ui-sortable-handle"><span class="SpanClass"><img src="Images/move.jpg" /></span>\
                                        <span id="ColorSpan' + i + '" class="color btnBoot pop-right tooltipstered" runat="server" data-toggle="popover"> </span> \
                                        <input id="txtGroupCode' + i + '" type="text" name="myGroupCode" class="inputItemCode" runat="server" maxlength="2"/>\
                                        <input id="txtGroupName' + i + '" type="text" name="mytext" class="inputItem" runat="server" maxlength="35"/>\
                                        <a href="#" class="remove_field" style="float: right; margin-top: 15px;padding-right:8px;"><img src="Images/close-group.jpg" /></a></li>'); //add input box


            var letters = ['rgb(245, 101, 69)', 'rgb(164, 0, 0)', 'rgb(124, 176, 216)', 'rgb(138, 226, 52)', 'rgb(233, 185, 110)', 'rgb(252, 175, 62)', 'rgb(241, 241, 78)', 'rgb(211, 215, 207)',
                           'rgb(204, 0, 0)', 'rgb(117, 80, 123)', 'rgb(52, 101, 164)', 'rgb(115, 210, 22)', 'rgb(193, 125, 17)', 'rgb(245, 121, 0)', 'rgb(237, 212, 0)', 'rgb(186, 189, 182)',
                          'rgb(0, 128, 0)', 'rgb(92, 53, 102)', 'rgb(32, 74, 135)', 'rgb(78, 154, 6)', 'rgb(237, 212, 0)', 'rgb(143, 89, 2)', 'rgb(206, 92, 0)', 'rgb(196, 160, 0)', 'rgb(255,255,255)']; //Set your colors here
            x++; //text box increment
            if (letters[groupcodename1.length] == "rgb(255,255,255)")
                $("#ColorSpan" + i).css('background-color', 'none');
            else {
                // document.getElementsByClassName('color')[i].style.display = 'block';
                $("#ColorSpan" + i).css('background-color', letters[i]);
              //  document.getElementsByClassName('color')[i].style.backgroundColor = letters[i];
            }
            
            $('#ColorSpan' + i).tooltipster();

            $('#ColorSpan' + i).tooltipster({
                html: true,
                'trigger': 'click',
                'position': 'right',
                'multiple': true,
                content: function () {
                    return $('#tip2').html();
                },
                'functionReady': function () {
                    $('.ColorPicker').on('click', function (e) {
                        debugger
                        var color = $(this).css("background-color");
                        $("#" + currentTargetID).css("background-color", color);
                        $('.ColorPicker').removeClass('ColorPicker DivBackImage').addClass('ColorPicker');
                    });
                }
            });
            
            rows = i.toString();
            i++;
            
            colorspanid = [];
            groupcodeid.push(rows);
            colorspanid = groupcodename1.concat(groupcodeid); 
            $('.header1').scrollTop($('.header1').height());
        }
        $("#hfTotalRows").val(i);

    });

    

    $(wrapper).on("click", ".color", function (e) {

        $('#ColorSpan' + i).tooltipster('show');


        currentTargetID = e.currentTarget.id;
        var aSelectedColorArray = [];
        var oColor = $('.color');

        for (var w = 0; w < oColor.length; w++) {
            var oBackgroundColor = oColor[w].style.backgroundColor;
            if (oBackgroundColor == "") {
                oBackgroundColor = "rgb(0, 128, 0)"
            }
            aSelectedColorArray.push(oBackgroundColor);
        }

        var colorPickerDiv = $('.ColorPicker');
        var p = 0;
        for (var t = 0; t < aSelectedColorArray.length; t++) {
            for (var q = 0; q < colorPickerDiv.length; q++) {
                if (colorPickerDiv[q].style.backgroundColor == aSelectedColorArray[t]) {
                    var rr = colorPickerDiv[q].id;
                    $('#' + rr).removeClass("ColorPicker").addClass("ColorPicker DivBackImage");
                    p++;
                }
            }
        }
    })

    $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
        
        if ($("#hfRemoveStatus").val() == "0") {// || ) {
            e.preventDefault();
            $(this).parent('li').remove();
            x--;
            //  i--;
            var getCodeID = e.currentTarget.parentNode.children[1].id;
            var idnoGroup = getCodeID.substr(getCodeID.length - 1);
            var idname = idnoGroup;
            colorspanid = jQuery.grep(colorspanid, function (value) {
                return value != idname;
            });

            groupcodeid = jQuery.grep(groupcodeid, function (value) {
                return value != idname;
            });

        }
        if ($("#hfRemoveStatus").val() == "Deleted") {
            e.preventDefault();
            $(this).parent('li').remove();
            var idname1 = $("#hfidno").val();
            x--;
            //  groupcodename = $("#hfID").val().slice(0, -2);
            //--;
            //  $("#hfID").val().slice(0, -2);

            groupcodename1 = jQuery.grep(groupcodename1, function (value1) {
                return value1 != idname1;
            });

        }
        $("#hfTotalRows").val(i);
    })


});

function DeletePopUpRow(objthis, objid, idname) {
    

    if (confirm("Do you want to delete this shift group?")) {

        $.ajax({
            type: "POST",
            url: "ESS0951.aspx/DeleteShiftGroup",
            data: "{ shiftgroupId: '" + objid + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (response) {
                
                if (response.d == "Deleted") {
                    alert("Record deleted successfully.");
                    $("#hfRemoveStatus").val("Deleted");
                    $("#hfidno").val(idname);

                }
                else {
                    alert("This shift group is used for " + objid);
                    $("#hfRemoveStatus").val("1");
                }
            }
        });
    }
    else
        $("#hfRemoveStatus").val("NotDeleted");

    return false;
}





$(function () {

    //----- OPEN
    $('[data-popup-open]').on('click', function (e) {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

        e.preventDefault();
    });

    //----- CLOSE
    $('[data-popup-close]').on('click', function (e) {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

        e.preventDefault();
        document.location = 'ESS0951.aspx';
    });
});
