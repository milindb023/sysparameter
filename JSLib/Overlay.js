﻿
var divNameOverlay = "overlay";
var updateProgressID = "messageweboverlay"

var prm = Sys.WebForms.PageRequestManager.getInstance();
prm.add_initializeRequest(InitializeRequest);
prm.add_endRequest(EndRequest);
var postBackElement;

function InitializeRequest(sender, args) {
    if (prm.get_isInAsyncPostBack())
        args.set_cancel(true);
    postBackElement = args.get_postBackElement();
    
    if (postBackElement.className.indexOf('preventdoubleclick') != -1) {
        ShowOverlay();
    }
}

function EndRequest(sender, args) {
    HideOverlay();
}

function ShowOverlay() {
    var obj = document.getElementById(divNameOverlay);
    if (obj == null) { CreateOverlayDiv(); }
    if (obj != null) { obj.style.display = "block"; }

    var objMessageDiv = document.getElementById(updateProgressID);
    if (objMessageDiv == null) { CreateMessageDiv(); }
    if (objMessageDiv != null) { objMessageDiv.style.display = "block"; }
}

function HideOverlay() {
    var obj = document.getElementById(divNameOverlay);
    if (obj != null) {
        obj.style.display = "none";
    }

    var obj = document.getElementById(updateProgressID);
    if (obj != null) {
        obj.style.display = "none";
    }
}

function CreateOverlayDiv() {
    var maskObj = document.createElement("div");
    maskObj.setAttribute('id', divNameOverlay);
    maskObj.style.position = "absolute";
    maskObj.style.top = "0";
    maskObj.style.right = "0";
    maskObj.style.bottom = "0";
    maskObj.style.left = "0";
    maskObj.style.margin = "0";
    maskObj.style.padding = "0";
    maskObj.style.background = "#000000";
    maskObj.style.filter = "Alpha(opacity=15);";
    maskObj.style.opacity = "0.15";
    maskObj.style.width = "100%";
    maskObj.style.height = "100%";
    maskObj.style.zIndex = "101";
    maskObj.style.display = "block";
    document.body.appendChild(maskObj);
}

function CreateMessageDiv()
{
    var maskObj = document.createElement("div");
    maskObj.setAttribute('id', updateProgressID);
    maskObj.style.position = "absolute";
    maskObj.style.top = "0";
    maskObj.style.right = "0";
    maskObj.style.left = "0";
    maskObj.style.bottom = "0";
    maskObj.style.margin = " auto";
    maskObj.style.padding = "0";
    maskObj.style.background = "#fff";
    maskObj.style.border ="1px solid #ccc";
    maskObj.style.width = "200px";
    maskObj.style.height = "100px";
    maskObj.style.zIndex = "102";
    maskObj.style.display = "block";
    var innerTable = '<table cellspacing="0" cellpadding="0" align="center" width="100%" style="height: 100%;"> ' +
                            '<tr>'+
                                '<td align="center" valign="middle" height="100%">&nbsp;&nbsp;&nbsp;<img src="images/progress.gif" alt="" style="background-color: White;" />'+
                                    '<font class="textStyle5">&nbsp;Please wait..</font>'+
                                '</td>' +
                            '</tr>'+
                        '</table>';

    maskObj.innerHTML = innerTable;
    document.body.appendChild(maskObj);
}
