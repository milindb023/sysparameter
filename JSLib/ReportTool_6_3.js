/* class TBarService */
function dx_TBarService() 
{
	this.all = new Object();
	this.searchWindow = null;
	this.textRange = null;
	this.pageName = "";
	this.printElement = null;
	this.printFrame = null;
	this.userAgent = navigator.userAgent.toLowerCase();
    this.searchUrl = "";
    this.imgElement = null;

	this.prevhandler = window.onbeforeunload;
	window.onbeforeunload = function() { TBarService.onunload(); }
	
	this.onunload = function() {
		if(this.prevhandler != null) 
			this.prevhandler();
		this.closeWindow(this.searchWindow);
		var img = this.getImgElement();
		for(var i in this.all) {
		    if(img != null)
		        img.src = this.all[i].getPath("state", "unload");
		}
	}	
	this.closeWindow = function(win) {
	    if(win != null && !win.closed) {
    		win.close();
    	}
	}	
	this.setFrameSrc = function(src) {
		var el = this.getPrintElement();
		if(el != null)
			el.src = src;
	}	
	this.print = function() {
		if(this.printFrame != null) { 
			this.printFrame.focus();
			this.printFrame.print();
		} 
	}	
	this.syncFrameDir = function(repId) {
		if(document.dir != null && document.dir.length > 0) {
			var f = this.getFrame(repId);
			if(f != null && f.document != null)			
				f.document.dir = document.dir;
		}
	}
	this.getPath = function(val) {
		if(this.pageName.indexOf("?") > 0)
				return this.pageName + "&" + val;
		return this.pageName + "?" + val;
	}
	this.getImgElement = function() {
		if(this.imgElement == null) {
		    this.imgElement = document.createElement("img");
		    this.imgElement.style.width = "0px";
		    this.imgElement.style.height = "0px";
		    this.imgElement.id = "DXDummyImage";
		    document.body.appendChild(this.imgElement);
		} 
		return this.imgElement;
	}
	this.getPrintElement = function() {
		if(this.printElement == null) {
			this.printElement = this.createFrameElement("DXPrinter");
			this.printFrame = window.frames[this.printElement.id];
		} 
		return this.printElement;
	}
	this.createFrameElement = function(name) {
		var f = document.createElement("iframe");
		f.frameBorder = "0";
		f.style.overflow = "hidden";
		f.style.width = "0px";
		f.style.height = "0px";
		f.name = name;
		f.id = name;
		f.src = null;
		document.body.appendChild(f);
		return f;
	}
	this.getTextRange = function(repId, up) {
		if( !Exists(this.textRange) ) {
			var f = this.getFrame(repId);
			this.textRange = new dx_TextRange(f.document, up);
		}
		return this.textRange;
		}
	this.closeTextRange = function() {
		if( Exists(this.textRange) )
			this.textRange.empty();
		this.textRange = null;
	}
	this.getFrame = function(repId) {
		return window.frames[ this.genClientID(repId,"Frame") ];
	}
	this.showSearchWindow = function(repId) { 
		if(this.searchUrl == null || this.searchUrl.length == 0)
            return;
        if(this.searchWindow != null && !this.searchWindow.closed && this.searchWindow["repId"] != repId)
		    this.closeWindow(this.searchWindow);
	    if(this.searchWindow == null || this.searchWindow.closed) {
			var h = 105; var w = 350;
			var l = (screen.width - w) / 2;
			var t = (screen.height - h) / 2;
			var args = "left=" + l + ",top=" + t + ",height=" + h + ",width=" + w + ",status=no,toolbar=no,menubar=no,location=no,resizable=no,scrollbars=no";
	        this.searchWindow = window.open(this.searchUrl, '_blank', args);
	        this.searchWindow["repId"] = repId;
	        this.searchWindow["TBarService"] = this;
	        this.closeTextRange();
		}
		this.searchWindow.focus();
	}
	this.findText = function(repId, s) { 
		var f = new dx_ParamFormatter();
		f.parse(s);
		this.findTextCore(repId, f.params);
	}
	this.findTextCore = function(repId, params) {
		var up = params["up"];
		var range = this.getTextRange(repId, up);
		if( !range.findText(params["txt"], params["word"], params["case"], up) ) {
			var bar = this.getBar(repId);
			if(bar != null) bar.searchPage(params);
		}
	}
	this.onPageLoad = function(repId, pageIndex, pageCount) { 
		this.textRange = null;
		var bars = this.getBars(repId);
		for(var i = 0; i < bars.length; i++)
			bars[i].onPageLoad(pageIndex, pageCount);
	}
	this.getBar = function(repId) { 
		var bars = this.getBars(repId);
		return (bars.length > 0) ? bars[0] : null;
	}
	this.getBars = function(repId) { 
		var items = [];
		var i = 0;
		for(var barId in this.all) {
			var item = this.all[barId];
			if(item != null && item.repId == repId) {
			    items[i] = item;
				i++;
			}
		}
		return items;
	}
	this.handleButton = function(barId, btnId) {
		this.all[barId].handleButton(btnId);
	}
	this.createTBar = function(barId, repId, param) {
		var bar = this.all[barId];
		if(!Exists(bar) && barId.length != 0) {
			bar = new dx_TBar(barId, repId, param);
			this.all[barId] = bar;
			bar.showPage(0);
		}
	} 
	this.genClientID = function(id1, id2) {
		return id1 + "_" + id2;
	}
}
/* class TextRange */
function dx_TextRange(doc, up) 
{
	this.isTrue = function(val) {
		return val == true || val == "true";
	}
	this.selText = "";
	this.doc = doc;
	this.range = doc.body.createTextRange();
	if( this.isTrue(up) ) this.range.moveStart("textedit", 1);
	
	this.empty = function() { 
		this.doc.selection.empty();
	}
	this.select = function(text) { 
		this.range.select();
		this.selText = text;
	}
	this.findText = function(text, mword, mcase, up) {
		if(text == null || text.length == 0)
			return true;
		var fl = this.getFlags(mword, mcase);
		var val = this.isTrue(up) ? this.findUp(text, fl) :
			this.findDown(text, fl);		 
		if(val) this.select(text);
		return val;
	}
	this.getFlags = function(mword, mcase) {
		var fl = 0;
		if( this.isTrue(mword) ) fl += 2;
		if( this.isTrue(mcase) ) fl += 4;
		return fl;
	}
	this.findUp = function(text, fl) { 
		this.range.moveEnd("character", -this.selText.length);
		var val = this.range.findText(text, -1000, fl);               
		if(!val) this.range.moveEnd("character", this.selText.length);
		return val;
	}
	this.findDown = function(text, fl) { 
		this.range.moveStart("character", this.selText.length);
		var val = this.range.findText(text, 1000, fl);
		if(!val) this.range.moveStart("character", -this.selText.length);
		return val;
	}
}
/* class TBar */
function dx_TBar(barId, repId, param) 
{
    this.param = param;
	this.barId = barId;
	this.repId = repId;
	this.pageIndex = 0;
	this.pageCount = 0;

	this.initialize = function() { 
		try {
			if(document.body.createTextRange() != null)
				return;
		} catch(e) {}
		this.setElemDisabled("Search", true);
	}
	this.onPageLoad = function(pageIndex, pageCount) {
	    this.pageIndex = pageIndex;
	    this.pageCount = pageCount; 
		this.updateView();
		TBarService.syncFrameDir(this.repId);
	}
	this.searchPage = function(params) {
		var f = new dx_ParamFormatter();
		f.params = params;
		f.addParam("idx", this.pageIndex);
		this.setFrameSrc("search", f.getValue());
	}
	this.showPage = function(index) {
		if(index != this.pageIndex) {
			var f = new dx_ParamFormatter();
			this.setFrameSrc("page", f.format("idx", index));
		}
	}
	this.setFrameSrc = function(keyName, params) {
		var frame = this.getFrameElement();
		if(frame != null) {
			frame.src = this.getPath(keyName, params);
		}
	}
	this.getPath = function(keyName, params) {
		var path = TBarService.getPath(this.getKey(keyName) + params);
		if(this.param != null && this.param.length > 0)
		    path += '&dxrep=' + this.param;    
		return path;
	}
	this.getKey = function(name) {
		return this.repId + "_" + name + "=";
	}
	this.handleButton = function(btnId) {
		if(btnId.indexOf("Search") >= 0) {
			TBarService.showSearchWindow(this.repId);
			return;
		}
		if(btnId.indexOf("Print") >= 0) {
			var f = new dx_ParamFormatter();
			var val = (btnId.indexOf("PrintPage") >= 0) ? this.pageIndex : "";
	        var src = this.getPath("print", f.format("idx", val));
		    TBarService.setFrameSrc(src);
			return;
		}
		if(btnId.indexOf("SaveToWindow") >= 0) {
			var src = this.getPath("saveToWindow", this.getSaveFormat()); 
	        var d = new Date();
		    src += '&t=' + d.getTime();    
	        window.open(src, '_blank', 'toolbars=no, resizable=yes, scrollbars=yes');
			return;
		}
		if(btnId.indexOf("SaveToDisk") >= 0) {
		    var src = this.getPath("saveToDisk", this.getSaveFormat());
		    TBarService.setFrameSrc(src);
			return;
		}
		if(this.pageIndex < 0 || this.pageCount <= 0)
			return;
		var index = this.pageIndex;
		if(btnId.indexOf("FirstPage") >= 0) {
			index = 0;
		} else if(btnId.indexOf("PreviousPage") >= 0) {
			index = this.pageIndex - 1;
		} else if(btnId.indexOf("NextPage") >= 0) {
			index = this.pageIndex + 1;
		} else if(btnId.indexOf("LastPage") >= 0) {
			index = this.pageCount - 1;
		} else if(btnId.indexOf("PageNumber") >= 0) {
			index = this.getElemIntValue("PageNumber") - 1;
		} 
		index = Math.max(0, Math.min(index, this.pageCount - 1));
		if(index != this.pageIndex)
			this.showPage(index);
	}
	this.getSaveFormat = function() {
		var format = this.getElemValue("SaveFormat");
		format = format.toLowerCase();
		if(format == "mht") format += "&x=.mht"; // bug 22895
		return format;
	}
	this.updateView = function() {
		this.setElemValue("PageNumber", this.pageIndex + 1);
		var val = (this.pageIndex == 0);
		this.setElemDisabled("FirstPage", val);
		this.setElemDisabled("PreviousPage", val);
		
		val = this.pageIndex == this.pageCount - 1;
		this.setElemDisabled("NextPage", val);
		this.setElemDisabled("LastPage", val);
	}
	this.getFrameElement = function() {
		return document.getElementById( TBarService.genClientID(this.repId,"Frame") );
	}
	this.setElemValue = function(id, val) {
		var el = document.getElementById( this.genClientID(id) );
		if(el != null) el.value = "" + val;
	}
	this.getElemValue = function(id) {
		var el = document.getElementById( this.genClientID(id) );
		return (el != null) ? el.value : null;
	}
	this.getElemIntValue = function(id) {
		var val = this.getElemValue(id);
		return (val != null) ? parseInt(val) : -1;
	}
	this.setElemDisabled = function(id, val) {
		var el = document.getElementById( this.genClientID(id) );
		if(el != null && el.disabled != val) {
			el.disabled = val;
			this.setImageDisabled(el.childNodes[0], val);
		}
	}
	this.setImageDisabled = function(img, val) {
		 img.style.filter = val ? "progid:DXImageTransform.Microsoft.Alpha(opacity=40)" :
		    "progid:DXImageTransform.Microsoft.Alpha(opacity=100)";
	}
	this.genClientID = function(id) {
		return TBarService.genClientID(barId, id);
	}
	this.initialize();
}
/* class ParamFormatter */
function dx_ParamFormatter() 
{
	this.params = [];
	this.addParam = function(name, val) {
		this.params[name] = val;
	}
	this.getValue = function() {
		var val = "";
		for(var i in this.params)
			val += this.format(i, this.params[i]);
		return val;
	}
	this.format = function(name, val) {
		return name + ":" + val + ";";
	}
	this.parse = function(s) {
		var ss = s.split(";");
		for(var i = 0; i < ss.length; i++) {
			var sss = ss[i].split(":");
			if(sss.length == 2)
				this.addParam(sss[0], sss[1]);
		}
	}
}

// globals
var TBarService = new dx_TBarService();
function Exists(obj) {
	return obj != null && obj != "undefined";
}

