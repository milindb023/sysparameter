﻿$(initPage)
var objShiftGroup;
var arrShiftList = [];
var arrShift = [];
var shiftData;
var lastIndex;
var editor;
var activeBatchID;
var arrTempShift = [];
var globaltemplateConstant = "AMS_SHIFT_APPROVAL";

function initPage() {
    onPageLoad();
    currentBatch = $('#hfID').val();
}

var onPageLoad = function () {
    loadEmptyObj();
    getShiftList();

    $("#addNewGroup").on('click', function () {
        addNewGroup();
    });

    $('#linkNew').on('click', function () {
        $("#txtgroupname0").val("");
        $("#txtrate0").val("");
        arrShift = [];
    });
    $("#btnSendReq").on('click', sendReqToManager);
    $("#btnSendReminder").on('click', sendReminder);

    $("#btnCancel").on('click', function () {
        clearShiftSelection(this);
    });

    $('#btnchange').click(function () {
        $('#demo11w').addClass('in')
        $('#demo11w').css('height', 'auto');
        $('.table.fade').show();
        $('.table.fade').addClass('in')
    });


    $("#lnkclose").click(function () {
        $('body .container').hide();
    });


    $("#btnSearch").click(function () {

        $('tbodyShiftStatus').empty();
        loadManagerView();
    });

    $("#txtsearch").keypress(function (event) {
        if (event.keyCode == 13) {
            $("#btnSearch").click();
            return false;
        }
    });
}

var loadEmptyObj = function () {
    var Input = {};
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1974.aspx/GetEmptyShiftApprovalGroupObject",
        data: JSON.stringify(Input),
        dataType: "json",
        success: function (response) {
            objShiftGroup = JSON.parse(response.d);
            tempObj = objShiftGroup;
        },
        failure: function (response) {
            messagePopup('Some thing went wrong!');
        },
        error: function (response) {
            alert('Error : ' + response);
        }
    });
}

var getShiftList = function () {
    var Input = {};
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1974.aspx/GetAllShifts",
        data: JSON.stringify(Input),
        dataType: "json",
        success: loadAllShifts,
        failure: function (response) {
            messagePopup('Some thing went wrong!');
        },
        error: function (response) {
            //  alert('Error : ' + response);
        }
    });
}

var loadAllShifts = function (response) {
    if (response != null) {
        arrShiftList = [];
        var activeShifts = JSON.parse(response.d);
        $(activeShifts).each(function (index, activeShift) {
            var objShiftGroup = {};
            objShiftGroup["ShiftCode"] = activeShift.ShiftCode;
            objShiftGroup["ShiftName"] = activeShift.ShiftName;
            arrShiftList.push(objShiftGroup);
        });
        bindShifttoAdd();
        loadShiftBatch();
    }
}

var loadShiftBatch = function () {
    var Input = {};
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1974.aspx/GetShiftApprovalGroupList",
        data: JSON.stringify(Input),
        dataType: "json",
        success: OnGetShiftApprovalBacthesSuccess,
        failure: function (response) {
            messagePopup('Some thing went wrong!');
        },
        error: function (response) {
            //  alert('Error : ' + response);
        }
    });
}

var bindShifttoAdd = function () {
    var $divShift = $("#divShift");
    var html = '';
    var shift = arrShiftList;

    $(shift).each(function () {
        html += '' +
       '<label> <input type="checkbox" onChange="addchkChange(this);" class="addchkShift" value="' + this.ShiftCode + '"/>' + this.ShiftName + '</label>';
    });

    $("#divShift").html(html);

}

var OnGetShiftApprovalBacthesSuccess = function (response) {
    var $tbodyActiveShiftBatch = $('#tbodyActiveShiftBatch');
    if (response != null) {
        var html = '';
        response = JSON.parse(response.d);

        $(response).each(function (index) {
            html += viewShiftBatch(index, this) + editShiftBatch(index, this)
            lastIndex = index;
        });
        $('#tbodyActiveShiftBatch').html(html);
        regEvents();
    }
}

var viewShiftBatch = function (index, response) {
    var html = '';
    var selectedShifts = getShiftName(response.ApplicableShifts);
    var selectedShiftCount = 0;
    if (selectedShifts != null) {
        selectedShiftCount = selectedShifts.length;
    }

    html = '<tr>' +
            '<td class="text-left col-md-2 clsgroupname">' + response.Name + '</td>' +
            '<td class="text-left col-md-2 clsrate">' + response.Rate + '</td>' +
            '<td class="text-left col-md-4 ">' + selectedShiftCount + '</td>' +
            '<td class="clsselectedshifts clsselectedshift" style="display:none;">' + selectedShifts + '</td>' +
            '<td class="text-right col-md-3"> ' +
               '<a href="javascript:void(0)" class="edit neswedit clsshiftname" onClick="getShift(this);" data-toggle="collapse" ' +
                'data-parent="#demo12w" data-target="#edit' + index + '"><i class="icon-edit"></i></a>' +
                 '<a href="javascript:void(0)" style="display:none;" class="edit neswedit clsshiftname padd-left-15" onClick="DeleteShiftGroup(this);" data-toggle="collapse" ' +
                'data-parent="#demo12w" data-target="#edit' + index + '"><i class=" icon-delete"></i></a>' +
                '<input class="hfShiftApprovalBatch clsshiftgroupid" type="hidden" value=' + response.ShiftGroupID +

            '></td>' +
         '</tr>';
    return html;
}

var getShiftName = function (response) {
    if (response != null) {
        var arrShiftCode = response.map(function (a) { return a.ShiftCode; });
        return arrShiftCode;
    }
}

var editShiftBatch = function (index, response) {
    var html = '';
    html += '<tr class="closetr">' +
       '<td colspan="12" class="hiddenRow">' +
           '<div class="accordian-body collapse" aria-controls="demo12w" id="edit' + index + '" aria-expanded="false" style="height: 0px;">' +
             '<table class="table greycolortable">' +
                '<tr>' +
                  '<td class="text-left col-md-2">' +
                    '<div class="form-group">' +
                    '<input class="hfShiftApprovalBatch clsshiftgroupid" type="hidden" value=' + response.ShiftGroupID + '>' +
                        '<div class="input-group">' +
                            '<input type="text" id="txtgroupname" class="form-control clsgroupname" value="' + response.Name + '"/>' +
                        '</div>' +
                    '</div>' +
                '</td>' +
                '<td class="text-left col-md-2">' +
                    '<div class="form-group">' +
                        '<div class="input-group" id="datetimepicker45">' +
                        '<input type="text" id="txtrate" class="form-control clsrate"  value=' + response.Rate + ' />' +
                        '</div>' +
                    '</div>' +
                '</td>' +
                '<td style="display:none;" class="clsselectedshift">' + response.ApplicableShifts + '</td>' +
                '<td class="text-left col-md-4">' +
                '<div class="shiftdiv">';
    html += bindShift(response.ApplicableShifts);
    html += '</div></td>' +
   '<td class="text-right  col-md-3">' +
       '<button type="button" class="btn btn-primary pull-right withoutbg cancelclose cancelclosedes marg-left10"  onClick="resetData(this)">Cancel</button>' +
       '<button type="button" class="btn btn-primary pull-right " onClick="onSave(this);">Save</button>' +
   '</td>' +
'</tr>' +
'</table>' +
'</div>' +
'</td>' +
'</tr>';

    return html;
}

var addNewGroup = function () {

    var isValid = validate("#newrowid");
    if (isValid) {
        var $groupName = $('#txtgroupname0').val();
        var $rate = $('#txtrate0').val();

        if (arrShift.length == 0) {
            messagePopup("Please select atleast one shift");
            return false;
        }

        fillObj(0, $groupName, $rate);

        var objString = JSON.stringify(objShiftGroup);
        var objShiftSelected = JSON.stringify(objShiftGroup.ApplicableShifts);
        var Input = { 'jsonStringShiftGroup': objString, 'jsonShiftSelected': objShiftSelected };

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "ESS1974.aspx/AddNewGroup",
            data: JSON.stringify(Input),
            dataType: "json",
            success: function (response) {

                if (response.d != "success") {
                    messagePopup(response.d);
                    return;
                }
                onAddSuccess(this);
            },
            failure: function (response) {
                $('#divloader').modal('hide');
                messagePopup('Failure : ' + response);
            },
            error: function (response) {
                $('#divloader').modal('hide');
                messagePopup('Error occurred while adding record.');
            }
        });
    }
}

var convertDateFormat = function (inputdate) {
    var newdate = null;

    if (inputdate != null) {
        var date = inputdate;
        var d = new Date(date.split("/").reverse().join("-"));
        var dd = d.getDate();
        var mm = d.getMonth() + 1;
        var yy = d.getFullYear();
        newdate = yy + "/" + mm + "/" + dd;
    }
    return newdate;
}

var convertDateToYMD = function (inputdate, splitter) {
    var newdate = null;

    if (inputdate != null) {
        var date = inputdate;
        var d = new Date(date.split("/").reverse().join("-"));
        var dd = d.getDate();
        dd = (dd < 10) ? "0" + dd : dd;

        var mm = d.getMonth() + 1;
        mm = (mm < 10) ? "0" + mm : mm;

        var yy = d.getFullYear();
        newdate = yy + splitter + mm + splitter + dd;
    }
    return newdate;
}

var onAddSuccess = function (id) {

    var $tbodyActiveShiftBatch = $('#tbodyActiveShiftBatch');

    lastIndex++;
    $tbodyActiveShiftBatch.append(viewShiftBatch((lastIndex), objShiftGroup) + editShiftBatch((lastIndex), objShiftGroup));
    regEvents();
    getShiftList();
    //getActiveBatchID();
    // closeRow(id);
    //Reload(false);
    clearShiftSelection(id);
    arrShift = [];
    $('.cancelclosedes').click();
    //$('#btnchange').click();
    $('#divloader').modal('hide');


}

var onSave = function (id) {
    var isValid = validate(id);
    if (isValid) {
        var $groupname = $(id).closest('tr').find('.clsgroupname').val();
        var $rate = $(id).closest('tr').find('.clsrate').val();
        var $groupid = $(id).closest('tr').find('.clsshiftgroupid').val();
        //var $applicableshifts = $(id).closest('tr').find('.clsapplicableshifts').val();
        if (arrShift.length == 0)
        {
            messagePopup("Please select atleast one shift");
            return false;
        }

        fillObj($groupid, $groupname, $rate);
        var obj = JSON.stringify(objShiftGroup);

        var objString = JSON.stringify(objShiftGroup);
        var objShiftSelected = JSON.stringify(objShiftGroup.ApplicableShifts);
        var Input = { 'jsonStringShiftGroup': objString, 'jsonShiftSelected': objShiftSelected };

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "ESS1974.aspx/UpdateShiftGroup",
            data: JSON.stringify(Input),
            dataType: "json",
            success: function (response) {
                if (response.d != "success") {
                    messagePopup(response.d);
                    return;
                }
                onAddSuccess(this);
            },
            failure: function (response) {
                $('#divloader').modal('hide');
                messagePopup('Failure : ' + response);
            },
            error: function (response) {
                $('#divloader').modal('hide');
                messagePopup('Error occurred while addind record.');
            }
        });
    }
}

var updateRow = function (id) {

    var dtFrom = $(id).closest('tr').find('.dtfrom').val();
    var dtTo = $(id).closest('tr').find('.dtto').val();

    $(id).closest('.opentr').prev().find('.clsfromdate').html(dtFrom);
    $(id).closest('.opentr').prev().find('.clstodate').html(dtTo);

    $(id).closest('.opentr').prev().find('.clsshiftName').html(shiftData);
}

var fillObj = function (shiftGroupID, groupName, rate) {
    var arrSelected = [];

    jQuery.grep(arrShiftList, function (value) {
        if (jQuery.inArray(value.ShiftCode, arrShift) != -1) arrSelected.push(value);
    });

    arrSelected.forEach(function (e) { delete e.IsSelected });

    objShiftGroup.ShiftGroupID = shiftGroupID;
    objShiftGroup.Name = groupName;
    objShiftGroup.Rate = rate;
    objShiftGroup.UpdatedBy = 'temp';
    objShiftGroup.UpdatedOn = '2001-01-01'
    objShiftGroup.ApplicableShifts = arrSelected;
}

var ValidateShiftExist = function (strObject) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1974.aspx/ValidateShift",
        data: JSON.stringify(strObject),
        dataType: "json",
        success: function (response) {
            if (response.d != "Success") {
                return false;
            }
            return true;
        },
        failure: function (response) {
            return false;
        },
        error: function (response) {
            $('#divloader').modal('hide');
            messagePopup('Error occurred while adding record.');
        }
    });
}
var deactivateCurrentShift = function () {

    var $batchID = activeBatchID;
    var Input = { batchID: $batchID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1973.aspx/DeactivateBatch",
        data: JSON.stringify(Input),
        dataType: "json",
        success: function (response) {
            activeBatchID = response;
        },
        failure: function (response) {
            messagePopup('Some thing went wrong!');
        },
        error: function (response) {
            alert('Error : ' + response);
        }
    });

}

var clearShiftSelection = function (id) {
    $("#txtgroupname").val('');
    $("#txtrate").val('');

    $(id).closest('tr').find('input[type="checkbox"]').removeAttr('checked');
}

var bindShift = function (response) {

    var html = '';
    var shift = arrShiftList;
    var selectedShift = response;

    var arrShiftCode = selectedShift.map(function (a) { return a.ShiftCode; });

    jQuery.grep(shift, function (value) {
        if (jQuery.inArray(value.ShiftCode, arrShiftCode) == -1) {
            value["IsSelected"] = false;
        }
        else {
            value["IsSelected"] = true;
        }
    });

    $(shift).each(function () {
        html += '';
        if (this.IsSelected)
            html += '<label> <input type="checkbox" onChange="editchkChange(this);"  checked class="chkShift" value="' + this.ShiftCode + '"/>' + this.ShiftName + '</label>';
        else
            html += '<label> <input type="checkbox" onChange="editchkChange(this);"  class="chkShift" value="' + this.ShiftCode + '"/>' + this.ShiftName + '</label>';
    });
    return html;
}

var addchkChange = function (id) {

    var shift = $(id).val();

    var count = arrShift.length;

    arrShift = jQuery.grep(arrShift, function (value) {
        return value != shift;
    });

    if (count == arrShift.length)
        arrShift.push($(id).val());

    shiftData = arrShift.join(',');
}

var editchkChange = function (id) {

    var shift = $(id).val();

    var count = arrShift.length;

    arrShift = jQuery.grep(arrShift, function (value) {
        return value != shift;
    });

    if (count == arrShift.length)
        arrShift.push($(id).val());

    shiftData = arrShift.join(',');
}

var sendReqToManager = function () {
    $("#divloader").modal();
    var Input = { batchID: activeBatchID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1973.aspx/SendRequestToManagers",
        data: JSON.stringify(Input),
        dataType: "json",
        success: function (response) {
            loadHeader();
            loadManagerView();
            messagePopup('Request has been sent to managers.');
            $("#divloader").modal('hide');
        },
        failure: function (response) {
            messagePopup('Some thing went wrong!');
        },
        error: function (response) {
            messagePopup('Unable to send request to manager.');
        }
    });
}

var sendReminder = function () {
    $("#divloader").modal();
    var $batchID = activeBatchID;
    var Input = { batchID: $batchID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1973.aspx/SendReminder",
        data: JSON.stringify(Input),
        dataType: "json",
        success: function (response) {
            loadHeader();
            messagePopup('Reminder has been sent to managers who have not approved shift records');
            $("#divloader").modal('hide');
        },
        failure: function (response) {
            messagePopup('Some thing went wrong!');
        },
        error: function (response) {
            messagePopup('Unable to send reminder.');
        }
    });
}

var regEvents = function () {

    /* for parent tr hide 9 jan 2017**/
    $('.edit').on('click', function () {
        $(this).closest('tr').hide();
        $('.hiddenRow').css('border-top', '0px')
    })
    /* for parent tr hide 9 jan 2017**/

    $('.collapse').on('show.bs.collapse', function () {
        $('.collapse.in').collapse('hide');
        $(this).closest('tr').addClass('opentr').removeClass('closetr');
        $('.opentr').prev().addClass('opentrp').removeClass('closetrp');

        /* for parent tr hide 9 jan 2017**/
        $('.edit').on('click', function () {
            $(this).closest('tr').hide();
            $('.hiddenRow').css('border-top', '0px')
        })
        /* for parent tr hide 9 jan 2017**/

        $('.opentrp').find('.fa').addClass('fa-minus-circle').removeClass('fa-plus-circle');

    }).on('hidden.bs.collapse', function () {
        $(this).closest('tr').addClass('closetr').removeClass('opentr')
        $('.closetr').prev().addClass('closetrp').removeClass('opentrp');
        $('.closetrp').find('.fa').addClass('fa-plus-circle').removeClass('fa-minus-circle');

    });

    $('.cancelclosedes').click(function () {
        arrShift = [];
        $(this).parentsUntil('.hiddenRow').addClass('sedr');
        $('.accordian-body.sedr').collapse('hide');

        /* for parent tr hide 9 jan 2017**/
        $(this).closest('.hiddenRow').parent('tr').prev('tr').addClass('newshowhide').show();
        /* for parent tr hide 9 jan 2017**/

    })

    $("#btndownload").click(function (e) {
        e.preventDefault();

        //getting data from our table
        var data_type = 'data:application/vnd.ms-excel';
        var table_div = document.getElementById('divDownload');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');

        var a = document.createElement('a');
        a.href = data_type + ', ' + table_html;
        var fileName = $('#hfBatchFrom').val() + '-' + $('#hfBatchTo').val() + '.xls';
        a.download = fileName
        a.click();
    });
}

var getShift = function (id) {

    var shiftlist = $(id).closest('tr').find('.clsselectedshift').html();

    if (shiftlist != "") {
        {
            arrShift = shiftlist.split(",");
            preserveData(id);
        }
        shiftData = arrShift.join(',');
    }
}

var DeleteShiftGroup = function (id) {
    var $shiftGroupId = $(id).closest('tr').find('.clsshiftgroupid').val();
    if($shiftGroupId != null)
    {
        var Input = { shiftGroupID: $shiftGroupId };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "ESS1974.aspx/DeleteShiftGroup",
            data: JSON.stringify(Input),
            dataType: "json",
            success: function (response) {
                var message = response.d;
                if (message != null)
                {
                    messagePopup(message);
                }
            },
            failure: function (response) { messagePopup('Some thing went wrong!'); },
            error: function (response) { messagePopup('Error occured'); }
        });
    }
}

var preserveData = function (id) {

    var $fromDate = $(id).closest('tr').find('.clsfromdate').html();
    var $toDate = $(id).closest('tr').find('.clstodate').html();

    tempObj.FromDate = $fromDate;
    tempObj.ToDateText = $toDate;
    tempObj.FromDateText = $fromDate;
    tempObj.ToDate = $toDate;
    tempObj.ApplicableShifts = arrShift;

}

var resetData = function (id) {

    if (tempObj.IsActive == 'Active')
        $(id).closest('tr').find('.ckhActive').attr('checked');
    else
        $(id).closest('tr').find('.ckhActive').removeAttr('checked');

    $(id).closest('tr').find('.dtfrom').val(tempObj.FromDate);
    $(id).closest('tr').find('.dtto').val(tempObj.ToDate);

    onCancelSave(id);
}

var closeRow = function (id) {
    $(id).parentsUntil('.hiddenRow').addClass('sedr');
    $('.accordian-body.sedr').collapse('hide');
}

var checkIsActive = function ($isActive, batchID) {

    if ($isActive && activeBatchID != batchID && activeBatchID != 0) {
        if (confirm('Current active batch will be deactivated. do you want to continue?')) {
            return true;
        } else {
            return 'NO';
        }
    }
    else {
        return false;
    }
}


var validate = function (id) {
    $drate = $(id).closest('tr').find('.clsrate').val();
    $sgroupname = $(id).closest('tr').find('.clsgroupname').val();

    if ($sgroupname == "") {
        alert("Please enter group name");
        return false;
    }

    if ($drate == "" || isNaN($drate)) {
        alert("Please enter valid rate");
        return false;
    }
    return true;
}

var messagePopup = function (msgText) {
    $('#message').html(msgText);
    $('#alert').modal();
}

var onCancelSave = function (id) {

    var $divShift = $(id).closest('tr').find('.shiftdiv');

    var html = '';
    var shift = arrShiftList;

    var arrShiftCode = tempObj.ApplicableShifts;

    jQuery.grep(shift, function (value) {
        if (jQuery.inArray(value.ShiftCode, arrShiftCode) == -1) {
            value["IsSelected"] = false;
        }
        else {
            value["IsSelected"] = true;
        }
    });

    $(shift).each(function () {
        html += '';
        if (this.IsSelected)
            html += '<label> <input type="checkbox" onChange="editchkChange(this);"  checked class="chkShift" value="' + this.ShiftCode + '"/>' + this.ShiftName + '</label>';
        else
            html += '<label> <input type="checkbox" onChange="editchkChange(this);"  class="chkShift" value="' + this.ShiftCode + '"/>' + this.ShiftName + '</label>';
    });

    $divShift.html(html);
}
