﻿initMapWithGeoLocation// 2. Initialize geolocation objects
var map;
var placeOnMap;
var options = {
    enableHighAccuracy: true,
    timeout: 10000,
    maximumAge: 0
};

function errorGettigLocation(error) {
    //$("#lblLocationMessage").html("<i class='icon-location' title='(" + error.code + ") " + error.message + "' onclick='alert(\"" + error.message + "\")'></i> could not obtain your location");
    if (typeof positionObtained == "function") {
        positionObtained(false, null);
    }
}



function geocodeLatLng(geocoder, map, position) {

    // 3.1.3.1 Initialize object for Reverse Geocode call
    var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
    };

    // 3.1.3.2 Place a marker on obtained location. Also set the zoom level and center the marker
    var marker = new google.maps.Marker({
        position: pos,
        map: map
    });
    map.setZoom(15);
    map.setCenter(pos);
    map.zoomControl = true;
    // 3.1.3.3 Try to obtain the address from coords
    geocoder.geocode({ 'location': pos }, function (results, status) {
        if (status === 'OK') {
            if (results[1]) {

                // 3.1.3.3.1 Show human readable address value to user. Thanks google-geocoder!
                //$("#lblLocationMessage").html('<i class="icon-location2"></i> ' + results[1].formatted_address);
                if (typeof addressObtained == "function" ){
                    addressObtained(results[1].formatted_address);
                }

            } else {
                // Maintaining comment for dubug purpose
                //window.alert('No results found');
            }
        } else {
            // Maintaining comment for dubug purpose
            //window.alert('Geocoder failed due to: ' + status);
        }
    });
}

function initMapWithGeoLocation() {
    // 1. Initialize google map to show punch location  *** Test key should be replaced with production key
    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 18.5123024, lng: 73.7761346 },
        zoomControl: true,
        zoom: 8
    });

    // 3. Check if client supports Geolocation
    if (typeof positionRequested == "function") {
        positionRequested();
    }
    if (navigator.geolocation) {

        // 3.1 If it supports then obtain location
        navigator.geolocation.getCurrentPosition(function (position) {

            // 3.1.1 Mark latlng values to be persisted
            //$("#hiddenLatitude").val(position.coords.latitude);
            //$("#hiddenLongitude").val(position.coords.longitude);

            // 3.1.2 Set interim status before Reverse Geocoding
            //$("#lblLocationMessage").html('<i class="icon-location2"></i> your location is tagged.');
            if (typeof positionObtained == "function" ){
                positionObtained(true, position);
            }
            // 3.1.3 Reverse Geocoding (obtain address from coords)                    
            geocodeLatLng(geocoder, map, position);
        },

        // 3.2 Handle error, if occurs during getting location
        errorGettigLocation,

        // 3.3 Enabling high accuracy
        options);
    }
    else {
        // 3.3 Gracefully handle situation if location is not supported
        if (typeof positionObtained == "function") {
            positionObtained(false, null);
        }
    }
}

function initMapOnly() {
    // Initialize google map to show punch location  *** Test key should be replaced with production key
    map = new google.maps.Map(document.getElementById('map'), {
        streetViewControl: false,
        zoomControl: true,
        center: { lat: 18.5123024, lng: 73.7761346 },
        zoom: 8
    });
}

function markPunchLocation(position) {

    // 1. Initialize Geocoder to obtain human readable address later
    var geocoder = new google.maps.Geocoder;

    //// 2. Mark latlng values to be persisted
    //$("#hiddenLatitude").val(position.coords.latitude);
    //$("#hiddenLongitude").val(position.coords.longitude);

    // 3. Reverse Geocoding (obtain address from coords)                    
    geocodeLatLng(geocoder, map, position);
}
