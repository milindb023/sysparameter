﻿$(initPage)
var objShift;
var arrShiftList = [];
var arrShift = [];
var arrCostCentreList = [];
var arrCC = [];
var shiftData;
var shiftData1;
var shiftStatus;
var lastIndex;
var editor;
var activeBatchID;
var arrTempShift = [];
var globaltemplateConstant = "AMS_SHIFT_APPROVAL";

function initPage() {
    onPageLoad();
}

var onPageLoad = function () {
    getCostCentreList();

    $("#lnkclose").click(function () {
        window.location = "ESS1974.aspx";
    });

    $("#mBtn").click(function () {
        window.location = "ESS1974.aspx";
    });

}

var getCostCentreList = function () {
    var Input = {};
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1975.aspx/GetAllCostCentres",
        data: JSON.stringify(Input),
        dataType: "json",
        success: function (response) {
            OnGetCostCentreSuccess(response);
        },
        failure: function (response) {
            messagePopup('Some thing went wrong!');
        },
        error: function (response) {
            //  alert('Error : ' + response);
        }
    });
}

var OnGetCostCentreSuccess = function (response) {
    var html = '';
    var $tbodyActiveCC = $('#tbodyActiveCC');
    var activeCostCentres = JSON.parse(response.d);
    $(activeCostCentres).each(function (index) {
        html += viewCostCentre(index, this);

    });
    $('#tbodyActiveCC').html(html);

}

var viewCostCentre = function (index, response) {
    var html = '';

    html = '<tr>' +
            '<td class="text-left col-md-2">' + response.CostCode + '</td>' +
            '<td class="text-left col-md-4">' + response.CostCentre + '</td>' +
            '<td class="text-center col-md-3">';
            if (response.IsEnabled == true) {
                html += '<input id="' + response.CostCode + '" type="checkbox" checked name="ccgroup" value="' + response.IsEnabled + '"/>';
                }
            else {
                html += '<input id="' + response.CostCode + '" type="checkbox" name="ccgroup" value="' + response.IsEnabled + '"/>';
            }
    html += '</td>' +
            '<td class="text-left col-md-3"></td>' +
            '</tr>';
    return html;
}

var resetCC = function () {
    getCostCentreList();
}

var SaveCC = function () {
    var selectedCC = [];
    $.each($("input[name='ccgroup']:checked"), function () {
        selectedCC.push($(this).attr("id"));
    });

    saveCostCentre(selectedCC);
}

var saveCostCentre = function (selectedCC) {
    var Input = {arrCostCodes: selectedCC};
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1975.aspx/SaveApplicableDepartments",
        data: JSON.stringify(Input),
        dataType: "json",
        success: function (response) {
            $('#divloader').modal('hide');
            messagePopup('Records updated successfully.');
        },
        failure: function (response) {
            $('#divloader').modal('hide');
            messagePopup('Unable to Update Record.');
        },
        error: function (response) {
            $('#divloader').modal('hide');
            messagePopup('Error Occurred while Updating Record.');
        }
    });
}




var messagePopup = function (msgText) {
    $('#message').html(msgText);
    $('#alert').modal();
}
