﻿$(initPage)
var objShift;
var arrShiftList = [];
var arrShift = [];
var shiftData;
var shiftStatus;
var lastIndex;
var editor;
var activeBatchID;
var arrTempShift = [];
var globaltemplateConstant = "AMS_SHIFT_APPROVAL";

function initPage() {
    onPageLoad();
    currentBatch = $('#hfBatchID').val();
}

var onPageLoad = function () {
  
    editor = new Jodit('#jodit', {
        removeButtons: [
            "about"
        ],
        placeholder: '',
        popap: false
    });

    $('#txtsearch').val('');
    getActiveBatchID();
    loadEmptyObj();
    getShiftList();

    $("#addNewShift").on('click', function ()
    {
        addNewShift();
    });

    $('#linkNew').on('click', function () {
        arrShift = [];
    });
    $("#btnSendReq").on('click', sendReqToManager);
    $("#btnSendReminder").on('click', sendReminder);

    $("#btnSave").on('click', function () {
        updateEmailTemplate();
    });

    $('#aDefault').on('click', function () {
        defaultEmailTemplate();
    });

    $("#btnCancel").on('click', function () {
        clearShiftSelection(this);
    });

    $('#btnchange').click(function () {
        $('#demo11w').addClass('in')
        $('#demo11w').css('height', 'auto');
        $('.table.fade').show();
        $('.table.fade').addClass('in')
    });


    $("#lnkclose").click(function () {
        $('body .container').hide();
    });


    $("#btnSearch").click(function () {
      
        $('tbodyShiftStatus').empty();
        loadManagerView();
    });

    $("#txtsearch").keypress(function (event) {
        if (event.keyCode == 13) {
            $("#btnSearch").click();
            return false;
        }
    });
    clearsearch();
}

function clearsearch() {
    $(document).ready(function () {
        $('.search-close').click(function () {
            $('#txtsearch').val('');

            $('#tbodyShiftStatus').empty();
            loadManagerView();
        })
    })
}


function Reload(hideBatchGrid) {

    $.ajax({
        url: "ESS1973.aspx",
        cache: false,
        success: function (data) {

            $(initPage);

            var arrShiftList = [];
            var arrShift = [];
            var arrTempShift = [];

            if (activeBatchID == 0) {
                $("#divActiveShiftCard").hide();
                $("#divmanagerview").hide();
                $("#btnchange").click();
            }
            else {
                $("#divActiveShiftCard").show();
                $("#divmanagerview").show();
                if (hideBatchGrid) {
                    $("#divGridShiftBatch").hide();
                }
                else {
                    $("#divGridShiftBatch").show();
                }
            }
        }
    });
}

var getActiveBatchID = function () {
    var Input = {};
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1973.aspx/GetActiveBatch",
        data: JSON.stringify(Input),
        dataType: "json",
        success: function (response) {
            activeBatchID = response.d;
            loadHeader();
            loadManagerView();
            //loadDownload();
            manageDashboard()

        },
        failure: function (response) {
            messagePopup('Some thing went wrong!');
        },
        error: function (response) {
            alert('Error : ' + response);
        }
    });
}

var manageDashboard = function () {
    if (activeBatchID == 0) {
        $("#divActiveShiftCard").hide();
        $("#divmanagerview").hide();
        $("#divGridShiftBatch").show();
        $("#btnchange").click();
    }
    else {
        $("#divActiveShiftCard").show();
        $("#divmanagerview").show();
        $("#divGridShiftBatch").hide();
    }
}

var loadEmptyObj = function () {
    var Input = {};
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1973.aspx/GetEmptyShiftApprovalObject",
        data: JSON.stringify(Input),
        dataType: "json",
        success: function (response) {
            objShift = JSON.parse(response.d);
            tempObj = objShift;
        },
        failure: function (response) {
            messagePopup('Some thing went wrong!');
        },
        error: function (response) {
            alert('Error : ' + response);
        }
    });
}

var getShiftList = function () {
    var Input = {};
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1973.aspx/GetAllShifts",
        data: JSON.stringify(Input),
        dataType: "json",
        success: loadAllShifts,
        failure: function (response) {
            messagePopup('Some thing went wrong!');
        },
        error: function (response) {
            //  alert('Error : ' + response);
        }
    });
}

var loadAllShifts = function (response) {
    if (response != null) {

        arrShiftList = [];

        var activeShifts = JSON.parse(response.d);

        $(activeShifts).each(function (index, activeShift) {
            var objShift = {};
            objShift["ShiftCode"] = activeShift.ShiftCode;
            objShift["ShiftName"] = activeShift.ShiftName;
            arrShiftList.push(objShift);
        });
        bindShifttoAdd();
        loadShiftBatch();
    }
}

var loadShiftBatch = function () {
    var Input = {};
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1973.aspx/GetShiftApprovalBacthes",
        data: JSON.stringify(Input),
        dataType: "json",
        success: OnGetShiftApprovalBacthesSuccess,
        failure: function (response) {
            messagePopup('Some thing went wrong!');
        },
        error: function (response) {
            //  alert('Error : ' + response);
        }
    });
}

var loadHeader = function () {
    var Input = { batchID: activeBatchID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1973.aspx/GetSummaryView",
        data: JSON.stringify(Input),
        dataType: "json",
        success: getActiveBatchDetails,
        failure: function (response) {
            messagePopup('Some thing went wrong!');
        },
        error: function (response) {
            messagePopup('Unable to get summary view');
        }
    });
}

var getActiveBatchDetails = function (response) {

    if (response != null) {
        readyForApprovalCount();
        var summary = JSON.parse(response.d);
        //Shifts
        $('#spanActivePeriod').html(summary.DaysToGo);
        $('#spanFromDate').html(summary.FromDate);
        $('#spanToDate').html(summary.ToDate);
        $('#spanApprovedCount').html(summary.Approved);
        $('#spanPendingCount').html(summary.Pending);
        $('#hfBatchID').html(summary.ShiftApprovalBatchID);
        $('#spanDeclineCount').html(summary.Declined);

        $('#hfBatchFrom').val(convertDateToYMD(summary.FromDate, ""));
        $('#hfBatchTo').val(convertDateToYMD(summary.ToDate, ""));


        //objShift

        $('#spanShift').html(' <a id="shidtdata" data-toggle="tooltip" data-placement="right" title="Applicable Shifts : ' + summary.Shifts + '" href="javascript:void(0)" class="cleafix form-group btn-block lightfour"> View Shifts<i class="icon-help-outline outlineshift"></i> </a>');

        $('[data-toggle="tooltip"]').tooltip();

        var reqDate = summary.LastApprovalRequestSentOn;
        var remDate = summary.LastReminderSentOn;

        var remDay = summary.LastReminderSentOnDaysAgo;

        var reqDay = summary.LastApprovalRequestSentOnDaysAgo;

        if (reqDate == null) {
            $('#divReqSent').html('<b>Last Status:</b><span> No Request sent</span>');
        }
        else {
            if (reqDay == null)
                $('#divReqSent').html('<b>Last Status:</b><span> No Request sent</span>');
            else
                $('#divReqSent').html('<b>Last Status:</b><span> Request sent on </span> <span class="libGreen" id="spanReqSendDate">' + reqDate + '</span> <span id="spanReqSentDay">(' + reqDay + ' days ago ) </span>');
        }

        if (reqDate == null) {
            $('#divRemSend').html('<b>Last Reminder:</b><span>  No Request sent </span>');
        }
        else {
            if (remDay == null)
                $('#divRemSend').html('<b>Last Reminder:</b><span>  No Request sent </span>');
            else
                $('#divRemSend').html('<b>Last Reminder:</b><span> sent on</span> <span class="libGreen" id="spanlastremindersenton">' + remDate + '</span> (<span id="spanRemDay">' + remDay + ' days ago )</span>');
        }
        // }

        if (summary.Pending > 0)
            $('#btnSendReminder').removeAttr('disabled');
        else
            $('#btnSendReminder').attr('disabled', 'disabled');
    }
}

var readyForApprovalCount = function () {
    var Input = { batchID: activeBatchID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1973.aspx/GetReadyForApprovalCount",
        data: JSON.stringify(Input),
        dataType: "json",
        success: function (response) {
        
            $('#spanApprovalCount').html(response.d);
            if (response.d == "0")
                $('#btnSendReq').attr('disabled', 'disabled');
            else
                $('#btnSendReq').removeAttr('disabled');
        },
        failure: function (response) {
            messagePopup('Some thing went wrong!');
        },
        error: function (response) {
            //  alert('Error : ' + response);
        }
    });

}

var loadManagerView = function () {
  
    var searchString = $("#txtsearch").val() != "" ? $("#txtsearch").val() : "";
    var Input = { batchID: activeBatchID, searchString: searchString };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1973.aspx/GetManagerView",
        data: JSON.stringify(Input),
        dataType: "json",
        success: onloadManagerViewSuccess,
        failure: function (response) {
            messagePopup('Some thing went wrong!');
        },
        error: function (response) {
            //  alert('Error : ' + response);
        }
    });
}

var onloadManagerViewSuccess = function (response) {

    var $tbodyShiftStatus = $('#tbodyShiftStatus');
    var html = '';
    if (response != null) {
        response = JSON.parse(response.d);
        $(response).each(function () {
            html += '<tr class="">' +
                        '<td class="text-left col-md-4"><a href="javascript:void(0);" onClick="Navigate(\'' + this.ReportTo + '\')">' + this.Name + '</a></td>' +
                              '<td class="text-left col-md-2">' + this.Mobile + '</td>' +
                              '<td class="text-left libGreen col-md-2">' + this.Approved + '</td>' +
                        '<td class="text-left libOrg col-md-2">' + this.Pending + '</td>' +
                        '<td class="text-left libBlue col-md-2">' + this.Declined + '</td>' +
                    '</tr>';
        });

        $tbodyShiftStatus.html(html);
    }
    else {
        $('#btndownload').attr('disabled', 'disabled');
    }
}

function Navigate(ReportTo) {
 
    window.location.href = '\ESS1972.aspx?HashEmpNo=' + ReportTo;
}

var loadDownload = function () {
    var Input = { batchID: activeBatchID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1973.aspx/DownloadShiftApprovalForPayroll",
        data: JSON.stringify(Input),
        dataType: "json",
        success: loadDownloadSuccess,
        failure: function (response) {
            messagePopup('Some thing went wrong!');
        },
        error: function (response) {
            alert('Error : ' + response);
        }
    });
}

//var loadDownloadSuccess = function (response) {

//    if (response != null) {
//        var html = '';
//        response = JSON.parse(response.d);
//        $(response).each(function (index, data) {
//            html += '<tr><td>\'' + this.EmpNo + '</td><td>' + this.ShiftCode + '</td><td>' + this.DaysAttended + '</td></tr>';
//        });

//        $('#tbodyDownload').html(html);
//    }
//}

var OnGetShiftApprovalBacthesSuccess = function (response) {
    var $tbodyActiveShiftBatch = $('#tbodyActiveShiftBatch');
    if (response != null) {

        var html = '';
        response = JSON.parse(response.d);
      
        $(response).each(function (index) {
            html += viewShiftBatch(index, this) + editShiftBatch(index, this)
            lastIndex = index;
        });
        $('#tbodyActiveShiftBatch').html(html);
        regEvents();
    }
}

var viewShiftBatch = function (index, response) {
    var html = '';
    var shiftStatus = (response.IsActive == true) ? "Active" : "Closed";

    var selectedShifts = getShiftName(response.ApplicableShifts);
    html = '<tr>' +
            '<td class="text-left col-md-2 clsfromdate">' + response.FromDateText + '</td>' +
            '<td class="text-left col-md-2 clstodate">' + response.ToDateText + '</td>' +
            '<td class="text-left col-md-4 shiftName">' + selectedShifts + '</td>' +
            '<td class="text-left col-md-1 shiftStatus">' + shiftStatus + '</td>' +
            '<td class="text-right col-md-3"><a href="javascript:void(0)" class="edit neswedit" onClick="getShift(this);" data-toggle="collapse" data-parent="#demo12w" data-target="#edit' + index + '"><i class="icon-edit"></i></a><input class="hfShiftApprovalBatch " type="hidden" value=' + response.ShiftApprovalBatchID + '></td>' +
         '</tr>';
    return html;
}

var getShiftName = function (response) {
    if (response != null) {
        var arrShiftCode = response.map(function (a) { return a.ShiftCode; });
        return arrShiftCode;
    }
    else
    {
        return 'FS';
    }
}

var editShiftBatch = function (index, response) {

    var html = '';
    html += '<tr class="closetr">' +
       '<td colspan="12" class="hiddenRow">' +
           '<div class="accordian-body collapse" aria-controls="demo12w" id="edit' + index + '" aria-expanded="false" style="height: 0px;">' +
             '<table class="table greycolortable">' +
                '<tr>' +
                  '<td class="text-left col-md-2">' +
                    '<div class="form-group">' +
                    '<input class="hfShiftApprovalBatch" type="hidden" value=' + response.ShiftApprovalBatchID + '>' +
                    '<input class="hfCreatedOn" type="hidden" value=' + response.CreatedOn + '>' +
                    '<input class="hfCreatedBy" type="hidden" value=' + response.CreatedBy + '>' +
                    '<input class="hfLastReqSent" type="hidden" value=' + response.LastApprovalRequestOnText + '>' +
                    '<input class="hfLastRemSend" type="hidden" value=' + response.LastReminderOnText + '>' +
                        '<div class="input-group date datepicker" id="datetimepicker4a">' +
                            '<input type="text" id="dtfrom" class="form-control datepicker dtfrom" value=' + response.FromDateText + ' />' +
                            '<span class="input-group-addon"><i class="md-icons dp16">&#xE8A3;</i></span>' +
                        '</div>' +
                    '</div>' +
                '</td>' +
                '<td class="text-left col-md-2">' +
                    '<div class="form-group">' +
                        '<div class="input-group date datepicker" id="datetimepicker45">' +
                        '<input type="text" id="dtto" class="form-control datepicker dtto"  value=' + response.ToDateText + ' />' +

                            '<span class="input-group-addon"><i class="md-icons dp16">&#xE8A3;</i></span>' +
                        '</div>' +
                    '</div>' +
                '</td>' +
                '<td class="text-left col-md-4">' +
                '<div class="shiftdiv">';
    html += bindShift(response.ApplicableShifts);
    html += '</div></td>' +
   '<td class="text-left col-md-1">';
    if (response.IsActive) {
        html += ' <label> <input type="checkbox" class="ckhActive" checked value="Deactive"> Active</label>';
    }
    else {
        html += ' <label> <input type="checkbox" class="ckhActive" value="Deactive"> Active</label>';
    }
    html += '</td>' +
   '<td class="text-right  col-md-3">' +
       '<button type="button" class="btn btn-primary pull-right withoutbg cancelclose cancelclosedes marg-left10"  onClick="resetData(this)">Cancel</button>' +
       '<button type="button" class="btn btn-primary pull-right " onClick="onSave(this);">Save</button>' +
   '</td>' +
'</tr>' +
'</table>' +
'</div>' +
'</td>' +
'</tr>';

    return html;
}

var addNewShift = function () {
  
    var isValid = validate(true,0);
    if (isValid) {

        var $fromDate = $('#dtfrom0').val();
        var $toDate = $('#dtto0').val();
        var $isActive = $('#ckhIsActive').prop('checked');

        var isConfirm = checkIsActive($isActive, 0);

        if (isConfirm) { deactivateCurrentShift(); }

        if (isConfirm != 'NO') {
            $('#divloader').modal();
            fillObj(0, 0, $fromDate, $toDate, $isActive, null, null);

            var obj = JSON.stringify(objShift);

            var Input = { 'jsonStringShiftApprovalBatch': obj };

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ESS1973.aspx/AddNewBatch",
                data: JSON.stringify(Input),
                dataType: "json",
                success: function (response) {
                    onAddSuccess(this);
                },
                failure: function (response) {
                    $('#divloader').modal('hide');
                    messagePopup('Failure : ' + response);
                },
                error: function (response) {
                    $('#divloader').modal('hide');
                    messagePopup('Error occurred while addind record.');
                }
            });
        }
    }
}

var convertDateFormat = function (inputdate) {
    var newdate = null;

    if (inputdate != null) {
        var date = inputdate;
        var d = new Date(date.split("/").reverse().join("-"));
        var dd = d.getDate();
        var mm = d.getMonth() + 1;
        var yy = d.getFullYear();
        newdate = yy + "/" + mm + "/" + dd;
    }
    return newdate;
}

var convertDateToYMD = function (inputdate, splitter) {
    var newdate = null;

    if (inputdate != null) {
        var date = inputdate;
        var d = new Date(date.split("/").reverse().join("-"));
        var dd = d.getDate();
        dd = (dd < 10) ? "0" + dd : dd;

        var mm = d.getMonth() + 1;
        mm = (mm < 10) ? "0" + mm : mm;

        var yy = d.getFullYear();
        newdate = yy + splitter + mm + splitter + dd;
    }
    return newdate;
}


var onAddSuccess = function (id) {

    var $tbodyActiveShiftBatch = $('#tbodyActiveShiftBatch');

    lastIndex++;
    $tbodyActiveShiftBatch.append(viewShiftBatch((lastIndex), objShift) + editShiftBatch((lastIndex), objShift));
    regEvents();
    getShiftList();
    getActiveBatchID();
    // closeRow(id);
    //Reload(false);
    clearShiftSelection(id);
    arrShift = [];
    $('.cancelclosedes').click();
    //$('#btnchange').click();
    $('#divloader').modal('hide');


}

var onSave = function (id) {

    var isValid = validate(false, id);
    if (isValid) {
    
        var $isActive = $(id).closest('tr').find('.ckhActive').prop('checked');
        var $fromDate = $(id).closest('tr').find('.dtfrom').val();
        var $toDate = $(id).closest('tr').find('.dtto').val();
        var $id = $(id).closest('tr').find('.hfShiftApprovalBatch').val();
        var $lastApprovalRequestOn = $(id).closest('tr').find('.hfLastReqSent').val();
        var $lastReminderOn = $(id).closest('tr').find('.hfLastRemSend').val();

        var isConfirm = checkIsActive($isActive, $id);
        if (isConfirm != 'NO') {
            $('#divloader').modal();
            if (isConfirm) { deactivateCurrentShift(); }

            fillObj(id, $id, $fromDate, $toDate, $isActive, $lastApprovalRequestOn, $lastReminderOn);

            var obj = JSON.stringify(objShift);

            var Input = { 'jsonStringShiftApprovalBatch': obj };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ESS1973.aspx/UpdateBatch",
                data: JSON.stringify(Input),
                dataType: "json",
                success: function (response) {
                    closeRow(id);
                    updateRow(id);
                    arrShift = [];
                    $('#divloader').modal('hide');

                    messagePopup('Record updated successfully.');
                    getActiveBatchID();
                    getShiftList();
                    //Reload(true);


                },
                failure: function (response) {
                    $('#divloader').modal('hide');
                    messagePopup('Unable to update record.');
                },
                error: function (response) {
                    $('#divloader').modal('hide');
                    messagePopup('Error occurred while updating record.');
                }
            });
        }
        else { return; }
    }
}

var updateRow = function (id) {
    var shiftStatus = $(id).closest('tr').find('.ckhActive').val();

    var dtFrom = $(id).closest('tr').find('.dtfrom').val();
    var dtTo = $(id).closest('tr').find('.dtto').val();

    $(id).closest('.opentr').prev().find('.clsfromdate').html(dtFrom);
    $(id).closest('.opentr').prev().find('.clstodate').html(dtTo);

    $(id).closest('.opentr').prev().find('.shiftName').html(shiftData);
    $(id).closest('.opentr').prev().find('.shiftStatus').html(shiftStatus);
}

var fillObj = function (id, shiftApprovalBatchID, fromDate, toDate, isActive, lastApprovalRequestOn, lastReminderOn) {
    var arrSelected = [];

    jQuery.grep(arrShiftList, function (value) {
        if (jQuery.inArray(value.ShiftCode, arrShift) != -1) arrSelected.push(value);
    });

    arrSelected.forEach(function (e) { delete e.IsSelected });

    if (shiftApprovalBatchID > 0) {
        var CreatedOn = $(id).closest('tr').find('.hfCreatedOn').val();
        var CreatedBy = $(id).closest('tr').find('.hfCreatedBy').val();

        objShift.CreatedBy = CreatedBy;
        objShift.CreatedOn = CreatedOn;
    }

    objShift.ShiftApprovalBatchID = shiftApprovalBatchID;
    objShift.FromDate = convertDateFormat(fromDate);
    objShift.ToDate = convertDateFormat(toDate);

    lastApprovalRequestOn = lastApprovalRequestOn == "NULL" ? null : lastApprovalRequestOn;
    lastReminderOn = lastReminderOn == "NULL" ? null : lastReminderOn;


    objShift.LastApprovalRequestOn = convertDateFormat(lastApprovalRequestOn);
    objShift.LastReminderOn = convertDateFormat(lastReminderOn);

    objShift.ApplicableShifts = arrSelected;
    objShift.IsActive = isActive;
}

var deactivateCurrentShift = function () {

    var $batchID = activeBatchID;
    var Input = { batchID: $batchID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1973.aspx/DeactivateBatch",
        data: JSON.stringify(Input),
        dataType: "json",
        success: function (response) {
            activeBatchID = response;
        },
        failure: function (response) {
            messagePopup('Some thing went wrong!');
        },
        error: function (response) {
            alert('Error : ' + response);
        }
    });

}

var clearShiftSelection = function (id) {
    $(id).closest('tr').find('input[type="checkbox"]').removeAttr('checked');
}

var bindShift = function (response) {
    if (response == null)
    {
        return;
    }

    var html = '';
    var shift = arrShiftList;
    var selectedShift = response;

    var arrShiftCode = selectedShift.map(function (a) { return a.ShiftCode; });

    jQuery.grep(shift, function (value) {
        if (jQuery.inArray(value.ShiftCode, arrShiftCode) == -1) {
            value["IsSelected"] = false;
        }
        else {
            value["IsSelected"] = true;
        }
    });

    $(shift).each(function () {
        html += '';
        if (this.IsSelected)
            html += '<label> <input type="checkbox" onChange="editchkChange(this);"  checked class="chkShift" value="' + this.ShiftCode + '"/>' + this.ShiftName + '</label>';
        else
            html += '<label> <input type="checkbox" onChange="editchkChange(this);"  class="chkShift" value="' + this.ShiftCode + '"/>' + this.ShiftName + '</label>';
    });
    return html;
}

var bindShifttoAdd = function () {
    var $divShift = $("#divShift");
    var html = '';
    var shift = arrShiftList;

    $(shift).each(function () {
        html += '' +
       '<label> <input type="checkbox" onChange="addchkChange(this);" class="addchkShift" value="' + this.ShiftCode + '"/>' + this.ShiftName + '</label>';
    });

    $("#divShift").html(html);

}

var addchkChange = function (id) {
  
    var shift = $(id).val();

    var count = arrShift.length;

    arrShift = jQuery.grep(arrShift, function (value) {
        return value != shift;
    });

    if (count == arrShift.length)
        arrShift.push($(id).val());

    shiftData = arrShift.join(',');
}

var editchkChange = function (id) {

    var shift = $(id).val();

    var count = arrShift.length;

    arrShift = jQuery.grep(arrShift, function (value) {
        return value != shift;
    });

    if (count == arrShift.length)
        arrShift.push($(id).val());

    shiftData = arrShift.join(',');
}

var sendReqToManager = function () {
    $("#divloader").modal();
    var Input = { batchID: activeBatchID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1973.aspx/SendRequestToManagers",
        data: JSON.stringify(Input),
        dataType: "json",
        success: function (response) {
            loadHeader();
            loadManagerView();
            messagePopup('Request has been sent to managers.');
            $("#divloader").modal('hide');
        },
        failure: function (response) {
            messagePopup('Some thing went wrong!');
        },
        error: function (response) {
            messagePopup('Unable to send request to manager.');
        }
    });
}

var sendReminder = function () {
    $("#divloader").modal();
    var $batchID = activeBatchID;
    var Input = { batchID: $batchID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1973.aspx/SendReminder",
        data: JSON.stringify(Input),
        dataType: "json",
        success: function (response) {
            loadHeader();
            messagePopup('Reminder has been sent to managers who have not approved shift records');
            $("#divloader").modal('hide');
        },
        failure: function (response) {
            messagePopup('Some thing went wrong!');
        },
        error: function (response) {
            messagePopup('Unable to send reminder.');
        }
    });
}

var updateEmailTemplate = function () {

    var emailBody = editor.getEditorValue();
    var $subject = $("#txtSubject").val();
    var isValid = validateTemplate();

    if (isValid) {
        var Input = { emailTemplateConstant: globaltemplateConstant, emailBody: emailBody, emailSubject: $subject };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "ESS1973.aspx/UpdateEmailTemplate",
            data: JSON.stringify(Input),
            dataType: "json",
            success: function (response) {
                messagePopup('Email template updated successfully.');
                //editor.val('');
                $("#emailTemplate").modal('hide');
            },
            failure: function (response) {
                messagePopup('Some thing went wrong!');
            },
            error: function (response) {
                messagePopup('Unable to load email template.');
            }
        });
    }
}

var getEmailTemplate = function (templateConstant) {

    var Input = { emailTemplateConstant: templateConstant, loadDefaultTemplate: false };

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1973.aspx/GetEmailTemplate",
        data: JSON.stringify(Input),
        dataType: "json",
        success: function (response) {
            var template = JSON.parse(response.d);
            editor.val(template.EmailBody);
            $("#txtSubject").val(template.EmailSubject);
            $('#emailTemplate').modal();
        },
        failure: function (response) {
            messagePopup('Some thing went wrong!');
        },
        error: function (response) {
            messagePopup('Unable to load email template.');
        }
    });
}

var defaultEmailTemplate = function () {

    var Input = { emailTemplateConstant: globaltemplateConstant, loadDefaultTemplate: true };

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1973.aspx/GetEmailTemplate",
        data: JSON.stringify(Input),
        dataType: "json",
        success: function (response) {
            var template = JSON.parse(response.d);
            editor.val(template.EmailBody);
            $("#txtSubject").val(template.EmailSubject);
        },
        failure: function (response) {
            messagePopup('Some thing went wrong!');
        },
        error: function (response) {
            messagePopup('Unable to load email template.');
        }
    });
}



var regEvents = function () {

    /* for parent tr hide 9 jan 2017**/
    $('.edit').on('click', function () {
        $(this).closest('tr').hide();
        $('.hiddenRow').css('border-top', '0px')
    })
    /* for parent tr hide 9 jan 2017**/

    $('.collapse').on('show.bs.collapse', function () {
        $('.collapse.in').collapse('hide');
        $(this).closest('tr').addClass('opentr').removeClass('closetr');
        $('.opentr').prev().addClass('opentrp').removeClass('closetrp');

        /* for parent tr hide 9 jan 2017**/
        $('.edit').on('click', function () {
            $(this).closest('tr').hide();
            $('.hiddenRow').css('border-top', '0px')
        })
        /* for parent tr hide 9 jan 2017**/

        $('.opentrp').find('.fa').addClass('fa-minus-circle').removeClass('fa-plus-circle');

    }).on('hidden.bs.collapse', function () {
        $(this).closest('tr').addClass('closetr').removeClass('opentr')
        $('.closetr').prev().addClass('closetrp').removeClass('opentrp');
        $('.closetrp').find('.fa').addClass('fa-plus-circle').removeClass('fa-minus-circle');

    });

    $('.cancelclosedes').click(function () {
        arrShift = [];
        $(this).parentsUntil('.hiddenRow').addClass('sedr');
        $('.accordian-body.sedr').collapse('hide');

        /* for parent tr hide 9 jan 2017**/
        $(this).closest('.hiddenRow').parent('tr').prev('tr').addClass('newshowhide').show();
        /* for parent tr hide 9 jan 2017**/

    })
   
    

    $('.ckhActive').change(function () {
        if ($(this).prop('checked')) {
            shiftStatus = 'Active';
            $(this).val(shiftStatus);
        }
        else {
            shiftStatus = 'Deactived';
            $(this).val(shiftStatus);
        }
    });

    $('.email-template').on('click', function () {
        var template = $(this).attr('title');
        globaltemplateConstant = template;
        getEmailTemplate(template);
    })

    var date = new Date();
    $('.datepicker').datetimepicker({
        defaultDate: date,
        format: 'DD/MM/YYYY'
    });

    $("#btndownload").click(function (e) {
        e.preventDefault();

        ////getting data from our table
        //var data_type = 'data:application/vnd.ms-excel';
        //var table_div = document.getElementById('divDownload');
        //var table_html = table_div.outerHTML.replace(/ /g, '%20');

        //var a = document.createElement('a');
        //a.href = data_type + ', ' + table_html;
        //var fileName = $('#hfBatchFrom').val() + '-' + $('#hfBatchTo').val() + '.xls';
        //a.download = fileName
        //a.click();

        var Input = { batchID: activeBatchID };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "ESS1973.aspx/DownloadShiftApprovalForPayroll",
            data: JSON.stringify(Input),
            dataType: "json",
            success: function (response) {
                var fileName = response.d;
                var iframe = document.getElementById('invisible');
                iframe.src = fileName;

                //var dlink = document.createElement('a');
                //dlink.download = name;
                //dlink.href = fileName; //window.URL.createObjectURL(fileName);
                //dlink.onclick = function (e) {
                //    // revokeObjectURL needs a delay to work properly
                //    var that = this;
                //    setTimeout(function () {
                //        window.URL.revokeObjectURL(that.href);
                //    }, 1500);
                //};

                //dlink.click();
                //dlink.remove();
            },
            failure: function (response) {
                messagePopup('Some thing went wrong!');
            },
            error: function (response) {
                messagePopup('Unable to send request to manager.');
            }
        });
    });
}

var DownloadBatchDetails = function () {
    var Input = { batchID: activeBatchID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ESS1973.aspx/Download",
        data: JSON.stringify(Input),
        dataType: "json",
        success: function (response) {

        },
        failure: function (response) {
            messagePopup('Some thing went wrong!');
        },
        error: function (response) {
            messagePopup('Unable to send request to manager.');
        }
    });
}

var getShift = function (id) {
  
    var shiftlist = $(id).closest('tr').find('.shiftName').html();

    if (shiftlist != "") {
        {
            arrShift = shiftlist.split(",");
            preserveData(id);
        }
        shiftData = arrShift.join(',');
    }
}

var preserveData = function (id) {

    var $isActive = $(id).closest('tr').find('.shiftStatus').html();
    var $fromDate = $(id).closest('tr').find('.clsfromdate').html();
    var $toDate = $(id).closest('tr').find('.clstodate').html();

    tempObj.FromDate = $fromDate;
    tempObj.ToDateText = $toDate;
    tempObj.FromDateText = $fromDate;
    tempObj.ToDate = $toDate;
    tempObj.ApplicableShifts = arrShift;
    tempObj.IsActive = $isActive;
}

var resetData = function (id) {

    if (tempObj.IsActive == 'Active')
        $(id).closest('tr').find('.ckhActive').attr('checked');
    else
        $(id).closest('tr').find('.ckhActive').removeAttr('checked');

    $(id).closest('tr').find('.dtfrom').val(tempObj.FromDate);
    $(id).closest('tr').find('.dtto').val(tempObj.ToDate);

    onCancelSave(id);
}

var closeRow = function (id) {
    $(id).parentsUntil('.hiddenRow').addClass('sedr');
    $('.accordian-body.sedr').collapse('hide');
}

var checkIsActive = function ($isActive, batchID) {

    if ($isActive && activeBatchID != batchID && activeBatchID != 0) {
        if (confirm('Current active batch will be deactivated. do you want to continue?')) {
            return true;
        } else {
            return 'NO';
        }
    }
    else {
        return false;
    }
}

var validate = function (isAdd, id) {
  
    $dtfrom = $('#dtfrom').val();
    $dtto = $('#dtto').val();

    if (isAdd) {
        var startDate = $('#dtfrom0').val();
        var endDate = $('#dtto0').val();

        var sdate = startDate.split('/');
        var edate = endDate.split('/');

        endDate = new Date(edate[2], edate[1] - 1, edate[0]);
        endDate = endDate.getTime();

        startDate = new Date(sdate[2], sdate[1] - 1, sdate[0]);
        startDate= startDate.getTime();

        if (endDate < startDate) {
            messagePopup("To date should be greater than From date");
            return false;
        }
    }
    else {
        var startDate = $(id).closest('tr').find('.dtfrom').val();
        var endDate = $(id).closest('tr').find('.dtto').val();

        var sdate = startDate.split('/');
        var edate = endDate.split('/');

          endDate = new Date(edate[2], edate[1] -1, edate[0]);
          endDate = endDate.getTime();

          startDate = new Date(sdate[2], sdate[1] - 1, sdate[0]);
          startDate = startDate.getTime();
    
        if (endDate < startDate) {
            messagePopup("To date should be greater than From date");
            return false;
        }
    }

    if ($dtfrom == "") {
        messagePopup('From date can not be empty');
        return false;
    } else if ($dtto == "") {
        messagePopup('To date can not be empty');
        return false;
    } else if (arrShiftList.length == 0)
    {
        messagePopup("The shifts are not configured in shift group");
        return false;
    }else if (arrShift.length == 0) {
        messagePopup('Please select atleast one shift.');
        return false;
    }
    else {
        return true;
    }
}

var messagePopup = function (msgText) {
    $('#message').html(msgText);
    $('#alert').modal();
}

var validateTemplate = function () {
    if ($("#txtSubject").val() == "") {
        messagePopup("Email subject cannot be empty.")
        return false;
    } else if (editor.val() == "") {
        messagePopup("Email body cannot be empty.")
        return false;
    }
    else {
        return true;
    }
}

var onCancelSave = function (id) {

    var $divShift = $(id).closest('tr').find('.shiftdiv');

    var html = '';
    var shift = arrShiftList;

    var arrShiftCode = tempObj.ApplicableShifts;

    jQuery.grep(shift, function (value) {
        if (jQuery.inArray(value.ShiftCode, arrShiftCode) == -1) {
            value["IsSelected"] = false;
        }
        else {
            value["IsSelected"] = true;
        }
    });

    $(shift).each(function () {
        html += '';
        if (this.IsSelected)
            html += '<label> <input type="checkbox" onChange="editchkChange(this);"  checked class="chkShift" value="' + this.ShiftCode + '"/>' + this.ShiftName + '</label>';
        else
            html += '<label> <input type="checkbox" onChange="editchkChange(this);"  class="chkShift" value="' + this.ShiftCode + '"/>' + this.ShiftName + '</label>';
    });

    $divShift.html(html);
}



