﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TMS0102.aspx.cs" Inherits="LocalESS.Pages.TMS0102" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

     <!-- bootstrap-->
    <link rel="shortcut icon" type="image/x-icon" href="et-uiassets/images/faviconn.ico"/>
    <link href="et-uiassets/css/et-style.css" rel="stylesheet" />
    <script type="text/javascript" src="et-uiassets/js/jquery.js"></script>
    <script type="text/javascript" src="et-uiassets/js/bootstrap.js"></script>
    <script type="text/javascript" src="et-uiassets/js/common.js"></script>
     <!-- bootstrap-->

    <script src="et-uiassets/texteditor/ckeditor.js"></script>
    <script src="et-uiassets/texteditor/samples/js/sample.js"></script>
    <link rel="stylesheet" href="et-uiassets/texteditor/samples/css/samples.css" />
    <link rel="stylesheet" href="et-uiassets/texteditor/samples/toolbarconfigurator/lib/codemirror/neo.css" />

    <!-- date time picker -->
    <link href="et-uiassets/datetimepicker/datetimepicker.css" rel="stylesheet" />
    <script src="et-uiassets/datetimepicker/moment.js" type="text/javascript"></script>
    <script src="et-uiassets/datetimepicker/datetimepicker.js" type="text/javascript"></script>
    <!-- date time picker -->


    <!-- range slider -->
    <link href="et-uiassets/rangleslider/bootstrap-slider.css" rel="stylesheet" />
    <link href="et-uiassets/rangleslider/jquerysctipttop.css" rel="stylesheet" />
    <script src="et-uiassets/rangleslider/bootstrap-slider.js" type="text/javascript"></script>
    <!-- range slider -->
    
        <script type="text/javascript">

            //$(document).ready(function () {
            //    $('#finYear').prop('disabled', true).trigger("chosen:updated");
            //    $('#btnSwitch').prop('checked', false);
            //    LoadFinYear();
            //    SetMonth();
            //    $("#btnSaveMyTab1").click(function () {
            //        SaveMyTab1();
            //    });
            //    $("#btnSaveMyTab2").click(function () {
            //        SaveMyTab2();
            //    });
            //})
        </script>
</head>
<body>
    <form id="form1" runat="server">
       
         <et-mainpagecontent>
             <et-breadcrumb name="breadcrumbs">
	<et-breadcrumb-item >Code Files</et-breadcrumb-item>
	<et-breadcrumb-item>Dashboard</et-breadcrumb-item>
</et-breadcrumb>
		
		
		<et-list-card-seven>
		
			<et-content-area-5 class="content-area">
				<et-contentbgcolor-white>
					<et-list-card-seven-set>
						<et-content-body>												
							<div class="clearfix">Customer</div>
							<div class="clearfix"><a href="javascript: window.location.href = 'TMS0103.aspx'" class="applycomoff">&#x2192;    Click Here</a></div>
						</et-content-body>
					</et-list-card-seven-set>
				</et-contentbgcolor-white>												
			</et-content-area-5>

			<et-content-area-5 class="content-area">
				<et-contentbgcolor-white>
					<et-list-card-seven-set>
						<et-content-body>													
							<div class="clearfix">Projects  </div>
							<div class="clearfix"><a href="javascript:window.location.href = 'TMS0104.aspx'" class="applycomoff">&#x2192;    Click Here</a></div>
						</et-content-body>
					</et-list-card-seven-set>
				</et-contentbgcolor-white>												
			</et-content-area-5>

			<et-content-area-5 class="content-area">
				<et-contentbgcolor-white>
					<et-list-card-seven-set>
						<et-content-body>													
							<div class="clearfix">Task </div>
							<div class="clearfix"><a href="javascript:window.location.href = 'TMS0105.aspx'" class="applycomoff">&#x2192;    Click Here</a></div>
						</et-content-body>
					</et-list-card-seven-set>
				</et-contentbgcolor-white>												
			</et-content-area-5>

			<et-content-area-5 class="content-area">
				<et-contentbgcolor-white>
					<et-list-card-seven-set>
						<et-content-body>													
							<div class="clearfix">Project team </div>
							<div class="clearfix"><a href="javascript:window.location.href = 'TMS0106.aspx'" class="applycomoff">&#x2192;    Click Here</a></div>
						</et-content-body>
					</et-list-card-seven-set>
				</et-contentbgcolor-white>												
			</et-content-area-5>

		
		</et-list-card-seven>
             </et-mainpagecontent>
    </form>
</body>
</html>
