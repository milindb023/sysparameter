﻿using ESSLib.Data.DataObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SysParameter
{
    public partial class TMS0103 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string GetCustomer(string FromRow, string ToRow)
        {
            ResponseObject response = null;
            try
            {
                Customers oCustomers = new Customers();
                DataTable dt = oCustomers.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow));
                response = new ResponseObject(true, "Data obtained",  dt);// JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                response = new ResponseObject(false, ex.Message);
            }
            return JsonConvert.SerializeObject(response);
        }

        [WebMethod]
        public static string SortCustomerCode(string FromRow, string ToRow, string Sort)
        {
            ResponseObject response = null;
            try
            {
                Customers oCustomers = new Customers();
                DataTable dt = null;
                if (Sort == "true")
                    dt = oCustomers.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), true);
                else
                    dt = oCustomers.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), false);
                response = new ResponseObject(true, "Data obtained", dt);// JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                response = new ResponseObject(false, ex.Message);
            }
            return JsonConvert.SerializeObject(response);
        }

        [WebMethod]
        public static string SortCustomerName(string FromRow, string ToRow, string Sort)
        {
            ResponseObject response = null;
            try
            {
                Customers oCustomers = new Customers();
                DataTable dt = null;
                if (Sort == "true")
                    dt = oCustomers.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), false, true);
                else
                    dt = oCustomers.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), false, false);
                response = new ResponseObject(true, "Data obtained", dt);// JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                response = new ResponseObject(false, ex.Message);
            }
            return JsonConvert.SerializeObject(response);
        }

        [WebMethod]
        public static string GetSearchData(string FromRow, string ToRow, string SearchText)
        {
            ResponseObject response = null;
            try
            {
                Customers oCustomers = new Customers();
                DataTable dt = null;
                if (SearchText != "")
                    dt = oCustomers.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), false, false, SearchText);
                else
                    dt = oCustomers.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow));
                response = new ResponseObject(true, "Data obtained", dt);// JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                response = new ResponseObject(false, ex.Message);
            }
            return JsonConvert.SerializeObject(response);
        }

        [WebMethod]
        public static string SaveData(string Ccode, string Cname)
        {
            string result = string.Empty;
            Ccode = Ccode.ToUpper();
            Customers oCustomers = Customers.Load(Ccode);
            if (oCustomers == null)
            {
                oCustomers = new Customers();
                oCustomers.CustomerCode = Ccode;
                oCustomers.CustomerName = Cname;
                oCustomers.Save();
                result = "true";
            }
            else
            {
                result ="false"; 
            }
            return result;
        }

        [WebMethod]
        public static string DeleteCustomer(string Ccode)
        {
            string result = string.Empty;
            Ccode = Ccode.ToUpper();
            Customers oCustomers = Customers.Load(Ccode);
            if (oCustomers != null)
            {
                oCustomers.Delete();
                result = "true";
            }
            else
            {
                result = "false";
            }
            return result;
        }

        [WebMethod]
        public static string SaveEditData(string CusCode, string CusName)
        {
            string result = string.Empty;
            CusCode = CusCode.ToUpper();
            Customers oCustomers = Customers.Load(CusCode);
            if (oCustomers != null)
            {
                oCustomers.CustomerName = CusName;
                oCustomers.Update();
                result = "true";
            }
            else
            {
                result = "false";
            }
            return result;
        }
    }





    public class ResponseObject
    {
        public bool SUC { get; set; }
        public string MSG { get; set; }
        public object GenericObject { get; set; }

        public ResponseObject()
        {
        }

        public ResponseObject(bool success, string message, object genericObject = null)
        {
            this.SUC = success;
            this.MSG = message;
            this.GenericObject = genericObject;
        }
    }
}