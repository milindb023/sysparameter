﻿var mypager = new etPager();
var SortableCode = false;
var SortableName = false;
var id = 0;


var rtemplate = '<tr><td class="et-font-size-12">{CustomerCode}</td>' +
                '<td class="et-font-size-12">{CustomerName}</td>' +
                '<td class="et-text-right">' +
                '<a  class=""  ' +
                '   onclick="return showETDeleteConirmation(\'DeleteCustomer(\\\'{CustomerCode}\\\')\', null)"' +
                '   id="de{j}" ><i class="et-delete3 et-font-size-20 mar-right-15"></i></a>' +
                '<a data-toggle="collapse" class="" data-parent="#accordion" ' +
                '   onclick="SetValues({code:\'{CustomerCode}\', value:\'{CustomerName}\'});"' +
                '   id="hf{j}" href="#ff{j}"><et-edit-row></et-edit-row></a></td></tr>' +
                '<tr class="hiddenrow">' +
                    '<td colspan="15">' +
                        '<et-content-body id="ff{j}" class="panel-collapse collapse et-hdden-row">' +
                        '</et-content-body>' +
                    '</td>' +
                '</tr>'
debugger
listAll("TMS0103.aspx/GetCustomer", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows }), clearCustomers, bindCustomer, noCustomer, "tblCustomer");
debugger
function clearCustomers() {
}

function noCustomer() {
    var noData = "<tr class='searchtr'>" +
       "<td colspan='15' class='et-text-center et-text-danger'>No Records Found</td>" +
       "</tr>";
    $("#tblCustomer > tbody").append(noData);
}

function bindCustomer(cust) {

    var tr1 = rtemplate.replace("{CustomerCode}", cust.CustomerCode);
    tr1 = tr1.replace("{CustomerCode}", cust.CustomerCode);
    tr1 = tr1.replace("{CustomerName}", cust.CustomerName);
    tr1 = tr1.replace("{CustomerName}", cust.CustomerName);
    tr1 = tr1.replace("{CustomerCode}", cust.CustomerCode);
    tr1 = tr1.replace("{CustomerName}", cust.CustomerName);
    tr1 = tr1.replace("{j}", id);
    tr1 = tr1.replace("{j}", id);
    tr1 = tr1.replace("{j}", id);
    tr1 = tr1.replace("{j}", id);
    $("#custbody").append(tr1);
    id++;
}

function DeleteCustomer(id) {
    debugger
    asyncCall('TMS0103.aspx/DeleteCustomer', JSON.stringify({ 'Ccode': id}), DeleteRow);
    return true;
}

function DeleteRow(result) {
    if (result == "true") {
        toastr.success("Customer deleted successfully.");// alert("Customer deleted successfully.");
        listAll("TMS0103.aspx/GetCustomer", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows }), clearCustomersRows, bindCustomer, noCustomer, "tblCustomer");
        return true;
    }
}

function SetValues(detail) {
    $('.customcode').val(detail.code);
    $('.customname').val(detail.value);
    $('.customcode').attr('readonly', true);
    $('.customcode').css('background-color', 'lightgrey');
}

function SaveData(obj, div) {
    if (SaveDataPopUp(obj, div)) {
        asyncCall('TMS0103.aspx/SaveData', JSON.stringify({ 'Ccode': document.getElementById('txtCustCodePopUp').value, 'Cname': document.getElementById('txtCustNamePopUp').value }), SaveReult);
    }
    //  formvalidation(obj.id, "txtCustCodePopUp", "txtCustNamePopUp");
    //if (document.getElementById('txtCustCodePopUp').value == "" || document.getElementById('txtCustNamePopUp').value == "") {
    //    alert("Please enter customer code and customer name.");
    //    return false;
    //}
    //else {
    //     asyncCall('TMS0103.aspx/SaveData', JSON.stringify({ 'Ccode': document.getElementById('txtCustCodePopUp').value, 'Cname': document.getElementById('txtCustNamePopUp').value }), SaveReult);
    //}
}

function SaveReult(result) {
    if (result == "true") {
        $('#myModal').modal('hide');
        toastr.success("Customer saved successfully.");
        listAll("TMS0103.aspx/GetCustomer", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows }), clearCustomersRows, bindCustomer, noCustomer, "tblCustomer");
        return true;
    }
    else {
        toastr.warning(document.getElementById('txtCustCodePopUp').value + " customer code is already exist.");
        return false;
    }
}

function SaveEditCustomer() {
    var CustomerCode = $('.customcode').val();
    var CustomerName = $('.customname').val();

    if (CustomerName == "") {
        toastr.warning("Please enter customer name.");
    }
    else {
        asyncCall('TMS0103.aspx/SaveEditData', JSON.stringify({ 'CusCode': CustomerCode, 'CusName': CustomerName }), SaveEditedCustomer);
    }
}

function SaveEditedCustomer(result) {
    if (result == "true") {
        listAll("TMS0103.aspx/GetSearchData", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'SearchText': document.getElementById('txtSearchText').value }), clearCustomersRows, bindCustomer, noCustomer, "tblCustomer");
        toastr.success("Customer data updated successfully.");
    }
}

function clearCustomersRows() {
    $("#custbody").find('.edittablefrom').css({ 'display': 'none', 'position': 'absolute' }).insertBefore($("#tblCustomer").parent().find('.editrft'))
    $("#custbody tr").remove();
}

function NextPage() {
    SortableCode = false;
    SortableName = false;
    mypager.fromRowsStart = mypager.fromRowsStart + mypager.endIndex;
    mypager.toRows = mypager.endIndex + mypager.toRows;
    mypager.totalRowsCount = parseInt(mypager.totalRowsCount);
    listAll("TMS0103.aspx/GetSearchData", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'SearchText': document.getElementById('txtSearchText').value }), clearCustomersRows, bindNextPageCustomer, noCustomer, "tblCustomer");
}

function bindNextPageCustomer(cust) {
    closedit();
    bindCustomer(cust);
    if (mypager.toRows >= mypager.totalRowsCount) {
        document.getElementById('PageCount').innerHTML = mypager.fromRowsStart + "-" + mypager.totalRowsCount + " of ";
        $("a.next").hide();
    }
    else {
        document.getElementById('PageCount').innerHTML = mypager.fromRowsStart + "-" + mypager.toRows + " of ";
        $("a.next").show();
    }
}

function PrevPage() {
    SortableCode = false;
    SortableName = false;
    mypager.fromRowsStart = mypager.fromRowsStart - mypager.endIndex;
    mypager.toRows = mypager.toRows - mypager.endIndex;
    listAll("TMS0103.aspx/GetSearchData", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'SearchText': document.getElementById('txtSearchText').value }), clearCustomersRows, bindPrevPageCustomer, noCustomer, "tblCustomer");
    document.getElementById('PageCount').innerHTML = mypager.fromRowsStart + "-" + mypager.toRows + " of ";
}

function bindPrevPageCustomer(cust) {
    closedit();
    bindCustomer(cust);
}

function PageNumber(obj) {
    SortableCode = false;
    SortableName = false;
    mypager.fromRowsStart = mypager.startIndex;
    mypager.toRows = parseInt(obj);
    mypager.endIndex = parseInt(obj);

    listAll("TMS0103.aspx/GetSearchData", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'SearchText': document.getElementById('txtSearchText').value }), clearCustomersRows, bindPageSizeCustomer, noCustomer, "tblCustomer");
    document.getElementById('PgIndex').innerHTML = obj;
}

function bindPageSizeCustomer(cust) {
    closedit();
    bindCustomer(cust);
}

function MakeCodeSortable() {

    if (!SortableCode) {
        listAll("TMS0103.aspx/SortCustomerCode", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'Sort': 'true' }), clearCustomersRows, bindCustomer, noCustomer, "tblCustomer");
        SortableCode = true;
    }
    else {
        listAll("TMS0103.aspx/SortCustomerCode", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'Sort': 'false' }), clearCustomersRows, bindCustomer, noCustomer, "tblCustomer");
        SortableCode = false;
    }
}

function MakeNameSortable() {

    if (!SortableCode) {
        listAll("TMS0103.aspx/SortCustomerName", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'Sort': 'true' }), clearCustomersRows, bindCustomer, noCustomer, "tblCustomer");
        SortableCode = true;
    }
    else {
        listAll("TMS0103.aspx/SortCustomerName", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'Sort': 'false' }), clearCustomersRows, bindCustomer, noCustomer, "tblCustomer");
        SortableCode = false;
    }
}

function SearchResult(obj) {
    
    mypager.fromRowsStart = mypager.startIndex;
    mypager.toRows = mypager.endIndex;
    listAll("TMS0103.aspx/GetSearchData", JSON.stringify({ 'FromRow': mypager.startIndex, 'ToRow': mypager.endIndex, 'SearchText': obj.value }), clearCustomersRows, bindCustomer, noCustomer, "tblCustomer");
}

function validateCustCode(obj) {
    if (obj.id == "txtCustCodePopUp") {
        if (obj.value != "")
            return true;
        else
            return false;
    }

    if (obj.id == "txtCustNamePopUp") {
        if (obj.value != "")
            return true;
        else
            return false;
    }
}
