﻿using ESSLib.Business.BusinessObjects;
using ESSLib.Data.DataObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SysParameter
{
    public partial class TMS0104 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                if (Request.Form["hdnNextActionProject"] != "" && Request.Form["hdnNextActionProject"] == "manage tasks")
                {
                    var selectedProject = Request.Form["hdnSelectedProject"];
                    Session["Project"] = selectedProject;
                    hdnNextActionProject.Value = "";
                    Server.Transfer("TMS0105.aspx");
                }
                if (Request.Form["hdnNextActionEmployee"] != "" && Request.Form["hdnNextActionEmployee"] == "manage employee")
                {
                    var selectedProject = Request.Form["hdnSelectedEmployeeProject"];
                    Session["Employee"] = selectedProject;
                    hdnNextActionEmployee.Value = "";
                    Server.Transfer("TMS0106.aspx");
                }
            }
        }

        [WebMethod]
        public static string GetProject(string FromRow, string ToRow)
        {
            ResponseObjectProject response = null;
            try
            {
                Projects oProjects = new Projects();
                DataTable dt = oProjects.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow));
                response = new ResponseObjectProject(true, "Data obtained",  dt);// JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                response = new ResponseObjectProject(false, ex.Message);
            }
            return JsonConvert.SerializeObject(response);
        }

        [WebMethod]
        public static string SortProjectCode(string FromRow, string ToRow, string Sort)
        {
            ResponseObjectProject response = null;
            try
            {
                Projects oProjects = new Projects();
                DataTable dt = null;
                if (Sort == "true")
                    dt = oProjects.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), true);
                else
                    dt = oProjects.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), false);
                response = new ResponseObjectProject(true, "Data obtained", dt);// JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                response = new ResponseObjectProject(false, ex.Message);
            }
            return JsonConvert.SerializeObject(response);
        }

        [WebMethod]
        public static string SortProjectName(string FromRow, string ToRow, string Sort)
        {
            ResponseObjectProject response = null;
            try
            {
                Projects oProjects = new Projects();
                DataTable dt = null;
                if (Sort == "true")
                    dt = oProjects.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), false, true);
                else
                    dt = oProjects.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), false, false);
                response = new ResponseObjectProject(true, "Data obtained", dt);// JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                response = new ResponseObjectProject(false, ex.Message);
            }
            return JsonConvert.SerializeObject(response);
        }

        [WebMethod]
        public static string GetSearchData(string FromRow, string ToRow, string SearchText)
        {
            ResponseObjectProject response = null;
            try
            {
                Projects oProjects = new Projects();
                DataTable dt = null;
                if (SearchText != "")
                    dt = oProjects.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), false, false, SearchText);
                else
                    dt = oProjects.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow));
                response = new ResponseObjectProject(true, "Data obtained", dt);// JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                response = new ResponseObjectProject(false, ex.Message);
            }
            return JsonConvert.SerializeObject(response);
        }

        [WebMethod]
        public static string SaveData(string projectCode, string projectName, string custmCode, string projectManager)
        {
            string result = string.Empty;
            projectCode = projectCode.ToUpper();
            Projects oProjects = Projects.Load(projectCode);
            if (oProjects == null)
            {
                oProjects = new Projects();
                oProjects.ProjectCode = projectCode;
                oProjects.ProjectName = projectName;
                oProjects.CustomerCode = custmCode;
                oProjects.PMEmpNo = projectManager;
                oProjects.IsActive = true;
                oProjects.ApprovalTypeID = 1;
                oProjects.Save();
                result = "true";
            }
            else
            {
                result ="false"; 
            }
            return result;
        }

        [WebMethod]
        public static string DeleteProject(string Ccode)
        {
            string result = string.Empty;
            Ccode = Ccode.ToUpper();
            Projects oProjects = Projects.Load(Ccode);
            if (oProjects != null)
            {
                oProjects.Delete();
                result = "true";
            }
            else
            {
                result = "false";
            }
            return result;
        }

        [WebMethod]
        public static string SaveEditData(string ProjectCode, string ProjectName, string custCode, string projManager, string approval, bool isactive)
        {
            string result = string.Empty;
            ProjectCode = ProjectCode.ToUpper();
            Projects oProjects = Projects.Load(ProjectCode);
            if (oProjects != null)
            {
                oProjects.ProjectName = ProjectName;
                oProjects.CustomerCode = custCode;
                oProjects.PMEmpNo = projManager;
                oProjects.ApprovalTypeID = Convert.ToInt16(approval);
                oProjects.IsActive = isactive;
                oProjects.Update();
                result = "true";
            }
            else
            {
                result = "false";
            }
            return result;
        }

        [WebMethod]
        public static ESSLib.Business.DataForSelect[] GetCustomerCodes(int searchPage, string searchFilter)
        {
            ResponseObject response = null;
           
                //var r = CountryData.Instance.GetDataByPage(searchPage, searchFilter);
                //return r;
            DataTable dt1 = Customers.GetCustomersByPage(searchPage, searchFilter);
              //  DataTable dt = Customers.LoadListAsTable(); 
               // response = new ResponseObject(true, "Data obtained",  dt);

            return ESSLib.Business.CommonFunctions.MapDataTable(dt1, dt1.Rows.Count);
        }

        [WebMethod]
        public static string GetEmplyees()
        {
            ResponseObject response = null;
            try
            {
                DataTable dt = ThisCompany.GetEmployees("", "007", ModuleType.PayrollManagement);
                response = new ResponseObject(true, "Data obtained", dt);// JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                response = new ResponseObject(false, ex.Message);
            }
            return JsonConvert.SerializeObject(response);
        }
    }





    public class ResponseObjectProject
    {
        public bool SUC { get; set; }
        public string MSG { get; set; }
        public object GenericObject { get; set; }

        public ResponseObjectProject()
        {
        }

        public ResponseObjectProject(bool success, string message, object genericObject = null)
        {
            this.SUC = success;
            this.MSG = message;
            this.GenericObject = genericObject;
        }
    }
}