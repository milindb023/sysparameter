﻿var mypager = new etPager();
var SortableCode = false;
var SortableName = false;
var id = 0;
var checkApprovaltype = "";

var rtemplate = '<tr><td class="et-font-size-12">{ProjectCode}</td>' +
                '<td class="et-font-size-12">{ProjectName}</td>' +
                '<td class="et-font-size-12">{CustomerCode}</td>' +
                '<td class="et-font-size-12">{PMEmpNo}</td>' +
                '<td class="et-font-size-12">{status}</td>' +
                '<td class="et-text-right">' +
                '<a  class=""  ' +
                '   onclick="return showETDeleteConirmation(\'DeleteProject(\\\'{ProjectCode}\\\')\', null)"' +
                '   id="de{j}" ><i class="et-delete3 et-font-size-20 mar-right-15"></i></a>' +
                '<a data-toggle="collapse" class="" data-parent="#accordion" ' +
                '   onclick="SetValues({code:\'{ProjectCode}\', value:\'{ProjectName}\',customercode:\'{CustomerCode}\', name:\'{Name}\',approvaltypeid:\'{ApprovalTypeID}\', isactive:\'{IaActive}\'});"' +
                '   id="hf{j}" href="#ff{j}"><et-edit-row></et-edit-row></a>' +
                '<et-form-group-block-button-right class="mar-left-15">' +
				'<button type="submit" id=btnEmp{j} class="et-btn et-btn-transparent" onclick="assignProject(\'{ProjectCode}\',\'{ProjectName}\')">Tasks ( {TotalTask} )</button>' +
			    '</et-form-group-block-button-right>'+
                '<et-form-group-block-button-right class="mar-left-15">' +
                '<button type="submit" id=btnTask{j} class="et-btn et-btn-transparent" onclick="assignEmployee(\'{ProjectCode}\',\'{ProjectName}\')">Member ( {TotalEmployee} )</button>' +
                '</et-form-group-block-button-right></td></tr>' +
                '<tr class="hiddenrow">' +
                    '<td colspan="15">' +
                        '<et-content-body id="ff{j}" class="panel-collapse collapse et-hdden-row">' +
                        '</et-content-body>' +
                    '</td>' +
                '</tr>'
$(document).ready(function () {
    listAll("TMS0104.aspx/GetProject", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows }), clearProjects, bindProject, noProject, "tblProject");
    GetCustomerCodes();
  //  GetEmplyees();

});


function assignProject(code, name) {
    
    $('#hdnSelectedEmployeeProject').val('');
    $('#hdnNextActionEmployee').val('');
    $('#hdnSelectedProject').val(code + ',' + name);
    $('#hdnNextActionProject').val('manage tasks'); 
    return true;
}

function assignEmployee(code, name) {
    
    $('#hdnSelectedProject').val('');
    $('#hdnNextActionProject').val('');
    $('#hdnSelectedEmployeeProject').val(code + ',' + name);
    $('#hdnNextActionEmployee').val('manage employee');
    return true;
}

function clearProjects() {
}

function noProject() {
    var noData = "<tr class='searchtr'>" +
       "<td colspan='15' class='et-text-center et-text-danger'>No Records Found</td>" +
       "</tr>";
    $("#tblProject > tbody").append(noData);
}

function bindProject(Project) {
    
    var tr1 = rtemplate.replace("{ProjectCode}", Project.ProjectCode);
    tr1 = tr1.replace("{ProjectCode}", Project.ProjectCode);
    tr1 = tr1.replace("{ProjectCode}", Project.ProjectCode);
    tr1 = tr1.replace("{ProjectName}", Project.ProjectName);
    tr1 = tr1.replace("{ProjectName}", Project.ProjectName);
    tr1 = tr1.replace("{ProjectCode}", Project.ProjectCode);
    tr1 = tr1.replace("{ProjectCode}", Project.ProjectCode);
    tr1 = tr1.replace("{ProjectName}", Project.ProjectName);
    tr1 = tr1.replace("{ProjectName}", Project.ProjectName);
    tr1 = tr1.replace("{CustomerCode}", Project.CustomerCode);

    tr1 = tr1.replace("{CustomerCode}", Project.CustomerCode);
    tr1 = tr1.replace("{PMEmpNo}", Project.Name);
    tr1 = tr1.replace("{Name}", Project.Name);
    tr1 = tr1.replace("{ApprovalTypeID}", Project.ApprovalTypeID);
    tr1 = tr1.replace("{IaActive}", Project.IsActive);
    if (Project.ApprovalTypeID) {
        tr1 = tr1.replace("{checkApprovaltype}", "checked='checked'");
    }
    if (Project.IsActive) {
        tr1 = tr1.replace("{status}", "<et-success-badge><i class='et-tick-9'></i>Active</et-success-badge>");
    }
    else {
        tr1 = tr1.replace("{status}", "<et-danger-badge><i class='et-clear-2'></i>Inactive</et-danger-badge>");
    }
    tr1 = tr1.replace("{j}", id);
    tr1 = tr1.replace("{j}", id);
    tr1 = tr1.replace("{j}", id);
    tr1 = tr1.replace("{j}", id);
    tr1 = tr1.replace("{j}", id);
    tr1 = tr1.replace("{j}", id);
    tr1 = tr1.replace("{TotalTask}", Project.TaskCount);
    tr1 = tr1.replace("{TotalEmployee}", Project.AssignEmployee);
    $("#Projectbody").append(tr1);
    id++;
}

function actionDelete(obj) {
}

function DeleteProject(id) {
    asyncCall('TMS0104.aspx/DeleteProject', JSON.stringify({ 'Ccode': id }), DeleteRow);
    return true;
}

function DeleteRow(result) {
    if (result == "true") {
        toastr.success("Project deleted successfully.");// alert("Project deleted successfully.");
        listAll("TMS0104.aspx/GetProject", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows }), clearProjectsRows, bindProject, noProject, "tblProject");
        return true;
    }
}

function SetValues(detail) {
    
    $('.projectcode').val(detail.code);
    $('.projectname').val(detail.value);
    var customCode = detail.customercode.split('~');
    $('.customer').val(customCode[0]).trigger('chosen:updated');
    var empno = detail.name.split('~');
    $('.projectmanager').val(empno[0]).trigger('chosen:updated');
    if (detail.approvaltypeid == "1")
        $('.approvaltype').val(1).trigger('chosen:updated');
    else
        $('.approvaltype').val(0).trigger('chosen:updated');
    if (detail.isactive == "true")
        $('.status').prop('checked', true);
    else
        $('.status').prop('checked', false);
    $('.projectcode').attr('readonly', true);
    $('.projectcode').css('background-color', 'lightgrey');
}

function bindCustomerCodesForEdit(custCode) {
    //Bind Customer on Edit Form
    $('#ddlCustomerCodeEdit').append($('<option>', {
        value: custCode.CustomerCode,
        text: custCode.CustomerCode + '~' + custCode.CustomerName
    }, '</option>'));
    $('#ddlCustomerCodeEdit').trigger("chosen:updated");
}

function SaveData(obj, div) {
    if (SaveDataPopUp(obj, div)) {
        var projectCode = document.getElementById('txtProjectCodePopUp').value;
        var projectName = document.getElementById('txtProjectNamePopUp').value; 
        var e = document.getElementById('ddlCustomerCode');
        var custmCode = e.options[e.selectedIndex].value;
        var projectManager = document.getElementById('ddlPMEmployeeNo').options[e.selectedIndex].value;
        asyncCall('TMS0104.aspx/SaveData', JSON.stringify({ 'projectCode': projectCode, 'projectName': projectName, 'custmCode': custmCode, 'projectManager': projectManager }), SaveReult);
    }
}

function SaveReult(result) {
    if (result == "true") {
        $('#myModal').modal('hide');
        toastr.success("Project saved successfully.");
        listAll("TMS0104.aspx/GetProject", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows }), clearProjectsRows, bindProject, noProject, "tblProject");
        return true;
    }
    else {
        toastr.warning(document.getElementById('txtProjectCodePopUp').value + " Project code is already exist.");
        return false;
    }
}

function SaveEditProject() {
    
    var projectCode = $('.projectcode').val();
    var projectName = $('.projectname').val();

    var custCode = $('.customer').val();
    var projManager = $('.projectmanager').val(); 
  //  var approval = ($('.approvaltype').prop("checked") === 'true');
   // var approval = $('#chkApprovalType').prop("checked");
    var approval = $('.approvaltype').val();  //$('#chkApprovalType').prop("checked");// $('.approvaltype').val();
    var isactive = $('#chkStatus').is(':checked'); //($('#chkStatus').prop("checked") === 'true'); //$('.status').val();
   // var isactive = $('#chkStatus').prop("checked");

    
    asyncCall('TMS0104.aspx/SaveEditData', JSON.stringify({ 'ProjectCode': projectCode, 'ProjectName': projectName, 'custCode': custCode, 'projManager': projManager, 'approval': approval, 'isactive': isactive }), SaveEditedProject);
}

function SaveEditedProject(result) {
    if (result == "true") {
        listAll("TMS0104.aspx/GetSearchData", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'SearchText': document.getElementById('txtSearchText').value }), clearProjectsRows, bindProject, noProject, "tblProject");
        toastr.success("Project data updated successfully.");
    }
}

function clearProjectsRows() {
    $("#Projectbody").find('.edittablefrom').css({ 'display': 'none', 'position': 'absolute' }).insertBefore($("#tblProject").parent().find('.editrft'))
    $("#Projectbody tr").remove();
}

function NextPage() {
    SortableCode = false;
    SortableName = false;
    mypager.fromRowsStart = mypager.fromRowsStart + mypager.endIndex;
    mypager.toRows = mypager.endIndex + mypager.toRows;
    mypager.totalRowsCount = parseInt(mypager.totalRowsCount);
    listAll("TMS0104.aspx/GetSearchData", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'SearchText': document.getElementById('txtSearchText').value }), clearProjectsRows, bindNextPageProject, noProject, "tblProject");
}

function bindNextPageProject(Project) {
    closedit();
    bindProject(Project);
    if (mypager.toRows >= mypager.totalRowsCount) {
        document.getElementById('PageCount').innerHTML = mypager.fromRowsStart + "-" + mypager.totalRowsCount + " of ";
        $("a.next").hide();
    }
    else {
        document.getElementById('PageCount').innerHTML = mypager.fromRowsStart + "-" + mypager.toRows + " of ";
        $("a.next").show();
    }
}

function PrevPage() {
    SortableCode = false;
    SortableName = false;
    mypager.fromRowsStart = mypager.fromRowsStart - mypager.endIndex;
    mypager.toRows = mypager.toRows - mypager.endIndex;
    listAll("TMS0104.aspx/GetSearchData", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'SearchText': document.getElementById('txtSearchText').value }), clearProjectsRows, bindPrevPageProject, noProject, "tblProject");
    document.getElementById('PageCount').innerHTML = mypager.fromRowsStart + "-" + mypager.toRows + " of ";
}

function bindPrevPageProject(Project) {
    closedit();
    bindProject(Project);
}

function PageNumber(obj) {
    SortableCode = false;
    SortableName = false;
    mypager.fromRowsStart = mypager.startIndex;
    mypager.toRows = parseInt(obj);
    mypager.endIndex = parseInt(obj);

    listAll("TMS0104.aspx/GetSearchData", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'SearchText': document.getElementById('txtSearchText').value }), clearProjectsRows, bindPageSizeProject, noProject, "tblProject");
    document.getElementById('PgIndex').innerHTML = obj;
}

function bindPageSizeProject(Project) {
    closedit();
    bindProject(Project);
}

function MakeCodeSortable() {

    if (!SortableCode) {
        listAll("TMS0104.aspx/SortProjectCode", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'Sort': 'true' }), clearProjectsRows, bindProject, noProject, "tblProject");
        SortableCode = true;
    }
    else {
        listAll("TMS0104.aspx/SortProjectCode", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'Sort': 'false' }), clearProjectsRows, bindProject, noProject, "tblProject");
        SortableCode = false;
    }
}

function MakeNameSortable() {

    if (!SortableCode) {
        listAll("TMS0104.aspx/SortProjectName", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'Sort': 'true' }), clearProjectsRows, bindProject, noProject, "tblProject");
        SortableCode = true;
    }
    else {
        listAll("TMS0104.aspx/SortProjectName", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'Sort': 'false' }), clearProjectsRows, bindProject, noProject, "tblProject");
        SortableCode = false;
    }
}

function SearchResult(obj) {
    
    mypager.fromRowsStart = mypager.startIndex;
    mypager.toRows = mypager.endIndex;
    listAll("TMS0104.aspx/GetSearchData", JSON.stringify({ 'FromRow': mypager.startIndex, 'ToRow': mypager.endIndex, 'SearchText': obj.value }), clearProjectsRows, bindProject, noProject, "tblProject");
}

function validateProjectCode(obj) {
    
    switch (obj.id) {
        case "txtProjectCodePopUp":
            if (obj.value != "")
                return true;
            else
                return false;
            break;
        case "txtProjectNamePopUp":
            if (obj.value != "")
                return true;
            else
                return false;
            break;
        //case "ddlCustomerCode":
        //    if (obj.value != "")
        //        return true;
        //    else
        //        return false;
        //    break;
        case "ddlPMEmployeeNo":
            if (obj.value != "")
                return true;
            else
                return false;
            break;
        case "txtProjectNameEdit":
            if (obj.value != "")
                return true;
            else
                return false;
            break;
        case "ddlCustomerCodeEdit":
            if (obj.value != "")
                return true;
            else
                return false;
            break;
        case "ddlProjectManagerEdit":
            if (obj.value != "")
                return true;
            else
                return false;
            break;
    }

}

// Bind Customer Codes
function GetCustomerCodes() {debugger
    // listAll("TMS0104.aspx/GetCustomerCodes", JSON.stringify(), emptyBind, bindCustomerCodes, emptyBind);
    var countrySelect = new etSelectLoadOnDemand($('#ddlCustomerCode'));
    countrySelect.dataurl = 'TMS0104.aspx/GetCustomerCodes';
   // countrySelect.dataurl = 'TMS0104.aspx/GetCustomerCodes(' + 1 + ',' + countrySelect.control.text() + ')';
    countrySelect.multiple = false;
    countrySelect.init();
}

function emptyBind() {
}
function bindCustomerCodes(custCode) {
    //Bind Customer on ADD Form
    $('#ddlCustomerCode').append($('<option>', {
        value: custCode.CustomerCode,
        text: custCode.CustomerCode+'~'+custCode.CustomerName
    }, '</option>'));
    $('#ddlCustomerCode').trigger("chosen:updated");

    //Bind Customer on Edit Form
    $('#ddlCustomerCodeEdit').append($('<option>', {
        value: custCode.CustomerCode,
        text: custCode.CustomerCode + '~' + custCode.CustomerName
    }, '</option>'));
    $('#ddlCustomerCodeEdit').trigger("chosen:updated");
}
// End Bind Customer Codes

// Bind Employee No
function GetEmplyees() {
    listAll("TMS0104.aspx/GetEmplyees", JSON.stringify(), emptyBind, bindEmplyee, emptyBind);
}

function bindEmplyee(emp) {
    
    $('#ddlPMEmployeeNo').append($('<option>', {
        value: emp.EmpNo,
        text:  emp.Name
    }, '</option>'));
    $('#ddlPMEmployeeNo').trigger("chosen:updated");

    $('#ddlProjectManagerEdit').append($('<option>', {
        value: emp.EmpNo,
        text: emp.Name
    }, '</option>'));
    $('#ddlProjectManagerEdit').trigger("chosen:updated");
}
// End Bind Employee No
