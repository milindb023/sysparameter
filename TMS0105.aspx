﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TMS0105.aspx.cs" Inherits="SysParameter.TMS0105" Theme="" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!-- bootstrap-->
    
    <link rel="shortcut icon" type="image/x-icon" href="et-uiassets/images/faviconn.ico" />
    <link href="et-uiassets/css/et-style.css" rel="stylesheet" />
    <script type="text/javascript" src="et-uiassets/js/jquery.js"></script>
    <script type="text/javascript" src="et-uiassets/js/bootstrap.js"></script>
    <script type="text/javascript" src="et-uiassets/js/common.js"></script>

    <!-- bootstrap-->
    <script src="et-uiassets/texteditor/ckeditor.js"></script>

    <!-- date time picker -->
    <link href="et-uiassets/datetimepicker/datetimepicker.css" rel="stylesheet" />
    <script src="et-uiassets/datetimepicker/moment.js" type="text/javascript"></script>
    <script src="et-uiassets/datetimepicker/datetimepicker.js" type="text/javascript"></script>
    <!-- date time picker -->


    <!-- range slider -->
    <link href="et-uiassets/rangleslider/bootstrap-slider.css" rel="stylesheet" />
    <link href="et-uiassets/rangleslider/jquerysctipttop.css" rel="stylesheet" />
    <script src="et-uiassets/rangleslider/bootstrap-slider.js" type="text/javascript"></script>
    <script src="et-uiassets/js/et-script.js"></script>
    <!-- range slider  tblTask-->

    <link href="et-uiassets/css/toastr.css" rel="stylesheet" />
    <script src="et-uiassets/js/toastr.js"></script>
    <script src="TMS0105.js"></script>
  <style>
      .chosen-container-single {
          width:100% !important;
      }
  </style>
</head>
<body>
    <form id="form1" runat="server" >
        <input type="hidden" id="hdnActivePage" value="1"/>
        <input type="hidden" id="hdnProjectCode" runat="server"/>
        <et-mainpagecontent>
            <et-breadcrumb name="breadcrumbs">
	            <et-breadcrumb-item >Code Files</et-breadcrumb-item>
	            <et-breadcrumb-item>Dashboard</et-breadcrumb-item>
                <et-breadcrumb-item>Tasks</et-breadcrumb-item>
            </et-breadcrumb>
            <%--<et-content-area-24>--%>
		<div class="clearfix mar-bottom-8">
			<et-block-form-group-button-right>
				<button type="button" class="et-btn et-btn-primary " onclick="clearValues(this,'TMS0105');"  data-toggle="modal" data-target="#myModal"> ADD </button>
			</et-block-form-group-button-right>
		</div>
<%--</et-content-area-24>--%>	
            <et-contentbgcolor-white>
            <et-content-body class="padd-left-0 padd-right-0 padd-top-0">
	<div class="table-responsive tableedge">
		<et-head-table>
			<et-table-search>
       <et-search-thin>
	
	<div class="clearfix padd-bottom-10 padd-top-10">
		<et-searchthin>												
			<a href="javascript:void(0)" class="etsearch"><i class="et-search-1"></i></a>
			
			<input type="text" id="txtSearchText" value="" placeholder="search" onkeyup="SearchResult(this);">
		</et-searchthin>
	</div>
	
	
</et-search-thin> 
      </et-table-search>
		</et-head-table>  								
		<table class="table display" id="tblTask">
			<thead>
				<tr>
					<th class="et-text-left">Task Code <a href="javascript:void(0);" onclick="MakeCodeSortable();"><i class="et-arrow-down-7"></i></a></th>
					<th class="et-text-left">Task Name <a href="javascript:void(0);" onclick="MakeNameSortable();"><i class="et-arrow-down-7"></i></a></th>
                    <th class="et-text-left">Project </th>
					<th class="et-text-left">Activity Code </th>
                    <th class="et-text-left">Status</th>
						
                    <th class="et-text-right">&nbsp;</th>
				</tr>
			</thead>

			<tbody id="Taskbody">
												
			</tbody>									

			<tfoot>
				<tr>
					<td colspan="15">
						<et-table-footer>
                            <et-table-pagination>
							
							<span>Row per page:</span>
							<span ><b class="dropdown" ><a class="dropdown-toggle" type="button" data-toggle="dropdown" 
                                href="javascript:void(0);" aria-expanded="false"><i id="arrow" class="et-arrow-down-7"></i></a>
							    <ul class="dropdown-menu">
								    <li><a href="javascript:void(0);" onclick="PageNumber('1');">1</a></li>
								    <li><a href="javascript:void(0);" onclick="PageNumber('2');">2</a></li>
								    <li><a href="javascript:void(0);" onclick="PageNumber('3');">3</a></li>
								    <li><a href="javascript:void(0);" onclick="PageNumber('4');">4</a></li>
								    <li><a href="javascript:void(0);" onclick="PageNumber('5');">5</a></li>
								    <li><a href="javascript:void(0);" onclick="PageNumber('6');">6</a></li>
								    <li><a href="javascript:void(0);" onclick="PageNumber('7');">7</a></li>
								    <li><a href="javascript:void(0);" onclick="PageNumber('8');">8</a></li>
								    <li><a href="javascript:void(0);" onclick="PageNumber('9');">9</a></li>
								    <li><a href="javascript:void(0);" onclick="PageNumber('10');">10</a></li>
							    </ul>
							<span id="PgIndex"></span></b></span>
							<b><span id="PageCount"></span><span id="TotalRows"></span> </b>
							<span>
							<a href="javascript:void(0)" class="pagiarrow previous" onclick="PrevPage();"><i class="et-arrow-back-2"></i> </a>
							<a href="javascript:void(0)" class="pagiarrow next" onclick="NextPage();"><i class="et-arrow-forward-2"></i></a>
							</span>
							</et-table-pagination>
							
						</et-table-footer>											
					</td>
				</tr>
			</tfoot>
		</table>

			<div class="edittablefrom clearfix hiddendiv">

				<et-content-area-3>
					<et-block-form-group>
						<et-label-block>Task Code</et-label-block>
						<input type="text" class="Taskcode"/>
					</et-block-form-group>
				</et-content-area-3>

				<et-content-area-5>
					<et-block-form-group>
						<et-label-block>Task Name</et-label-block>
						<input type="text" class="Taskname" id="txtTaskNameEdit" data-validationcontrolid="valTaskNameEdit" onkeyup="validateSingleInput(this,validateTaskCode);"/>
                         <formerror id="valTaskNameEdit" class="et-hide">Please enter Task name</formerror>
					</et-block-form-group>
				</et-content-area-5>
                <et-content-area-5>
					<et-block-form-group>
						<et-label-block>Project</et-label-block>
                        <input type="text" class="project"/>
					</et-block-form-group>
				</et-content-area-5>
                 <et-content-area-5>
					<et-block-form-group>
						<et-label-block>Activity Code</et-label-block>
                         <input type="text" id="txtActivityCodeEdit" class="activitycode" maxlength="15" data-validationcontrolid="valActivityCodeEdit" onkeyup="validateSingleInput(this,validateTaskCode);">
                         <formerror id="valActivityCodeEdit" class="et-hide">Please enter activity code</formerror>
					</et-block-form-group>
				</et-content-area-5>
               
                <et-content-area-3>
					<et-block-form-group>
						<et-label-block>Status</et-label-block>
						<et-switch-new>
	                        <label class="et-switch">
	                        <input type="checkbox" class="status" id="chkStatus"/>
	                        <span class="et-switch-slider"></span>
	                        </label>
                       </et-switch-new>
					</et-block-form-group>
				</et-content-area-3>

				<et-content-area-24>
					<et-block-form-group-button-right>
						<button type="button" class="et-btn et-btn-primary"  onclick="SaveEditTask();">Save</button>
						<button type="button" class="et-btn et-btn-primary-reverse closetabletd" id="btnClose">Close</button>
					</et-block-form-group-button-right>
				</et-content-area-24>
			</div>											
			<div class="editrft" style="display:none"></div>												
		</div>	
                
                							
</et-content-body>
                </et-contentbgcolor-white>
         </et-mainpagecontent>

    </form>


    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header"  >
                    <a  class="close" data-dismiss="modal">
                        <et-modal-close></et-modal-close>
                    </a>
                    <h4 class="modal-title project">Add Task</h4>
                </div>
                <div class="modal-body">

                    <et-block-form-group>
	                    <et-label-block>Task Code</et-label-block>
	                    <input type="text" id="txtTaskCodePopUp" data-validationcontrolid="valTaskCodePopUp"  onkeyup="validateSingleInput(this,validateTaskCode);">
                        <formerror id="valTaskCodePopUp" class="et-hide">Please enter Task code</formerror>
	                 </et-block-form-group>

                    <et-block-form-group>
		                <et-label-block>Task Name</et-label-block>
		                <input type="text" id="txtTaskNamePopUp" data-validationcontrolid="valTaskNamePopUp" onkeyup="validateSingleInput(this,validateTaskCode);">
                         <formerror id="valTaskNamePopUp" class="et-hide">Please enter task name</formerror>
		            </et-block-form-group>
                  
                     <et-block-form-group>
		               <et-label-block>Activity Code</et-label-block>							
	                         <input type="text" id="txtActivityCode" maxlength="15" data-validationcontrolid="valActivityCodePopUp" onkeyup="validateSingleInput(this,validateTaskCode);">
                         <formerror id="valActivityCodePopUp" class="et-hide">Please enter activity code</formerror>
		            </et-block-form-group>
                </div>
                <div class="modal-footer">
                    <button class="et-btn et-btn-primary" id="btnPopSave"   onclick="javascript:return SaveData(this,myModal);"  >Save</button>
                    <button type="button" class="et-btn et-btn-primary-reverse" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div id="messageBoxModal" class="modal fade" role="dialog"></div>
      <!-- combobox picker -->
        <link href="et-uiassets/combobox/chosen.css" rel="stylesheet" />
        <script src="et-uiassets/combobox/chosen.jquery.js" type="text/javascript"></script>
        <script src="et-uiassets/combobox/prism.js" type="text/javascript"></script>
        <script src="et-uiassets/combobox/init.js" type="text/javascript"></script>
        <!-- combobox picker -->

        <!-- fixed right slider -->
        <link href="et-uiassets/fancyscroll/simplebar.css" rel="stylesheet" />
        <script src="et-uiassets/fancyscroll/simplebar.js" type="text/javascript"></script>
        <!-- fixed right slider -->
</body>
</html><%--class="modal fade" role="dialog"--%>
