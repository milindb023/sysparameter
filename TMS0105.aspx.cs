﻿using ESSLib.Business.BusinessObjects;
using ESSLib.Data.DataObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SysParameter
{
    public partial class TMS0105 : System.Web.UI.Page
    {
        string ProjectCode = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            string[] sString = Session["Project"].ToString().Split(',');
            Session["ProjectCode"] = sString[0];
            hdnProjectCode.Value = Session["Project"].ToString();
        }

        [WebMethod]
        public static string GetTasks(string FromRow, string ToRow)
        {
            ResponseObjectTask response = null;
            try
            {
                if (HttpContext.Current.Session["ProjectCode"].ToString() != "")
                {
                    Tasks oTasks = new Tasks();
                    DataTable dt = oTasks.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), false, false, "", HttpContext.Current.Session["ProjectCode"].ToString());
                    response = new ResponseObjectTask(true, "Data obtained", dt);// JsonConvert.SerializeObject(dt);
                }
            }
            catch (Exception ex)
            {
                response = new ResponseObjectTask(false, ex.Message);
            }
            return JsonConvert.SerializeObject(response);
        }

        [WebMethod]
        public static string SortTaskCode(string FromRow, string ToRow, string Sort)
        {
            ResponseObjectTask response = null;
            try
            {
                if (HttpContext.Current.Session["ProjectCode"].ToString() != "")
                {
                    Tasks oTasks = new Tasks();
                    DataTable dt = null;
                    if (Sort == "true")
                        dt = oTasks.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), true, false, "", HttpContext.Current.Session["ProjectCode"].ToString());
                    else
                        dt = oTasks.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), false, false, "", HttpContext.Current.Session["ProjectCode"].ToString());
                    response = new ResponseObjectTask(true, "Data obtained", dt);// JsonConvert.SerializeObject(dt);
                }
            }
            catch (Exception ex)
            {
                response = new ResponseObjectTask(false, ex.Message);
            }
            return JsonConvert.SerializeObject(response);
        }

        [WebMethod]
        public static string SortTaskName(string FromRow, string ToRow, string Sort)
        {
            ResponseObjectTask response = null;
            try
            {
                if (HttpContext.Current.Session["ProjectCode"].ToString() != "")
                {
                    Tasks oTasks = new Tasks();
                    DataTable dt = null;
                    if (Sort == "true")
                        dt = oTasks.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), false, true, "", HttpContext.Current.Session["ProjectCode"].ToString());
                    else
                        dt = oTasks.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), false, false, "", HttpContext.Current.Session["ProjectCode"].ToString());
                    response = new ResponseObjectTask(true, "Data obtained", dt);// JsonConvert.SerializeObject(dt);
                }
            }
            catch (Exception ex)
            {
                response = new ResponseObjectTask(false, ex.Message);
            }
            return JsonConvert.SerializeObject(response);
        }

        [WebMethod]
        public static string GetSearchData(string FromRow, string ToRow, string SearchText)
        {
            ResponseObjectTask response = null;
            try
            { 
                Tasks oTasks = new Tasks();
                DataTable dt = null;
                if (SearchText != "" && HttpContext.Current.Session["ProjectCode"].ToString() != "")
                    dt = oTasks.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), false, false, SearchText, HttpContext.Current.Session["ProjectCode"].ToString());
                else
                    dt = oTasks.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), false, false, "", HttpContext.Current.Session["ProjectCode"].ToString());
                response = new ResponseObjectTask(true, "Data obtained", dt);
            }
            catch (Exception ex)
            {
                response = new ResponseObjectTask(false, ex.Message);
            }
            return JsonConvert.SerializeObject(response);
        }

        [WebMethod]
        public static string SaveData(string TaskCode, string TaskName, string ProjectCode, string ActivityCode)
        {
            string result = string.Empty;
            TaskCode = TaskCode.ToUpper();
            Tasks oTasks = Tasks.Load(TaskCode,HttpContext.Current.Session["ProjectCode"].ToString());
            if (oTasks == null)
            {
                oTasks = new Tasks();
                oTasks.TaskCode = TaskCode;
                oTasks.TaskName = TaskName;
                oTasks.ProjectCode = ProjectCode.ToUpper();
                oTasks.ActivityCode = ActivityCode.ToUpper();
                oTasks.IsActive = true;
                oTasks.Save();
                result = "true";
            }
            else
            {
                result ="false"; 
            }
            return result;
        }

        [WebMethod]
        public static string DeleteTask(string Tcode)
        {
            string result = string.Empty;
            if (HttpContext.Current.Session["ProjectCode"].ToString() != "")
            {
                Tcode = Tcode.ToUpper();
                Tasks oTasks = Tasks.Load(Tcode, HttpContext.Current.Session["ProjectCode"].ToString());
                if (oTasks != null)
                {
                    oTasks.Delete();
                    result = "true";
                }
                else
                {
                    result = "false";
                }
            }
                return result;            
        }

        [WebMethod]
        public static string SaveEditData(string TaskCode, string TaskName, string ProjectCode, string ActivityCode, bool isactive)
        {
            string result = string.Empty;
            if (HttpContext.Current.Session["ProjectCode"].ToString() != "")
            {
                TaskCode = TaskCode.ToUpper();
                Tasks oTasks = Tasks.Load(TaskCode, HttpContext.Current.Session["ProjectCode"].ToString());
                if (oTasks != null)
                {
                    oTasks.TaskName = TaskName;
                    oTasks.TaskCode = TaskCode;
                    oTasks.ProjectCode = ProjectCode.ToUpper();
                    oTasks.ActivityCode = ActivityCode.ToUpper();
                    oTasks.IsActive = isactive;
                    oTasks.Update();
                    result = "true";
                }
                else
                {
                    result = "false";
                }
            }
            return result;
        }

        [WebMethod]
        public static string GetProjectCodes()
        {
            ResponseObject response = null;
            try
            {
                DataTable dt = Projects.LoadListAsTable(); 
                response = new ResponseObject(true, "Data obtained",  dt);
            }
            catch (Exception ex)
            {
                response = new ResponseObject(false, ex.Message);
            }
            return JsonConvert.SerializeObject(response);
        }
    }

    public class ResponseObjectTask
    {
        public bool SUC { get; set; }
        public string MSG { get; set; }
        public object GenericObject { get; set; }

        public ResponseObjectTask()
        {
        }

        public ResponseObjectTask(bool success, string message, object genericObject = null)
        {
            this.SUC = success;
            this.MSG = message;
            this.GenericObject = genericObject;
        }
    }
}