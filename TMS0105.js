﻿var mypager = new etPager();
var SortableCode = false;
var SortableName = false;
var id = 0;
var checkApprovaltype = "";

var rtemplate = '<tr><td class="et-font-size-12">{TaskCode}</td>' +
                '<td class="et-font-size-12">{TaskName}</td>' +
                '<td class="et-font-size-12">{ProCode}</td>' +
                '<td class="et-font-size-12">{ActivityCode}</td>' +
                '<td class="et-font-size-12">{IsActive}</td>' +
                '<td class="et-text-right">' +
                '<a  class=""  ' +
                '   onclick="return showETDeleteConirmation(\'DeleteTask(\\\'{TaskCode}\\\')\', null)"' +
                '   id="de{j}" ><i class="et-delete3 et-font-size-20 mar-right-15"></i></a>' +
                '<a data-toggle="collapse" class="" data-parent="#accordion" ' +
                '   onclick="SetValues({code:\'{TaskCode}\', value:\'{TaskName}\',procode:\'{ProCode}\', activitycode:\'{ActivityCode}\', isactive:\'{IsActive}\'});"' +
                '   id="hf{j}" href="#ff{j}"><et-edit-row></et-edit-row></a>' +
                '</td></tr>' +
                '<tr class="hiddenrow">' +
                    '<td colspan="15">' +
                        '<et-content-body id="ff{j}" class="panel-collapse collapse et-hdden-row">' +
                        '</et-content-body>' +
                    '</td>' +
                '</tr>'

listAll("TMS0105.aspx/GetTasks", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows }), clearTasks, bindTask, noTask, "tblTask");

function clearTasks() {
}

function noTask() {
    var noData = "<tr class='searchtr'>" +
       "<td colspan='15' class='et-text-center et-text-danger'>No Records Found</td>" +
       "</tr>";
    $("#tblTask > tbody").append(noData);
}

function bindTask(Task) {
    
    var tr1 = rtemplate.replace("{TaskCode}", Task.TaskCode);
    tr1 = tr1.replace("{TaskCode}", Task.TaskCode);
    tr1 = tr1.replace("{TaskName}", Task.TaskName);
    tr1 = tr1.replace("{TaskName}", Task.TaskName);
    tr1 = tr1.replace("{TaskCode}", Task.TaskCode);
    tr1 = tr1.replace("{TaskName}", Task.TaskName);
    tr1 = tr1.replace("{ProCode}", Task.ProCode);

    tr1 = tr1.replace("{ProCode}", Task.ProCode);
    tr1 = tr1.replace("{ActivityCode}", Task.ActivityCode);
    tr1 = tr1.replace("{ActivityCode}", Task.ActivityCode);
   
    if (Task.IsActive) {
        tr1 = tr1.replace("{IsActive}", "<et-success-badge><i class='et-tick-9'></i>Active</et-success-badge>");
    }
    else {
        tr1 = tr1.replace("{IsActive}", "<et-danger-badge><i class='et-clear-2'></i>Inactive</et-danger-badge>");
    }
    tr1 = tr1.replace("{IsActive}", Task.IsActive);
    tr1 = tr1.replace("{j}", id);
    tr1 = tr1.replace("{j}", id);
    tr1 = tr1.replace("{j}", id);
    tr1 = tr1.replace("{j}", id);
    tr1 = tr1.replace("{j}", id);
    tr1 = tr1.replace("{j}", id);
    $("#Taskbody").append(tr1);
    id++;

    var sString = $('#hdnProjectCode').val().split(',');
    $('h4.project').text('Add Task for ' + sString[0] + ' - ' + sString[1]);
}

function DeleteTask(id) {
    asyncCall('TMS0105.aspx/DeleteTask', JSON.stringify({ 'Tcode': id }), DeleteRow);
    return true;
}

function DeleteRow(result) {
    if (result == "true") {
        toastr.success("Task deleted successfully.");// alert("Task deleted successfully.");
        listAll("TMS0105.aspx/GetTasks", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows }), clearTasksRows, bindTask, noTask, "tblTask");
        return true;
    }
}

function SetValues(detail) {
    $('.Taskcode').val(detail.code);
    $('.Taskname').val(detail.value);
    $('.project').val(detail.procode);
    $('.activitycode').val(detail.activitycode);
   
    if (detail.isactive == "true")
        $('.status').prop('checked', true);
    else
        $('.status').prop('checked', false);
    $('.Taskcode').attr('readonly', true);
    $('.Taskcode').css('background-color', 'lightgrey');
    $('.project').attr('readonly', true);
    $('.project').css('background-color', 'lightgrey');
}


function SaveData(obj, div) {
    if (SaveDataPopUp(obj, div)) {
        var projCode = document.getElementById('hdnProjectCode').value.split(',');
        var TaskCode = document.getElementById('txtTaskCodePopUp').value;
        var TaskName = document.getElementById('txtTaskNamePopUp').value; 
        var activityCode = document.getElementById('txtActivityCode').value;
        asyncCall('TMS0105.aspx/SaveData', JSON.stringify({ 'TaskCode': TaskCode, 'TaskName': TaskName, 'ProjectCode': projCode[0], 'ActivityCode': activityCode }), SaveReult);
    }
}

function SaveReult(result) {
    if (result == "true") {
        $('#myModal').modal('hide');
        toastr.success("Task saved successfully.");
        listAll("TMS0105.aspx/GetTasks", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows }), clearTasksRows, bindTask, noTask, "tblTask");
        return true;
    }
    else {
        toastr.warning(document.getElementById('txtTaskCodePopUp').value + " Task code is already exist.");
        return false;
    }
}

function SaveEditTask() {
    var TaskCode = $('.Taskcode').val();
    var TaskName = $('.Taskname').val();
    var projCodeEdit = $('.project').val().split('~');
    var activitycode = $('.activitycode').val();
    var isactive = $('#chkStatus').is(':checked');
    asyncCall('TMS0105.aspx/SaveEditData', JSON.stringify({ 'TaskCode': TaskCode, 'TaskName': TaskName, 'ProjectCode': projCodeEdit[0], 'ActivityCode': activitycode, 'isactive': isactive }), SaveEditedTask);
}

function SaveEditedTask(result) {
    if (result == "true") {
        listAll("TMS0105.aspx/GetSearchData", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'SearchText': document.getElementById('txtSearchText').value }), clearTasksRows, bindTask, noTask, "tblTask");
        toastr.success("Task data updated successfully.");
    }
}

function clearTasksRows() {
    $("#Taskbody").find('.edittablefrom').css({ 'display': 'none', 'position': 'absolute' }).insertBefore($("#tblTask").parent().find('.editrft'))
    $("#Taskbody tr").remove();
}

function NextPage() {
    SortableCode = false;
    SortableName = false;
    mypager.fromRowsStart = mypager.fromRowsStart + mypager.endIndex;
    mypager.toRows = mypager.endIndex + mypager.toRows;
    mypager.totalRowsCount = parseInt(mypager.totalRowsCount);
    listAll("TMS0105.aspx/GetSearchData", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'SearchText': document.getElementById('txtSearchText').value }), clearTasksRows, bindNextPageTask, noTask, "tblTask");
}

function bindNextPageTask(Task) {
    
    closedit();
    bindTask(Task);
    if (mypager.toRows >= mypager.totalRowsCount) {
        document.getElementById('PageCount').innerHTML = mypager.fromRowsStart + "-" + mypager.totalRowsCount + " of ";
        $("a.next").hide();
    }
    else {
        document.getElementById('PageCount').innerHTML = mypager.fromRowsStart + "-" + mypager.toRows + " of ";
        $("a.next").show();
    }
}

function PrevPage() {
    
    SortableCode = false;
    SortableName = false;
    mypager.fromRowsStart = mypager.fromRowsStart - mypager.endIndex;
    mypager.toRows = mypager.toRows - mypager.endIndex;
    listAll("TMS0105.aspx/GetSearchData", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'SearchText': document.getElementById('txtSearchText').value }), clearTasksRows, bindPrevPageTask, noTask, "tblTask");
    document.getElementById('PageCount').innerHTML = mypager.fromRowsStart + "-" + mypager.toRows + " of ";
}

function bindPrevPageTask(Task) {
    closedit();
    bindTask(Task);
}

function PageNumber(obj) {
    SortableCode = false;
    SortableName = false;
    mypager.fromRowsStart = mypager.startIndex;
    mypager.toRows = parseInt(obj);
    mypager.endIndex = parseInt(obj);

    listAll("TMS0105.aspx/GetSearchData", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'SearchText': document.getElementById('txtSearchText').value }), clearTasksRows, bindPageSizeTask, noTask, "tblTask");
    document.getElementById('PgIndex').innerHTML = obj;
}

function bindPageSizeTask(Task) {
    closedit();
    bindTask(Task);
}

function MakeCodeSortable() {

    if (!SortableCode) {
        listAll("TMS0105.aspx/SortTaskCode", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'Sort': 'true' }), clearTasksRows, bindTask, noTask, "tblTask");
        SortableCode = true;
    }
    else {
        listAll("TMS0105.aspx/SortTaskCode", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'Sort': 'false' }), clearTasksRows, bindTask, noTask, "tblTask");
        SortableCode = false;
    }
}

function MakeNameSortable() {

    if (!SortableCode) {
        listAll("TMS0105.aspx/SortTaskName", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'Sort': 'true' }), clearTasksRows, bindTask, noTask, "tblTask");
        SortableCode = true;
    }
    else {
        listAll("TMS0105.aspx/SortTaskName", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'Sort': 'false' }), clearTasksRows, bindTask, noTask, "tblTask");
        SortableCode = false;
    }
}

function SearchResult(obj) {
    
    mypager.fromRowsStart = mypager.startIndex;
    mypager.toRows = mypager.endIndex;
    listAll("TMS0105.aspx/GetSearchData", JSON.stringify({ 'FromRow': mypager.startIndex, 'ToRow': mypager.endIndex, 'SearchText': obj.value }), clearTasksRows, bindTask, noTask, "tblTask");
}

function validateTaskCode(obj) {
    switch (obj.id) {
        case "txtTaskCodePopUp":
            if (obj.value != "")
                return true;
            else
                return false;
            break;
        case "txtTaskNamePopUp":
            if (obj.value != "")
                return true;
            else
                return false;
            break;
        case "txtProjectCode":
            if (obj.value != "")
                return true;
            else
                return false;
            break;
        case "txtActivityCode":
            if (obj.value != "")
                return true;
            else
                return false;
            break;
        case "txtTaskNameEdit":
            if (obj.value != "")
                return true;
            else
                return false;
            break;
        case "ddlProjectCodeEdit":
            if (obj.value != "")
                return true;
            else
                return false;
            break;
        case "txtActivityCodeEdit":
            if (obj.value != "")
                return true;
            else
                return false;
            break;
    }

}

// Bind Project Codes
function GetProjectCodes() {
  //  listAll("TMS0105.aspx/GetProjectCodes", JSON.stringify(), emptyBind, bindProjectCodes, emptyBind);
    
}

function emptyBind() {
}
function bindProjectCodes(projectCode) {
    
    //Bind Project on ADD Form
    //$('#ddlProjectCode').append($('<option>', {
    //    value: projectCode.ProjectCode,
    //    text: projectCode.ProjectCode + '~' + projectCode.ProjectName
    //}, '</option>'));
    //$('#ddlProjectCode').trigger("chosen:updated");
   // alert($('#hdnProjectCode').val());
    
  //  $('#txtProjectCode').val($('#hdnProjectCode').val());
    //Bind Project on Edit Form
   
    $('#ddlProjectCodeEdit').append($('<option>', {
        value: projectCode.ProjectCode,
        text: projectCode.ProjectCode + '~' + projectCode.ProjectName
    }, '</option>'));
    $('#ddlProjectCodeEdit').trigger("chosen:updated");
}
// End Bind Customer Codes


