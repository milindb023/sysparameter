﻿using ESSLib.Business.BusinessObjects;
using ESSLib.Data.DataObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SysParameter
{
    public partial class TMS0106 : System.Web.UI.Page
    {
        string ProjectCode = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            string[] sString = Session["Employee"].ToString().Split(',');
            Session["sProjectCode"] = sString[0];
            hdnProjectCode.Value = Session["Employee"].ToString();
        }

        [WebMethod]
        public static string GetProjectMember(string FromRow, string ToRow)
        {
            ResponseObjectProjectTeams response = null;
            try
            {
                if (HttpContext.Current.Session["sProjectCode"].ToString() != "")
                {
                    ProjectTeams oProjectTeams = new ProjectTeams();
                    DataTable dt = oProjectTeams.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), false, false, "", HttpContext.Current.Session["sProjectCode"].ToString());
                    response = new ResponseObjectProjectTeams(true, "Data obtained", dt);// JsonConvert.SerializeObject(dt);
                }
            }
            catch (Exception ex)
            {
                response = new ResponseObjectProjectTeams(false, ex.Message);
            }
            return JsonConvert.SerializeObject(response);
        }

        [WebMethod]
        public static string SortEmpNo(string FromRow, string ToRow, string Sort)
        {
            ResponseObjectProjectTeams response = null;
            try
            {
                if (HttpContext.Current.Session["sProjectCode"].ToString() != "")
                {
                    ProjectTeams oProjectTeams = new ProjectTeams();
                    DataTable dt = null;
                    if (Sort == "true")
                        dt = oProjectTeams.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), false, true, "", HttpContext.Current.Session["sProjectCode"].ToString());
                    else
                        dt = oProjectTeams.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), false, false, "", HttpContext.Current.Session["sProjectCode"].ToString());
                    response = new ResponseObjectProjectTeams(true, "Data obtained", dt);// JsonConvert.SerializeObject(dt);
                }
            }
            catch (Exception ex)
            {
                response = new ResponseObjectProjectTeams(false, ex.Message);
            }
            return JsonConvert.SerializeObject(response);
        }

        [WebMethod]
        public static string SortProjectRole(string FromRow, string ToRow, string Sort)
        {
            ResponseObjectProjectTeams response = null;
            try
            {
                if (HttpContext.Current.Session["sProjectCode"].ToString() != "")
                {
                    ProjectTeams oProjectTeams = new ProjectTeams();
                    DataTable dt = null;
                    if (Sort == "true")
                        dt = oProjectTeams.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), true, false, "", HttpContext.Current.Session["sProjectCode"].ToString());
                    else
                        dt = oProjectTeams.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), false, false, "", HttpContext.Current.Session["sProjectCode"].ToString());
                    response = new ResponseObjectProjectTeams(true, "Data obtained", dt);// JsonConvert.SerializeObject(dt);
                }
            }
            catch (Exception ex)
            {
                response = new ResponseObjectProjectTeams(false, ex.Message);
            }
            return JsonConvert.SerializeObject(response);
        }

        [WebMethod]
        public static string GetSearchData(string FromRow, string ToRow, string SearchText)
        {
            ResponseObjectProjectTeams response = null;
            try
            {
                ProjectTeams oProjectTeams = new ProjectTeams();
                DataTable dt = null;
                if (SearchText != "" && HttpContext.Current.Session["sProjectCode"].ToString() != "")
                    dt = oProjectTeams.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), false, false, SearchText, HttpContext.Current.Session["sProjectCode"].ToString());
                else
                    dt = oProjectTeams.GetPaginingData(Convert.ToInt16(FromRow), Convert.ToInt16(ToRow), false, false, "", HttpContext.Current.Session["sProjectCode"].ToString());
                response = new ResponseObjectProjectTeams(true, "Data obtained", dt);
            }
            catch (Exception ex)
            {
                response = new ResponseObjectProjectTeams(false, ex.Message);
            }
            return JsonConvert.SerializeObject(response);
        }

        [WebMethod]
        public static string SaveData(string TeamMember, string ProjectRole, string ProjectCode)
        {
            string result = string.Empty;
            TeamMember = TeamMember.ToUpper();
            ProjectTeams oProjectTeams = ProjectTeams.Load(ProjectCode, ProjectRole, TeamMember);
            if (oProjectTeams == null)
            {
                oProjectTeams = new ProjectTeams();
                oProjectTeams.ProjectCode = ProjectCode;
                oProjectTeams.ProjectRole = ProjectRole.ToUpper();
                oProjectTeams.EmpNo = TeamMember.ToUpper();
                oProjectTeams.StartDate = DateTime.Now;
                oProjectTeams.EndDate = DateTime.Now; 
                oProjectTeams.IsActive = true;
                oProjectTeams.Save();
                result = "true";
            }
            else
            {
                result ="false"; 
            }
            return result;
        }

        [WebMethod]
        public static string DeleteProjectMembar(string ProjectRole, string ProjectMembar)
        {
            string result = string.Empty;
            string[] sProjectMembar = ProjectMembar.Split('~');
            if (HttpContext.Current.Session["sProjectCode"].ToString() != "")
            {
                ProjectMembar = ProjectMembar.ToUpper();
                ProjectTeams oProjectTeams = ProjectTeams.Load(HttpContext.Current.Session["sProjectCode"].ToString(), ProjectRole, sProjectMembar[0]);
                if (oProjectTeams != null)
                {
                    oProjectTeams.Delete();
                    result = "true";
                }
                else
                {
                    result = "false";
                }
            }
                return result;            
        }

        [WebMethod]
        public static string SaveEditData(string ProjectCode, string ProjectRole, string ProjectMember, bool isactive)
        {
            string result = string.Empty;
            if (HttpContext.Current.Session["sProjectCode"].ToString() != "")
            {
                ProjectCode = ProjectCode.ToUpper();
              //  bool IsUpdate = ProjectTeams.UpdateProjectTeam(ProjectRole, isactive, ProjectCode, ProjectMember);
                
             
                ProjectTeams OoProjectTeams = new ProjectTeams();
                OoProjectTeams.EmpNo = ProjectMember;
                OoProjectTeams.ProjectCode = ProjectCode;
                ProjectTeamsCollection oProjectTeamsCollection = ProjectTeams.Search(OoProjectTeams);
                if (oProjectTeamsCollection != null)
                {
                    ProjectTeams oProjectTeams = ProjectTeams.Load(ProjectCode, oProjectTeamsCollection[0].ProjectRole, ProjectMember);
                    if (oProjectTeams != null)
                    {
                        //  oProjectTeams.ProjectCode = ProjectCode;
                       // oProjectTeams.ProjectRole = ProjectRole.ToUpper();
                        //   oProjectTeams.EmpNo = ProjectMember.ToUpper();
                        oProjectTeams.IsActive = isactive;
                        oProjectTeams.Update();
                        result = "true";
                    }
                    else
                    {
                        result = "false";
                    }
                }
                else
                    result = "false";
            }
            return result;
        }

        [WebMethod]
        public static string GetProjectCodes()
        {
            ResponseObject response = null;
            try
            {
                DataTable dt = Projects.LoadListAsTable(); 
                response = new ResponseObject(true, "Data obtained",  dt);
            }
            catch (Exception ex)
            {
                response = new ResponseObject(false, ex.Message);
            }
            return JsonConvert.SerializeObject(response);
        }

        [WebMethod]
        public static string GetEmplyees()
        {
            ResponseObject response = null;
            try
            {
                DataTable dt = ThisCompany.GetEmployees("", "007", ModuleType.PayrollManagement);
                response = new ResponseObject(true, "Data obtained", dt);// JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                response = new ResponseObject(false, ex.Message);
            }
            return JsonConvert.SerializeObject(response);
        }
    }

    public class ResponseObjectProjectTeams
    {
        public bool SUC { get; set; }
        public string MSG { get; set; }
        public object GenericObject { get; set; }

        public ResponseObjectProjectTeams()
        {
        }

        public ResponseObjectProjectTeams(bool success, string message, object genericObject = null)
        {
            this.SUC = success;
            this.MSG = message;
            this.GenericObject = genericObject;
        }
    }
}