﻿var mypager = new etPager();
var SortableCode = false;
var SortableName = false;
var id = 0;
var checkApprovaltype = "";

var rtemplate = '<tr><td class="et-font-size-12">{ProCode}</td>' +
                '<td class="et-font-size-12">{ProjectRole}</td>' +
                '<td class="et-font-size-12">{Name}</td>' +
                '<td class="et-font-size-12">{IsActive}</td>' +
                '<td class="et-text-right">' +
                '<a  class=""  ' +
                '   onclick="return showETDeleteConirmation(\'DeleteProjectMembar(\\\'{ProjectRole}\\\',\\\'{EmpNo}\\\')\', null)"' +
                '   id="de{j}" ><i class="et-delete3 et-font-size-20 mar-right-15"></i></a>' +
                '<a data-toggle="collapse" class="" data-parent="#accordion" ' +
                '   onclick="SetValues({procode:\'{ProCode}\', projectrole:\'{ProjectRole}\',name:\'{Name}\', isactive:\'{IsActive}\'});"' +
                '   id="hf{j}" href="#ff{j}"><et-edit-row></et-edit-row></a>' +
                '</td></tr>' +
                '<tr class="hiddenrow">' +
                    '<td colspan="15">' +
                        '<et-content-body id="ff{j}" class="panel-collapse collapse et-hdden-row">' +
                        '</et-content-body>' +
                    '</td>' +
                '</tr>'

listAll("TMS0106.aspx/GetProjectMember", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows }), clearProjectMember, bindTask, noTask, "tblProjectMember");
GetEmplyees();
function clearProjectMember() {
}

function noTask() {
    var noData = "<tr class='searchtr'>" +
       "<td colspan='15' class='et-text-center et-text-danger'>No Records Found</td>" +
       "</tr>";
    $("#tblProjectMember > tbody").append(noData);
}

function bindTask(ProjectTeam) {
    
    var tr1 = rtemplate.replace("{ProCode}", ProjectTeam.ProCode);
    tr1 = tr1.replace("{ProjectRole}", ProjectTeam.ProjectRole);
    tr1 = tr1.replace("{ProjectRole}", ProjectTeam.ProjectRole);
    tr1 = tr1.replace("{ProjectRole}", ProjectTeam.ProjectRole);
    tr1 = tr1.replace("{Name}", ProjectTeam.Name);
    tr1 = tr1.replace("{Name}", ProjectTeam.Name);
    tr1 = tr1.replace("{EmpNo}", ProjectTeam.Name);
    tr1 = tr1.replace("{ProCode}", ProjectTeam.ProCode);
   
    if (ProjectTeam.IsActive) {
        tr1 = tr1.replace("{IsActive}", "<et-success-badge><i class='et-tick-9'></i>Active</et-success-badge>");
    }
    else {
        tr1 = tr1.replace("{IsActive}", "<et-danger-badge><i class='et-clear-2'></i>Inactive</et-danger-badge>");
    }
    tr1 = tr1.replace("{IsActive}", ProjectTeam.IsActive);
    tr1 = tr1.replace("{j}", id);
    tr1 = tr1.replace("{j}", id);
    tr1 = tr1.replace("{j}", id);
    tr1 = tr1.replace("{j}", id);
    tr1 = tr1.replace("{j}", id);
    tr1 = tr1.replace("{j}", id);
    $("#Projectbody").append(tr1);
    id++;

    var sString = $('#hdnProjectCode').val().split(',');
    $('h4.project').text('Add member for ' + sString[0] + ' - ' + sString[1] + " project");
}

function DeleteProjectMembar(projectrole,empno) {
    asyncCall('TMS0106.aspx/DeleteProjectMembar', JSON.stringify({ 'ProjectRole': projectrole, 'ProjectMembar': empno }), DeleteRow);
    return true;
}

function DeleteRow(result) {
    if (result == "true") {
        toastr.success("Project member deleted successfully.");
        listAll("TMS0106.aspx/GetProjectMember", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows }), clearProjectMemberRows, bindTask, noTask, "tblProjectMember");
        return true;
    }
}

function SetValues(detail) {
    $('.projectteamcode').val(detail.procode);
    $('.projectrole').val(detail.projectrole);
    //var empno = detail.name.split('~');
    //$('.projectmanager').val(empno[0]).trigger('chosen:updated');
    $('.employee').val(detail.name);
    $('.employeeno').val(detail.name);
   
    if (detail.isactive == "true")
        $('.status').prop('checked', true);
    else
        $('.status').prop('checked', false);
    $('.projectteamcode').attr('readonly', true);
    $('.projectteamcode').css('background-color', 'lightgrey');

    $('.employeeno').attr('readonly', true);
    $('.employeeno').css('background-color', 'lightgrey');

    $('.projectrole').attr('readonly', true);
    $('.projectrole').css('background-color', 'lightgrey');
}


function SaveData(obj, div) {
    if (SaveDataPopUp(obj, div)) {
        var projCode = document.getElementById('hdnProjectCode').value.split(',');
        var e = document.getElementById('ddlProjectTeamEmployeeNo');
        var teamMember = e.options[e.selectedIndex].value.split('~');
        var projectRole = document.getElementById('txtProjectRolePopUp').value;
        
        asyncCall('TMS0106.aspx/SaveData', JSON.stringify({ 'TeamMember': teamMember[0], 'ProjectRole': projectRole, 'ProjectCode': projCode[0] }), SaveReult);
    }
}

function SaveReult(result) {
    if (result == "true") {
        $('#myModal').modal('hide');
        toastr.success(document.getElementById('ddlProjectTeamEmployeeNo').selectedOptions[0].text + " saved successfully.");
        listAll("TMS0106.aspx/GetProjectMember", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows }), clearProjectMemberRows, bindTask, noTask, "tblProjectMember");
        return true;
    }
    else {
        var e = document.getElementById('ddlProjectTeamEmployeeNo');
        var teamMember = e.selectedOptions[0].text;
        toastr.warning(teamMember + " member is already exist.");
        return false;
    }
}

function SaveEditProjectTeam() {
    var projectcode = $('.projectteamcode').val().split('~');
    var projectrole = $('.projectrole').val();
    var projectmanager = $('.projectmanager').val().split('~');
    var isactive = $('#chkStatus').is(':checked');
    asyncCall('TMS0106.aspx/SaveEditData', JSON.stringify({ 'ProjectCode': projectcode[0], 'ProjectRole': projectrole, 'ProjectMember': projectmanager[0], 'isactive': isactive }), SaveEditedTask);
}

function SaveEditedTask(result) {
    
    if (result == "true") {
        listAll("TMS0106.aspx/GetSearchData", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'SearchText': document.getElementById('txtSearchText').value }), clearProjectMemberRows, bindTask, noTask, "tblProjectMember");
        toastr.success("Project member updated successfully.");
    }
    else {
        //var e = document.getElementById('ddlProjectTeamEmployeeNoEdit');
        //var teamMember = e.selectedOptions[0].text;
        toastr.warning($('.employeeno').val() + " member is already exist.");
        return false;
    }
}

function clearProjectMemberRows() {
    $("#Projectbody").find('.edittablefrom').css({ 'display': 'none', 'position': 'absolute' }).insertBefore($("#tblProjectMember").parent().find('.editrft'))
    $("#Projectbody tr").remove();
}

function NextPage() {
    SortableCode = false;
    SortableName = false;
    mypager.fromRowsStart = mypager.fromRowsStart + mypager.endIndex;
    mypager.toRows = mypager.endIndex + mypager.toRows;
    mypager.totalRowsCount = parseInt(mypager.totalRowsCount);
    listAll("TMS0106.aspx/GetSearchData", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'SearchText': document.getElementById('txtSearchText').value }), clearProjectMemberRows, bindNextPageTask, noTask, "tblProjectMember");
}

function bindNextPageTask(Task) {
    
    closedit();
    bindTask(Task);
    if (mypager.toRows >= mypager.totalRowsCount) {
        document.getElementById('PageCount').innerHTML = mypager.fromRowsStart + "-" + mypager.totalRowsCount + " of ";
        $("a.next").hide();
    }
    else {
        document.getElementById('PageCount').innerHTML = mypager.fromRowsStart + "-" + mypager.toRows + " of ";
        $("a.next").show();
    }
}

function PrevPage() {
    
    SortableCode = false;
    SortableName = false;
    mypager.fromRowsStart = mypager.fromRowsStart - mypager.endIndex;
    mypager.toRows = mypager.toRows - mypager.endIndex;
    listAll("TMS0106.aspx/GetSearchData", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'SearchText': document.getElementById('txtSearchText').value }), clearProjectMemberRows, bindPrevPageTask, noTask, "tblProjectMember");
    document.getElementById('PageCount').innerHTML = mypager.fromRowsStart + "-" + mypager.toRows + " of ";
}

function bindPrevPageTask(Task) {
    closedit();
    bindTask(Task);
}

function PageNumber(obj) {
    SortableCode = false;
    SortableName = false;
    mypager.fromRowsStart = mypager.startIndex;
    mypager.toRows = parseInt(obj);
    mypager.endIndex = parseInt(obj);

    listAll("TMS0106.aspx/GetSearchData", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'SearchText': document.getElementById('txtSearchText').value }), clearProjectMemberRows, bindPageSizeTask, noTask, "tblProjectMember");
    document.getElementById('PgIndex').innerHTML = obj;
}

function bindPageSizeTask(Task) {
    closedit();
    bindTask(Task);
}

function MakeEmpNoSortable() {
    debugger
    if (!SortableCode) {
        listAll("TMS0106.aspx/SortEmpNo", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'Sort': 'true' }), clearProjectMemberRows, bindTask, noTask, "tblProjectMember");
        SortableCode = true;
    }
    else {
        listAll("TMS0106.aspx/SortEmpNo", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'Sort': 'false' }), clearProjectMemberRows, bindTask, noTask, "tblProjectMember");
        SortableCode = false;
    }
}

function MakeProjectRoleSortable() {
    debugger
    if (!SortableCode) {
        listAll("TMS0106.aspx/SortProjectRole", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'Sort': 'true' }), clearProjectMemberRows, bindTask, noTask, "tblProjectMember");
        SortableCode = true;
    }
    else {
        listAll("TMS0106.aspx/SortProjectRole", JSON.stringify({ 'FromRow': mypager.fromRowsStart, 'ToRow': mypager.toRows, 'Sort': 'false' }), clearProjectMemberRows, bindTask, noTask, "tblProjectMember");
        SortableCode = false;
    }
}

function SearchResult(obj) {
    
    mypager.fromRowsStart = mypager.startIndex;
    mypager.toRows = mypager.endIndex;
    listAll("TMS0106.aspx/GetSearchData", JSON.stringify({ 'FromRow': mypager.startIndex, 'ToRow': mypager.endIndex, 'SearchText': obj.value }), clearProjectMemberRows, bindTask, noTask, "tblProjectMember");
}

function validateProjectCode(obj) {
    switch (obj.id) {
        case "txtProjectRolePopUp":
            if (obj.value != "")
                return true;
            else
                return false;
            break;
        case "ddlProjectTeamEmployeeNo":
            if (obj.value != "")
                return true;
            else
                return false;
            break;
        case "txtActivityCode":
            if (obj.value != "")
                return true;
            else
                return false;
            break;
        case "txtProjectRoleEdit":
            if (obj.value != "")
                return true;
            else
                return false;
            break;
            case "dtStartDate":
            if (obj.value != "")
                return true;
            else
                return false;
            break;
        case "txtActivityCodeEdit":
            if (obj.value != "")
                return true;
            else
                return false;
            break;
    }

}

// Bind Employee No
function GetEmplyees() {
    listAll("TMS0106.aspx/GetEmplyees", JSON.stringify(), emptyBind, bindEmplyee, emptyBind);
}

function bindEmplyee(emp) {

    $('#ddlProjectTeamEmployeeNo').append($('<option>', {
        value: emp.EmpNo,
        text: emp.Name
    }, '</option>'));
    $('#ddlProjectTeamEmployeeNo').trigger("chosen:updated");

    //$('#ddlProjectTeamEmployeeNoEdit').append($('<option>', {
    //    value: emp.EmpNo,
    //    text: emp.Name
    //}, '</option>'));
    //$('#ddlProjectTeamEmployeeNoEdit').trigger("chosen:updated");
}
// End Bind Employee No




// Bind Project Codes
function GetProjectCodes() {
  //  listAll("TMS0106.aspx/GetProjectCodes", JSON.stringify(), emptyBind, bindProjectCodes, emptyBind);
    
}

function emptyBind() {
}
function bindProjectCodes(projectCode) {
    
    //Bind Project on ADD Form
    //$('#ddlProjectCode').append($('<option>', {
    //    value: projectCode.ProjectCode,
    //    text: projectCode.ProjectCode + '~' + projectCode.ProjectName
    //}, '</option>'));
    //$('#ddlProjectCode').trigger("chosen:updated");
   // alert($('#hdnProjectCode').val());
    
  //  $('#txtProjectCode').val($('#hdnProjectCode').val());
    //Bind Project on Edit Form
   
    $('#ddlProjectCodeEdit').append($('<option>', {
        value: projectCode.ProjectCode,
        text: projectCode.ProjectCode + '~' + projectCode.ProjectName
    }, '</option>'));
    $('#ddlProjectCodeEdit').trigger("chosen:updated");
}
// End Bind Customer Codes


