$.fn.isOnScreen = function(){
    
    var win = $(window);
    
    var viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();
    
    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();
    
    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
    
};




/*** highlight search */
jQuery.fn.highlight = function (pat) {
    function innerHighlight(node, pat) {
        var skip = 0;
        if (node.nodeType == 3) {
            var pos = node.data.toLowerCase().indexOf(pat);
            if (pos >= 0) {
                var spannode = document.createElement('span');
                spannode.className = 'highlight';
                var middlebit = node.splitText(pos);
                var endbit = middlebit.splitText(pat.length);
                var middleclone = middlebit.cloneNode(true);
                spannode.appendChild(middleclone);
                middlebit.parentNode.replaceChild(spannode, middlebit);
                skip = 1;
            }
        }
        else if (node.nodeType == 1 && node.childNodes && !/(script|style)/i.test(node.tagName)) {
            for (var i = 0; i < node.childNodes.length; ++i) {
                i += innerHighlight(node.childNodes[i], pat);
            }
        }
        return skip;
    }
    return this.each(function () {
        innerHighlight(this, pat.toLowerCase());
    });
};

jQuery.fn.removeHighlight = function () {
    function newNormalize(node) {
        for (var i = 0, children = node.childNodes, nodeCount = children.length; i < nodeCount; i++) {
            var child = children[i];
            if (child.nodeType == 1) {
                newNormalize(child);
                continue;
            }
            if (child.nodeType != 3) { continue; }
            var next = child.nextSibling;
            if (next == null || next.nodeType != 3) { continue; }
            var combined_text = child.nodeValue + next.nodeValue;
            new_node = node.ownerDocument.createTextNode(combined_text);
            node.insertBefore(new_node, child);
            node.removeChild(child);
            node.removeChild(next);
            i--;
            nodeCount--;
        }
    }
    return this.find("span.highlight").each(function () {
        var thisParent = this.parentNode;
        thisParent.replaceChild(this.firstChild, this);
        newNormalize(thisParent);
    }).end();
};

/*** highlight search */

function chosencontainersingle() {
    
    var chosencontainersinglel = $('.chosen-container-single').length;
    
	
    if(chosencontainersinglel > 0)
    {
        var count = 0;
        $( ".chosen-container-single" ).each(function(i) {
            var outerw = $( this ).parent().outerWidth();
            var outerwi = outerw -12;
            $( this ).css({'width':outerwi})
            count++;		
        });
				
        $('.panel-collapse').on('shown.bs.modal', function (e) {
            $('.chosen-container-single').parent().outerWidth();
            var count = 0;
            $( ".chosen-container-single" ).each(function(i) {
                var outerw = $( this ).parent().outerWidth();
                var outerwi = outerw -12;
                $( this ).css({'width':outerwi})
                count++
            });
        })
		
		
        $(window).on('shown.bs.collapse', function () {
         
            $('.chosen-container-single').parent().outerWidth();
            var count = 0;
            $( ".chosen-container-single" ).each(function(i) {
                var outerw = $( this ).parent().outerWidth();
                var outerwi = outerw -12;
                $( this ).css({'width':outerwi})
                count++
            });
        })


        $('.modal').on('hidden.bs.modal', function () {
            $(this).find('.chosen-container-single').removeClass('qwert')            
            var count = 0;
            $(".qwert").each(function (i) {
                $(".chosen-container-single").css({ 'width': 'auto'})
                count++
            });
        })




        $('.modal').on('shown.bs.modal', function () {
            $(this).find('.chosen-container-single').addClass('qwert')
            var count = 0;
            $(".qwert").each(function (i) {
                var edr = $(this).parent().outerWidth();
                var outerwi = edr - 12;
                $(this).css({ 'width': outerwi })
                count++
            });
        })

	}
}

function accordianscroll() {
    $('.accordion').on('shown.bs.collapse', function () {
        var panel = $(this).find('.in');
        $('html, body').animate({
            scrollTop: panel.offset().top
        }, 500);
    });

}


function addadvance() {
	
	var addcahadvancerow =  $('.addcahadvance et-row').length;
	
	if(addcahadvancerow > '1')
	{
		$('.addcahadvance et-row').first().find('.addanother').hide();
		$('.addcahadvance et-row').first().find('.deleteicon').show();
		$('.addcahadvance et-row').find('.addanother').hide();
		$('.addcahadvance et-row').last().find('.addanother').show();
	}
	
	
}

function chekboxcontainersave() {
	
	
var ckbox = $('.chekboxcontainersave');

    $('.chekboxcontainersave').on('click',function () {
        if (ckbox.is(':checked')) {
			$(this).parent().next().addClass('et-btn-primary').removeClass('et-btn-disabled').removeAttr('disabled');
        } else {
			$(this).parent().next().addClass('et-btn-disabled').removeClass('et-btn-primary').attr("disabled","disabled");
        }
    });	
	
	
	
}


function stepform() {


    var etstepformw = $('et-stepform et-stepform-ul').outerWidth();
	var etstepformli = $('et-stepform .nav-tabs li').length;
	var etstepformlit = parseInt(etstepformw) / parseInt(etstepformli) ; 
	var etstepformlitc = 100 / parseInt(etstepformli) ; 
	$('et-stepform .nav-tabs').css({ 'width': '2000px', 'display': 'block' })
    $('et-stepform et-stepform-ul').css({ 'width': etstepformw, 'display': 'block', 'overflow': 'hidden' })
	$('et-stepform .nav-tabs li').css({ 'width': etstepformlit})
	var rigthscroll =  etstepformlit * 2;
	
	$('et-stepform .stepformcirlce li').css({ 'width': etstepformlitc + '%' })
	
	var stepformcirlcew = $('et-stepform .stepformcirlce').outerWidth();
	var stepformcirlceli = $('et-stepform .stepformcirlce li').length;
	var stepformcirlcelip = 100 / parseInt(stepformcirlceli);
	$('et-stepform .stepformcirlce li').css({ 'width': stepformcirlcelip + '%' })


    var count = 0; // To Count Blank Fields
    /*------------ Validation Function-----------------*/
    /*$(".submit_btn").click(function (event) {
        var radio_check = $('.rad'); // Fetching Radio Button By Class Name
        var input_field = $('.text_field'); // Fetching All Inputs With Same Class Name text_field & An HTML Tag textarea
        var text_area = $('textarea');
        // Validating Radio Button
        if (radio_check[0].checked == false && radio_check[1].checked == false) {
            var y = 0;
        } else {
            var y = 1;
        }
        // For Loop To Count Blank Inputs
        for (var i = input_field.length; i > count; i--) {
            if (input_field[i - 1].value == '' || text_area.value == '') {
                count = count + 1;
            } else {
                count = 0;
            }
        }
        // Notifying Validation
        if (count != 0 || y == 0) {
            alert("*All Fields are mandatory*");
            event.preventDefault();
        } else {
            return true;
        }
    });*/
    /*---------------------------------------------------------*/
	
	if($('.activestepform1').hasClass('activestepformdone1'))
		{
			$('.activestepformdone1').closest('li').find('.cirlce').html("<i class=et-tick-9></i>");	
	}

	if ($('.activestepform1').hasClass('activestepformdone1')) {
	    $('.activestepformdone1').closest('li').find('.cirlce').html("<i class=et-tick-9></i>");
	}
	
    $(".next_btn").click(function () { // Function Runs On NEXT Button Click
        $(this).closest('stepform-fieldset').next().fadeIn('slow');
        $(this).closest('stepform-fieldset').css({
            'display': 'none'
        });
        // Adding Class Active To Show Steps Forward;
        $('.activestepform').next().addClass('activestepform');
        $('.activestepform').prev().addClass('activestepformdone');
		
		
		$('.activestepform1').next().addClass('activestepform1');
        $('.activestepform1').prev().addClass('activestepformdone1');
		
		if($('.activestepform1').hasClass('activestepformdone1'))
		{
			$('.activestepformdone1').closest('li').find('.cirlce').html("<i class=et-tick-9></i>");	
		}
		
		
		
		
				
		
		$('.activestepform').next().addClass('tabonscreen');
		$('.activestepform').prev().removeClass('tabonscreen');
		//alert($('.tabonscreen').isOnScreen());
		if($('.tabonscreen').isOnScreen()) {
			
		}
		
		else {
			$('et-stepform .nav-tabs').animate({"right":rigthscroll});
		}
			
    });
	
	
    $(".next_btn1").click(function () { // Function Runs On NEXT Button Click
        $(this).closest('stepform-fieldset').next().fadeIn('slow');
        $(this).closest('stepform-fieldset').css({
            'display': 'none'
        });
		$('.activestepform1').next().addClass('activestepform1');
        $('.activestepform1').prev().addClass('activestepformdone1');
		
		if($('.activestepform1').hasClass('activestepformdone1'))
		{
			$('.activestepformdone1').closest('li').find('.cirlce').html("<i class=et-tick-9></i>");	
		}
			
    });	
	
	
    $(".next_btn2").click(function () { // Function Runs On NEXT Button Click
        $(this).closest('stepform-fieldset').next().fadeIn('slow');
        $(this).closest('stepform-fieldset').css({
            'display': 'none'
        });
		$('.activestepform2').next().addClass('activestepform2');
        $('.activestepform2').prev().addClass('activestepformdone2');
		
		if($('.activestepform2').hasClass('activestepformdone2'))
		{
			$('.activestepformdone2').closest('li').find('.cirlce').html("<i class=et-tick-9></i>");	
		}
			
    });		
	
	
	
	
	
	
    $(".pre_btn").click(function () { // Function Runs On PREVIOUS Button Click
        $(this).closest('stepform-fieldset').prev().fadeIn('slow');
        $(this).closest('stepform-fieldset').css({
            'display': 'none'
        });
        // Removing Class Active To Show Steps Backward;
        $('.activestepform:last').removeClass('activestepform');
        $('.activestepform:last').removeClass('activestepformdone');
    });
    // Validating All Input And Textarea Fields
    /*$(".submit_btn").click(function (e) {
        if ($('input').val() == "" || $('textarea').val() == "") {
            alert("*All Fields are mandatory*");
            return false;
        } else {
            return true;
        }
    });*/



}

function versionofcssjs() {

    var min = 1,
        max = 5,
        NumberResult = Math.random() * (max - min) + min;
    $('link[href="et-uiassets/css/et-style.css"]').replaceWith('<link href=et-uiassets/css/et-style.css?v=' + NumberResult + ' rel="stylesheet">');
    $('link[href="et-uiassets/css/toastr.css"]').replaceWith('<link href=et-uiassets/css/toastr.css?v=' + NumberResult + ' rel="stylesheet">');
    $('link[href="et-uiassets/css/et-fonts.css"]').replaceWith('<link href=et-uiassets/css/et-fonts.css?v=' + NumberResult + ' rel="stylesheet">');
    $('script[src="et-uiassets/js/common.js"]').replaceWith('<script  src=et-uiassets/js/common.js?v=' + NumberResult + ' type="text/javascript"></script>');
    $('script[src="et-uiassets/js/etmasonry.js"]').replaceWith('<script  src=et-uiassets/js/etmasonry.js?v=' + NumberResult + ' type="text/javascript"></script>');
  
}

function etinlinesearchall() {
    $('et-search-all et-inlinesearchall').on('mouseenter , mouseover', function () {
        $('et-search-all et-inlinesearchall .etsearch').css({ 'right': '0', 'left': 'unset', 'color': '#5d64ca' })
        $('et-search-all et-inlinesearchall .etsearch i').css({ 'color': '#5d64ca' })
    }).on('mouseout , mouseleave', function () {
        $('et-search-all et-inlinesearchall .etsearch').css({ 'right': 'unset', 'left': '0px' })
        $('et-search-all et-inlinesearchall .etsearch i').css({ 'color': '#949494' })
    })
}

function navtabs() {
    $('#taboutput li').first().addClass('active')
    $('#pitrestoutput .tab-pane').first().addClass('tab-pane fade in active');

    $(".nav-tabs a").click(function () {
        searchwithintable();
        $(this).parent().parent().parent().find('input[type=text]').val('');
        $(this).parent().parent().next().find('#pitrestoutput .dashboardlink').show();

    });


    $('.nav-tabs a').on('shown.bs.tab-pane', function (event) {
        searchwithintable();

    });

}

function menutabs() {

    var count = 0;
    $(".accordionsmenudashboard-accordion").each(function (i) {
        $(this).attr("accordion-id", ".dahboardmenu" + count);
        count++;
    });


    $("#masonrydiv .accordionsmenudashboard-accordion").each(function (i) {
        var idd = $(this).attr('accordion-id');

        var url = idd;
        var type = url.split('.');
        var dotsed = '';
        if (type.length > 1)
            dotsed = type[1];


        $(this).parent().next().find('.accordionsmenudashboardpanelgroup').addClass(dotsed)
        //accordionsmenudashboardpanelgroup
    })



    $(".accordionsmenudashboard-accordion").on("click", function () {
        var accordionId = $(this).attr("accordion-id"),
          numPanelOpen = $(accordionId + ' .collapse.in').length;
        $(this).toggleClass("active");
        if (numPanelOpen == 0) {
            openAllPanels(accordionId);
        } else {
            closeAllPanels(accordionId);
        }
    })

    openAllPanels = function (aId) {
        //console.log("setAllPanelOpen");
        $(aId + ' .panel-collapse:not(".in")').collapse('show');
    }
    closeAllPanels = function (aId) {
        //console.log("setAllPanelclose");
        $(aId + ' .panel-collapse.in').collapse('hide');
    }
}

function tabscroll() {
    var count = 0;

    $('.slidetabs et-tabpage-bg').each(function () {
        var w8 = $(this).width();
        $(this).css({ 'width': w8 })
    })

    $('.slidetabs et-tabpage-bg .slidetabslist ').each(function () {
        var w9 = $(this).find('li').width();
        //$(this).find('li').css({'width':w9});
        $(this).find('li').first().addClass('firsttab')
        $(this).find('li').last().addClass('lasttab')
    })

    $('.lefttab').bind("click", function () {
        event.preventDefault();
        count++;
        var rightside = $(this).next().next().find("li:nth-child(" + +count + ")").addClass('rightwidth');
        var e1 = $('.rightwidth').width();
        var e2 = parseInt(e1);
        var e3 = Math.round(e2);
        console.log(e3);
        $(this).next().next().animate({ right: "+=" + e3 }, "slow");
        $(this).next().next().find('li').removeClass('rightwidth')
    })


    /*
      
        $('.righttab').click(function() {
            event.preventDefault();
            $('.slidetabslist').animate({right: "-="+slidewitht}, "slow");
            var w8 = $('.slidetabslist').css('right');
            console.log (w8);
            if(w8 == slidewitht+'px')
            {
                $(this).hide();				
                $('.lefttab').show();
            }
            else{
    
            }
       });	
    */


}


function tableedgetab() {

    if ($('.tab-pane').hasClass('.in')) {
        $('.tab-pane et-content-body .tableedge').parent().css({ 'margin-left': '-10px', 'margin-right': '-10px' })
        $('.tab-pane.in .tableedge').parent().css({ 'margin-left': '-10px', 'margin-right': '-10px' })
    }

    $('.tab-pane.in .tableedge').parent().css({ 'margin-left': '-10px', 'margin-right': '-10px' })

    var edir = $('.tab-pane et-content-body .tableedge tfoot et-table-footer').width();
    $('et-tab-section').find('et-table-footer').css({ 'width': edir })

    $('et-tab-section .nav-tabs li a').bind("click", function () {
        $('.tab-pane.in .tableedge').parent().css({ 'margin-left': '-10px', 'margin-right': '-10px' })
    })


}

function popover() {

    $('.hoveropup[data-toggle="popover"]').popover({
        trigger: 'hover'
    });

    $(".htmlpopovercontent[data-toggle=popover]").each(function (i, obj) {
        $(this).popover({
            html: true,
            content: function () {
                var id = $(this).attr('id');
                return $('.popover-content-' + id).html();
            }
        });
    });

}


function enableEditSupport() {
    
    //$('.et-hdden-row').find('et-switch').css({'visibility':'hidden'})
    $('.et-hdden-row').on('show.bs.collapse', function () {

        $('.collapse.in').each(function () {
            $(this).collapse('hide');
            $(this).parent().parent().prev().show().removeClass('parenttd');

        });

        $(this).parent().parent().prev().hide().addClass('parenttd');
        //$('.et-hdden-row').find('et-switch').css({'visibility':'hidden'})

        var sdrht = $(this).parent().parent().parent().parent().parent().find('.edittablefrom').outerHeight();
        $(this).parent().parent().parent().parent().parent().find('.edittablefrom').appendTo($(this));
        $(this).css({ 'height': sdrht })
        $(this).parent().parent().parent().parent().parent().find('.edittablefrom').css({ 'display': 'block', 'position': 'relative', 'top': '0px' })
        $(this).css({ 'height': sdrht })

    });

    $('.et-hdden-row').on('shown.bs.collapse', function () {

        $('.et-hdden-row').find('et-switch').css({ 'visibility': 'visible' })
        var a1 = $(this).find('.et-switch-light > div div').innerWidth();
        var a1t = parseInt(a1) * 2 + 10;
        $(this).find('.et-switch-light > div div').css({ 'width': a1 })
        $(this).find('et-switch').css({ 'width': a1t })
        $(this).find('.et-switch-light > div div:nth-child(2)').css({ 'left': '0px' })
        $(this).find('.et-switch-light > div div').css({ 'left': '0px' });

        var sdrht = $(this).parent().parent().parent().parent().parent().find('.edittablefrom').outerHeight();
        $(this).parent().parent().parent().parent().parent().find('.edittablefrom').appendTo($(this));
        $(this).css({ 'height': sdrht })
        $(this).parent().parent().parent().parent().parent().find('.edittablefrom').css({ 'display': 'block', 'position': 'relative' })
        $(this).css({ 'height': sdrht })


    })



    $('.closetabletd').bind("click", function () {
        $('.et-hdden-row.collapse.in').collapse('hide');
        $('.parenttd').show();
        $('.sedr').css({ 'display': 'none', 'position': 'absolute', 'top': '0px' })
    })
    /** accordian**/

    $("body .et-hdden-row").each(function () {

        var ethiddenrowidth = $(this).prev().addClass('ethiddenrowparent').outerWidth();
        $(this).css({ 'width': ethiddenrowidth })
        $('.table-panel-collapse.collapse.in , .table-panel-collapse.collapse').css({ 'width': ethiddenrowidth });

    })
	
	 $('.et-switchshohide').bind("click", function () {
		$(this).find($('input + .et-switch-slider')).parent().parent().parent().next().hide();
		$(this).find($('input:checked + .et-switch-slider')).parent().parent().parent().next().show();
		chosencontainersingle();
	 })
	
	

}


function closeEdit(saveButton) {
    $this = $(saveButton).closest("table").parent();
    $this.find('.edittablefrom').css({ 'display': 'none', 'position': 'absolute' }).insertBefore($this.find('.editrft'))
    $this.find('.collapse.in').collapse('hide').css({ 'height': '0' }).attr("aria-expanded", "false").removeClass('in');
    $this.find('.parenttd').show().removeClass('parenttd');
}

function closedit() {

    $('.savetds').bind("click", function () {
        $this = $(this).closest("table").parent();
        $this.find('.edittablefrom').css({ 'display': 'none', 'position': 'absolute' }).insertBefore($this.find('.editrft'))
        $this.find('.collapse.in').collapse('hide').css({ 'height': '0' }).attr("aria-expanded", "false").removeClass('in');
        $this.find('.parenttd').show().removeClass('parenttd');
    })

}

function imageuplodhandler() {
    $('.btnUpload').click(function () {
        var fileUpload = $(".FileUpload1").get(0);
        var files = fileUpload.files;
        var test = new FormData();
        for (var i = 0; i < files.length; i++) {
            test.append(files[i].name, files[i]);
        }
        $.ajax({
            url: "UploadHandler.ashx",
            type: "POST",
            contentType: false,
            processData: false,
            data: test,
            // dataType: "json",
            success: function (result) {
                alert(result);
            },
            error: function (err) {
                alert(err.statusText);
            }
        });
    });
}




function listingstyle() {

    $('.nextprevfadediv .next').each(function (i) {
        var $this = $(this);
        var newClass = "next" + i++;
        $this.addClass(newClass);
    });

    $('.stackcardparendiv').each(function (i) {
        $(this).find('.listing').last().addClass('lastinclass');
        $(this).find('.listing').first().addClass('firstinclass');
    })



    var sdse = $('.stackcardparendiv').outerHeight();
    var sdfw = $('.stackcardparendiv').outerWidth();
    $('.listing').css('width', sdfw);

    $('.listing').css({ 'height': sdse })
    $('.listing').last().addClass('lastinclass');
    $('.listing').first().addClass('firstinclass');


    $('.listing:nth-child(1)').css({ 'top': '00px', 'z-index': '6', 'width': '100%', 'left': '00px', 'overflow': 'hidden' })
    $('.listing:nth-child(2)').css({ 'top': '10px', 'z-index': '5', 'width': '85%', 'left': '7.5%', 'overflow': 'hidden' }).find('.swipledeck').fadeOut()
    $('.listing:nth-child(3)').css({ 'top': '20px', 'z-index': '4', 'width': '75%', 'left': '12.5%', 'overflow': 'hidden' }).find('.swipledeck').fadeOut()
    $('.listing:nth-child(4)').css({ 'top': '30px', 'z-index': '3', 'width': '65%', 'left': '17.5%', 'overflow': 'hidden' }).find('.swipledeck').fadeOut()
    $('.listing:nth-child(5)').css({ 'top': '40px', 'z-index': '2', 'width': '55%', 'left': '22.5%', 'overflow': 'hidden' }).find('.swipledeck').fadeOut()
    $('.listing:nth-child(6)').css({ 'top': '50px', 'z-index': '1', 'width': '45%', 'left': '27.5%', 'overflow': 'hidden' }).find('.swipledeck').fadeOut()


    $('.next').bind("click", function () {

        var $this = $(this).parent().parent().prev()
        $this.find('.listing').first().delay("100").addClass('listingcard').animate({ left: "120%" }, '100');
        $this.find('.listing').first().delay("00").animate({ zIndex: "1" }, '100');
        $this.find('.listing .collapse').first().collapse('hide')
        $this.find('.listing:nth-child(2)').delay("100").animate({ top: "00px", width: '100%', left: '00px', 'z-index': '5' }, '100').find('.swipledeck').fadeIn();
        $this.find('.listing:nth-child(3)').delay("250").animate({ top: "10px", width: '85%', left: '7.5%', 'z-index': '4' }, '100').find('.swipledeck').fadeOut();
        $this.find('.listing:nth-child(4)').delay("400").animate({ top: "20px", width: '75%', left: '12.5%', 'z-index': '3' }, '100').find('.swipledeck').fadeOut();
        $this.find('.listing:nth-child(5)').delay("550").animate({ top: "30px", width: '65%', left: '17.5%', 'z-index': '2' }, '100').find('.swipledeck').fadeOut();
        $this.find('.listing:nth-child(6)').delay("700").animate({ top: "40px", width: '55%', left: '22.5%', 'z-index': '1' }, '100').find('.swipledeck').fadeOut();
        $this.find('.listingcard').insertAfter($this.find(".listing:last-child")).delay("0").animate({ left: "22.5%", 'width': '55%', 'top': '0px' }, '0').find('.swipledeck').fadeOut(2200);
        $this.find('.listingcard').delay("0").animate({ 'top': '40px' }, '0').removeClass('listingcard');


        var lastinclassindex = $this.find('.lastinclass').index();

        if (lastinclassindex == '0') {
            $(this).parent().find('.nextprevfade').show()
        }
        else {
            $('.next').parent().find('.nextprevfade').hide();
            $('.prev').parent().find('.nextprevfade').hide()
        }
    })


    $('.prev').bind("click", function () {
        var $this = $(this).parent().parent().prev()
        $this.find('.listing').last().delay("00").addClass('listingcard').animate({ top: "0%" }, '0');
        $this.find('.listing').last().delay("00").addClass('listingcard').animate({ left: "110%", width: "100%", zIndex: "9" }, '0').find('.swipledeck').fadeIn();
        $this.find('.listing').last().delay("00").animate({ zIndex: "9" }, '0').find('.swipledeck').fadeIn();
        $this.find('.listing  .collapse').first().collapse('hide')
        $this.find('.listing:nth-child(1)').delay("1200").animate({ 'z-index': '5' }, ' 100').find('.swipledeck').fadeIn();
        $this.find('.listing:nth-child(1)').delay("100").animate({ top: "10px", left: '7.5%', width: '85%', 'z-index': '5' }, ' 0').find('.swipledeck').fadeIn();
        $this.find('.listing:nth-child(2)').delay("2100").animate({ top: "20px", width: '75%', left: '12.5%', 'z-index': '4' }, ' 100').find('.swipledeck').fadeIn();
        $this.find('.listing:nth-child(3)').delay("2400").animate({ top: "30px", width: '65%', left: '17.5%', 'z-index': '3' }, '100').find('.swipledeck').fadeOut();
        $this.find('.listing:nth-child(4)').delay("2700").animate({ top: "40px", width: '55%', left: '22.5%', 'z-index': '2' }, '100').find('.swipledeck').fadeOut();
        $this.find('.listingcard').insertBefore($this.find('.listing:first-child')).delay("100").animate({ left: "0px", 'width': '100%', 'top': '0px' }, '0').find('.swipledeck').fadeIn();
        $('.listingcard').delay("0").animate({ 'top': '0px' }, '0').removeClass('listingcard');

        var firstinclassindex = $this.find('.firstinclass').index();

        if (firstinclassindex == '0') {
            $(this).parent().find('.nextprevfade').show()
        }
        else {
            $('.next').parent().find('.nextprevfade').hide();
            $('.prev').parent().find('.nextprevfade').hide();
            $this.find('.firstinclass').find('.swipledeck').fadeOut();
        }

    })

}


function scrollbarcommon() {
	if ($(".scrollbarcommon").length > 0) {
        $('.scrollbarcommon').scrollbar();
    }
}




function hasslider() {

    if ($(".slider").length > 0) {
        rangeslider();
    }

    else {

    }

}




function datetimepickerf() {

    if ($(".datepicker").length > 0) {

        /** date picker **/
        $('.datepicker').datetimepicker({
            format: 'L',
            format: 'MM-DD-YYYY'
        });
        /** date picker **/

    }


    if ($(".timepicker").length > 0) {
        /** time picker **/
        $('.timepicker').datetimepicker({
            format: 'LT'
        });
        /** time picker **/
    }






}

function rangeslider() {
    $('.simplerangeslider').slider({});
    $(".simplerangeslider").on('slide', function (slideEvt) {
        $(".simplerangesliderSliderVal").text(slideEvt.value);

        var div = slideEvt.value; // this is what the plugin says
        $(".simplerangesliderCurrentSliderValLabel").html(div);

    });

    $(".wordrangeslider").slider({
        ticks: [0, 100, 200, 300, 400],
        ticks_labels: ['$0', '$100', '$200', '$300', '$400'],
        ticks_snap_bounds: 30,
        value: 200
    });
    $(".wordrangeslider").on('slide', function (slideEvt) {
        $(".wordrangesliderSliderVal").text(slideEvt.value);
        var divs = slideEvt.value; // this is what the plugin says
        $(".wordslidervalue").html(divs);
    });



}


function withoutfixedtopright() {

    if ($("et-fixedrightbar").length > 0 || $("et-fixedtopbar").length > 0) {

    }

    else {

        $('et-mainpagecontent').css({ 'width': '97%', 'margin-top': '0' })
    }

}

function calendariconclick() {
    $('et-calendarformgroup').click(function () {
        $(this).find('.datepicker').focus();
    })

    $('et-timeformgroup').click(function () {
        $(this).find('.timepicker').focus();
    })


}



function switchlight() {

    $("et-switch").each(function () {

        var etswitchlightinlinew = $(this).find('div div').outerWidth();
        var etswitchlightinlinewt = parseInt(etswitchlightinlinew) + 15;
        $(this).css({ 'width': etswitchlightinlinew })
        var etswitchlightinlinewa = parseInt(etswitchlightinlinewt) * 2;
        $(this).css({ 'width': etswitchlightinlinewa })
    })


}

function tipsydisplay() {

    $('.errortipsy').click(function () {
        // make it not dissappear
        toastr.error("Please read the error. And then click", "This is error", {
            "timeOut": "0",
            "extendedTImeout": "0"
        });
    });
    $('.infotipsy').click(function () {
        // title is optional
        toastr.info("Info Message", "This is info");
    });
    $('.warningtipsy').click(function () {
        toastr.warning("Warning");
    });
    $('.successtipsy').click(function () {
        toastr.success("You have done a successfull tipsy. Cheers");
    });

}

function formvalidation() {

    $('.errorbtn').bind("click", function () {
        $('#errorinput').prev().parent().addClass('has-error')
        $('#errorinput').prev().parent().find('formerror').show();
    })
}



function searchwithintable() {
    $("et-inlinesearch input[type=text]").on("keyup", function () {
        var value = $(this).val().toLowerCase().trim();
        $(this).parent().parent().parent().find('tbody tr').filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            var searchTerm = $("et-inlinesearch input[type=text]").val();
            $('.table tbody').removeHighlight();
            if (searchTerm) {
                // highlight the new term
                $('.table tbody').highlight(searchTerm);
            }


            var tbodyh = $(".table tbody").height();

            if (tbodyh == '0') {

                $(this).find('.searchtr').show();

            }

            else {
                $('.searchtr').hide();
            }
        });
    });


    $("et-inlinesearch input[type=text]").on("keyup", function () {
        var value = $(this).val().toLowerCase().trim();
        $(this).parent().parent().parent().parent().parent().next().find('tbody tr').filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)

            var tbodyh = $(this).closest('tbody').height();
            console.log(tbodyh);
            if (tbodyh < '54') {
                $(this).closest('tbody .searchtr').addClass('searchtrdisplay').removeClass('searchtr')

            }

            else {
                $(this).closest('tbody .searchtrdisplay').removeClass('searchtrdisplay').addClass('searchtr')
            }

            var searchTerm = $("et-inlinesearch input[type=text]").val();

            $('.table tbody').removeHighlight();
            if (searchTerm) {
                // highlight the new term
                $('.table tbody').highlight(searchTerm);
            }



        });
    });


    $("et-inlinesearch input[type=text]").on("keyup", function () {
        var value = $(this).val().toLowerCase().trim();
        $(this).parent().parent().parent().parent().parent().parent().next().find('#pinBoot .white-panel').filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });




        var tbodyh = $(".table tbody");

        if (tbodyh == '0') {
            $('.searchtr').show();
        }

        else {
            $('.searchtr').hide();
        }

    });



    $(".search10 input[type=text]").on("keyup", function () {
        $(this).parent().parent().parent().parent().parent().next().addClass('good')
        var value = $(this).val().toLowerCase().trim();
        $(this).parent().parent().parent().parent().parent().next().find('et-personinfo-two').filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            var searchTerm = $(".search10 input[type=text]").val();
            $('et-personinfo-two').removeHighlight();
            if (searchTerm) {
                // highlight the new term
                $('et-personinfo-two').highlight(searchTerm);
            }
        });
    });
}

function ettablefooter() {
    $("body et-table-footer").each(function () {
        var ettablefooterw = $(this).parent().outerWidth() - 30;
        $(this).css({ 'width': ettablefooterw })
    });
}


function tabsettablefooter() {
    $('et-tab-section .nav-tabs li a').on("click", function () {
        //alert($(this).html())
        var efrt = $('et-tab-section').find('.table').outerWidth() - 30;
        //$('et-tab-section').find('et-table-footer').css({'width':efrt})
        //$('et-table-footer').css({'width':'100%'})
    });
}

function clearinput() {
    $('et-inlinesearch a:nth-child(2)').bind("click", function () {
        $(this).parent().find('input[type=text]').val('');
        $(this).parent().parent().parent().find('tbody tr').removeAttr("style")
    });
}


function clearinput2() {
    $('et-inlinesearch a:nth-child(2)').bind("click", function () {
        $(this).parent().find('input[type=text]').val('');
        $(this).parent().parent().parent().find('tbody tr').removeAttr("style")
    });
}


function collapsible() {
    $("et-collapse-toggle i").removeClass("et-expand-more-1").addClass("et-expand-less-3");
    $('.collpaseicon').on('shown.bs.collapse', function () {
        $(this).parent().find("et-collapse-toggle i").removeClass("et-expand-less-3").addClass("et-expand-more-1"); switchlight();
    }).on('hidden.bs.collapse', function () {
        $(this).parent().find("et-collapse-toggle i").removeClass("et-expand-more-1").addClass("et-expand-less-3"); switchlight();
    });
}


function windowbottstrap() {
    var wwidth = $(window).width();
    /*var wtrightwidth = $('et-fixedrightbar').width() +5;*/
    var wtrightwidth = $('et-fixedrightbar').width();
    var widthtotal = parseInt(wwidth) - parseInt(wtrightwidth) - 30;
	var fixedfooter1 = parseInt(wwidth) - parseInt(wtrightwidth) 
    $('et-mainpagecontent').css({ 'width': widthtotal, 'margin-left': wtrightwidth })
	$('fixedfooter1').css({ 'width': fixedfooter1, 'margin-left': wtrightwidth })

    var toph = $('et-fixedtopbar').outerHeight();
    var topht = parseInt(toph);
    $('et-fixedrightbar').css('margin-top', topht);
}


function loadafterosometime() {
    setTimeout(function () {

        $('.bodycover').hide();


    }, 100);
}

function scrolletfixedtopbar() {
    var fixedrightbarh = $('et-fixedrightbar').outerHeight() - 50;
    var wheight = $(window).height();

    if (fixedrightbarh > wheight) {
        //alert('1')
    }
    else {
        $('#etfixedrightbar').css({ 'height': fixedrightbarh })

    }

}


function bodyscrollbar() {
    var bodyscrollbarh = $('#bodyscrollbar').outerHeight();
    var wheight = $(window).height();

    $('#bodyscrollbar').css({ 'height': bodyscrollbarh })



}



(function($) {
    $(window).bind("load", function() {

    $('.searchtr').hide();

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {


        $('et-fixedrightbar').css({ 'left': '-130px' });
        /**sho hide left side menu for mobile**/
        $(".slide-toggle").click(function () {
            var hidden = $("et-fixedrightbar");
            if (hidden.hasClass('visible')) {
                hidden.animate({ "left": "-130px" }, "fast").removeClass('visible');
            } else {
                hidden.animate({ "left": "0px" }, "fast").addClass('visible');
            }
        });

        /**sho hide left side menu for mobile**/


        $("et-fixedrightbar a").click(function () {

            var hidden = $("et-fixedrightbar");
            hidden.animate({ "left": "-130px" }, "fast").removeClass('visible');

        })

    }

    else {


        windowbottstrap();

    }



    var toph = $('et-fixedtopbar').outerHeight();
    var topht = parseInt(toph) + 10;
    $('et-mainpagecontent').css('margin-top', topht);






    // Select all links with hashes
    $('.samepagelink[href*="#"]')
      // Remove links that don't actually link to anything
      .not('[href="#"]')
      .not('[href="#0"]')
      .click(function (event) {
          // On-page links
          if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
            &&
            location.hostname == this.hostname
          ) {
              // Figure out element to scroll to
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
              // Does a scroll target exist?
              if (target.length) {
                  // Only prevent default if animation is actually gonna happen
                  event.preventDefault();
                  $('html, body').animate({
                      scrollTop: target.offset().top - parseInt(topht)
                  }, 1000, function () {
                      // Callback after animation
                      // Must change focus!
                      var $target = $(target);
                      $target.focus();
                      if ($target.is(":focus")) { // Checking if the target was focused
                          return false;
                      } else {
                          $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                          $target.focus(); // Set focus again
                      };
                  });
              }
          }
      });



    /** accordian**/
	
    datetimepickerf();
    navtabs();
    calendariconclick();
    withoutfixedtopright();
    formvalidation();
    closedit();
    enableEditSupport();
    collapsible();
    clearinput();
    loadafterosometime();
    scrolletfixedtopbar();
    ettablefooter();
    searchwithintable();
    tipsydisplay();
    switchlight();
    hasslider();
    listingstyle();
    imageuplodhandler();
    popover();
    tableedgetab();
    tabsettablefooter();
    tabscroll();
    menutabs();
    $('.searchtr').hide();
    etinlinesearchall();
    scrollbarcommon();
    stepform();
    accordianscroll();
	chekboxcontainersave();
	addadvance();
	chosencontainersingle();
    versionofcssjs();
   

$('et-mainpagecontent').delay(800).removeClass('hide').fadeIn(100);
   
     
   
	});
	
})(jQuery);



$(window).resize(function () {
	
    datetimepickerf();


    windowbottstrap();
    withoutfixedtopright();
    formvalidation();
    closedit();
    enableEditSupport();
    collapsible();
    clearinput();
    loadafterosometime();
    scrolletfixedtopbar();
    ettablefooter();
    searchwithintable();
    tipsydisplay();
    switchlight();
    hasslider();
    listingstyle();
    imageuplodhandler();
    popover();
    tableedgetab();
    tabsettablefooter();
    tabscroll();
    etinlinesearchall()
    $('.searchtr').hide();
    scrollbarcommon();
    stepform();
    accordianscroll();
	chekboxcontainersave();
	addadvance();
	chosencontainersingle();
    versionofcssjs();
   $('et-mainpagecontent').delay(800).removeClass('hide').fadeIn(100);

});