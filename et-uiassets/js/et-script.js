﻿// ----- et-js-framework ------

// Global variables
var recordModeEnum = Object.freeze({ "NEW": 1, "EDIT": 2 });
var accessToken = '';

// Common objects
function etPager() {
    this.fromRowsStart = 1;
    this.toRows = 5;
    this.startIndex = 1;
    this.endIndex = 5;
    this.totalRowsCount;
    this.pagesPerRow = ["25", "50", "100", "200", "500", "ALL"];
    this.displayRowTemp =
        "<tr class='searchtr'>" +
             "<td colspan='15' class='et-text-center et-text-danger'>No Records Found</td>" +
        "</tr>";
}

// select control with infinite scroll ------------------------------------------------------------
var etslectloadondemandcontrol = null;

function etSelectLoadOnDemand(selectcontrol) {
    debugger
    this.control = selectcontrol,
    this.dataurl = null,
    this.width = '100%',
    this.allowClear = true,
    this.multiple = false,
    this.init = function () {
        $(this.control).select2({
            width: this.width,
            allowClear: this.allowClear,
            multiple: this.multiple,
            placeholder: {
                id: -1,
                text: '..please select..'
            },
            ajax: {
                type: 'POST',
                url: this.dataurl,
                contentType: "application/json; charset=utf-8",
                async: true,
                dataType: 'json',
                data: function (params) {
                    return "{'searchFilter':'" + (params.term || "") + "','searchPage':'" + (params.page || 1) + "'}";
                },
                processResults: function (res, params) {
                    debugger
                    //var jsonData = JSON.parse(res.d);
                    var jsonData = res.d;
                    params.page = params.page || 1;
                    var data = { more: (jsonData[0] != undefined ? jsonData[0].more : false), results: [] }, i;
                    for (i = 0; i < jsonData.length; i++) {
                        data.results.push({ id: jsonData[i].id, text: jsonData[i].text });
                    }
                    return {
                        results: data.results,
                        pagination: {
                            more: data.more
                        }
                    };
                }
            }
        });
    }
}



function ETCollectionItem(id, value, isNew) {
    this.id = id;
    this.value = value;
    this.isNew = isNew;
    this.isChanged = false;
    this.isDeleted = false;
}

function ETCollection() {

    this.items = [];

    var idcounter = 0;

    this.getNextId = function () {
        return --idcounter;
    }

    this.addItem = function (item) {
        this.items.push(item);
    }

    this.removeItem = function (id) {
        var index = null;
        var itemToRmove = null
        for (var i = 0; i < this.items.length; i++) {
            if (this.items[i].id === id) {
                index = i;
                itemToRmove = this.items[i];
                break;
            }
        }

        if (itemToRmove != null) {
            if (itemToRmove.isNew) {
                this.items.splice(index, 1);
            } else {
                detailObj.isDeleted = true;
            }
        }
    }

    this.contains = function (id) {
        var index = null;
        for (var i = 0; i < this.items.length; i++) {
            if (this.items[i].id == id) {
                index = i;
                break;
            }
        }
        return (index != null);
    }

    this.getItemWithID = function (id) {
        var index = null;
        for (var i = 0; i < this.items.length; i++) {
            if (this.items[i].id == id) {
                index = i;
                break;
            }
        }

        if (index != null) {
            return this.items[index];
        } else {
            return null;
        }
    }

    this.getItemIndex = function (id) {
        var index = null;
        for (var i = 0; i < this.items.length; i++) {
            if (this.items[i].id == id) {
                index = i;
                break;
            }
        }

        return index;
    }
}

// Common methods
function asyncCall(uri, jsonParams, successCallback) {
    
    $.ajax({
        url: uri,
        type: "POST",
        data: jsonParams,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            var json = JSON.stringify(response.d);
            var retval = JSON.parse(json);
            successCallback(retval);
        },
        failure: function (response) {
            OnError(response);
        },
        error: function (response) {
            OnError(response);
        }
    });

    function OnError(e) {
        alert(e.status + '\n' + e.statusText);
    }
}

function listAll(url, clearFunction, bindItemFunction, emptyFunction) {
    listAll(url, null, clearFunction, bindItemFunction, emptyFunction);
}

function listAll(url, jsonParams, clearFunction, bindItemFunction, emptyFunction, containerID) {
    
    asyncCall(url, jsonParams, function (response) {

        clearFunction();
        var res = $.parseJSON(response); 
        if (res.SUC) {
            var data = res.GenericObject;
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    bindItemFunction(data[i]);
                }
                showPager(containerID, data.length, data[0].TotalCount);
            } else {
                emptyFunction();
            }
        }
        else {
            showETMessageBox(res.MSG, 'Error occurred!', '', null);
        }
    });
}

function showPager(containerID, length, totalRows) {
    debugger
    var tr2 = mypager.displayRowTemp;
    $("#" + containerID + " > tbody").append(tr2);
    enableEditSupport(); 
    document.getElementById('PgIndex').innerHTML = mypager.endIndex;
    document.getElementById('PageCount').innerHTML = mypager.fromRowsStart + "-" + mypager.toRows + "  of ";
    document.getElementById('TotalRows').innerHTML = totalRows;
    mypager.totalRowsCount = totalRows;
    if (length < mypager.endIndex) {

        $("a.next").hide();
        $("a.previous").show();
    }
    else {
        $("a.next").show();
    }
    if (mypager.toRows == mypager.endIndex) {

        $("a.previous").hide();
        $("a.next").show();
    }
    else {
        $("a.previous").show();
    }
    if (mypager.toRows >= mypager.totalRowsCount) {
        document.getElementById('PageCount').innerHTML = mypager.fromRowsStart + "-" + mypager.totalRowsCount + " of ";
        $("a.next").hide();
    }
}

function getEntityObject(url, obtainEntity) {
    asyncCall(url, null, obtainEntity);
}

function listAllDetails(detailColletion, clearFunction, bindItemFunction, emptyFunction) {

    clearFunction();

    if (detailColletion.items.length > 0) {
        for (var i = 0; i < detailColletion.items.length; i++) {
            bindItemFunction(detailColletion.items[i]);
        }
    } else {
        emptyFunction();
    }
}

function showETMessageBox(message, title, methodNameYES, methodNameNO) {
    
    var innerHtml =
        //'<div id="myModal" class="modal fade" role="dialog">'+
    '<div class="modal-dialog">' +
    '    <!-- Modal content-->' +
    '    <div class="modal-content">' +
    '        <div class="modal-header">' +
    '            <a href="javascript:void(0)" class="close" data-dismiss="modal"> <et-modal-close/></a>' +
    '            <h4 class="modal-title">' + title + '</h4>' +
    '        </div>' +
    '        <div class="modal-body">' + message + '</div>' +
    '        <div class="modal-footer">' +
    '            <et-block-form-group-button-right>' +
    '                <button type="button" class="et-btn et-btn-primary" data-dismiss="modal" onclick="' + methodNameYES + '">Yes</button>';
    if (methodNameNO != null) {
        innerHtml +=
        '                <button type="button" class="et-btn et-btn-primary-reverse" onclick="' + methodNameNO + '">No</button>';
    } else {
        innerHtml +=
        '                <button type="button" class="et-btn et-btn-primary-reverse" data-dismiss="modal">No</button>';
    }
    '            </et-block-form-group-button-right>' +
    '        </div>' +
    '    </div>' +
    '</div>' +
   // '</div>'

    $("#messageBoxModal").html(innerHtml);

    $("#messageBoxModal").modal('toggle');
}

function showETDeleteConirmation(methodNameYES, methodNameNO) {
    showETMessageBox("Are you sure you want to delete this record?", "Please Confirm...", methodNameYES, methodNameNO)
}

function validateSingleInput(obj, validateCallBack) {
    var isvalid = validateCallBack(obj);
    if (isvalid) {
        $(obj).parent().find('formerror').hide();
        $(obj).parent().removeClass('has-error');
    }
    else {
        $(obj).parent().find('formerror').show();
        $(obj).parent().addClass('has-error');
    }
}

function clearValues(obj, pageID) {
    $(obj.dataset.target).find('input:text').each(function () {
        $('input:text[id=' + $(this).attr('id') + ']').val('');
        $('input:text[id=' + $(this).attr('id') + ']').parent().find('formerror').hide();
        $('input:text[id=' + $(this).attr('id') + ']').parent().removeClass('has-error');
    });
    $(obj.dataset.target).find('select').each(function () {
        if ($(this).data('ismandatory') === true) {
            var ids = $(this).attr('id');
            $('#' + ids).val(null).trigger("chosen:updated");
            $('select[id=' + $(this).attr('id') + ']').parent().find('formerror').hide();
            $('select[id=' + $(this).attr('id') + ']').parent().removeClass('has-error');
        }
    });
    //TODO: All the input types eg.:checkBox,Radio etc. need to be cleared similar to above.
   
}

function SaveDataPopUp(obj, div) {
    var result = true;
    $('#' + div.id).find('input:text').each(function () {
        if ($('input:text[id=' + $(this).attr('id') + ']').val() == "") {
            $('input:text[id=' + $(this).attr('id') + ']').parent().find('formerror').show();
            $('input:text[id=' + $(this).attr('id') + ']').parent().addClass('has-error');
            result = false;
        }
    });
    $('#' + div.id).find('select').each(function () {
        if ($(this).data('ismandatory') === true) {
            var ids = $(this).attr('id');
            if ($('#' + ids + ' option:selected').val() == "") {
                $('select[id=' + $(this).attr('id') + ']').parent().find('formerror').show();
                $('select[id=' + $(this).attr('id') + ']').parent().addClass('has-error');
                result = false;
            }
        }
    });
    return result;
}

function tipsydisplay() {

    $('.errortipsy').click(function () {
        // make it not dissappear
        toastr.error("Please read the error. And then click", "This is error", {
            "timeOut": "0",
            "extendedTImeout": "0"
        });
    });
    $('.infotipsy').click(function () {
        // title is optional
        toastr.info("Info Message", "This is info");
    });
    $('.warningtipsy').click(function () {
        toastr.warning("Warning");
    });
    $('.successtipsy').click(function () {
        toastr.success("You have done a successfull tipsy. Cheers");
    });

}

// ----------------------------