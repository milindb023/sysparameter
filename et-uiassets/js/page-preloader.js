﻿$(window).on('load', function () { // makes sure the whole site is loaded 
    $('#page-status').fadeOut(); // will first fade out the loading animation 
    $('#page-preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
    $('body').delay(350).fadeIn('slow')
})